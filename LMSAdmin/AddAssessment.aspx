﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddAssessment.aspx.cs" Inherits="AddAssessment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" translate="no">
<head runat="server">
    <title>Add AddAssessment</title>
    <meta charset="UTF-8" />
    <meta name="google" content="notranslate">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="js/croppie/croppie.css" />
    <link rel="stylesheet" href="css/styledev.css" />
    <link rel="stylesheet" href="css/custom-dev.css" />
    <style type="text/css">
        .upload-demo .upload-demo-wrap,
        .upload-demo .upload-result,
        .upload-demo.ready .upload-msg {
            display: none;
        }

        .upload-demo.ready .upload-demo-wrap {
            display: block;
        }

        .upload-demo.ready .upload-result {
            display: inline-block;
        }

        .upload-demo-wrap {
            width: 750px;
            height: 450px;
            margin: 0 auto;
        }

        .selectFile {
            background-color: #f7f7f7;
            font-size: 12px !important;
            color: #29295b !important;
            box-shadow: 0 3px 6px 0 rgba(0,0,0,0.2);
        }
    </style>
</head>

<body onload="AddTextBox()">
    <div class="loader" id="loader" runat="server" style="display: none">
        <img src="images/loaderp.svg" />
    </div>

    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Assessment 
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Assessment   - Add Assessment
     
                       
                        </div>
                    </div>
                </div>

                <div class="row" id="pageBody">
                    <p></p>
                    <div class="col-md-12">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxtResource">
                                    <h1>Add Assessments</h1>
                                </div>

                                <!--forms starts here-->
                                <form id="form1" name="myForm" runat="server" enctype="multipart/form-data" method="post">
                                    <asp:HiddenField ID="hidResourceSelectedType" runat="server" />
                                    <asp:HiddenField runat="server" ID="users" />
                                    <asp:HiddenField ID="hidresourceURL" runat="server" />
                                    <asp:HiddenField runat="server" ID="rsList" />
                                    <asp:HiddenField ID="hdnResourceID" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnResourecData" runat="server" Value="0" />
                                    <div class="userForm">
                                        <div class="row">
                                            <div class="col-sm-8 formTxt">
                                                <label>Assessment Title</label>
                                                <asp:TextBox runat="server" ID="FlashCardName" class="form-control" onchange="dynamicCtrlValidation('FlashCardName')" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                                <span id="lblCardName" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please enter Assessment Title</span>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <!-- first row ends here-->
                                        <div class="row">
                                            <div class="col-sm-3 formTxt">
                                                <label>Choose Track</label>
                                                <asp:DropDownList ID="ResourceTrack" onclick="dynamicCtrlValidation('ResourceTrack')" OnSelectedIndexChanged="ResourceTrack_SelectedIndexChanged" AutoPostBack="true" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Track--</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="lblflashtrack" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px" runat="server">Please select track</span>
                                            </div>
                                            <div class="col-sm-3 formTxt">
                                                <label>Choose Curriculum</label>
                                                <asp:DropDownList ID="FlashCurriculum" onclick="dynamicCtrlValidation('FlashCurriculum')" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Curriculum--</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="lblcurriculam" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please select curriculum</span>
                                            </div>

                                        </div>
                                        <div id="TextBoxContainer">
                                            <!--Textboxes will be added here -->
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <p class="addQun-flashcard" id="btnAdd" onclick="AddTextBox()" style="cursor: pointer;"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Question</p>
                                            </div>
                                        </div>
                                        <%--<input id="btnAdd" type="button" value="add" onclick="AddTextBox()" />--%>
                                        <div class="row userBut">
                                            <div class="col-md-6"></div>
                                            <div class="col-md-3 userBut1">
                                                <a href="flashCard.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to Assessment</a>

                                                <asp:Label ID="lblMesg" Text="" runat="server" />
                                            </div>

                                            <div class="col-md-3">
                                                <%--   <asp:Button Text="Add User" class="btn text-white userBut2"  runat="server" ID="btnAdd" />--%>
                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClientClick="this.disabled = true; this.value = 'Submitting...';" UseSubmitBehavior="false" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <asp:Button Text="Add Assessment" class="btn text-white userBut2" OnClick="btnAddFlash_Click" runat="server" ID="btnAssessment" OnClientClick="return dynamicCtrlbtnValidation();"  />
                                            </div>
                                        </div>
                                        <!--back to user ends here-->
                                    </div>
                                    <%-- <asp:DropDownList ID="ResourceTrack" AutoPostBack="true" OnSelectedIndexChanged="ResourceTrack_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Track</asp:ListItem>
                                                </asp:DropDownList>
                                     <asp:DropDownList ID="ResourceCurriculum" AutoPostBack="true" OnSelectedIndexChanged="ResourceCurriculum_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Curriculum</asp:ListItem>
                                                </asp:DropDownList>--%>
                                </form>
                                <!--forms ends here-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/jquery.validate.js"></script>

    <script src="js/jquery.form.js"></script>
    <script type="text/javascript">
        function showAlert(msg, type) {
            $("#resourceAlert").text(msg);
            $("#resourceAlert").show();
        }

        function hideAlert() {
            $("#resourceAlert").hide();
        }


    </script>


    <script type="text/javascript">
        var serialNumber = 1;
        function GetDynamicTextBox(value) {
            return '<div class="row"><div class="col-sm-8 formTxt"><input name="file' + serialNumber + '" type="file" id="file' + serialNumber + '" name = "file' + serialNumber + '" accept="image/x-png,image/gif,image/jpeg"   class=form-control  data-prompt-position=bottomRight:-100,3 style="visibility:hidden;" /><label>Question ' + serialNumber + ' </label><input name="txtQst' + serialNumber + '" type="text" id="txtQst' + serialNumber + '"  class="form-control  data-prompt-position=bottomRight:-100,3" /></div><div class="col-sm-4 formTxt"><label>Number of Options </label><select name="drpoptions' + serialNumber + '" id="drpoptions' + serialNumber + '" onchange=GetDynamicOptionTextBox(' + serialNumber + ')><option value="0">No. of Options</option><option value="1">1</option><option value="2">2</option><option value="3">3</option>><option value="4">4</option>><option value="5">5</option></select><span id="erroroption' + serialNumber + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:22px">Please select No. of Options ' + serialNumber + '</span></div><span id="errortxtQst' + serialNumber + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:22px">Please Enter Question ' + serialNumber + '</span> </div>' +
                '<div id=optionload' + serialNumber + '></div>' +
                '<br>' +
                '<div class="row" id="divtxtCorrectAns' + serialNumber + '" style="display:none"><div class="col-sm-2 formTxt"><label>Correct Answer(In Number)</label><input name = "txtCorrectAns' + serialNumber + '" type="text"  id="txtCorrectAns' + serialNumber + '"  name = "txtCorrectAns' + serialNumber + '"  class=form-control  data-prompt-position=bottomRight:-100,3 onkeypress="return isNumber(event)" maxlength="2" /><span id="errortxtCorrectAns' + serialNumber + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:9px;margin-top:10px">Please Enter Correct Ans ' + serialNumber + '</span><span id="errorInvalidCorrectAns' + serialNumber + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:9px;margin-top:10px">No of Options is not Matching with Correct Answers ' + serialNumber + '</span></div> </div>' +
                '<br>' +
                '<input type="button" value="Remove Question" class="btn text-white userBut2" onclick = "RemoveTextBox(this)"  id="remove' + serialNumber + '" /></div><br><br>'
        }

        function GetDynamicOptionTextBox(value) {
            var abc;
            var addSlnoY = 0;
            //alert(value);
            //alert(document.getElementById("drpoptions" + value).value)
            document.getElementById('optionload' + value).innerHTML = "";
            for (var i = 0; i < document.getElementById("drpoptions" + value).value; i++) {
                addSlnoY++
                abc = $('<div class="row"><div class="col-sm-8 formTxt"><label>Option ' + addSlnoY + '</label><textarea name="txtoptAns_' + addSlnoY + '_' + value + '" type="text" id="txtoptAns_' + addSlnoY + '_' + value + '"  class=form-control  data-prompt-position=bottomRight:-100,3></textarea></div><div class=col-sm-4></div><span id="errortxtoptAns_' + addSlnoY + '_' + value + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:22px">Please Enter Option ' + addSlnoY + '</span> </div>').appendTo($('#optionload' + value));

            }
            document.getElementById("divtxtCorrectAns" + value).style.display = "block";
            return abc;
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        /*button submit validation*/

        function dynamicCtrlbtnValidation() {

            debugger;
            var empTable = document.getElementById('TextBoxContainer').childElementCount; //div,TextBoxContainer,,form1
            var loopFlag = true;
            var ctrlCardNameValue = document.getElementById("FlashCardName").value;
            if (ctrlCardNameValue == null || ctrlCardNameValue == "") {
                document.getElementById("lblCardName").style.display = "block";
                document.getElementById("FlashCardName").focus();
                return false;
            }
            var trackValue = document.getElementById("ResourceTrack");
            var droptrackvlaue = trackValue.options[trackValue.selectedIndex].value;

            if (droptrackvlaue == 0) {
                document.getElementById("lblflashtrack").style.display = "block";
                return false;
            }
            var curriValue = document.getElementById("FlashCurriculum");
            var dropCurrivlaue = curriValue.options[curriValue.selectedIndex].value;

            if (dropCurrivlaue == 0) {
                document.getElementById("lblcurriculam").style.display = "block";
                return false;
            }
            //var courseValue = document.getElementById("FlashCourse");
            //var dropCoursevlaue = courseValue.options[courseValue.selectedIndex].value;

            //if (dropCoursevlaue == 0) {

            //    document.getElementById("lblflashcourse").style.display = "block";
            //    loopFlag = false;
            //}
            for (var i = 0; i < empTable; i++) {
                var y = i + 1;

                var txtQst = document.getElementById("txtQst" + y).value;
                if (txtQst == "") {
                    document.getElementById("errortxtQst" + y).style.display = "block";
                    document.getElementById("txtQst" + y).focus();
                    return false;
                }
                else {
                    document.getElementById("errortxtQst" + y).style.display = "none";
                }
                var txtOpt = document.getElementById("drpoptions" + y).value;
                if (txtOpt == "0") {
                    document.getElementById("erroroption" + y).style.display = "block";
                    document.getElementById("drpoptions" + y).focus();
                    return false;
                }
                else {
                    document.getElementById("erroroption" + y).style.display = "none";
                }
                if (txtOpt != "0") {
                    for (var j = 0; j < parseInt(txtOpt); j++) {
                        var z = j + 1;
                        var txtQstOptStr = document.getElementById("txtoptAns_" + z + "_" + y).value;
                        if (txtQstOptStr == "") {
                            document.getElementById("errortxtoptAns_" + z + "_" + y).style.display = "block";
                            document.getElementById("txtoptAns_" + z + "_" + y).focus();
                            return false;
                        }
                        else {
                            document.getElementById("errortxtoptAns_" + z + "_" + y).style.display = "none";
                        }
                    }

                    var txtQstOptsAnsStr = document.getElementById("txtCorrectAns" + y).value;
                    if (txtQstOptsAnsStr == "") {
                        document.getElementById("errortxtCorrectAns" + y).style.display = "block";
                        document.getElementById("txtCorrectAns" + y).focus();
                        return false;
                    } 
                    else if (txtQstOptsAnsStr <= txtOpt)
                    {
                        document.getElementById("errortxtCorrectAns" + y).style.display = "none";
                        document.getElementById("errorInvalidCorrectAns" + y).style.display = "none";
                        
                    }
                    else {
                        document.getElementById("errortxtCorrectAns" + y).style.display = "none";
                        document.getElementById("errorInvalidCorrectAns" + y).style.display = "block";
                        document.getElementById("txtCorrectAns" + y).focus();
                        return false;
                    }
                }
            }
        }

        /*dynamic control click validation*/

        function dynamicCtrlValidation(ctrlId) {


            var isvalid = true;
            if (ctrlId == "FlashCardName") {
                var ctrlValue = document.getElementById(ctrlId).value;
                if (ctrlValue == null || ctrlValue == "") {

                    document.getElementById("lblCardName").style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("lblCardName").style.display = "none";

                }
            }

            else if (ctrlId == "ResourceTrack") {
                var trackValue = document.getElementById(ctrlId);
                var droptrackvlaue = trackValue.options[trackValue.selectedIndex].value;
                if (droptrackvlaue == 0) {
                    document.getElementById("lblflashtrack").style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("lblflashtrack").style.display = "none";
                }
            }

            else if (ctrlId == "ResourceList") {
                var resourceValue = document.getElementById(ctrlId);
                var dropresourcevlaue = resourceValue.options[resourceValue.selectedIndex].value;
                if (dropresourcevlaue == 0) {
                    document.getElementById("lblResourceName").style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("lblResourceName").style.display = "none";
                }
            }

            else if (ctrlId == "FlashCurriculum") {
                if (dropCurrivlaue == 0) {

                    document.getElementById("lblcurriculam").style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("lblcurriculam").style.display = "none";
                }
            }
            //else if (ctrlId == "FlashCourse") {
            //    var trackValue = document.getElementById(ctrlId);
            //    var droptrackvlaue = trackValue.options[trackValue.selectedIndex].value;
            //    if (droptrackvlaue == 0) {
            //        document.getElementById("lblflashcourse").style.display = "block";
            //        isvalid = false;
            //    }
            //    else {
            //        document.getElementById("lblflashcourse").style.display = "none";
            //    }
            //}

            var ctrlValue = document.getElementById(ctrlId).value;
            var txt = ctrlId;
            var serialNum = txt.match(/\d/g);
            serialNum = serialNum.join("");
            var ctrlIdNew = ctrlId.split(serialNum);
            var removeserctrlid = ctrlIdNew[0];
            if (removeserctrlid == "txtQst") {
                if (ctrlValue == null || ctrlValue == "") {
                    document.getElementById("errortxtQst" + serialNum).style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("errortxtQst" + serialNum).style.display = "none";
                }
            }

            if (removeserctrlid == "txtAns") {
                if (ctrlValue == null || ctrlValue == "") {
                    document.getElementById("errortxtAns" + serialNum).style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("errortxtAns" + serialNum).style.display = "none";
                }

            }
            //if (removeserctrlid == "file") {
            //    if (ctrlValue == null || ctrlValue == "") {
            //        document.getElementById("errortxtFile" + serialNum).style.display = "block";
            //        isvalid = false;
            //    }
            //    else {
            //        document.getElementById("errortxtFile" + serialNum).style.display = "none";
            //    }

            //}
            if (removeserctrlid == "txtKnowmore") {
                if (ctrlValue == null || ctrlValue == "") {
                    document.getElementById("errortxtKnow" + serialNum).style.display = "block";
                    isvalid = false;
                }
                else {
                    document.getElementById("errortxtKnow" + serialNum).style.display = "none";
                }
            }

            return isvalid;
        }



        function AddTextBox() {

            var div = document.createElement('DIV');
            div.innerHTML = GetDynamicTextBox("");
            document.getElementById("TextBoxContainer").appendChild(div);
            serialNumber++;
        }

        function RemoveTextBox(div) {

            if (div["id"] != "remove1") {
                document.getElementById("TextBoxContainer").removeChild(div.parentNode);
            }
        }

        function RecreateDynamicTextboxes() {
            // alert();
            var values = eval('<%=Values%>');
            //alert(values);
            if (values != null) {
                var html = "";
                for (var i = 0; i < values.length; i++) {
                    //  alert(i);
                    html += "<div>" + GetDynamicTextBox(values[i]) + "</div>";
                }
                document.getElementById("TextBoxContainer").innerHTML = html;
            }
        }
        window.onload = AddTextBox;
    </script>

    <style type="text/css">
        label.error {
            color: red;
        }

        label.error1 {
            color: red;
        }

        .resourcethumb {
            margin-top: -50px;
        }
    </style>
    <script type="text/javascript">
        function LabelTextChange() {
            document.getElementById("Label4").innerText = 'Question4';
        }


    </script>

    <style type="text/css">
        label.error {
            color: red;
        }

        label.error1 {
            color: red;
        }

        .resourcethumb {
            margin-top: -50px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ResourceList").select2({
                placeholder: "--Select Resource Type--",
                allowClear: true
            });
        });

        function LabelTextChange() {
            document.getElementById("Label4").innerText = 'Question4';
        }

        $(function () {
            $("#ResourceList").change(function () {
                var selectedVal = "";
                //  alert($('option:selected', this).val());
                selectedVal = $('option:selected', this).val();

                var fields = selectedVal.split('=');
                var val1 = fields[0];
                var val2 = fields[1];
                document.getElementById("hidResourceSelectedType").value = val1;
                document.getElementById("hidresourceURL").value = val2;
                // alert(document.getElementById("hidresourceURL").value);
            });
        });
    </script>
</body>

</html>
