﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Configuration;
using System.Runtime.InteropServices.WindowsRuntime;

public partial class AddAssessment : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected string Values;
    SqlConnection conn;
    public string connS = ConfigurationManager.AppSettings["AppDB"];
    public string user_id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //  BindData();
            try
            {
                // UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
                //user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
                user_Id = 5;
            }
            catch (UnauthorizedAccessException)
            {
                //Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
                return;
            }

            //string hdnResourceID = Request.QueryString["Id"];
            if (!IsPostBack)
            {
                BindData();
                //if (hdnResourceID != null)
                //{
                //    btnAddFlash.Text = "Update Resource";
                //    GetDataByResourseId(hdnResourceID);
                //}
                //else
                //{
                //    btnAddFlash.Text = "Add Resource";
                //}
            }
        }
    }
    protected void btnAddFlash_Click(object sender, EventArgs e)
    {
        Assessment assessment = new Assessment();

        assessment.AssessmentTitle = FlashCardName.Text.Trim();
        assessment.Type = 8;
        assessment.AssessmentTrackId = (Int64)ResourceTrack.SelectedIndex;
        assessment.AssessmentCurriculmId = FlashCurriculum.SelectedIndex;
        assessment.AssessmentCourseId = 0;
        assessment.AdminId = user_Id;
        int assessmentId = LearningManagementSystem.Components.Assessment.AddAssessment(assessment);
        for (int i = 0; i < Request.Files.Count; i++)
        {
            Assessment qstAssessment = new Assessment();
            int y = i + 1;
            string fileNameForDB = "";
            string txtQst = Request.Form["txtQst" + y];
            string txtCorrectAns = Request.Form["txtCorrectAns" + y];
            int dropselected = int.Parse(Request.Form["drpoptions" + y].ToString());
            qstAssessment.Question = txtQst.Trim();
            qstAssessment.CorrectAnswer = txtCorrectAns.Trim();
            qstAssessment.NoOptions = dropselected;
            qstAssessment.AssessmentID = assessmentId;
            qstAssessment.AdminId = user_Id;
            int insertAssessmentQuestion = LearningManagementSystem.Components.Assessment.AddAssessmentQuestions(qstAssessment);
            List<Assessment> assessmentObj = new List<Assessment>();
            for (int j = 1; j <= dropselected; j++)
            {
                
                string txtoptions = Request.Form["txtoptAns_" + j + "_" + y + ""];
                Assessment optAssessment = new Assessment();
                optAssessment.Options = txtoptions;
                optAssessment.QuestionId = insertAssessmentQuestion;
                assessmentObj.Add(optAssessment);
            }
            LearningManagementSystem.Components.Assessment.AddAssessmenOptions(assessmentObj);//calling AddFlahQuestions
            Response.Redirect("Assessment.aspx?IsSuccessflashCard=true", false);
        }
    }
    private void BindData()
    {
        ResourceTrack.DataSource = GetResourceTrackList().Tables[0];
        ResourceTrack.DataValueField = "TrackId";
        ResourceTrack.DataTextField = "TrackName";
        ResourceTrack.DataBind();
        ResourceTrack.Items.Insert(0, new ListItem("--Select Track--", "0"));
        ResourceTrack.SelectedIndex = ResourceTrack.Items.IndexOf(ResourceTrack.Items.FindByText("Global"));
        BindCurriculumDropdown(0);
        FlashCurriculum.SelectedIndex = FlashCurriculum.Items.IndexOf(FlashCurriculum.Items.FindByText("Global"));
    }

    private void BindCurriculumDropdown(long id)
    {
        FlashCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        FlashCurriculum.DataValueField = "CurriculumId";
        FlashCurriculum.DataTextField = "CurriculumName";
        FlashCurriculum.DataBind();
        FlashCurriculum.Items.Insert(0, new ListItem("--Select  Curriculum--", "0"));
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }

    public DataSet GetResourceTrackList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceTrack", parameters.ToArray());
    }

    public DataSet GetResourceCurriculumList(long trackId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@TrackId", trackId, ParameterDirection.Input));
        return dataManager.Execute("GetTrackCurriculumByTrackId", parameters.ToArray());
    }
    protected void ResourceTrack_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ResourceTrack.SelectedValue == "0")
        {
            lblflashtrack.Attributes.Add("display", "block");
        }
        else
        {
            BindCurriculumDropdown(Convert.ToInt64(ResourceTrack.SelectedItem.Value));
        }

    }

}