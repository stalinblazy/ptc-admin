﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddCourse.aspx.cs" Inherits="AddCourse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Course Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
    <link rel="stylesheet" href="js/croppie/croppie.css" />
    <link rel="stylesheet" href="css/styledev.css" />
    <link rel="stylesheet" href="css/custom-dev.css" />
    <style type="text/css">
        label.error {
            color: red;
        }

        label.error1 {
            color: red;
        }

        .upload-demo .upload-demo-wrap,
        .upload-demo .upload-result,
        .upload-demo.ready .upload-msg {
            display: none;
        }

        .upload-demo.ready .upload-demo-wrap {
            display: block;
        }

        .upload-demo.ready .upload-result {
            display: inline-block;
        }

        .upload-demo-wrap {
            width: 750px;
            height: 450px;
            margin: 0 auto;
        }

        .selectFile {
            background-color: #f7f7f7;
            font-size: 12px !important;
            color: #29295b !important;
            box-shadow: 0 3px 6px 0 rgba(0,0,0,0.2);
        }
    </style>
</head>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Course Manager
     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Course Manager - Add Course
     
                        </div>
                    </div>
                </div>

                <div class="row" id="pageBody">

                    <div class="col-md-12">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxtCourse">
                                    <h1>Course Details</h1>
                                </div>

                                <!--forms starts here-->
                                <form id="addCourse" runat="server">
                                  <div class="userForm">
                                    <div class="row">
                                       <div class="col-sm-7 ">
                                        <!-- first row starts here-->
                                        <div class="row">
                                            <asp:HiddenField ID="hdnCourseID" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnCourseImage" runat="server" Value="" />
                                            <div class="col-sm-12 formTxt">
                                                <label>Course Name</label>
                                                <asp:TextBox runat="server" ID="CourseName" class="form-control" placeholder="Course Name" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <%--<div class="col-sm-4"></div>--%>
                                        </div>
                                        <!-- first row ends here-->
                                        <!--second row starts here-->
                                        <div class="row">
                                            <div class="col-sm-12 formTxt">
                                                <label>Course Description</label>
                                                <asp:TextBox TextMode="MultiLine" runat="server" Columns="30" Rows="3" ID="CourseDescription" class="form-control" placeholder="Course Description" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <%--<div class="col-sm-4"></div>--%>
                                        </div>
                                        <!--second row ends here-->
                                        <!--third row starts here-->
                                        <div class="row">
                                            <div class="col-sm-6 formTxt">
                                                <label>Choose Track</label>
                                                <asp:DropDownList ID="ResourceTrack" AutoPostBack="true" OnSelectedIndexChanged="ResourceTrack_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-6 formTxt">
                                                <label>Choose Curriculum</label>
                                                <asp:DropDownList ID="ResourceCurriculum" AutoPostBack="true" OnSelectedIndexChanged="ResourceCurriculum_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    
                                                </asp:DropDownList>
                                            </div>
                                              <%-- <div class="col-sm-3 formTxt">
                                                <label>Choose Course</label>
                                                <asp:DropDownList ID="ResourceCourse" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Coutse</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>--%>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 formTxt">
                                                <label>Due In (No. of Days)</label>
                                                <%--<asp:TextBox runat="server" ID="DueDate" CssClass="form-control input-lg simple-field datepicker" placeholder="Due Date" data-prompt-position="bottomRight:-100,3"></asp:TextBox>--%>
                                                <input type="number" name="DueDate" runat="server" min="1" max="35" id="DueDate"  class="form-control input-lg simple-field datepicker" placeholder="Due In (No. of Days)" data-prompt-position="bottomRight:-100,3" onkeypress="return isNumber(event)" />
                                               <%--  <asp:TextBox runat="server" ID="DueDate"></asp:TextBox>--%>
                                            </div>
                                            <div class="col-sm-6 formTxt form-group">
                                                <label>Duration</label>
                                                 <%--<asp:TextBox runat="server" ID="CourseDuration" CssClass="form-control input-lg" placeholder="Duration" data-validation-engine="validate[required]" data-prompt-position="bottomRight:-100,3"></asp:TextBox>--%>
                                                <asp:DropDownList ID="CourseDuration" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select --</asp:ListItem>
                                                    <asp:ListItem Value="1"> 15</asp:ListItem>
                                                    <asp:ListItem Value="2"> 30 </asp:ListItem>
                                                    <asp:ListItem Value="3"> 45 </asp:ListItem>
                                                    <asp:ListItem Value="4"> 60 </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                          <%--  <div class="col-sm-4"></div>--%>
                                        </div>
                                          <!--third row ends here-->
                                      </div>
                                       <div class="col-sm-5" id="ResourceByCoursediv" runat="server">
                                            <table id="tblResourceBycourse" class="" style="width: 100%">
                                                <thead class="tableHeader">
                                                    <tr>
                                                      <th>Resources</th>
                                                   </tr>
                                                </thead>
                                                <tbody class="tableBody">
                                                    <asp:Repeater ID="ResourceRep" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Eval("Name")%></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                       </div>
                                    </div>
                                       
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label style="margin-bottom: 0px;" class="btn btn-default selectFile">
                                                    Select Image
                                                    <input accept="image/x-png,image/gif,image/jpeg" runat="server" name="courseImage" id="courseImage" type="file" hidden />
                                                </label>
                                                <input type="button" class="btn  btn-default selectFile" onclick="clearImage()" value="Clear Image" />
                                            </div>
                                            <div class="col-sm-4">

                                                <asp:Image ID="CropPreview" runat="server" class="img-fluid event-img" src="" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 aeformTxt">
                                                <label id="courseAlert" class="error1"></label>
                                            </div>
                                        </div>

                                      
                                        <!--back to user starts here-->
                                        <div class="row userBut">
                                            <div class="col-md-6 aeformTxt">
                                            </div>
                                            <div class="col-md-3 userBut1">
                                                <a href="CourseManagement.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to course</a>
                                            </div>
                                            <div class="col-md-3">

                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClientClick="this.disabled = true; this.value = 'Submitting...';" UseSubmitBehavior="false" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <asp:Button Text="Add Course" class="btn text-white userBut2" OnClick="Submit_Click" runat="server" ID="btnAddCourse" />
                                            </div>
                                        </div>
                                        <!--back to user ends here-->
                                    </div>
                                </form>
                                <!--forms ends here-->

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="imageModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crop Image</h5>
                </div>
                <div class="modal-body">
                    <div class="upload-demo-wrap">
                        <div id="upload-demo"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Clear</button>
                    <button type="button" class="btn btn-primary save-btn" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <link rel="stylesheet" href="js/datetime/bootstrap-datetimepicker.css" />

    <%--<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <%--  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="../scripts/jquery.ui.timepicker.js"></script>--%>
    <%--<script src="scripts/jquery.ui.position.min.js"></script>
    <script src="Scripts/jquery.mask.js"></script>--%>
    <%--<link href="../css/jquery.ui.timepicker.css" rel="stylesheet" />--%>
    <script src="js/datetime/bootstrap-datetimepicker.min.js"></script>
    <script src="scripts/jquery.validate.js"></script>
    <script src="js/croppie/croppie.js"></script>
    <script type="text/javascript">
        var isCourseImage = false;
        $(document).ready(function () {
            ChkValidation();
            $(".loader").fadeOut();

            if ($("#btnAddCourse").val() == "Update Course") {
               $("#CropPreview").attr("src",$("#hdnCourseImage").val());
                RescourceByCourse();
            }
            // $("#DueDate").mask('00/00/0000');
        });
        //$(function () {
        //    // $("#DueDate").datepicker({ dateFormat: 'mm/dd/yy', minDate: 0 });
        //    $('#DueDate').datetimepicker({
        //        format: 'L',
        //        onSelect: function (dateText, inst) {
        //            alert('select!');
        //        }
        //    });
        //});
        var validatorCourse;
        function ChkValidation() {
            validatorCourse = $("#addCourse").validate({
                rules: {
                <%= CourseName.UniqueID %>:{
                    required: true
                },
                <%= CourseDescription.UniqueID %>: {
                    required: true
                },
                <%= DueDate.UniqueID %>: {
                    required: true
                },
                 <%= CourseDuration.UniqueID %>: {
                    CheckDropDownList: true,
                },
              
                 },
        messages: {
                <%=CourseName.UniqueID %>: {
                required: "Course Name is required."
            },
                   <%=CourseDescription.UniqueID %>: {
                required: "Course Description is required."
            },
                    <%= DueDate.UniqueID %>: {
                required: "Due In (No. of Days)"
            },
                    <%= CourseDuration.UniqueID %>: {
                CheckDropDownList: "Duration is required."
            },

        }
        });
        $.validator.addMethod("CheckDropDownList", function (value, element, param) {
            if (value == "0")
                return false;
            else
                return true;
        });
      }

        $('#courseImage').on('change', function () {
            const file = this.files[0];
            const fileType = file['type'];
            const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
            if (validImageTypes.indexOf(fileType) == -1) {
                alert("Select valid image.");
                return;
            }
            $("#imageModal").modal("show");
        });

        $('#imageModal').on('shown.bs.modal', function () {
            var input = $('#courseImage');
            readFile(input[0]);
        });

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 550,
                height: 300
            },
            enableExif: true,
            enforceBoundary: true
        });

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(input.files[0]);
            }
            else {
                alert("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        function clearImage() {
            $("#CropPreview").attr("src", "");
            $("#CropPreview").hide();
        }

        $('.save-btn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                if (resp && resp != "data:,") {
                    hideAlert();
                    $("#hdnCourseImage").val(resp);
                    $("#CropPreview").attr("src", resp);
                    $("#CropPreview").fadeIn();
                    $('#imageModal').modal("hide");
                }
            });
        });

        function showAlert(msg, type) {
            $("#courseAlert").text(msg);
            $("#courseAlert").show();
        }

        function hideAlert() {
            $("#courseAlert").hide();
        }

        $(document).on('click', '#btnAddCourse', function () {
            addCourse();
            if (!isCourseImage) {
                return false;
            } else {
                return true;
            }
        })

        function addCourse() {
            hideAlert();
            if (!validatorCourse.form()) {
                
                if ($("#btnAddCourse").val() == "Update Course") {
                    if ($("#hdnCourseImage").val() == "" || $("#hdnCourseImage").val() == "0") {
                        let msg = "";
                        msg = !$("#CropPreview").attr("src") ? 'Course image should not be empty!' : msg;
                        if (msg) {
                            showAlert(msg);
                            return isCourseImage = false;
                        }
                        return isCourseImage = true;
                    }
                } else {
                    let msg = "";
                    msg = !$("#CropPreview").attr("src") ? 'Course image should not be empty!' : msg;
                    if (msg) {
                        showAlert(msg);
                        return isCourseImage = false;
                    }
                    return isCourseImage = true;
                }
            }
            else {
                if ($("#hdnCourseImage").val() == "" || $("#hdnCourseImage").val() == "0") {
                    if ($("#courseImage").val() == "") {
                        let msg = "";
                        msg = !$("#CropPreview").attr("src") ? 'Course image should not be empty!' : msg;
                        if (msg) {
                            showAlert(msg);
                            return isCourseImage = false;
                        }
                        return isCourseImage = true;
                    }
                    else
                        return isCourseImage = true;
                }
                else
                    return isCourseImage = true;
            }
        }

        function RescourceByCourse() {
    $('#tblResourceBycourse').DataTable({
        "bLengthChange": false,
        "searching": false,
        "paging": false,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        dom: 't',
       
       
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

    </script>


</body>
</html>
