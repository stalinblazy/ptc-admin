﻿using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddCourse : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        try
        {
            string hdnaddCourseID = Request.QueryString["Id"];
            if (!IsPostBack)
            {
                BindData();
                if (hdnaddCourseID != null)
                {
                    btnAddCourse.Text = "Update Course";
                    ResourceByCoursediv.Style.Remove("display");
                    GetDataByCourseId(hdnaddCourseID);
                }
                else
                {
                    ResourceByCoursediv.Style.Add("display", "none");
                    btnAddCourse.Text = "Add Course";
                }
            }
        }
        catch (Exception)
        {

        }
    }

    private void BindData()
    {
        ResourceTrack.DataSource = GetResourceTrackList().Tables[0];
        ResourceTrack.DataValueField = "TrackId";
        ResourceTrack.DataTextField = "TrackName";
        ResourceTrack.DataBind();
        ResourceTrack.SelectedIndex = ResourceTrack.Items.IndexOf(ResourceTrack.Items.FindByText("Global"));
        BindCurriculumDropdown(0);
        ResourceCurriculum.SelectedIndex = ResourceCurriculum.Items.IndexOf(ResourceCurriculum.Items.FindByText("Global"));
    }

    private void BindCurriculumDropdown(long id)
    {
        ResourceCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        ResourceCurriculum.DataValueField = "CurriculumId";
        ResourceCurriculum.DataTextField = "CurriculumName";
        ResourceCurriculum.DataBind();
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }

    public DataSet GetResourceCurriculumList(long trackId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@TrackId", trackId, ParameterDirection.Input));
        return dataManager.Execute("GetTrackCurriculumByTrackId", parameters.ToArray());
    }

    public DataSet GetResourceTrackList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceTrack", parameters.ToArray());
    }

    protected void ResourceTrack_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCurriculumDropdown(Convert.ToInt64(ResourceTrack.SelectedItem.Value));
    }
    protected void ResourceCurriculum_SelectedIndexChanged(object sender, EventArgs e)
    {
        //BindCourseDropdown(Convert.ToInt64(ResourceCurriculum.SelectedItem.Value));
    }

    #region AddUser

    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            ////hdnCourseID.Value = "0";            
            //String image = null;
            //if (!string.IsNullOrWhiteSpace(courseImage.ToString()))
            //{
            //    Stream stream = courseImage.PostedFile.InputStream;
            //    Byte[] bytes = null;
            //    using (var binaryReader = new BinaryReader(stream))
            //    {
            //        bytes = binaryReader.ReadBytes(courseImage.PostedFile.ContentLength);
            //    }
            //    image = "data:image/png;base64," + Convert.ToBase64String(bytes);
            //}
            //else
            //{
            //    image = hdnCourseImage.Value;
            //    hdnCourseImage.Value = "0";
            //}
            UserSession us = Session["User"] as UserSession;
            bool isUserCreated = CreateCourse(hdnCourseID.Value, CourseName.Text, CourseDescription.Text, DueDate.Value, Convert.ToInt32(CourseDuration.Items[CourseDuration.SelectedIndex].Text) * 60, hdnCourseImage.Value, Int16.Parse(us.UserID.ToString()));
            if (isUserCreated == true)
            {
                DataAccessManager dataManager = DataAccessManager.GetInstance();
                bool auditDetails = false;
                if (hdnCourseID.Value == "0")
                {
                    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Added a New Course from Add course page" + CourseName.Text + "", "Create Operation", Int16.Parse(us.UserID.ToString()));
                }
                else
                {
                    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Modified the Course from  course page" + CourseName.Text + " and Course ID: " + hdnCourseID.Value + "'", "Update Operation", Int16.Parse(us.UserID.ToString()));
                }
            }


            Response.Redirect("CourseManagement.aspx?IsFromCourse=true", false);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
    }

    private bool CreateCourse(string hdnCourseID, string CourseName, string CourseDescription, string DueDate, int CourseDuration, string image, int User_ID)
    {
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Name", CourseName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Description", CourseDescription, ParameterDirection.Input));
            //Code is commented because the requirement is changed to Date to Day
            //if (DueDate != null && DueDate != "")
            //{
            //    parameters.Add(dataManager.CreateParam("@DueDate", DueDate, ParameterDirection.Input));
            //}
            parameters.Add(dataManager.CreateParam("@DueInDay", DueDate, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Duration", CourseDuration, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Image", image, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CurriculumId", Int64.Parse(ResourceCurriculum.SelectedValue), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", User_ID, ParameterDirection.Input));
            if (hdnCourseID == "0")
            {
                parameters.Add(dataManager.CreateParam("@CourseID", 0, ParameterDirection.Input));
            }
            else
            {
                parameters.Add(dataManager.CreateParam("@CourseID", Convert.ToInt32(hdnCourseID), ParameterDirection.Input));
            }
            return dataManager.ExecuteNonQuery("SP_LMS_CREATE_COURSE", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return false;
        }
    }

    protected void GetDataByCourseId(string courseid)
    {
        try
        {
            DataTable dt = LearningManagementSystem.Components.Course.GetResourceByCourseId(courseid);
            ResourceRep.DataSource = dt;
            ResourceRep.DataBind();

            DataSet courseInfo = GetCourseInfo(courseid);
            if (courseInfo != null && courseInfo.Tables.Count > 0 && courseInfo.Tables[0].Rows.Count > 0)
            {
                hdnCourseID.Value = courseInfo.Tables[0].Rows[0]["Course_Id"].ToString();
                CourseName.Text = courseInfo.Tables[0].Rows[0]["Name"].ToString();
                CourseDescription.Text = courseInfo.Tables[0].Rows[0]["Description"].ToString();
                hdnCourseImage.Value = courseInfo.Tables[0].Rows[0]["Course_Image"].ToString();
                //DueDate.Text = Convert.ToDateTime(courseInfo.Tables[0].Rows[0]["DueDate"]).ToString("MM/dd/yyyy");
                DueDate.Value = courseInfo.Tables[0].Rows[0]["DueInDays"].ToString();
                string data = courseInfo.Tables[0].Rows[0]["Duration"].ToString();
                int duration = Convert.ToInt32(data) / 60;
                if (courseInfo.Tables[0].Rows[0]["TrackId"].ToString() != "")
                {
                    ResourceTrack.SelectedValue = courseInfo.Tables[0].Rows[0]["TrackId"].ToString();
                    BindCurriculumDropdown(Convert.ToInt64(ResourceTrack.SelectedValue));
                }
                if (courseInfo.Tables[0].Rows[0]["CurriculumId"].ToString() != "")
                {
                    ResourceCurriculum.SelectedValue = courseInfo.Tables[0].Rows[0]["CurriculumId"].ToString();
                    //BindCourseDropdown(Convert.ToInt64(ResourceCurriculum.SelectedValue));
                }
                if (duration == 15)
                {
                    CourseDuration.SelectedValue = "1";
                    CourseDuration.Items[CourseDuration.SelectedIndex].Text = duration.ToString();
                }
                else if (duration == 30)
                {
                    CourseDuration.SelectedValue = "2";
                    CourseDuration.Items[CourseDuration.SelectedIndex].Text = duration.ToString();
                }
                else if (duration == 45)
                {
                    CourseDuration.SelectedValue = "3";
                    CourseDuration.Items[CourseDuration.SelectedIndex].Text = duration.ToString();
                }
                else if (duration == 60)
                {
                    CourseDuration.SelectedValue = "4";
                    CourseDuration.Items[CourseDuration.SelectedIndex].Text = duration.ToString();
                }
                else
                {
                    CourseDuration.SelectedValue = "0";
                }

            }

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
    }

    public DataSet GetCourseInfo(string courseId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@CourseId", courseId, ParameterDirection.Input));
        return dataManager.Execute("GetCourseById", parameters.ToArray());
    }

    #endregion
}