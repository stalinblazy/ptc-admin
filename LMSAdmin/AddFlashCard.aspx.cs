﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Configuration;
using System.Runtime.InteropServices.WindowsRuntime;

public partial class AddFlash : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected string Values;
    SqlConnection conn;
    public string connS = ConfigurationManager.AppSettings["AppDB"];
    public string user_id = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //  BindData();
            try
            {
                UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
                user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
            }
            catch (UnauthorizedAccessException)
            {
                Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
                return;
            }

            string hdnResourceID = Request.QueryString["Id"];
            if (!IsPostBack)
            {
                BindData();
                //if (hdnResourceID != null)
                //{
                //    btnAddFlash.Text = "Update Resource";
                //    GetDataByResourseId(hdnResourceID);
                //}
                //else
                //{
                //    btnAddFlash.Text = "Add Resource";
                //}
            }
        }
    }


    #region DropDowns
    private void BindData()
    {
        //FlashCourse.DataSource = GetCourseList().Tables[0];
        //FlashCourse.DataValueField = "Course_Id";
        //FlashCourse.DataTextField = "Name";
        //FlashCourse.DataBind();
        //FlashCourse.Items.Insert(0, new ListItem("--Select Course--", "0"));

        ResourceTrack.DataSource = GetResourceTrackList().Tables[0];
        ResourceTrack.DataValueField = "TrackId";
        ResourceTrack.DataTextField = "TrackName";
        ResourceTrack.DataBind();
        ResourceTrack.Items.Insert(0, new ListItem("--Select Track--", "0"));
        ResourceTrack.SelectedIndex = ResourceTrack.Items.IndexOf(ResourceTrack.Items.FindByText("Global"));
        BindCurriculumDropdown(0);
        FlashCurriculum.SelectedIndex = FlashCurriculum.Items.IndexOf(FlashCurriculum.Items.FindByText("Global"));

       ResourceList.DataSource = GetResourceFlashCardListPDF().Tables[0];

        ResourceList.DataValueField = "Namedrp";
        ResourceList.DataTextField = "Name";
        ResourceList.DataBind();
        ResourceList.Items.Insert(0, new ListItem("--Select Resource Type--", "0"));
    }
    #endregion
    private void BindCurriculumDropdown(long id)
    {
        FlashCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        FlashCurriculum.DataValueField = "CurriculumId";
        FlashCurriculum.DataTextField = "CurriculumName";
        FlashCurriculum.DataBind();
        FlashCurriculum.Items.Insert(0, new ListItem("--Select  Curriculum--", "0"));
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }



    //protected void FlashTrack_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindCurriculumDropdown(Convert.ToInt64(FlashTrack.SelectedItem.Value));
    //}
    protected void FlashCurriculum_SelectedIndexChanged(object sender, EventArgs e)
    {
        //BindCourseDropdown(Convert.ToInt64(ResourceCurriculum.SelectedItem.Value));
    }


    public DataSet GetResourceTrackList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceTrack", parameters.ToArray());
    }
    public DataSet GetCourseList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("SP_LMS_GET_COURSE_LIST", parameters.ToArray());
    }

    public DataSet GetResourceFlashCardListPDF()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetFlashCardResourcePDF", parameters.ToArray());
    }

    public DataSet GetResourceCurriculumList(long trackId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@TrackId", trackId, ParameterDirection.Input));
        return dataManager.Execute("GetTrackCurriculumByTrackId", parameters.ToArray());
    }
    public DataSet GetResourceCourseList(long curriculumId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@CurriculumId", curriculumId, ParameterDirection.Input));
        return dataManager.Execute("GetCurriculumCourseByCurriculumId", parameters.ToArray());
    }

    protected void btnAddFlash_Click(object sender, EventArgs e)
    {
        //string[] questionValues = Request.Form.GetValues("Questions");
        //string[] answersValues = Request.Form.GetValues("Answers");
        //string[] resourceValues = Request.Form.GetValues("Resource");
        //string[] knowmoreValues = Request.Form.GetValues("KnowMore");
        //var flashCardData = (from n1 in questionValues.Select((item, index) => new { item, index })
        //                     join n2 in answersValues.Select((item, index) => new { item, index }) on n1.index equals n2.index
        //                     join n3 in resourceValues.Select((item, index) => new { item, index }) on n2.index equals n3.index
        //                     join n4 in knowmoreValues.Select((item, index) => new { item, index }) on n3.index equals n4.index
        //                     //join n5 in words.Select((item, index) => new { item, index }) on n4.index equals n5.index
        //                     select new { Question = n1.item, Answer = n2.item, Resource = n3.item, KnowMore = n4.item }).ToList();
        //JavaScriptSerializer serializer = new JavaScriptSerializer();
        //this.Values = serializer.Serialize(flashCardData);
        //string message = "";
        //foreach (var textboxValue in flashCardData)
        //{
        //    message += textboxValue + "\\n";
        //}

        try
        {
            loader.Style.Add("display", "block");
            FlashCard fCard = new FlashCard();
            fCard.SelectedResourceTypeId = int.Parse(hidResourceSelectedType.Value);
            fCard.CardTitle = FlashCardName.Text;
            fCard.Type = 7;
            fCard.FlashCardTrack = (Int64)ResourceTrack.SelectedIndex;
            fCard.FlashCardCurriculam = FlashCurriculum.SelectedIndex;
            fCard.FlashCardCourse = 0;
            fCard.AdminId = user_Id;
            int falshCardID = LearningManagementSystem.Components.FlashCard.AddFlashCard(fCard);//calling addfalshSP
            List<FlashCard> flashListObj = new List<FlashCard>();
            string extension = "";
            string resourcePath = System.Configuration.ConfigurationManager.AppSettings["ResourcePath"];
            for (int i = 0; i < Request.Files.Count; i++)
            {
                int y = i + 1;
                string fileNameForDB = "";
                string txtQst = Request.Form["txtQst" + y];
                string path = " ";
                string txtAns = Request.Form["txtAns" + y];
                string txtKnowmore = hidresourceURL.Value + "#page=" + Request.Form["txtKnowmore" + y];
                string FileName = string.Empty;
                HttpPostedFile PostedFile = Request.Files[i];
                string savePath = resourcePath + "/Resources/FlashCardQimage/";
                
                if (PostedFile.ContentLength > 0)
                {
                    FileName = System.IO.Path.GetFileName(PostedFile.FileName);
                    extension = System.IO.Path.GetExtension(PostedFile.FileName);
                    fileNameForDB = "Qst" + y + "_" + falshCardID + extension;
                    // PostedFile.SaveAs(Server.MapPath(resourcePath + "\\Resources\\FlashCardQimage") + txtQst + extension);
                    path = savePath + fileNameForDB;
                    PostedFile.SaveAs(path);
                }
            
                FlashCard flashObj = new FlashCard();
                flashObj.FlashCardID = falshCardID;
                flashObj.Question = txtQst;
                flashObj.Answer = txtAns;
                flashObj.File =fileNameForDB ;
                flashObj.KnowMore = txtKnowmore;
                flashListObj.Add(flashObj);
            }
            //FlashCourse.SelectedValue = "";
            //FlashCurriculum.SelectedValue = "";
            //FlashTrack.SelectedValue = "";
            LearningManagementSystem.Components.FlashCard.AddQuestions(flashListObj);//calling AddFlahQuestions
            Response.Redirect("flashCard.aspx?IsSuccessflashCard=true", false);
        }
        catch (Exception ex)
        {

            Console.WriteLine(ex.StackTrace);
        }
    }

    protected void ResourceTrack_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCurriculumDropdown(Convert.ToInt64(ResourceTrack.SelectedItem.Value));
    }
    protected void ResourceCurriculum_SelectedIndexChanged(object sender, EventArgs e)
    {
        //BindCourseDropdown(Convert.ToInt64(ResourceCurriculum.SelectedItem.Value));
    }

}