﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddGroup.aspx.cs" Inherits="Admin_AddGroup" %>

<%@ Register TagPrefix="LMS" TagName="GroupUserListDetail" Src="GroupUserListDetail.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Users and Groups
     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Users and Groups - <span id="spanHeader">Overview - Add User</span>
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="listofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row glistofusers">
                                    <div class="col-md-6 gleftlist" style="padding-left: 0px;">
                                        <ul class="guserlistleft">
                                            <li class="activitytxtGroup">
                                                <h1 class="active" id="lblgroupupdate">Group Details</h1>
                                           </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6"></div>

                                    <!-- Form Text -->
                                   
                                    <div class="col-md-3">
                                        <div class="gformTxt">
                                            <label>Group Name</label>
                                             <input type="text" class="form-control" id="txtGroupName" name="txtGroupName" placeholder="Group Name" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3" />
                                         </div>
                                        <div class="row" id="InvalidGroupMsg" style="display: none">
                                            <div class="col-sm-8 formTxt">
                                                <label class="error" id="errorg">Group Name is requried</label>
                                            </div>
                                        </div>
                                    </div>
                                      
                                  
                                    <div class="col-md-7"></div>
                                    <!-- End For Text -->
                                </div>
                                <div class="row listofusers">
                                    <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                        <ul class="userlistleft nav nav-tabs" id="userlistleft">
                                            <li><a href="#tab_UserList" data-toggle="tab">
                                                <h1 class="active">List of Users</h1>
                                            </a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 rightlist" id="rightlist">
                                        <ul class="userlistright" id="userTypes">
                                            <li class="active" data-val="Active">Active Users</li>
                                            <li data-val="Authorized">Authorized Users</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_UserList">
                                        <LMS:GroupUserListDetail ID="GroupUserListDetail1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/custom/GroupDetails.js"></script>
    <script type="text/javascript">
        function RestrictSpace() {
            if (event.keyCode == 32) {
                return false;
            }
        }
        </script>
      <style type="text/css">
        label.error {
            color: red;
        }
    </style>
</body>
</html>
