﻿using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddGroup : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }

        // string hdnpageName = Request.QueryString["page"];
        if (!IsPostBack)
        {
            //btnGroup.Text = 
            //if (hdnaddCourseID != null)
            //{
            //    btnAddCourse.Text = "Update Course";
            //    GetDataByCourseId(hdnaddCourseID);
            //}
            //else
            //{
            //    btnAddCourse.Text = "Add Course";
            //}
        }
    }

    #region AddandAssignGroups
    [WebMethod]
    public static bool CreateAndAssignGroups(string userIds, string groupName, string groupId, string UnchkUserId, string newUsers, bool? isFromCreate)
    {
        bool res = false;
        DataSet GroupId = null;
        int id = 0;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        if (!string.IsNullOrWhiteSpace(groupId))
            id = Convert.ToInt32(groupId);
        List<string> newUser = new List<string>();
        if (newUsers != null && newUsers != "")
        {
            newUser = newUsers.Split(',').ToArray().ToList();
        }
        try
        {
            if (groupName != null & isFromCreate.HasValue && isFromCreate.Value)
            {
                String referenceCode = Guid.NewGuid().ToString();
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(dataManager.CreateParam("@GroupName", groupName, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@status", "1", ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@id", id, ParameterDirection.InputOutput));
                parameters.Add(dataManager.CreateParam("@AdminId", user_Id, ParameterDirection.Input));
                GroupId = dataManager.Execute("CreateGroup", parameters.ToArray());
                id = Convert.ToInt32(GroupId.Tables[0].Rows[0]["ID"]);
            }
            foreach (string data in newUser)
            {
                int newuserID = Convert.ToInt32(data);
                bool isCourseAssigned = UpdateUserGroup(newuserID, groupName, id);
                if (isCourseAssigned)
                {
                    LearningManagementSystem.Components.Group.SendGroupEmailer(groupName, id, newuserID);
                }
            }
            if (!string.IsNullOrWhiteSpace(UnchkUserId))
            {
                List<string> unchkId = UnchkUserId.Split(',').ToArray().ToList();
                if (unchkId != null && unchkId.Count > 0)
                {
                    foreach (string item in unchkId)
                    {
                        int UserID = Convert.ToInt32(item);
                        if (UserID > 0)
                            UpdateUserGroup(UserID, "", 0, id);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return false;
        }
        return res;
    }


    public static bool UpdateUserGroup(int userId, string groupName, int GroupId, int? OldGroupId = null)
    {
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@GroupId", GroupId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@GroupName", groupName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", Convert.ToInt32(userId), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@OldGroupId", OldGroupId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", user_Id, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_Update_GroupName", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }

    }

    //[WebMethod]
    //public static string GetUnAssignUserList()
    //{
    //    string response = string.Empty;
    //    try
    //    {
    //        var dataTable = new DataTable();
    //        dataTable = LearningManagementSystem.Components.User.GetGroupDetails();

    //        var objData = DataAccessManager.DataTableToJSON(dataTable);

    //        JavaScriptSerializer js = new JavaScriptSerializer();
    //        response = js.Serialize(objData);

    //    }
    //    catch (Exception ex)
    //    {
    //        Medtrix.Trace.Logger.Log(ex.Message.ToString());
    //        response = "[]";
    //    }
    //    return response;
    //}

    [WebMethod]
    public static string BindUserListGroupWise(string userType, string groupName, bool? isFromAssign)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.User.GetUserListGroupWise(userType, groupName, isFromAssign);

            var objData = DataAccessManager.DataTableToJSON(dataTable);

            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }
    #endregion

    #region Mail
    private static void SendMail(string firstName, string lastName, string courseName, string emailId)
    {
        String senderMail = System.Configuration.ConfigurationManager.AppSettings["alertMail"];
        String iisName = System.Configuration.ConfigurationManager.AppSettings["iisname"];
        Page page = HttpContext.Current.Handler as Page;
        //string serverName = page.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + iisName;

        String body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/templates/courselaunch.html"));
        body = body.Replace("#YEAR#", DateTime.Now.Year.ToString());
        body = body.Replace("#LASTNAME#", lastName);
        body = body.Replace("#FIRSTNAME#", firstName);
        body = body.Replace("#MODULETITLE#", courseName);
        // body = body.Replace("#LINK#", serverName + "/login.aspx?page=learning");
        if (Medtrix.MailService.SendInBlue.SendEMail(emailId, System.Configuration.ConfigurationManager.AppSettings["contactMail"], "Launch of new course – PTC", body, null, true))
        {
            Medtrix.Trace.Logger.Log("Launch mail sent to: " + emailId);
        }
    }
    #endregion

    #region GroupExist
    [WebMethod]
    public static bool IsGroupNameAvailable(string groupName, string IsUpdate)
    {
        bool result = false;
        try
        {
            if (IsUpdate == "ASSIGNS GROUP")
                result = false;
            else
                result = LearningManagementSystem.Components.Group.IsGroupNameExists(groupName);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            result = true;
        }
        return result;
    }
    #endregion
}