﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddResource.aspx.cs" Inherits="AddResource" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Resource Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
    <link rel="stylesheet" href="js/croppie/croppie.css" />
    <link rel="stylesheet" href="css/styledev.css" />
    <link rel="stylesheet" href="css/custom-dev.css" />
    <style type="text/css">
        .upload-demo .upload-demo-wrap,
        .upload-demo .upload-result,
        .upload-demo.ready .upload-msg {
            display: none;
        }

        .upload-demo.ready .upload-demo-wrap {
            display: block;
        }

        .upload-demo.ready .upload-result {
            display: inline-block;
        }

        .upload-demo-wrap {
            width: 750px;
            height: 450px;
            margin: 0 auto;
        }

        .selectFile {
            background-color: #f7f7f7;
            font-size: 12px !important;
            color: #29295b !important;
            box-shadow: 0 3px 6px 0 rgba(0,0,0,0.2);
        }
    </style>

</head>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Resource Manager
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Resource Manager - Add Resource
     
                       
                        </div>
                    </div>
                </div>

                <div class="row" id="pageBody">

                    <div class="col-md-12">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxtResource">
                                    <h1>Resource Details</h1>
                                </div>

                                <!--forms starts here-->
                                <form id="AddresourceManager" runat="server">
                                    <asp:HiddenField runat="server" ID="users" />
                                    <asp:HiddenField runat="server" ID="rsList" />
                                    <asp:HiddenField ID="hdnResourceID" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnResourecData" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnThumbnailImg" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnThumbnailImgName" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnThumbnailImgURL" runat="server" Value="" />
                                    <asp:HiddenField ID="hdnUploadURL" runat="server" Value="" />
                                    <div class="userForm">
                                        <!-- first row starts here-->
                                        <div class="row">
                                            <div class="col-sm-8 formTxt">
                                                <label>Resource Name/Author, Year</label>
                                                <asp:TextBox runat="server" ID="ResourceName" class="form-control" placeholder="Resource Name" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <!-- first row ends here-->
                                        <!--second row starts here-->
                                        <div class="row">
                                            <div class="col-sm-8 formTxt">
                                                <label>Resource description/Title</label>
                                                <asp:TextBox TextMode="MultiLine" runat="server" ID="ResourceDescription" class="form-control" placeholder="Resource Description" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <!--second row ends here-->
                                        <div class="row">
                                            <div class="col-sm-3 formTxt">
                                                <label>Choose Track</label>
                                                <asp:DropDownList ID="ResourceTrack" AutoPostBack="true" OnSelectedIndexChanged="ResourceTrack_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Track</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 formTxt">
                                                <label>Choose Curriculum</label>
                                                <asp:DropDownList ID="ResourceCurriculum" AutoPostBack="true" OnSelectedIndexChanged="ResourceCurriculum_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Curriculum</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <%-- <div class="col-sm-3 formTxt">
                                                <label>Choose Course</label>
                                                <asp:DropDownList ID="ResourceCourse" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Coutse</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>--%>
                                        </div>

                                        <!--third row starts here-->
                                        <div class="row">
                                            <div class="col-sm-3 formTxt">
                                                <label>Resource Type</label>
                                                <asp:DropDownList ID="ResourceTypeD" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Resource Type</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 formTxt  websection" id="websection1" runat="server">
                                                <label>Resource Data</label>
                                                <%--<asp:FileUpload ID="ResourceData" runat="server" CssClass="file-input" />--%>
                                                <div>
                                                    <label class="btn-default selectFile">
                                                        <asp:FileUpload ID="ResourceData" runat="server" CssClass="file-input" />
                                                    </label>
                                                    <div class="progress">
                                                        <div class="progress-bar"></div>
                                                        <div class="percent">0%</div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-3 formTxt websection" id="websection2" runat="server">
                                                <%-- <label>Thumbnail Image</label>
                                                <asp:FileUpload ID="ThumbnailImg" accept=".png,.jpg,.jpeg,.gif" runat="server" CssClass="file-input" />--%>
                                                <label>Thumbnail Image </label>
                                                <div>
                                                    <label class="btn btn-default selectFile" style="padding: 10px;">
                                                        Select Thumbnail Image
                                                  
                                                        <input accept="image/x-png,image/gif,image/jpeg" id="ThumbnailImg" runat="server" type="file" hidden="hidden" />
                                                    </label>
                                                    <%--<img id="ThumbnailPreview" runat="server" class="img-fluid event-img" src="" />--%>
                                                    <asp:Image ID="ThumbnailPreview" runat="server" class="img-fluid event-img" src="" />
                                                </div>
                                            </div>


                                            <%--                                            <div class="col-sm-3 formTxt WebLinksection" id="WebLinksection1" runat="server">
                                                <label>Author</label>
                                                <asp:TextBox runat="server" ID="AuthorName" class="form-control" placeholder="Author" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3 formTxt WebLinksection" id="WebLinksection2" runat="server">
                                                <label>Publication</label>
                                                <asp:TextBox runat="server" ID="PublicationName" class="form-control" placeholder="Publication" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>--%>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 aeformTxt">
                                            </div>
                                            <div class="col-sm-4 aeformTxt">
                                                <label id="resourceAlert" class="error1"></label>
                                            </div>
                                        </div>

                                        <div class="row" id="WebLink" runat="server">
                                            <div class="col-md-6 formTxt">
                                                <label>Web Link</label>
                                                <asp:TextBox runat="server" ID="txtWebLink" class="form-control" placeholder="Web Link" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row" id="JornalSection" runat="server">
                                            <div class="col-sm-3 formTxt">
                                                <label>Year/Volume</label>
                                                <asp:TextBox runat="server" ID="txtvolisspg" class="form-control" placeholder="Year/Volume" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3 formTxt">
                                                <label>Author</label>
                                                <asp:TextBox runat="server" ID="txtAuthor" class="form-control" placeholder="Author" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3 formTxt">
                                                <label>Journal Name</label>
                                                <asp:TextBox runat="server" ID="txtPublication" class="form-control" placeholder="Journal Name" data-validation-engine="validate[required]" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row userBut">
                                            <div class="col-md-6"></div>
                                            <div class="col-md-3 userBut1">
                                                <a href="ResourceManagement.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to resources</a>
                                            </div>
                                            <div class="col-md-3">
                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClientClick="this.disabled = true; this.value = 'Submitting...';" UseSubmitBehavior="false" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <asp:Button Text="Add Resource" class="btn text-white userBut2" runat="server" ID="btnAddResource" OnClick="btnAddResource_Click" />
                                            </div>
                                        </div>
                                        <!--back to user ends here-->
                                    </div>
                                </form>
                                <!--forms ends here-->

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="imageModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crop Image</h5>
                </div>
                <div class="modal-body">
                    <div class="upload-demo-wrap">
                        <div id="upload-demo"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clearImage()">Clear</button>
                    <button type="button" class="btn btn-primary save-btn" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/jquery.validate.js"></script>
    <script src="js/croppie/croppie.js"></script>
    <script src="js/jquery.form.js"></script>
    <script type="text/javascript">
        var isResourceImage = false;
        $(document).ready(function () {
            var bar = $('.progress-bar');
            var percent = $('.percent');
            $('#AddresourceManager').ajaxForm({
                beforeSend: function () {
                    var percentVal = '0%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                },
                complete: function (xhr) {
                    window.location.href = "ResourceManagement.aspx?IsSuccessRescource=true";
                    //status.html(xhr.responseText);
                }
            });

            if (document.getElementById('<%= btnAddResource.ClientID %>').value == "Update Resource") {
                $("#ResourceTypeD").change(function () {
                    $('#ResourceData').val(null);
                    $("#ThumbnailPreview").attr("src", "");
                    if ($(this).val() == "3") {
                        $("#WebLink").show();
                        $(".WebLinksection").show();
                        $("#websection1").hide();
                        $("#JornalSection").hide();

                    }
                    else if ($(this).val() == "1") {
                        $("#WebLink").hide();
                        $("#JornalSection").show();
                        $(".WebLinksection").hide();
                        $(".websection").show();
                        //$("#txtvolisspg").show();
                    }
                    else {
                        $("#WebLink").hide();
                        $("#JornalSection").hide();
                        $(".WebLinksection").hide();
                        $(".websection").show();
                    }
                });
            } else {
                $("#JornalSection").hide();
                $("#WebLink").hide();
                $(".WebLinksection").hide();
            }


            ChkResourceValidation();
            $(".loader").fadeOut();
        });

        var validatorResource;
        function ChkResourceValidation() {
            validatorResource = $("#AddresourceManager").validate({
                rules: {
                    <%= ResourceName.UniqueID %>:{
                required: true
            },
                    <%= ResourceDescription.UniqueID %>: {
                    required: true
                },
                   <%-- <%= ResourceTrack.UniqueID %>: {
                    CheckDropDownList: true,
                },
                    <%= ResourceCurriculum.UniqueID %>: {
                    CheckDropDownList: true,
                },
                    <%= ResourceCourse.UniqueID %>: {
                    CheckDropDownList: true,
                },--%>
                    <%= ResourceTypeD.UniqueID %>: {
                    CheckDropDownList: true,
                },
                    <%= txtWebLink.UniqueID %>: {
                    required: true
                },
                    <%= txtvolisspg.UniqueID %>: {
                    required: true
                },
                    <%= txtAuthor.UniqueID %>: {
                    required: true
                },
                    <%= txtPublication.UniqueID %>: {
                    required: true
                },
                   <%-- <%= AuthorName.UniqueID %>: {
                    required: true
                },
                    <%= PublicationName.UniqueID %>: {
                    required: true
                },--%>
                     <%= ThumbnailImg.UniqueID %>: {
                    CheckImage: true,
                },
                   <%-- <%= thumbnailpreview.uniqueid %>: {
                        checkimage: true,
                },--%>
                    <%= ResourceData.UniqueID%>: {
                    Checkfile: true,
                }

                },
        messages: {
                    <%=ResourceName.UniqueID %>: {
                required: "Resource Name is required."
            },
                    <%=ResourceDescription.UniqueID %>: {
                required: "Resource Description is required."
            },
                  <%--  <%= ResourceTrack.UniqueID %>: {
                CheckDropDownList: "Resource Track is required.",
                    },
                    <%= ResourceCurriculum.UniqueID %>: {
                CheckDropDownList: "Resource Curriculum is required."
            },
                    <%= ResourceCourse.UniqueID %>: {
                CheckDropDownList: "Resource Course is required."
            },--%>
                    <%= ResourceTypeD.UniqueID %>: {
                CheckDropDownList: "Resource TypeD is required."
            },
                    <%=txtWebLink.UniqueID %>: {
                required: "WebLink is required."
            },
                    <%=txtvolisspg.UniqueID %>: {
                required: "volisspg is required."
            },
                    <%=txtAuthor.UniqueID %>: {
                required: "Author is required."
            },
                    <%=txtPublication.UniqueID %>: {
                required: "Publication is required."
            },
                  <%--  <%= AuthorName.UniqueID %>: {
                required: "Author is required."
            },
                    <%= PublicationName.UniqueID %>: {
                required: "Publication is required."
            },--%>
                    <%= ThumbnailImg.UniqueID %>: {
                CheckImage: "Please Select Image"
            },
                    <%--<%= ThumbnailPreview.UniqueID %>: {
                        CheckImage: "Please Select Image"
                    },--%>
                    <%= ResourceData.UniqueID%>: {
                Checkfile: "Please Select file"
            }
        }
            });

        $.validator.addMethod("CheckDropDownList", function (value, element, param) {
            if (value == "0")
                return false;
            else
                return true;
        });

        $.validator.addMethod("Checkfile", function (value, element, param) {
            if ($("#hdnUploadURL").val() != "" || $("#hdnUploadURL").val() != "0") {
                return true;
            }
            else if (value.trim() == "")
                return false;
            else
                return true;
        });

        $.validator.addMethod("CheckImage", function (value, element, param) {
            if (value.trim() == "")
                return false;
            else
                return true;
        });
            
        }

        $(function () {
            $("#ResourceTypeD").change(function () {
                $('#ResourceData').val(null);
                $("#ThumbnailPreview").attr("src", "");
                if ($(this).val() == "3") {
                    $("#WebLink").show();
                    $(".WebLinksection").show();
                    $("#websection1").hide();
                    $("#JornalSection").hide();

                } else if ($(this).val() == "6") {
                    $("#WebLink").show();
                    $(".WebLinksection").show();
                    $("#websection1").hide();
                    $("#JornalSection").hide();

                }

                else if ($(this).val() == "1") {
                    $("#WebLink").hide();
                    $("#JornalSection").show();
                    $(".WebLinksection").hide();
                    $(".websection").show();
                }
                else {
                    $("#WebLink").hide();
                    $("#JornalSection").hide();
                    $(".WebLinksection").hide();
                    $(".websection").show();
                }
            });
        });

        $('#ThumbnailImg').on('change', function () {
            const file = this.files[0];
            const fileType = file['type'];
            const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
            if (validImageTypes.indexOf(fileType) == -1) {
                alert("Select valid image.");
                return;
            }
            $("#hdnThumbnailImgName").val(file.name);
            $("#imageModal").modal("show");
        });

        $('#imageModal').on('shown.bs.modal', function () {
            var input = $('#ThumbnailImg');
            readFile(input[0]);
        });


        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 270,
                height: 100
            },
            enableExif: true,
            enforceBoundary: true
        });

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });

                }

                reader.readAsDataURL(input.files[0]);
            }
            else {
                alert("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        $('.save-btn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                if (resp && resp != "data:,") {
                    $("#hdnThumbnailImg").val(resp);
                    $("#ThumbnailPreview").attr("src", resp);
                    $("#ThumbnailPreview").fadeIn();
                    $('#imageModal').modal("hide");
                }
            });
        });
        function clearImage() {
            $("#hdnThumbnailImgURL").attr("value", "");
            $("#ThumbnailImg").val('');
        }
        var resourcePath = "C:\Work\Medtrix Source\PTCV2\Source\PTVV2GIT\PTCV2";
        $('#ResourceData').on('change', function () {
            const file = this.files[0];
            const fileType = file['type'];
            var FileSize = file.size / 1024 / 1024; // in MB
            if (FileSize > 100) {
                alert('File size must be less than 100 MB.');
                $(this).val('');
                return false;
            } else {

            }
            //JOURNALS = 1, .pdf
            //VIDEO = 2, .mp4
            //SlideDeck = 5,ppt or pptx file
            //Poster = 7,.mp4
            var validImageTypes = "";
            var msg;
            if ($("#ResourceTypeD").val() == 0) {
                alert("Please select Resource Type first");
                this.value = null;
                return false;
            }
            switch ($("#ResourceTypeD").val()) {
                case "1":
                    validImageTypes = ['application/pdf'];
                    msg = "pdf file."
                    break;
                case "4":
                    validImageTypes = ['application/pdf', 'image/jpeg', 'image/jpg'];
                    msg = "pdf or image file."
                    break;
                case "5":
                    validImageTypes = ['application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.ms-powerpoint'];//application/vnd.ms-powerpoint
                    msg = "ppt or pptx file."
                    break;
                default:
                    validImageTypes = ['video/mp4'];
                    msg = "mp4 file.";
            }
            if (validImageTypes.indexOf(fileType) == -1) {
                alert("Select valid " + msg);
                this.value = null;
                return false;
            }
        });

        $(document).on('click', '#btnAddResource', function () {
            addResource();
            if (!isResourceImage) {
                return false;
            } else {
               
                return true;
            }
        })

        function addResource() {
            hideAlert();
            if ($('#AddresourceManager').valid()) {

                if ($("#btnAddResource").val() == "Update Resource") {
                    if ($("#hdnThumbnailImgURL").val() == "" || $("#hdnThumbnailImgURL").val() == "0") {
                        let msg = "";
                        msg = !$("#ThumbnailPreview").attr("src") ? 'Thumbnail image should not be empty!' : msg;
                        if (msg) {
                            showAlert(msg);
                            return isResourceImage = false;
                        }
                        return isResourceImage = true;
                    }
                    if ($("#hdnUploadURL").val() != "" || $("#hdnUploadURL").val() != "0") {
                        //$("label.error").css({"display": "none"});
                    }
                } else {
                    let msg = "";
                    msg = !$("#ThumbnailPreview").attr("src") ? 'Thumbnail image should not be empty!' : msg;
                    if (msg) {
                        showAlert(msg);
                        return isResourceImage = false;
                    }
                    return isResourceImage = true;
                }

                if ($("#hdnThumbnailImgURL").val() == "" || $("#hdnThumbnailImgURL").val() == "0") {
                    if ($("#ThumbnailImg").val() == "") {
                        let msg = "";
                        msg = !$("#ThumbnailPreview").attr("src") ? 'Thumbnail image should not be empty!' : msg;
                        if (msg) {
                            showAlert(msg);
                            return isResourceImage = false;
                        }
                        return isResourceImage = true;
                    }
                    else
                        return isResourceImage = true;
                }
                else
                    return isResourceImage = true;
            }
        }

        function showAlert(msg, type) {
            $("#resourceAlert").text(msg);
            $("#resourceAlert").show();
        }

        function hideAlert() {
            $("#resourceAlert").hide();
        }


    </script>
    <style type="text/css">
        label.error {
            color: red;
        }

        label.error1 {
            color: red;
        }

        .resourcethumb {
            margin-top: -50px;
        }
    </style>
</body>
</html>
