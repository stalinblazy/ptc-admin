﻿using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddResource : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
  
    //private resoursePath = System.Configuration.ConfigurationManager.AppSettings["server"];
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }

        string hdnResourceID = Request.QueryString["Id"];
        if (!IsPostBack)
        {
            BindData();
            if (hdnResourceID != null)
            {
                btnAddResource.Text = "Update Resource";
                GetDataByResourseId(hdnResourceID);
            }
            else
            {
                btnAddResource.Text = "Add Resource";
            }

            // ResourceTrack.SelectedValue = "3";
            // BindCurriculumDropdown(3);
            //  ResourceCurriculum.SelectedValue = "4";
        }
    }
    #region DropDowns
    private void BindData()
    {
        ResourceTypeD.DataSource = GetResourceLibraryTypeList().Tables[0];
        ResourceTypeD.DataValueField = "ID";
        ResourceTypeD.DataTextField = "Name";
        ResourceTypeD.DataBind();
        ResourceTypeD.Items.Insert(0, new ListItem("--Select  ResourceType--", "0"));

        ResourceTrack.DataSource = GetResourceTrackList().Tables[0];
        ResourceTrack.DataValueField = "TrackId";
        ResourceTrack.DataTextField = "TrackName";
        ResourceTrack.DataBind();
        ResourceTrack.Items.Insert(0, new ListItem("--Select  Track--", "0"));
        ResourceTrack.SelectedIndex = ResourceTrack.Items.IndexOf(ResourceTrack.Items.FindByText("Global"));
        BindCurriculumDropdown(0);
        ResourceCurriculum.SelectedIndex = ResourceCurriculum.Items.IndexOf(ResourceCurriculum.Items.FindByText("Global"));
    }

    #endregion

    public DataSet GetResourceLibraryTypeList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceLibraryType", parameters.ToArray());
    }

    public DataSet GetResourceTrackList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceTrack", parameters.ToArray());
    }
    public DataSet GetResourceCurriculumList(long trackId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@TrackId", trackId, ParameterDirection.Input));
        return dataManager.Execute("GetTrackCurriculumByTrackId", parameters.ToArray());
    }
    public DataSet GetResourceCourseList(long curriculumId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@CurriculumId", curriculumId, ParameterDirection.Input));
        return dataManager.Execute("GetCurriculumCourseByCurriculumId", parameters.ToArray());
    }

    protected void btnAddResource_Click(object sender, EventArgs e)
    {

        string resourcePath = System.Configuration.ConfigurationManager.AppSettings["ResourcePath"];
        string resName = ResourceName.Text;
        string Vol_Iss_Pg = txtvolisspg.Text;
        string Author = txtAuthor.Text;
        string Publication = txtPublication.Text;
        string resDescription = ResourceDescription.Text.ToString();
        int resType = Convert.ToInt32(ResourceTypeD.SelectedValue);
        string path = " ";
        string ThumbnailPath = " ";
        var ThumbnailImgBase64 = hdnThumbnailImg.Value;
        string link = txtWebLink.Text.ToString();
        if (!string.IsNullOrWhiteSpace(ThumbnailImgBase64))
        {
            var BaseConvert = ThumbnailImgBase64.Split(new Char[] { ',' });
            byte[] imageBytes = Convert.FromBase64String(BaseConvert[1]);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

            string ThumbnailFile = hdnThumbnailImgName.Value;

            string saveThumbnail = resourcePath + "\\Resources\\ThumbnailImages\\";

            //image.Save(saveThumbnail + ThumbnailFile, System.Drawing.Imaging.ImageFormat.Jpeg);
            image.Save(saveThumbnail + ThumbnailFile);

            //ThumbnailPath = "../" + defined.ToString() + "/Resources/ThumbnailImages/" + ThumbnailFile;
            ThumbnailPath = "Resources/ThumbnailImages/" + ThumbnailFile;
        }
        else
            ThumbnailPath = hdnThumbnailImgURL.Value;
        switch (resType)
        {
            #region PDF
            case (int)ResourceType.JOURNALS:
                if (ResourceData.HasFile)
                {
                    string extension = System.IO.Path.GetExtension(ResourceData.FileName);
                    try
                    {
                        string filename = Path.GetFileName(ResourceData.FileName);
                        HttpPostedFile postedFile = ResourceData.PostedFile;
                        string savePath = resourcePath + "/Resources/PDF/";
                        if (filename != "")
                        {
                            path = savePath + filename;
                            postedFile.SaveAs(path);
                        }
                        path = "Resources/PDF/" + filename;
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('error occured : " + ex.Message + "')", true);
                    }
                }
                break;
            #endregion

            #region VIDEO
            case (int)ResourceType.VIDEO:
                if (ResourceData.HasFile)
                {
                    string extension = System.IO.Path.GetExtension(ResourceData.FileName);
                    if (extension == ".mp4" || extension == "" || extension == null)
                    {
                        try
                        {
                            string filename = Path.GetFileName(ResourceData.FileName);
                            HttpPostedFile postedFile = ResourceData.PostedFile;
                            string savePath = resourcePath + "/Resources/VIDEO/";
                            if (filename != "")
                            {
                                path = savePath + filename;
                                postedFile.SaveAs(savePath + filename);
                            }
                            path = "Resources/VIDEO/" + filename;
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('error occured : " + ex.Message + "')", true);
                        }
                    }
                }
                break;

            #endregion

            #region WEBLINK
            case (int)ResourceType.WEBLINK:
                //try
                //{                    
                //    Resource res = new Resource();
                //    res.Name = resName;
                //    res.Description = resDescription;
                //    res.ResourceLibraryTypeID = resType;
                //    res.ResourceID = Convert.ToInt64(hdnResourceID.Value);
                //    hdnResourceID.Value = "0";
                //    res.UploadURL = link;
                //    res.ThumbnailImgURL = "";
                //    res.ResourceTrack = Int64.Parse(ResourceTrack.SelectedValue);
                //    res.ResourceCurriculum = Int64.Parse(ResourceCurriculum.SelectedValue);
                //    res.ResourceCourses = Int64.Parse(ResourceCourse.SelectedValue);
                //    res.Vol_Iss_Pg = Vol_Iss_Pg;
                //    res.Author = AuthorName.Text;
                //    res.Publication = PublicationName.Text;
                //    LearningManagementSystem.Components.Resource.CreateResource(res);
                //    BindData();
                //    Response.Redirect("ResourceManagement.aspx", false);
                //}
                //catch (Exception ex)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('error occured : " + ex.Message + "')", true);
                //}
                path = link;
                break;

            #endregion
            #region iRead
            case (int)ResourceType.iRead:
                //try
                //{                    
                //    Resource res = new Resource();
                //    res.Name = resName;
                //    res.Description = resDescription;
                //    res.ResourceLibraryTypeID = resType;
                //    res.ResourceID = Convert.ToInt64(hdnResourceID.Value);
                //    hdnResourceID.Value = "0";
                //    res.UploadURL = link;
                //    res.ThumbnailImgURL = "";
                //    res.ResourceTrack = Int64.Parse(ResourceTrack.SelectedValue);
                //    res.ResourceCurriculum = Int64.Parse(ResourceCurriculum.SelectedValue);
                //    res.ResourceCourses = Int64.Parse(ResourceCourse.SelectedValue);
                //    res.Vol_Iss_Pg = Vol_Iss_Pg;
                //    res.Author = AuthorName.Text;
                //    res.Publication = PublicationName.Text;
                //    LearningManagementSystem.Components.Resource.CreateResource(res);
                //    BindData();
                //    Response.Redirect("ResourceManagement.aspx", false);
                //}
                //catch (Exception ex)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('error occured : " + ex.Message + "')", true);
                //}
                path = link;
                break;

            #endregion
            #region SlideDeck
            case (int)ResourceType.SlideDeck:
                if (ResourceData.HasFile)
                {
                    string extension = System.IO.Path.GetExtension(ResourceData.FileName);

                    try
                    {
                        string filename = Path.GetFileName(ResourceData.FileName);
                        HttpPostedFile postedFile = ResourceData.PostedFile;
                        string savePath = resourcePath + "/Resources/PPT/";
                        if (filename != "")
                        {
                            path = savePath + filename;
                            postedFile.SaveAs(savePath + filename);
                        }
                        path = "Resources/PPT/" + filename;
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('error occured : " + ex.Message + "')", true);
                    }

                }
                break;

            #endregion

            #region Poster
            case (int)ResourceType.Poster:
                if (ResourceData.HasFile)
                {
                    string extension = System.IO.Path.GetExtension(ResourceData.FileName);

                    try
                    {
                        string filename = Path.GetFileName(ResourceData.FileName);
                        HttpPostedFile postedFile = ResourceData.PostedFile;
                        string savePath = resourcePath + "/Resources/POSTER/";
                        if (filename != "")
                        {
                            path = savePath + filename;
                            postedFile.SaveAs(savePath + filename);
                        }

                        path = "Resources/POSTER/" + filename;
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('error occured : " + ex.Message + "')", true);
                        //Flatworld.Trace.Logger.Log(ex.Message.ToString());
                    }

                }
                break;

            #endregion

            #region INVALID
            case (int)ResourceType.Invalid:
                break;
                #endregion
        }
        if (string.IsNullOrEmpty(path.Trim()))
            path = hdnUploadURL.Value;
        Resource resv = new Resource();
        resv.Name = resName;
        resv.Description = resDescription;
        resv.ResourceLibraryTypeID = resType;
        resv.ResourceID = Convert.ToInt64(hdnResourceID.Value);
        hdnResourceID.Value = "0";
        resv.UploadURL = path;
        resv.ThumbnailImgURL = ThumbnailPath;
        resv.ResourceTrack = Int64.Parse(ResourceTrack.SelectedValue);
        resv.ResourceCurriculum = Int64.Parse(ResourceCurriculum.SelectedValue);
        //resv.ResourceCourses = Int64.Parse(ResourceCourse.SelectedValue);
        resv.Vol_Iss_Pg = Vol_Iss_Pg;
        resv.Author = Author;
        resv.Publication = Publication;
        resv.AdminId = user_Id;
        LearningManagementSystem.Components.Resource.CreateResource(resv);
        // bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        //   auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Created New Resource from Resource Page Resource Name:" + resName + "", "Insert Operation", user_Id);
        BindData();
        Response.Redirect("flashCard.aspx.aspx?IsSuccessRescource=true", false);
    }

    protected void GetDataByResourseId(string resourseid)
    {
        try
        {
            DataSet resourseInfo = GetResourseInfo(resourseid);
            if (resourseInfo != null && resourseInfo.Tables.Count > 0 && resourseInfo.Tables[0].Rows.Count > 0)
            {
                hdnResourceID.Value = resourseInfo.Tables[0].Rows[0]["ID"].ToString();
                ResourceName.Text = resourseInfo.Tables[0].Rows[0]["Name"].ToString();
                ResourceDescription.Text = resourseInfo.Tables[0].Rows[0]["Description"].ToString();
                hdnThumbnailImgURL.Value = Convert.ToString(resourseInfo.Tables[0].Rows[0]["ThumbnailImgURL"]);
                hdnUploadURL.Value = Convert.ToString(resourseInfo.Tables[0].Rows[0]["UploadURL"]);
                if (resourseInfo.Tables[0].Rows[0]["TrackId"].ToString() != "")
                {
                    ResourceTrack.SelectedValue = resourseInfo.Tables[0].Rows[0]["TrackId"].ToString();
                    BindCurriculumDropdown(Convert.ToInt64(ResourceTrack.SelectedValue));
                }
                if (resourseInfo.Tables[0].Rows[0]["CurriculumId"].ToString() != "")
                {
                    ResourceCurriculum.SelectedValue = resourseInfo.Tables[0].Rows[0]["CurriculumId"].ToString();
                    //BindCourseDropdown(Convert.ToInt64(ResourceCurriculum.SelectedValue));
                }
                if (resourseInfo.Tables[0].Rows[0]["CoursesId"].ToString() != "")
                {
                    //ResourceCourse.SelectedValue = resourseInfo.Tables[0].Rows[0]["CoursesId"].ToString();
                }
                if (resourseInfo.Tables[0].Rows[0]["ResourceLibraryTypeID"].ToString() != "")
                {
                    ResourceTypeD.SelectedValue = resourseInfo.Tables[0].Rows[0]["ResourceLibraryTypeID"].ToString();
                }

                if (resourseInfo.Tables[0].Rows[0]["Name1"].ToString() == "Weblink")
                {
                    websection1.Style.Add("display", "none");
                    JornalSection.Style.Add("display", "none");
                    WebLink.Style.Remove("display");
                    txtWebLink.Text = resourseInfo.Tables[0].Rows[0]["UploadURL"].ToString();
                }
                else if (resourseInfo.Tables[0].Rows[0]["Name1"].ToString() == "Journals")
                {
                    WebLink.Style.Add("display", "none");
                    JornalSection.Style.Remove("display");
                    txtvolisspg.Style.Remove("display");
                    txtAuthor.Style.Remove("display");
                    txtPublication.Style.Remove("display");
                    txtvolisspg.Text = resourseInfo.Tables[0].Rows[0]["VolPage"].ToString();
                    txtAuthor.Text = resourseInfo.Tables[0].Rows[0]["Author"].ToString();
                    txtPublication.Text = resourseInfo.Tables[0].Rows[0]["Publication"].ToString();
                }
                else
                {
                    WebLink.Style.Add("display", "none");
                    JornalSection.Style.Add("display", "none");
                }



            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
    }

    public DataSet GetResourseInfo(string resourseId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@ResourseId", resourseId, ParameterDirection.Input));
        return dataManager.Execute("GetResourseById", parameters.ToArray());
    }

    private void BindCurriculumDropdown(long id)
    {
        ResourceCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        ResourceCurriculum.DataValueField = "CurriculumId";
        ResourceCurriculum.DataTextField = "CurriculumName";
        ResourceCurriculum.DataBind();
        ResourceCurriculum.Items.Insert(0, new ListItem("--Select  Curriculum--", "0"));
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }



    protected void ResourceTrack_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCurriculumDropdown(Convert.ToInt64(ResourceTrack.SelectedItem.Value));
    }
    protected void ResourceCurriculum_SelectedIndexChanged(object sender, EventArgs e)
    {
        //BindCourseDropdown(Convert.ToInt64(ResourceCurriculum.SelectedItem.Value));
    }


    
}