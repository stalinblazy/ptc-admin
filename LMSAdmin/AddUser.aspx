﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddUser.aspx.cs" Inherits="Admin_AddUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
    <%--  <div class="loader">
        <img src="images/loaderp.svg" />
    </div>--%>
    <div id="app" class="container-fluid">

        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Users and Groups
     
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Users and Groups - Overview - Add User
     
                       
                        </div>
                    </div>
                </div>

                <div class="row" id="pageBody">

                    <div class="col-md-12">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxtUser">
                                    <h1>Users Details</h1>
                                </div>

                                <!--forms starts here-->
                                <form id="addUser" runat="server">
                                    <div class="userForm">
                                        <!-- first row starts here-->
                                        <div class="row">
                                            <asp:HiddenField ID="hidGroupID" runat="server" Value="0" />
                                            <asp:HiddenField ID="hidgroupIdChange" runat="server" Value="0" />
                                            <asp:HiddenField ID="hidGroupOldID" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnaddUserID" runat="server" Value="0" />
                                            <div class="col-sm-4 formTxt">
                                                <label>First Name</label>
                                                <asp:TextBox runat="server" ID="FirstName" class="form-control" placeholder="First Name"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4 formTxt">
                                                <label>Last Name</label>
                                                <asp:TextBox runat="server" ID="LastName" class="form-control" placeholder="Last Name"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 formTxt" id="InvalidFirstNameMsg" style="display: none">
                                                <label class="error" id="errorInvalidFirstNameMsg">First Name is requried</label>
                                            </div>
                                            <div class="col-sm-4 formTxt" id="InvalidLastNameMsg" style="display: none; float: right">
                                                <label class="error" id="errorInvalidLastNameMsg">Last Name is requried</label>
                                            </div>

                                        </div>
                                        <!-- first row ends here-->
                                        <!--second row starts here-->
                                        <div class="row">
                                            <div class="col-sm-4 formTxt">
                                                <label>Email Id</label>
                                                <asp:TextBox runat="server" ID="userMail" class="form-control" placeholder="Email Id"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4 formTxt">
                                                <label>Designation</label>
                                                <asp:TextBox runat="server" ID="Designation" class="form-control" placeholder="Designation"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 formTxt" id="InvalidMaildMsg" style="display: none">
                                                <label class="error" id="errorInvalidMaildMsg">Email Address is requried</label>
                                            </div>
                                            <div class="col-sm-4 formTxt" id="InvalidDesignationMsg" style="display: none; float: right">
                                                <label class="error" id="errorInvalidDesignationMsg">Designation is requried</label>
                                            </div>
                                        </div>
                                        <div class="row" id="InvalidMsg" style="display: none">
                                            <div class="col-sm-4 formTxt">
                                                <label class="error" id="error">Email Id is already Exist !!</label>
                                            </div>
                                        </div>
                                        <!--second row ends here-->
                                        <!--third row starts here-->
                                        <div class="row">
                                            <div class="col-sm-4 formTxt">
                                                <label>Group</label>
                                                <asp:DropDownList ID="GroupD" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">Group</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4 formTxt dropdown">
                                                <label>Country List</label>
                                                <asp:DropDownList ID="CountryD" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">Country</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <!--third row ends here-->
                                        <!--fourth row starts here-->
                                        <div class="row">
                                            <div class="col-sm-4 formTxt">
                                                <label>Region</label>
                                                <asp:DropDownList ID="RegionD" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">Region</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4 formTxt">
                                                <label>Department</label>
                                                <asp:DropDownList ID="DepartmentD" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">Department</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 formTxt">
                                            </div>
                                            <div class="col-sm-4 formTxt" id="InvalidDepartmentMsg" style="display: none; float: right">
                                                <label class="error" id="errorInvalidDepartmentMsg">Department is requried</label>
                                            </div>
                                        </div>
                                        <!--fourth row ends here-->
                                        <!--fifth row starts here-->
                                        <div class="row">
                                            <div class="col-sm-4 formTxt">
                                                <label>Manager</label>
                                                 <asp:TextBox runat="server" ID="ManagerD" class="form-control" placeholder="Manager"></asp:TextBox>
                                             <%--   <asp:DropDownList ID="ManagerD" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">Manger</asp:ListItem>
                                                </asp:DropDownList>--%>
                                            </div>
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 formTxt" id="InvalidMangerMsg" style="display: none; float: right">
                                                <label class="error" id="errorInvalidMangerMsg">Manager is requried</label>
                                            </div>
                                            <div class="col-sm-4 formTxt">
                                            </div>
                                        </div>
                                        <!--fifth row ends here-->
                                        <!--back to user starts here-->
                                        <div class="row userBut">
                                            <div class="col-md-6"></div>
                                            <div class="col-md-3 userBut1">
                                                <a href="UserManagement.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to users</a>
                                            </div>
                                            <div class="col-md-3">

                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClientClick="this.disabled = true; this.value = 'Submitting...';" UseSubmitBehavior="false" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <asp:Button Text="Add User" class="btn text-white userBut2" OnClick="Submit_Click" runat="server" ID="btnAdd" />
                                            </div>
                                        </div>
                                        <!--back to user ends here-->
                                    </div>
                                </form>
                                <!--forms ends here-->

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts" runat="server" />

    <script src="scripts/custom/UserManagement?v1.js"></script>
    <script src="scripts/app.js"></script>
    <script src="scripts/jquery.validate.js"></script>
    <script type="text/javascript">
        var isEmailExist = false;
        $(document).ready(function () {
            var previousGroupId = $("#GroupD").val();
            document.getElementById("hidGroupOldID").value = previousGroupId
           // alert(currentValue);
            $('#FirstName').on("cut copy paste", function (e) {
                //   alert("addsa");
                e.preventDefault();
            });
            $('#LastName').on("cut copy paste", function (e) {
                //   alert("addsa");
                e.preventDefault();
            });
            $('#userMail').on("cut copy paste", function (e) {
                //   alert("addsa");
                e.preventDefault();
            });
            $('#Designation').on("cut copy paste", function (e) {
                //   alert("addsa");
                e.preventDefault();
            });
            var e = document.getElementById("GroupD");
            var groupID = e.options[e.selectedIndex].value;
            document.getElementById("hidGroupID").value = groupID
            //   alert(document.getElementById("hidGroupID").value);
            ChkValidation();
            //  $(".loader").fadeOut();

        });
        $('#GroupD').change(function () {

            document.getElementById("hidgroupIdChange").value = "1";
        });
        $("#btnAdd").on("click", function (e) {
            
            var firstName = $("#FirstName");
            var lastName = $("#LastName");
            var emailId = $("#userMail");
            var designation = $("#Designation");
            var departmentId = $("#DepartmentD");
           // var managerId = $("#ManagerD");

            if (firstName.val() == "") {
                $("#InvalidFirstNameMsg").css("display", "block");
                return false;
            }
            else {
                $('label[id*="errorInvalidFirstNameMsg"]').text('');
            }

            if (lastName.val() == "") {
                $("#InvalidLastNameMsg").css("display", "block")
                return false;
            }
            else {
                $('label[id*="InvalidLastNameMsg"]').text('')
            }

            if (emailId.val() == "") {
                $("#InvalidMaildMsg").css("display", "block");
                return false;
            }
            else {
                $('label[id*="InvalidMaildMsg"]').text('')
            }

            if (designation.val() == "") {
                $("#InvalidDesignationMsg").css("display", "block");
                return false;
            }
            else {
                $('label[id*="errorInvalidDesignationMsg"]').text('')
            }

            if (departmentId.val() == "9") {
                $("#InvalidDepartmentMsg").css("display", "block");
                return false;
            }
            else {
                $('label[id*="errorInvalidDepartmentMsg"]').text('')
            }
            //if (managerId.val() == "16") {
            //    $("#InvalidMangerMsg").css("display", "block");
            //    return false;
            //}
            //else {
            //    $('label[id*="errorInvalidMangerMsg"]').text('');
            //}

            var a = validateEmail($("#userMail").val());
            if ($(this).val() == '') {
                $("#InvalidMaildMsg").attr("style", "display:block")
                $(this).css("border-color", "#FF0000");
                return false;
            }
            else if (a == false) {
                // alert(emailAddress);
                $(this).css("border-color", "#FF0000");
                $("#InvalidMaildMsg").attr("style", "display:block");
                $("#errorInvalidMaildMsg").text("Please enter valid Email Id!");
                return false;
            }
            else {
                //   alert("I am in else of emId");
                $(this).css("border-color", "#2eb82e");
                $('label[id*="InvalidMaildMsg"]').text('')
                $("#errorInvalidMaildMsg").text("");
                if ($("#userMail").val() != "") {
                    checkIsUserEmailAvilable();
                    //var isEmailExist = checkIsUserEmailAvilable();
                    if (isEmailExist) {
                      //  alert(isEmailExist);
                        return false;
                    }
                    else
                        return true;
                }

            }

            if ($("#userMail").val() != "") {
                checkIsUserEmailAvilable();
                //var isEmailExist = checkIsUserEmailAvilable();
                if (isEmailExist) {
                  //  alert(isEmailExist);
                    return false;
                }
                else
                    return true;
            }
        });

        $("#FirstName").focusout(function () {
            if ($(this).val() == '' || ($("#FirstName").val()).length == 0) {
                $(this).css("border-color", "#FF0000");
                // $("#error_FirstName").text("* Required!");
                $("#InvalidFirstNameMsg").attr("style", "display:block")
            }
            else {
                $(this).css("border-color", "#2eb82e");
                $('label[id*="InvalidFirstNameMsg"]').text('');

            }
        });

        $("#LastName").focusout(function () {
            if ($(this).val() == '' || ($("#FirstName").val()).length == 0) {
                $(this).css("border-color", "#FF0000");
                // $("#error_FirstName").text("* Required!");
                $("#InvalidLastNameMsg").attr("style", "display:block")
            }
            else {
                $(this).css("border-color", "#2eb82e");
                $('label[id*="InvalidLastNameMsg"]').text('')
            }
        });
        $("#userMail").focusout(function () {
            var a = validateEmail($("#userMail").val());
            if ($(this).val() == '') {
                $("#InvalidMaildMsg").attr("style", "display:block")
                $(this).css("border-color", "#FF0000");
            }
            else if (a == false) {
                // alert(emailAddress);
                $(this).css("border-color", "#FF0000");
                $("#InvalidMaildMsg").attr("style", "display:block");
                $("#errorInvalidMaildMsg").text("Please enter valid Email Id!");
                return false;
            }
            else {
                //   alert("I am in else of emId");
                $(this).css("border-color", "#2eb82e");
                $('label[id*="InvalidMaildMsg"]').text('')
                $("#errorInvalidMaildMsg").text("");

            }
        });


        $("#Designation").focusout(function () {
            if ($(this).val() == '' || ($("#FirstName").val()).length == 0) {
                $(this).css("border-color", "#FF0000");
                // $("#error_FirstName").text("* Required!");
                $("#InvalidDesignationMsg").attr("style", "display:block")
            }
            else {
                $(this).css("border-color", "#2eb82e");
                $('label[id*="errorInvalidDesignationMsg"]').text('')

            }
        });
        $("#DepartmentD").focusout(function () {
            if ($(this).val() == "9") {
                $(this).css("border-color", "#FF0000");
                // $("#error_FirstName").text("* Required!");
                $("#InvalidDepartmentMsg").attr("style", "display:block")
            }
            else {
                $(this).css("border-color", "#2eb82e");
                $('label[id*="errorInvalidDepartmentMsg"]').text('')

            }
        });

        //$("#ManagerD").focusout(function () {
        //    if ($(this).val() == "16") {
        //        $(this).css("border-color", "#FF0000");
        //        // $("#error_FirstName").text("* Required!");
        //        $("#InvalidMangerMsg").attr("style", "display:block")
        //    }
        //    else {
        //        $(this).css("border-color", "#2eb82e");
        //        $('label[id*="errorInvalidMangerMsg"]').text('');

        //    }
        //});

        

        function validateEmail(sEmail) {
           // alert(sEmail);
            //var filter = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,10}$");
            // var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,10})+$/
            var mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,10})+$/;
            if (sEmail.match(mailformat)) {
                return true;
            } else {
                //  alert('Please provide a valid email address');
                // sEmail.focus;
                return false;
            }
        }
        var checkIsUserEmailAvilable = function () {
            $.ajax({
                type: "POST",
                url: "AddUser.aspx/IsEmailIdAvailable",
                contentType: "application/json; charset=utf-8",
                async: false,
                dataType: "json",
                data: JSON.stringify({ emailId: $('#userMail').val(), IsUpdate: document.getElementById('<%= btnAdd.ClientID %>').value }),
                success: function (data) {
                    var msg = data.d;
                    isEmailExist = data.d;
                    if (msg == true) {
                        $("#InvalidMsg").css("display", "block");
                        $("#error").css("display", "block");

                        $("#userMail").addClass("error");
                    }
                    else {
                        $("#InvalidMsg").css("display", "none");
                        $("#error").css("display", "none");
                        $("#userMail").addClass("Valid");
                    }
                    return msg;
                },
                error: function (res) {
                    console.log(res);
                    isEmailExist = false;
                }
            });
            return false;
        }
    </script>
    <style type="text/css">
        label.error {
            color: red;
        }
    </style>

</body>
</html>
