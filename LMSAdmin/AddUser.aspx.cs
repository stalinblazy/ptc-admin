﻿using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LearningManagementSystem.Data;
using LearningManagementSystem.UserManager;

public partial class Admin_AddUser : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        try
        {
            string hdnaddUserID = Request.QueryString["Id"];
            if (!IsPostBack)
            {
                BindData();
                if (hdnaddUserID != null)
                {
                    btnAdd.Text = "Update User";
                    GetDataByemailId(hdnaddUserID);
                }
                else
                {
                    btnAdd.Text = "Add User";
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    #endregion


    #region DropDowns
    private void BindData()
    {
        RegionD.DataSource = GetRegionList().Tables[0];
        RegionD.DataValueField = "ID";
        RegionD.DataTextField = "Name";
        RegionD.DataBind();

        CountryD.DataSource = GetCountryList().Tables[0];
        CountryD.DataValueField = "countryid";
        CountryD.DataTextField = "country";
        CountryD.DataBind();

        DepartmentD.DataSource = GetDepartmentList().Tables[0];
        DepartmentD.DataValueField = "ID";
        DepartmentD.DataTextField = "Name";
        DepartmentD.DataBind();

        //ManagerD.DataSource = GetReportingManagerList().Tables[0];
        //ManagerD.DataValueField = "ID";
        //ManagerD.DataTextField = "Name";
        //ManagerD.DataBind();

        GroupD.DataSource = GetLMSGroup().Tables[0];
        GroupD.DataValueField = "ID";
        GroupD.DataTextField = "Name";
        GroupD.DataBind();
        GroupD.SelectedIndex = GroupD.Items.IndexOf(GroupD.Items.FindByText("NoGroup"));
    }

    public DataSet GetDepartmentList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetDepartmentList", parameters.ToArray());
    }

    public DataSet GetRegionList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetRegionList", parameters.ToArray());
    }

    public DataSet GetCountryList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetCountryList", parameters.ToArray());
    }

    public DataSet GetReportingManagerList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetReportingManagerList", parameters.ToArray());
    }

    public DataSet GetLMSGroup()
    {
        try
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            return dataManager.Execute("GetLMSGroupList", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
        return new DataSet();
    }
    #endregion

    #region AddUser

    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnaddUserID.Value != "0")
            {
                bool result = EditUser(Convert.ToInt64(hdnaddUserID.Value), FirstName.Text, LastName.Text, GroupD.SelectedItem.Text, GroupD.SelectedValue, CountryD.SelectedItem.Text, CountryD.SelectedValue, DepartmentD.SelectedItem.Text, ManagerD.Text, Designation.Text, RegionD.SelectedItem.Text);
                Response.Redirect("UserManagement.aspx?IsSuccess=true", false);
            }
            else
            {
                bool isUserCreated = CreateUser(userMail.Text, FirstName.Text, LastName.Text, Designation.Text, RegionD.SelectedItem.Text, CountryD.SelectedItem.Text, CountryD.SelectedValue, DepartmentD.SelectedItem.Text, ManagerD.Text, GroupD.SelectedItem.Text, GroupD.SelectedValue);
                Response.Redirect("UserManagement.aspx?IsSuccess=true", false);
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
    }

    private bool CreateUser(string eMailID, string Firstname, string Lastname, string Designation, string region, string country, string countryId, string department, string reportingManager, string groupName, string groupId)
    {
        try
        {
            string departName = string.Empty;
            if (groupId == "5")
            {
                departName = "BP - " + department;
            }
            else
            {
                departName = department;
            }
            String referenceCode = Guid.NewGuid().ToString();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@email", eMailID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@firstname", Firstname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@lastname", Lastname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@designation", Designation, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@region", region, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@department", departName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@country", country, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@countryid", Convert.ToInt64(countryId), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@usertype", 4, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@referencecode", referenceCode, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@reportingmanger", reportingManager, ParameterDirection.Input));

            parameters.Add(dataManager.CreateParam("@active", 1, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@accesscode", "PTC", ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@password", "12345678", ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@isdeleted", "0", ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@lmsgroup", groupName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@lmsgroupId", groupId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", user_Id, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", 0, ParameterDirection.Output));

            DataSet ds = dataManager.Execute("CreateUser", parameters.ToArray());
            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                String UserId = ds.Tables[0].Rows[0]["userid"].ToString();
                if (groupName.Trim() != "NoGroup")
                {
                    return LearningManagementSystem.Components.Group.SendGroupEmailer(groupName, Convert.ToInt32(groupId), Convert.ToInt32(UserId));
                }
            }

            return false;

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return false;
        }
    }

    [WebMethod]
    public static bool IsEmailIdAvailable(string emailId, string IsUpdate)
    {
        bool result = false;
        try
        {
            if (IsUpdate == "Update User")
                result = false;
            else
                result = LearningManagementSystem.Components.User.IsMailExists(emailId);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            result = true;
        }
        return result;
    }

    protected void GetDataByemailId(string userid)
    {
        try
        {
            DataSet userInfo = GetUserInfo(userid);
            if (userInfo != null && userInfo.Tables.Count > 0 && userInfo.Tables[0].Rows.Count > 0)
            {
                hdnaddUserID.Value = userInfo.Tables[0].Rows[0]["User_Id"].ToString();
                FirstName.Text = userInfo.Tables[0].Rows[0]["First_Name"].ToString();
                LastName.Text = userInfo.Tables[0].Rows[0]["Last_Name"].ToString();
                userMail.Text = userInfo.Tables[0].Rows[0]["Email_ID"].ToString().Trim();
                ManagerD.Text = userInfo.Tables[0].Rows[0]["ReportingManagerID"].ToString().Trim();
                userMail.Attributes.Add("readonly", "readonly");
                if (userInfo.Tables[0].Rows[0]["countryid"].ToString() != "")
                {
                    CountryD.SelectedValue = userInfo.Tables[0].Rows[0]["countryid"].ToString();
                }

                if (userInfo.Tables[0].Rows[0]["DepartmentId"].ToString() != "")
                {
                    DepartmentD.SelectedValue = userInfo.Tables[0].Rows[0]["DepartmentId"].ToString();
                }

                if (userInfo.Tables[0].Rows[0]["Designation"].ToString() != "")
                {
                    Designation.Text = userInfo.Tables[0].Rows[0]["Designation"].ToString();
                }

                if (userInfo.Tables[0].Rows[0]["LMSGroupID"].ToString() != "")
                {
                    GroupD.SelectedValue = userInfo.Tables[0].Rows[0]["LMSGroupID"].ToString();
                }

                //if (userInfo.Tables[0].Rows[0]["ManagerId"].ToString() != "")
                //{
                //    ManagerD.SelectedValue = userInfo.Tables[0].Rows[0]["ManagerId"].ToString();
                //}

                if (userInfo.Tables[0].Rows[0]["RegionId"].ToString() != "")
                {
                    RegionD.SelectedValue = userInfo.Tables[0].Rows[0]["RegionId"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
    }

    public DataSet GetUserInfo(string emailId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@UserID", emailId, ParameterDirection.Input));
        //parameters.Add(new SqlParameter { ParameterName = "@UserId", Value = emailId });
        return dataManager.Execute("GetUserByEMailId", parameters.ToArray());
    }

    private bool EditUser(long userId, String Firstname, String Lastname, string groupName, string groupId, string country, string countryId, string department, string reportingManager, string designation, string region)
    {
        try
        {
            // Response.Write(GroupD.SelectedItem.Value);
            String referenceCode = Guid.NewGuid().ToString();
            bool auditDetails = false;
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            string departName = string.Empty;
            if (groupId == "5")
            {
                departName = "BP - " + department;
            }
            else
            {
                departName = department;
            }
            parameters.Add(dataManager.CreateParam("@FirstName", Firstname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@LastName", Lastname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Designation", designation, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Country", country, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@countryid", Convert.ToInt64(countryId), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@GroupName", groupName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@GroupId", groupId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Department", departName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Manager", reportingManager, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Region", region, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", userId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@NoGroup", hidGroupID.Value, ParameterDirection.Input));
            LearningManagementSystem.Components.User us = LearningManagementSystem.Components.User.GetUser(userId);
            if (groupId != "9" && hidGroupID.Value != "9" && hidgroupIdChange.Value == "1")
            {
                // sts =0 or 1 Update the group id in the table
                // sts =2 then insert fresh record table
                // sts =(-1) then remove the data from table
                int updateQueryCourseMappingUser = 0;
                int updatepackagecourseID = 0;
                int insertIntoCourseMapping = 0;
                int insertIntoUserSubCourseDetails = 0;
                int removeCourseMapping = 0;
                int removeUserSubCourseDetails = 0;
                string getUserSubCourseDetails = "";
                string SubCourse_ID = "";
                int updateUserData = 0;
                int packageCourseIdRequired = 0;

                string checkPackageCourseID = "SELECT Course_Id,LMSGroupID,User_Course_Status FROM CourseUserMapping WHERE course_id IN(1,2,3) AND user_id = '" + userId + "' AND LMSGroupID='" + hidGroupOldID.Value + "'";
                DataTable dataTable = dataManager.GetRecordsBySql(checkPackageCourseID);
                DataRow[] rs0 = dataTable.Select("User_Course_Status>1");
                if (rs0.Length > 0)
                {
                    packageCourseIdRequired = 0;
                    updatepackagecourseID = dataManager.InsertSQL("UPDATE CourseUserMapping SET LMSGroupID='" + groupId + "' WHERE User_Id='" + userId + "' AND Course_Id IN(1,2,3) AND LMSGroupID='" + hidGroupOldID.Value + "'");
                }
                else
                {
                    packageCourseIdRequired = 1;
                    if (dataTable.Rows.Count > 0)
                    {
                        removeCourseMapping = dataManager.InsertSQL("DELETE FROM CourseUserMapping WHERE User_Id='" + userId + "' AND LMSGroupID='" + hidGroupOldID.Value + "' AND Course_Id IN (1,2,3)");
                        SubCourse_ID = "SELECT Sub_Course_Id FROM SubCourse WHERE Course_ID IN(1,2,3)";
                        DataSet dataSetSubCourse_Id = dataManager.GetDataSet(SubCourse_ID);
                        if (dataSetSubCourse_Id != null)
                        {
                            for (int k = 0; k < dataSetSubCourse_Id.Tables[0].Rows.Count; k++)
                            {
                                removeUserSubCourseDetails = dataManager.InsertSQL("DELETE FROM UserSubCourseDetails WHERE Subcourse_Id='" + dataSetSubCourse_Id.Tables[0].Rows[k]["Sub_Course_Id"] + "' AND User_Id='" + userId + "'");
                            }
                        }
                    }
                    //Remove the User from the Data base;
                }
                string queryForGroupSwap = "";
                string queryToInsertDueDate = "";
                string dueDayIn = "";
                if (packageCourseIdRequired == 0)
                {
                    queryForGroupSwap = "SELECT Course_Id, LMSGroupID  AS Group_ID,'-1' as sts,User_Course_Status from CourseUserMapping where User_Course_Status = 1 and course_id " +
                                      "NOT IN(SELECT course_id FROM CourseGroupMapping WHERE Group_Id = '" + groupId + "') and user_id = '" + userId + "'  AND Course_Id not in(1,2,3) UNION ALL " +
                                      "SELECT Course_Id, LMSGroupID  AS Group_ID,'0' AS sts,User_Course_Status FROM CourseUserMapping WHERE User_Course_Status > 1 and course_id " +
                                      "NOT IN(SELECT course_id FROM CourseGroupMapping WHERE Group_Id = '" + groupId + "') and user_id = '" + userId + "'  AND Course_Id not in(1,2,3) UNION ALL " +
                                      "SELECT Course_Id, LMSGroupID  AS Group_ID,'1' AS sts,User_Course_Status FROM CourseUserMapping WHERE course_id IN(SELECT " +
                                      "course_id FROM CourseGroupMapping WHERE Group_Id = '" + groupId + "') and user_id = '" + userId + "'  AND Course_Id not in(1,2,3) UNION ALL SELECT Course_Id, Group_ID,'2' AS sts,'0' " +
                                      "as User_Course_Status from CourseGroupMapping where course_id NOT IN(SELECT course_id FROM CourseUserMapping WHERE user_id = '" + userId + "') AND Group_Id ='" + groupId + "' AND Course_Id not in(1,2,3)";
                }
                else if (packageCourseIdRequired == 1)
                {
                    queryForGroupSwap = "SELECT Course_Id, LMSGroupID  AS Group_ID,'-1' as sts,User_Course_Status from CourseUserMapping where User_Course_Status = 1 and course_id " +
                                     "NOT IN(SELECT course_id FROM CourseGroupMapping WHERE Group_Id = '" + groupId + "') and user_id = '" + userId + "' UNION ALL " +
                                     "SELECT Course_Id, LMSGroupID  AS Group_ID,'0' AS sts,User_Course_Status FROM CourseUserMapping WHERE User_Course_Status > 1 and course_id " +
                                     "NOT IN(SELECT course_id FROM CourseGroupMapping WHERE Group_Id = '" + groupId + "') and user_id = '" + userId + "' UNION ALL " +
                                     "SELECT Course_Id, LMSGroupID  AS Group_ID,'1' AS sts,User_Course_Status FROM CourseUserMapping WHERE course_id IN(SELECT " +
                                     "course_id FROM CourseGroupMapping WHERE Group_Id = '" + groupId + "') and user_id = '" + userId + "' UNION ALL SELECT Course_Id, Group_ID,'2' AS sts,'0' " +
                                     "as User_Course_Status from CourseGroupMapping where course_id NOT IN(SELECT course_id FROM CourseUserMapping WHERE user_id = '" + userId + "') AND Group_Id ='" + groupId + "'";
                }
                DataSet dataSetCourseSwap = dataManager.GetDataSet(queryForGroupSwap);
                if (dataSetCourseSwap != null)
                {
                    for (int i = 0; i < dataSetCourseSwap.Tables[0].Rows.Count; i++)
                    {
                        if (dataSetCourseSwap.Tables[0].Rows[i]["sts"].ToString() == "0" || dataSetCourseSwap.Tables[0].Rows[i]["sts"].ToString() == "1")
                        {
                            //update courseuserMapping
                            updateQueryCourseMappingUser = dataManager.InsertSQL("UPDATE CourseUserMapping SET LMSGroupID='" + groupId + "' WHERE User_Id='" + userId + "' AND Course_Id='" + dataSetCourseSwap.Tables[0].Rows[i]["Course_Id"].ToString() + "' AND LMSGroupID='" + hidGroupID.Value + "'");
                        }
                        else if (dataSetCourseSwap.Tables[0].Rows[i]["sts"].ToString() == "2")
                        {
                            //insert into courseuserMapping and usersubcoursdetails
                            //Insert Due Date
                            queryToInsertDueDate = "SELECT DueInDays FROM Courses WHERE Course_Id='" + dataSetCourseSwap.Tables[0].Rows[i]["Course_Id"] + "'";
                            DataSet dataSetDueDaysCourse = dataManager.GetDataSet(queryToInsertDueDate);
                            if (dataSetDueDaysCourse != null)
                            {
                                dueDayIn = dataSetDueDaysCourse.Tables[0].Rows[0]["DueInDays"].ToString();
                            }
                            insertIntoCourseMapping = dataManager.InsertSQL("INSERT INTO CourseUserMapping(Course_Id,User_Id,Active,Assign_Date,Completion_Date,LMSGroupID,User_Course_Status,DueDate) VALUES('" + dataSetCourseSwap.Tables[0].Rows[i]["Course_Id"] + "','" + userId + "','1'," +
                                                    "'" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "',NULL,'" + groupId + "','1',DateAdd( day, " + dueDayIn + " , Cast( GetDate() as Datetime ) ) )");
                            getUserSubCourseDetails = "SELECT Sub_Course_Id FROM SubCourse WHERE Course_ID='" + dataSetCourseSwap.Tables[0].Rows[i]["Course_Id"] + "'";
                            DataSet dataSetSubCourse = dataManager.GetDataSet(getUserSubCourseDetails);
                            if (dataSetSubCourse != null)
                            {
                                for (int j = 0; j < dataSetSubCourse.Tables[0].Rows.Count; j++)
                                {
                                    insertIntoUserSubCourseDetails = dataManager.InsertSQL("INSERT INTO UserSubCourseDetails(User_Id,Subcourse_Id,Percentage,Sub_Course_Status,Completion_Date,LastModifiedDate,TimeSpentInSeconds,isAlreadyCompleted,Attempts)" +
                                        "VALUES('" + userId + "','" + dataSetSubCourse.Tables[0].Rows[j]["Sub_Course_Id"] + "',0,1,NULL,'" + DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss") + "',0,0,0)");
                                }
                            }
                        }
                        else if (dataSetCourseSwap.Tables[0].Rows[i]["sts"].ToString() == "-1")
                        {
                            //remove data from into courseuserMapping and usersubcoursdetails
                            removeCourseMapping = dataManager.InsertSQL("DELETE FROM CourseUserMapping WHERE User_Id='" + userId + "' AND LMSGroupID='" + dataSetCourseSwap.Tables[0].Rows[i]["Group_ID"] + "' AND Course_Id='" + dataSetCourseSwap.Tables[0].Rows[i]["Course_Id"] + "'");
                            SubCourse_ID = "SELECT Sub_Course_Id FROM SubCourse WHERE Course_ID='" + dataSetCourseSwap.Tables[0].Rows[i]["Course_Id"] + "'";
                            DataSet dataSetSubCourse_Id = dataManager.GetDataSet(SubCourse_ID);
                            if (dataSetSubCourse_Id != null)
                            {
                                for (int k = 0; k < dataSetSubCourse_Id.Tables[0].Rows.Count; k++)
                                {
                                    removeUserSubCourseDetails = dataManager.InsertSQL("DELETE FROM UserSubCourseDetails WHERE Subcourse_Id='" + dataSetSubCourse_Id.Tables[0].Rows[k]["Sub_Course_Id"] + "' AND User_Id='" + userId + "'");
                                }
                            }
                        }
                    }

                    // Above all Success
                    updateUserData = dataManager.InsertSQL("UPDATE Users SET LMSGroupID='" + groupId + "', LMSGroupName='" + groupName + "' WHERE User_Id='" + userId + "'");
                    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Swap User From Old Group :" + hidGroupOldID.Value + " to New Group :" + groupId + " for User Id's" + userId + "", "Update Operation", user_Id);
                    //if (dataManager.ExecuteNonQuery("SP_LMS_EDIT_USER", parameters.ToArray()))
                    //{
                    if (groupName.Trim() != "NoGroup" && Convert.ToInt32(groupId) != Convert.ToInt32(us.LMSGroupId))
                    {
                        return LearningManagementSystem.Components.Group.SendGroupEmailer(groupName, Convert.ToInt32(groupId), Convert.ToInt32(userId));
                    }
                    //  }
                }
                else
                {
                    if (dataManager.ExecuteNonQuery("SP_LMS_EDIT_USER", parameters.ToArray()))
                    {
                        auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Changed Group of the User Id's" + userId + " to Group Id :" + groupId + "", "Update Operation", user_Id);
                        if (groupName.Trim() != "NoGroup" && Convert.ToInt32(groupId) != Convert.ToInt32(us.LMSGroupId))
                        {
                            return LearningManagementSystem.Components.Group.SendGroupEmailer(groupName, Convert.ToInt32(groupId), Convert.ToInt32(userId));
                        }
                    }
                }

            }
            else
            {
                if (dataManager.ExecuteNonQuery("SP_LMS_EDIT_USER", parameters.ToArray()))
                {
                    if (hidgroupIdChange.Value == "0") { auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Modified the User Profile for User Id's" + userId + "", "Update Operation", user_Id); }
                    else { auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Changed Group of the User Id's" + userId + " from Old Group ID :" + hidGroupOldID.Value + " to Group Id :" + groupId + "", "Update Operation", user_Id); }
                    if (groupName.Trim() != "NoGroup" && Convert.ToInt32(groupId) != Convert.ToInt32(us.LMSGroupId))
                    {
                        return LearningManagementSystem.Components.Group.SendGroupEmailer(groupName, Convert.ToInt32(groupId), Convert.ToInt32(userId));
                    }
                }
            }
            return false;
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return false;
        }
    }
    #endregion
}