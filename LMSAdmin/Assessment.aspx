﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Assessment.aspx.cs" Inherits="Assessment" %>

<%@ Register TagPrefix="LMS" TagName="AssessmentList" Src="AssessmentList.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assessment</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="css/w3.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="loader" id="loader" runat="server" style="display: none">
            <img src="images/loaderp.svg" />
        </div>

        <div id="app" class="container-fluid">
            <!-- header -->
            <LMS:CommonHeader ID="CommonHeader" runat="server" />
            <div class="row">
                <!-- left Navigation -->
                <LMS:Menu ID="Menu" runat="server" />
                <!-- Page content -->
                <div class="col-md-10" id="pageHeader">
                    <!-- Header Title -->
                    <div class="pageheaderbar">
                        <div class="row">
                            <div class="col-md-8 pageHeaderTitle">
                                Assessment
     
                       
                            </div>
                            <div class="col-md-4 pageHeaderDesc">
                                Assessment - <span id="spanHeader">Overview</span>
                            </div>
                        </div>
                    </div>
                    <div class="row pageBody">
                        <!-- Activity -->
                        <div class="col-md-12">
                            <div class="row" id="listofusers">
                                <div class="col-md-12 cardCustom">
                                    <div class="row listofusers">
                                        <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                            <ul class="guserlistleft">
                                                <li class="activitytxtCourse1">
                                                    <h1 class="active">List of Assessment</h1>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_AssessmentList">
                                            <LMS:AssessmentList ID="AssessmentList1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- dfdsfsdf -->
                        </div>
                    </div>
                </div>
                <!-- End Page content -->
            </div>
        </div>
        <LMS:CommonScripts ID="CommonScripts" runat="server" />
        <script src="scripts/custom/Assessment.js"></script>

        <script src="js/jquery.form.js"></script>
        <script type="text/javascript">
            function showAlert(msg, type) {
                $("#resourceAlert").text(msg);
                $("#resourceAlert").show();
            }

            function hideAlert() {
                $("#resourceAlert").hide();
            }


    </script>
    </form>
</body>
</html>
