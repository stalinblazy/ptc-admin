﻿using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Assessment : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    //UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
        //    //user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        //}
        //catch (UnauthorizedAccessException)
        //{
        //    Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
        //    return;
        //}
    }

    [WebMethod]
    public static string GetAssessmentList(string fromdate, string enddate)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.Assessment.GetAssessmentList(fromdate, enddate);
            var objData = DataAccessManager.DataTableToJSON(dataTable);
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }
}