﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssessmentList.ascx.cs" Inherits="assessment" %>
<div class="table-top-action">
    <div class="bulkActionbtn">
        <a href="AddAssessment.aspx" id="btnFlashCard" style="text-decoration: none">
            <button type="button" class="createUser" id="userbtn"><i class='fa fa-plus'></i>&nbsp; Create Assessment </button>
        </a>
    </div>
    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
    <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
</div>
<!--table starts-->
<table id="pageAssessment" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 200px;">ASSESSMENT TITLE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">NO. OF QUESTIONS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 90px;">CREATE DATE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>

        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec"></span>
                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data" class="page-numb">1 of 1</span>
                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
<!--table ends-->
