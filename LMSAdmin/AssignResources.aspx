﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssignResources.aspx.cs" Inherits="AssignResources" %>

<%@ Register TagPrefix="LMS" TagName="ResourceList" Src="ResourceList.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assign Resources Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Course Manager
     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Course Manager - <span id="spanHeader">Overview - Assign Resources</span>
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 cardCustom">
                                <div class="row aumlistofusers">
                                    <div class="col-md-12" style="padding-left: 0px;">
                                        <ul class="aumUserGroup">
                                            <li>
                                                <h1>Assign Resources - <span id="lblModuleName"></span></h1>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_ResourceList">
                                        <LMS:ResourceList ID="ResourceList1" runat="server" />
                                    </div>
                                </div>

                                <!--Select Users Bottom -->
                                <div class="aumselectUser" style="display:none">
                                    <p>Selected users are:</p>
                                    <ul id="selectedCourseUsers">
                                    </ul>
                                </div>
                                <!--End Select Users Bottom-->
                                <!--table ends-->

                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-3 aumuserBut1">
                                        <a href="CourseManagement.aspx"><span class="fas fa-arrow-left aumuserIcon"></span>&nbsp;&nbsp;&nbsp;Back to course</a>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn text-white aumuserBut2" id="btnAssignCourseResource" href="#" role="button">ASSIGN RESOURCES</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>


                <%--<div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="rmlistofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row rmlistofusers">
                                    <div class="col-md-6 rmleftlist" style="padding-left: 0px;">
                                        <ul class="rmuserlistleft">
                                            <li>
                                                <h1 class="active">List of Resources</h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_ResourceList">
                                        <LMS:ResourceList ID="ResourceList1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>--%>
            </div>
            <!-- End Page content -->
        </div>
    </div>

    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/custom/AssignResources.js"></script>
</body>
</html>
