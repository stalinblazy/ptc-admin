﻿using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AssignResources : System.Web.UI.Page
{
    protected static int user_Id = 0;
    protected static UserType defined = UserType.Admin;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
    }

    [WebMethod]
    public static string GetAssignResourceData(string fromDate = "", string endDate = "", string courseId = "0")
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = Resource.GetAssignResource(fromDate, endDate, Convert.ToInt64(courseId));

            var objData = DataAccessManager.DataTableToJSON(dataTable);

            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    [WebMethod]
    public static string BullkAssignResources(string courseId, List<Resource> resourceIds, string activeResourceId)
    {
        string res = "";
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        bool auditDetails = false;
        try
        {
            Resource.UnAssignResourcetoCourse(Convert.ToInt64(0), Convert.ToInt64(courseId),user_Id);
            if (resourceIds != null && resourceIds.Count > 0)
            {
                foreach (var item in resourceIds)
                {
                    Resource.AssignResourcetoCourse(Convert.ToInt64(item.ResourceID), Convert.ToInt64(courseId), item.IsRequired,user_Id);
                }
             //   auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Assigned the Resource to Course :" + courseId+"", "INSERT Operation", user_Id);
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }

        return res;
    }
}