﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssignUsers.aspx.cs" Inherits="AssignUsers" %>

<%@ Register TagPrefix="LMS" TagName="CourseAssignUsers" Src="CourseAssignUsers.ascx" %>
<%@ Register TagPrefix="LMS" TagName="CourseAssignGroup" Src="CourseAssignGroup.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assign Users Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
    <link rel="stylesheet" href="js/datetime/bootstrap-datetimepicker.css" />
</head>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Course Manager
     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Course Manager - <span id="spanHeader">Overview - Assign Users</span>
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 cardCustom">
                                <div class="row aumlistofusers">
                                    <div class="col-md-12" style="padding-left: 0px;">
                                        <ul class="aumUserGroup">
                                            <li>
                                                <h1>Assign Users - <span id="lblModuleName"></span></h1>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="table-top-action">
                                    <div class="aumRadioList">
                                        <ul>
                                            <li><%--<a href="#tab_UserList" data-toggle="tab" class="text-darkgray">--%>
                                                <input type="radio" onchange="onchangeTab()" name="gender" id="userRadio" value="Users" checked /><label style="padding-left: 5px;" for="userRadio">Users</label>
                                                <%--</a>--%>
                                            </li>
                                            <li><%--<a href="#tab_GroupList" data-toggle="tab" class="text-darkgray">--%>
                                                <input type="radio" onchange="onchangeTab()" name="gender" id="groupRadio" value="Groups" /><label style="padding-left: 5px;" for="groupRadio"> Groups</label>
                                                <%--</a>--%></li>
                                        </ul>
                                    </div>
                                    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
                                    <span style="display: none;" id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; May 24 - Jun 02</span>
                                </div>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_UserList">
                                        <LMS:CourseAssignUsers ID="CourseAssignUsers1" runat="server" />
                                    </div>
                                    <div class="tab-pane" id="tab_GroupList">
                                        <LMS:CourseAssignGroup ID="CourseAssignGroup1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>
            <!-- End Page content -->
        </div>
    </div>

    <div class="modal fade" id="setDateModal" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit due date for <span id="selEmail"></span></h5>
                </div>
                <div class="modal-body f-12">
                    <input type="hidden" id="selUserId" />
                    <input type="hidden" id="selCourseId" />
                    <input type="hidden" id="selrow" />
                    
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="dateReq" value="true" />
                            <label class="form-check-label" for="dateReq">Not Required</label>
                        </div>

                    </div>
                    <div class="form-group input-group-sm">
                        <label>Select Date</label>
                        <input type="text" id="selDueDate" class="form-control datepicker" />
                    </div>
                    <div>
                       
                            <p class="mb-1"> 1. If due date is not applicable for a user, check the "Not Required" box and click Save changes.</p>
                            <p class="mb-1"> 2. To assign the course due date, leave the due date field empty. </p>
                            <p class="mb-1"> 3. When a due date has been set, uncheck the "Not Required" box.</p>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-custom dudabtn" onclick="onSaveDueDate()">Save changes</button>
                    <button type="button" class="btn btn-sm btn-light dudabtn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <LMS:CommonScripts ID="CommonScripts" runat="server" />
     <script src="js/adminservice.js"></script>
    <script src="js/datetime/bootstrap-datetimepicker.min.js"></script>
    <script src="scripts/custom/CourseAssignUsers.js?v1"></script>
</body>
</html>
