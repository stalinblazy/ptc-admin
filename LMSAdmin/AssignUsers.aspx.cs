﻿using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AssignUsers : System.Web.UI.Page
{
    protected static int user_Id = 0;
    protected static UserType defined = UserType.Admin;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }

    }

    #region CourseUser

    [WebMethod]
    public static string BindUnAssignedCourseUserList(int courseId)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.User.GetUnAssignedCourseUserList(courseId);

            var objData = DataAccessManager.DataTableToJSON(dataTable);


            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    #endregion

    #region Assign User
    [WebMethod]
    public static string BullkAsignUser(string userIds, int courseId, string activeUserIds, string courseName, string newUsers)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        bool auditDetails = false;
        string res = "";
        try
        {
            List<string> userId = null;
            List<string> activeUserId = null;
            List<string> newUser = new List<string>();
            if (!string.IsNullOrWhiteSpace(userIds))
                userId = userIds.Split(',').ToArray().ToList();
            if (!string.IsNullOrWhiteSpace(activeUserIds))
                activeUserId = activeUserIds.Split(',').ToArray().ToList();
            if (newUsers != null && newUsers != "")
            {
                newUser = newUsers.Split(',').ToArray().ToList();
            }
            if (activeUserId != null && activeUserId.Count > 0)
            {
                foreach (string item in activeUserId)
                {
                    int ID = Convert.ToInt32(item);
                    if (ID > 0)
                    {
                        DeleteUserRecord(ID, courseId);
                    }
                    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin deleted User from Assign User page User Id's" + ID + " and Course Id's" + courseId + "", "Bulk Operation Delete", user_Id);

                }

               
            }
            if (newUser != null && newUser.Count > 0)
            {
                foreach (string item in newUser)
                {

                    int ID = Convert.ToInt32(item);
                    if (ID > 0)
                    {
                        bool isCourseAssigned = false;
                        if (courseId <= 3)
                        {
                            for (var i = 1; i <= 3; i++)
                            {
                                isCourseAssigned = UpdateUserActive(ID, i);
                            }
                        }
                        else
                        {
                            isCourseAssigned = UpdateUserActive(ID, courseId);
                        }
                        auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Assigned course Id :"+courseId+" to users from Assign User page User Id's" + ID + " ", "Bulk Operation Insert", user_Id);
                        if (isCourseAssigned)
                        {
                            foreach (string data in newUser)
                            {
                                int newuserID = Convert.ToInt32(data);
                                if (isCourseAssigned && ID == newuserID)
                                {
                                    bool mailSent = false;
                                    String eMailId = String.Empty;
                                    if (!mailSent)
                                    {
                                        try
                                        {
                                            if (newUser != null && newUser.Count > 0)
                                            {
                                                if (newuserID > 0)
                                                {
                                                    LearningManagementSystem.Components.User user = LearningManagementSystem.Components.User.GetUser(Convert.ToInt64(newuserID));
                                                    string firstName = user.FirstName.ToString();
                                                    string lastName = user.LastName.ToString();
                                                    eMailId = user.EMailID.ToString();
                                                    LearningManagementSystem.Components.SubCourse.SendCourseLaunchEmail(firstName, lastName, eMailId, courseId.ToString(), courseName);
                                                    try
                                                    {
                                                        DataSet Id = null;
                                                        var notification = "<strong>" + courseName + "</strong> has been assigned";
                                                        List<SqlParameter> parameters = new List<SqlParameter>();
                                                        parameters.Add(dataManager.CreateParam("@notification", notification, ParameterDirection.Input));
                                                        parameters.Add(dataManager.CreateParam("@notificationid", 0, ParameterDirection.Output));
                                                        Id = dataManager.Execute("SP_LMS_ADD_NOTIFICATION", parameters.ToArray());
                                                        int notificationid = Convert.ToInt32(Id.Tables[0].Rows[0]["notificationid"]);
                                                        if (notificationid > 0)
                                                        {
                                                            DataAccessManager dataManager1 = DataAccessManager.GetInstance();
                                                            List<SqlParameter> parameters1 = new List<SqlParameter>();
                                                            parameters1.Add(dataManager1.CreateParam("@userid", newuserID, ParameterDirection.Input));
                                                            parameters1.Add(dataManager1.CreateParam("@notificationid", notificationid, ParameterDirection.Input));
                                                            dataManager1.ExecuteNonQuery("SP_LMS_ADD_UserNOTIFICATION", parameters1.ToArray());
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Medtrix.Trace.Logger.Log(ex.Message.ToString() + ex.StackTrace.ToString());
                                                    }
                                                    //LearningManagementSystem.Components.Course.SendCourseAssignMail(eMailId, firstName +" "+ lastName, courseName);
                                                    mailSent = true;
                                                }
                                            }


                                        }
                                        catch (Exception mail)
                                        {
                                            Medtrix.Trace.Logger.Log(mail.Message.ToString());
                                        }

                                    }
                                }
                            }
                        }

                    }

                }
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }

        return res;
    }


    private static bool DeleteUserRecord(int userId, int courseId)
    {
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", userId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", courseId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("RemoveCourseToUser", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
    }


    [WebMethod]
    public static bool UpdateUserActive(int userId, int courseId)
    {

        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseID", Convert.ToInt32(courseId), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", Convert.ToInt32(userId), ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("AssignCourseToUser", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }

    }

    #endregion

    #region CourseGroup

    [WebMethod]
    public static string BindUnAssignedCourseGroupList(string fromDate, string endDate, int courseId)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.User.GetUnAssignedCourseGroupList(fromDate, endDate, courseId);

            var objData = DataAccessManager.DataTableToJSON(dataTable);


            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    #endregion

    #region Assign Group
    [WebMethod]
    public static string BullkAsignGroup(string groupIds, int courseId, string activeGroupIds)
    {
        string res = "";
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        try
        {
            List<string> groupId = null;
            List<string> activeGroupId = null;
            if (!string.IsNullOrWhiteSpace(groupIds))
                groupId = groupIds.Split(',').ToArray().ToList();
            if (!string.IsNullOrWhiteSpace(activeGroupIds))
                activeGroupId = activeGroupIds.Split(',').ToArray().ToList();

            if (activeGroupId != null && activeGroupId.Count > 0)
            {
                foreach (string item in activeGroupId)
                {
                    int ID = Convert.ToInt32(item);
                    if (ID > 0)
                    {
                        DeleteGroupRecord(ID, courseId);
                    }

                }
                auditDetails = dataManager.ExecuteAuditTrailDetails("Admin deleted Group from Assign page User Id's" + activeGroupIds + " and group Id's" + groupIds + " and Course Id's"+courseId+"", "Bulk Operation Delete", user_Id);
            }
            if (groupId != null && groupId.Count > 0)
            {
                foreach (string item in groupId)
                {
                    int ID = Convert.ToInt32(item);
                    if (ID > 0 && !LearningManagementSystem.Components.Group.checkCourseInGroup(ID, courseId))
                    {
                        var result = UpdateGroupActive(ID, courseId);
                    }

                }
            }
            auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Assigned a course to a Group from Assign page group Id's" + groupIds + " and Course Id's" + courseId + "", "Bulk Operation Insert", user_Id);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }

        return res;
    }


    private static bool DeleteGroupRecord(int groupId, int courseId)
    {
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@groupId", groupId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", courseId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("RemoveCourseFromGroup", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
    }


    [WebMethod]
    public static bool UpdateGroupActive(int groupId, int courseId)
    {

        try
        {
            if (courseId >= 1 && courseId <= 3)
            {
                for (var i = 1; i <= 3; i++)
                {
                    String referenceCode = Guid.NewGuid().ToString();
                    DataAccessManager dataManager = DataAccessManager.GetInstance();
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(dataManager.CreateParam("@CourseID", Convert.ToInt32(i), ParameterDirection.Input));
                    parameters.Add(dataManager.CreateParam("@GroupID", Convert.ToInt32(groupId), ParameterDirection.Input));
                    dataManager.ExecuteNonQuery("SP_LMS_ASSIGN_COURSE_TO_GROUP", parameters.ToArray());
                }
            }
            else
            {
                string var = "INSERT INTO CourseUserMapping (Course_Id, User_Id, Active, Assign_Date, Completion_Date, User_Course_Status, LMSGroupID, DueDate ) Select CG.Course_Id,U.User_Id,1, GETDATE(), NULL, 1,groupId,DateAdd( day, cor.DueIndays , Cast( GetDate() as Datetime ) ) from [dbo].[CourseGroupMapping] CG Left Join Users U on U.LMSGroupID = CG.Group_Id Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=U.User_Id Left Join Courses cor ON cor.Course_ID=cm.course_id Where CG.Group_Id = groupIdAND CM.Course_User_Id IS NULL ";
                String referenceCode = Guid.NewGuid().ToString();
                DataAccessManager dataManager = DataAccessManager.GetInstance();
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(dataManager.CreateParam("@CourseID", Convert.ToInt32(courseId), ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@GroupID", Convert.ToInt32(groupId), ParameterDirection.Input));
                dataManager.ExecuteNonQuery("SP_LMS_ASSIGN_COURSE_TO_GROUP", parameters.ToArray());
            }
            return LearningManagementSystem.Components.SubCourse.SendCoursetoGroupEmail(groupId, courseId);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }

    }

    #endregion

}