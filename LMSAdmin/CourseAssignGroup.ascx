﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseAssignGroup.ascx.cs" Inherits="CourseAssignGroup" %>

<table id="pageGroups" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 29px; padding: 0px 13px !important;">
                <div class="checkbox">
                    <input type="checkbox" id="chkGroupsList" class="dt-checkboxes" /><label></label>
                </div>
            </th>
            <th style="width:150px;">GROUP NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width:150px;">NO OF USERS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width:150px;">DATE CREATED<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width:300px;">MODULES ASSIGNED<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec1"></span>
                <button id="firstBtn1" class="pagg-btn btn group-pagg-btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn1" class="pagg-btn btn group-pagg-btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data1" class="page-numb">1 of 1</span>
                <button id="nextBtn1" class=" pagg-btn btn group-pagg-btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn1" class=" pagg-btn btn group-pagg-btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>

<!--Select Users Bottom -->
<div class="aumselectUser">
    <p>Selected Groups are:</p>
    <ul id="selectedCourseGroups">       
    </ul>
</div>
<!--End Select Users Bottom-->
<!--table ends-->

<div class="row" style="margin-bottom: 10px;">
    <div class="col-md-6"></div>
    <div class="col-md-3 aumuserBut1">
        <a href="CourseManagement.aspx"><span class="fas fa-arrow-left aumuserIcon"></span>&nbsp;&nbsp;&nbsp;Back to course</a>
    </div>
    <div class="col-md-3">
        <a class="btn text-white aumgroupBut2" href="#" id="btnAssignCourseGroup" role="button">ASSIGN GROUPS</a>
    </div>
</div>