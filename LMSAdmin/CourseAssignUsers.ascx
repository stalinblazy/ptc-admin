﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseAssignUsers.ascx.cs" Inherits="CourseAssignUsers" %>

<table id="pageUsers" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 29px; padding: 0px 13px !important;">
                <%-- <div class="checkbox">
                    <input type="checkbox" id="chkUsersList" class="dt-checkboxes" /><label></label>
                </div>--%>
            </th>
            <th>First Name<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>LAST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>EMAIL ADDRESS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>DESIGNATION<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>DEPARTMENT<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>MANAGER<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="min-width:100px;">Due Date<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec"></span>
                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data" class="page-numb">1 of 1</span>
                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>

<!--Select Users Bottom -->
<div class="aumselectUser">
    <p>Selected users are:</p>
    <ul id="selectedCourseUsers">       
    </ul>
</div>
<!--End Select Users Bottom-->
<!--table ends-->

<div class="row" style="margin-bottom: 10px;">
    <div class="col-md-6"></div>
    <div class="col-md-3 aumuserBut1">
        <a href="CourseManagement.aspx"><span class="fas fa-arrow-left aumuserIcon"></span>&nbsp;&nbsp;&nbsp;Back to course</a>
    </div>
    <div class="col-md-3">
        <a class="btn text-white aumuserBut2" id="btnAssignCourseUser" href="#" role="button">ASSIGN USERS</a>
    </div>
</div>
