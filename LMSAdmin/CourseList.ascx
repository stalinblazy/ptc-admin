﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseList.ascx.cs" Inherits="CourseList" %>


<div class="table-top-action">
    <div class="bulkActionbtn">
        <select class="browser-default custom-select" id="ddlbulk">
            <option disabled selected="selected">Select Action</option>
            <option value="0"> Inactive</option>
            <option value="1"> Active</option>
            <option value="2"> Delete</option>
        </select>
        <button type="submit" class="btn btn-light applyBtn" id="btnApply">Apply</button>
         <a href="AddCourse.aspx" id="btnCreateCourse">
            <button type="button" class="createUser" id="userbtn"><i class='far fa-calendar-check'></i>&nbsp; Create Course </button>
        </a>
    </div>
    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
    <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
</div>
<!--table starts-->
<table id="pageCourse" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 29px; padding: 0px 13px !important;">
                <div class="checkbox">
                    <input type="checkbox" id="chkCourseList" class="dt-checkboxes" /><label></label>
                </div>
            </th>
            <th style="width: 40px;">Id<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 165px;">Course Name<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 228px;">Description<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 120px;">Date Created<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 80px;">Status<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 120px;"></th>
            <th style="width: 110px;"></th>
        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec"></span>
                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data" class="page-numb">1 of 1</span>
                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
<!--table ends-->
