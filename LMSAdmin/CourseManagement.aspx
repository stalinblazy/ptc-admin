﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CourseManagement.aspx.cs" Inherits="CourseManagement" %>

<%@ Register TagPrefix="LMS" TagName="CourseList" Src="CourseList.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Course Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
     <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                          Course Manager
     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                           Course Manager - <span id="spanHeader">Overview</span>     
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="listofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row listofusers">
                                    <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                        <ul class="guserlistleft">
                                            <li class="activitytxtCourse1"> 
                                                <h1 class="active" >List of Courses</h1>
                                            </li>   
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_CourseList">
                                        <LMS:CourseList ID="CourseList1" runat="server" /> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>
            <!-- End Page content -->
        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/custom/CourseManagement.js"></script>
</body>
</html>
