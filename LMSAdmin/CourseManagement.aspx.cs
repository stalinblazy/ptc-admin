﻿using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CourseManagement : System.Web.UI.Page
{
    protected static int user_Id = 0;
    protected static UserType defined = UserType.Admin;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }

    }

    #region Course

    [WebMethod]
    public static string BindCourseList(string fromdate, string enddate)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.Course.GetCourses(fromdate, enddate);

            var objData = DataAccessManager.DataTableToJSON(dataTable);

            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }
    #endregion

    #region CourseBullkDelete

    [WebMethod]
    public static string BullkCourseActions(string courseIds, string state)
    {
        string res = "";
        var result = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        bool auditDetails = false;
        try
        {
            List<string> courseId = courseIds.Split(',').ToArray().ToList();
            if (state == "2")
            {
                if (courseId != null && courseId.Count > 0)
                {
                    foreach (string item in courseId)
                    {
                        int ID = Convert.ToInt32(item);
                        if (ID > 0)
                        {
                            result = DeleteCourseRecord(ID);
                        }

                    }
                }
                if (result == true)
                {

                    res = "Course Deleted Successfully..";
                  //  auditDetails = dataManager.ExecuteAuditTrailDetails("Admin deleted Course from Course Management Course Id's" + courseIds + "", "Delete Operation", user_Id);
                }
                else
                {
                    res = "Failed to delete Course!!";
                }
            }
            else
            {
                if (courseId != null && courseId.Count > 0)
                {
                    foreach (string item in courseId)
                    {
                        int ID = Convert.ToInt32(item);
                        if (ID > 0)
                        {
                            result = UpdateCourseActive(ID, state);
                        }

                    }
                }
                if (result == true)
                {
                    string status;
                    if (state == "0")
                    {
                        status = "Inactive";
                    }
                    else
                    {
                        status = "Active";
                    }
                //    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin " + status + " the Course from Course Management Course Id's" + courseIds + "", "Update Operation", user_Id);
                    res = "Course Update SuccessFully..";
                }
                else
                {
                    res = "Failed to update Course!!";
                }
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            res = "Failed to update Course!!";
        }

        return res;
    }

    private static bool DeleteCourseRecord(int courseId)
    {
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        try
        {
            String referenceCode = Guid.NewGuid().ToString();

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseID", courseId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", user_Id, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_DELETE_COURSE", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
        finally
        {
          //  auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Delete the Course from Course Management Course Id's" + courseId + "", "Delete Operation", user_Id);

        }
    }

    [WebMethod]
    public static bool UpdateCourseActive(int courseId, string IsActive)
    {
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        try
        {
            String referenceCode = Guid.NewGuid().ToString();

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Active", IsActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", Convert.ToInt32(courseId), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", user_Id, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_Update_ActiveCOURSE", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
        finally
        {
         //   auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Changed Status to " + IsActive + " of the Course from Course Management Course Id's" + courseId + "", "Update Operation", user_Id);
        }


    }

    #endregion
}