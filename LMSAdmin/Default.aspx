﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="bg-dark">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>PTC - Admin</title>
    <meta name="description" content="PTC Admin" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <%--<link rel="stylesheet" href="css/app.v2.css" type="text/css" />--%>
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css" />

    <style>
        .aside-xxl{
            width:450px;
        }
        
        .m-t-lg {
    margin-top: 30px;
}

        .fadeInUp {
    -webkit-animation-name: fadeInUp;
    -moz-animation-name: fadeInUp;
    -o-animation-name: fadeInUp;
    animation-name: fadeInUp;
}
.animated {
    -webkit-animation-fill-mode: both;
    -moz-animation-fill-mode: both;
    -ms-animation-fill-mode: both;
    -o-animation-fill-mode: both;
    animation-fill-mode: both;
    -webkit-animation-duration: 0.5s;
    -moz-animation-duration: 0.5s;
    -ms-animation-duration: 0.5s;
    -o-animation-duration: 0.5s;
    animation-duration: 0.5s;
}

body {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
    background: url(images/Ptcdegree_admin_login_bg.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
    </style>
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>
<body>
    <section id="content" class="m-t-lg animated fadeInUp">
        <div class="container aside-xxl">
            <section class="panel panel-default bg-white m-t-lg">
                <header class="panel-heading text-center">
                    <img src="images/logo.png" style="width:72px" />
                </header>
                <form id="loginForm" runat="server" class="panel-body wrapper-lg">
                    <div runat="server" id="login">
                        <asp:Label ID="resetMsg" runat="server" Visible="false"></asp:Label>
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label class="control-label" for="TextUserName">Login Id</label>
                            <asp:TextBox runat="server" CssClass="form-control input-lg" placeholder="Login Id" ID="TextUserName" data-prompt-position="bottomRight:-100, -25" data-validation-engine="validate[required]"></asp:TextBox>
                        </div>
                        <br />
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label class="control-label" for="TextPassword">Password</label>
                            <asp:TextBox runat="server" CssClass="form-control input-lg" TextMode="Password" placeholder="Password" ID="TextPassword" data-prompt-position="bottomRight:-100, -25" data-validation-engine="validate[required]"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label Text="Invalid Credentials" runat="server" ForeColor="Red" Visible="false" ID="loginMsg"></asp:Label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="KeepMe" runat="server" />
                                Keep me logged in
                            </label>
                        </div>
                        <span style="cursor: pointer;" onclick="ShowMail();" class="pull-right m-t-xs"><small>Forgot Password ?</small></span>
                        <asp:Button ID="Button1" CssClass="btn" Style="font-weight:bold; background-color:#793c7f;color:#fff" OnClick="Login_Click" Text="Login" runat="server" />
                    </div>
                    <div runat="server" id="forgot" style="display:none">
                        
                        <div class="form-group">
                            <label class="control-label">Your Email ID</label>
                            <asp:TextBox runat="server" CssClass="form-control input-lg" placeholder="Your Email ID" ID="TextEmail" data-validation-engine="validate[required,custom[email]]"></asp:TextBox>
                        </div>
                        <div> <asp:Label ID="forgotMessage" runat="server" Visible="false"></asp:Label></div>
                        <span style="cursor: pointer;" onclick="LoginForm();" class="pull-right m-t-xs"><small>Login</small></span>
                        <asp:Button ID="SubmitEmailID" OnClick="SubmitEmailID_Click" runat="server" CssClass="btn" Style="font-weight:bold; background-color:#793c7f; color:#fff" Text="Submit"></asp:Button>
                    </div>

                </form>
            </section>
        </div>
    </section>
    <!-- / footer -->
    <script src="js/app.v2.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="js/jquery.validationEngine-en.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#loginForm").validationEngine();
        });

        function ShowMail() {
            document.getElementById('login').style.display = "none";
            document.getElementById('forgot').style.display = "block";
        }
        

        function LoginForm() {
            document.getElementById('login').style.display = "block";
            document.getElementById('forgot').style.display = "none";
        }
    </script>
</body>
</html>