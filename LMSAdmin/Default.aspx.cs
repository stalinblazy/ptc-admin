﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;


public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TextUserName.Focus();
            if (Request.QueryString["type"] == "logout")
            {
                Session.Abandon();
                HttpCookie sessionCookie = Request.Cookies["val"];

                if (sessionCookie != null)
                {
                    sessionCookie.Expires = DateTime.Now.AddDays(-14);

                    Response.Cookies.Add(sessionCookie);
                }
                return;
            }
            UserSession us = Session["User"] as UserSession;

            if (us == null)
            {
                HttpCookie val = new HttpCookie("val");
                val = Request.Cookies["val"];
                if (val != null && !string.IsNullOrEmpty(val.Value))
                {
                    string[] CookieValues = val.Value.Split(',');
                    us = new UserSession();
                    us.UserID = Convert.ToUInt64(CookieValues[1]);
                    us.UserType = (UserType)Enum.Parse(typeof(UserType), CookieValues[2], true);
                    Session["User"] = us;
                    Response.Redirect("dashboard.aspx");
                }
                return;
            }
            Response.Redirect("dashboard.aspx");
        }
    }

    protected void Login_Click(object sender, EventArgs e)
    {
        String userName = TextUserName.Text.ToString();
        String passWord = TextPassword.Text.ToString();
        try
        {
            User user = LearningManagementSystem.Components.User.Authenticate(userName, passWord);
            UserSession us = new UserSession();
            us.UserID = Convert.ToUInt64(user.UserID);
            us.UserType = (UserType)user.Role;

            if (us.UserType == UserType.Admin || us.UserType == UserType.UserViewer)
            {
                if (KeepMe.Checked)
                {
                    HttpCookie val = new HttpCookie("val", TextUserName.Text + "," + us.UserID.ToString() + "," + us.UserType.ToString());
                    val.Expires = DateTime.Now.AddDays(14);
                    Response.Cookies.Add(val);
                }

                Session["User"] = us;
                Response.Cookies.Add(new HttpCookie("AdminUserName", user.EMailID));
                Response.Redirect("dashboard.aspx");
            }

            else
            {
                loginMsg.Text = "Access Denied.";
                loginMsg.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            loginMsg.Visible = true;
        }
    }

    protected void SubmitEmailID_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = LearningManagementSystem.Components.User.GetUserByMailId(TextEmail.Text.ToString());
            if (dt.Rows.Count > 0 && dt.Rows[0]["Role"].ToString() == "1")
            {
                String body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/templates/adminpassword.html"));

                body = body.Replace("#YEAR#", DateTime.Now.Year.ToString());
                body = body.Replace("#PASSWORD#", dt.Rows[0]["Password"].ToString());
               
                String senderEmail = System.Configuration.ConfigurationManager.AppSettings["sender"];

                if (Medtrix.MailService.SendInBlue.SendEMail(dt.Rows[0]["Email_ID"].ToString(), senderEmail, "Admin Password - PTC Degree", body, null,false))
                {
                    forgotMessage.Text = "The password has been sent to your email.";
                    forgotMessage.ForeColor = System.Drawing.Color.Green;
                    forgotMessage.Visible = true;
                }
            } else
            {
                forgotMessage.Text = "Invalid Email.";
                forgotMessage.ForeColor = System.Drawing.Color.Red;
                forgotMessage.Visible = true;
            }
            login.Style.Add("display", "none");
            forgot.Style.Add("display", "block");

        }
       
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
        }
    }
}