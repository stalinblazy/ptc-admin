﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FlashCardList.ascx.cs" Inherits="FlashCardList" %>

<div class="table-top-action">
    <div class="bulkActionbtn">
        <a href="AddFlashCard.aspx" id="btnFlashCard" style="text-decoration:none">
            <button type="button" class="createUser" id="userbtn"><i class='fa fa-plus'></i>&nbsp; Create Card </button>
        </a>
        <a href="FlashCardQuestion.aspx" id="btnMergeFlashCard">
            <button type="button" class="createUser" id="userbtn"><i class='fa fa-plus'></i>&nbsp;  Create Merge Card </button>
        </a>
    </div>
    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
    <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
</div>
<!--table starts-->
<table id="pageFlashCard" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
       

           <%-- <th style="width: 200px;">CARD TITLE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>COMMENTS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">AGREE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>GOT ANSWER<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 50px;">NEW LEARN<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>No. Of Card<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">VIEWS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>DATE CREATED<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>--%>
            <th style="width: 200px;">CARD TITLE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">COMMENTS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">AGREE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">GOT ANSWER<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 50px;">NEW LEARN<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">No. Of Card<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 60px;">VIEWS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 90px;">Create Date<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>

        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec"></span>
                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data" class="page-numb">1 of 1</span>
                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
<!--table ends-->
