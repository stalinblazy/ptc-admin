﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlashCardQuestion.aspx.cs" Inherits="FlashCardQuestion" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flash Card Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
    
        <%-- <div class="loader">
        <img src="images/loaderp.svg" />
    </div>--%>
        <div id="app" class="container-fluid">
            <!-- header -->
            <LMS:CommonHeader ID="CommonHeader" runat="server" />
            <div class="row">
                <!-- left Navigation -->
                <LMS:Menu ID="Menu" runat="server" />
                <!-- Page content -->
                <div class="col-md-10" id="pageHeader">
                    <!-- Header Title -->
                    <div class="pageheaderbar">
                        <div class="row">
                            <div class="col-md-8 pageHeaderTitle">
                                Flash Card Manager                      
                            </div>
                            <div class="col-md-4 pageHeaderDesc">
                                Flash Card - <span id="spanHeader">Overview</span>
                            </div>
                        </div>
                    </div>
                    <div class="row pageBody">
                        <!-- Activity -->
                        <div class="col-md-12">
                            <div class="row" id="listofusers">
                                <div class="col-md-12 cardCustom">
                                    <div class="row listofusers">
                                        <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                            <ul class="guserlistleft">
                                                <li class="activitytxtCourseFlashQst">
                                                    <h1 class="active">List of Flash Card Questions</h1>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_CourseList">
                                            <div class="table-top-action">
                                                <div class="bulkActionbtn">
                                                    <input type="hidden" id="hidQstid" />
                                                    <button type="button" class="createUser btnFlashCard" id="btnFlashCard"><i class='fa fa-plus'></i>&nbsp; Merge FlashCard </button>
                                                     <a href="flashCard.aspx" id="btnMergeFlashCard">
            <button type="button" class="createUser" id="userbtn">&nbsp;  Back to Flash Card </button>
        </a>
                                                    
                                                    <%--<asp:LinkButton ID="btnFlashCard" runat="server" Text="Merge FlashCard" CssClass="createUser" OnClick="btnFlashCard_Click" />--%>
                                           <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true" ></asp:Label>
                                                    
                                                      <%--<asp:Button ID="btnFlashCard" runat="server" Text="Merge FlashCard" CssClass="createUser" OnClick="btnFlashCard_Click" />--%>

                                                    <%--<asp:Button ID="btnFlashCard" runat="server" CssClass='far fa-calendar-check' class="createUser" Text="Merge Flash Card" />--%>
                                                    <%--<a href="AddFlashCard.aspx" id="btnFlashCard">
            <button type="button" class="createUser" id="userbtn"><i class='far fa-calendar-check'></i>&nbsp; Create Flash Card </button>
        </a>--%>
                                                </div>
                                                <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
                                               <%-- <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>--%>
                                            </div>
                                            <!--table starts-->
                                            <table id="pageFlashCardQuestion" class="" style="width: 100%">
                                                <thead class="tableHeader">
                                                    <tr>
                                                        <th style="width: 29px; padding: 0px 13px !important;">
                                                            <div class="checkbox">
                                                                <input type="checkbox" id="chkCourseList" class="dt-checkboxes" /><label></label>
                                                            </div>
                                                        </th>
                                                        <th style="width: 120px;">Questions</th>
                                                        <th style="width: 120px;">Answers</th>
                                                        <th style="width: 120px;">Active</th>

                                                    </tr>
                                                </thead>
                                                <tbody class="tableBody">
                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>
                                            <div class="custom-pagination">
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-6">
                                                    </div>
                                                    <div class="col-md-6 col-lg-6">
                                                        <div class="datatable-pagination pull-right">
                                                            <span id="total-rec"></span>
                                                            <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                                                            <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                                                            <span id="page-data" class="page-numb">1 of 1</span>
                                                            <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                                                            <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- dfdsfsdf -->
                        </div>
                    </div>
                </div>
                <!-- End Page content -->
            </div>
        </div>
        <LMS:CommonScripts ID="CommonScripts" runat="server" />
        <script src="scripts/custom/FlashCardQuestion.js"></script>
     

</body>
</html>
