﻿using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FlashCardQuestion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetFlashcardQuestion(string fromdate, string enddate)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.FlashCard.GetFlashCardQuestion(fromdate, enddate);
            var objData = DataAccessManager.DataTableToJSON(dataTable);
            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }



    //protected void btnFlashCard_Click(object sender, EventArgs e)
    //{
    //        Response.Redirect("FlashCardQuestionMigration.aspx?FlashCardQuestionId=" + hidQstid.Value);
    //}
}