﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlashCardQuestionMigration.aspx.cs" Inherits="FlashCardQuestionMigration" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" translate="no">
<head runat="server">
    <title>Merge Flash Card</title>
      <LMS:CommonStyles ID="CommonStyles" runat="server" />
    <meta charset="UTF-8" />
    <meta name="google" content="notranslate">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="js/croppie/croppie.css" />
    <link rel="stylesheet" href="css/styledev.css" />
    <link rel="stylesheet" href="css/custom-dev.css" />
    <style type="text/css">
        .upload-demo .upload-demo-wrap,
        .upload-demo .upload-result,
        .upload-demo.ready .upload-msg {
            display: none;
        }

        .upload-demo.ready .upload-demo-wrap {
            display: block;
        }

        .upload-demo.ready .upload-result {
            display: inline-block;
        }

        .upload-demo-wrap {
            width: 750px;
            height: 450px;
            margin: 0 auto;
        }

        .selectFile {
            background-color: #f7f7f7;
            font-size: 12px !important;
            color: #29295b !important;
            box-shadow: 0 3px 6px 0 rgba(0,0,0,0.2);
        }
    </style>
</head>

<body>
    <div class="loader" id="loader" runat="server" style="display: none">
        <img src="images/loaderp.svg" />
    </div>

    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Flash Card 
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Flash Card   - Add FlashCard
                        </div>
                    </div>
                </div>

                <div class="row" id="pageBody">
                    <p></p>
                    <div class="col-md-12">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxtResource">
                                    <h1>Flash Card Details</h1>
                                </div>

                                <!--forms starts here-->
                                <form id="form1" name="myForm" runat="server" enctype="multipart/form-data" method="post">
                                    <asp:HiddenField ID="hidResourceSelectedType" runat="server" />
                                    <asp:HiddenField runat="server" ID="users" />
                                    <asp:HiddenField ID="hidresourceURL" runat="server" />
                                    <asp:HiddenField runat="server" ID="rsList" />
                                    <asp:HiddenField ID="hdnResourceID" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnResourecData" runat="server" Value="0" />
                                    <div class="userForm">
                                        <!-- first row starts here-->
                                         <div class="row">
                                            <div class="col-sm-8 formTxt">
                                                <label>Resource Type</label>
                                                <asp:DropDownList ID="ResourceList" onclick="dynamicCtrlValidation('ResourceList')" runat="server" class="form-control form-control-lg formTxtopt js-states">
                                                    <asp:ListItem Value="0">
                                                        --Select Track--</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="lblResourceName" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please select resource name</span>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-8 formTxt">
                                                <label>Card Title</label>
                                                <asp:TextBox runat="server" ID="FlashCardName" class="form-control" onchange="dynamicCtrlValidation()" data-prompt-position="bottomRight:-100,3"  onkeypress="myFunction()"></asp:TextBox>
                                                <span id="lblCardName" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please enter flash card name</span>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <!-- first row ends here-->
                                        <div class="row">
                                            <div class="col-sm-3 formTxt">
                                                <label>Choose Track</label>
                                                <asp:DropDownList ID="ResourceTrack" onclick="dynamicCtrlValidation('ResourceTrack')" AutoPostBack="true" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Track--</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="lblflashtrack" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please select track</span>
                                            </div>
                                            <div class="col-sm-3 formTxt">
                                                <label>Choose Curriculum</label>
                                                <asp:DropDownList ID="FlashCurriculum" onclick="dynamicCtrlValidation('FlashCurriculum')" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Curriculum--</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="lblcurriculam" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please select curriculum</span>
                                            </div>
                                            <%--  <div class="col-sm-3 formTxt">
                                                <label>Choose Course</label>
                                                <asp:DropDownList ID="FlashCourse"  runat="server" onclick="dynamicCtrlValidation('FlashCourse')" onchange="dynamicCtrlValidation('FlashCourse')" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Course</asp:ListItem>
                                                </asp:DropDownList>
                                                <span id="lblflashcourse" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px"">Please select course</span>
                                            </div>--%>
                                        </div>
                                        <div id="TextBoxContainer">
                                            <!--Textboxes will be added here -->
                                        </div>
                                        <%--<input id="btnAdd" type="button" value="add" onclick="AddTextBox()" />--%>
                                        <div class="row userBut">
                                            <div class="col-md-6"></div>
                                            <div class="col-md-3 userBut1">
                                                <a href="FlashCardQuestion.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to Cards</a>

                                                <asp:Label ID="lblMesg" Text="" runat="server" />
                                            </div>

                                            <div class="col-md-3">
                                                <%--   <asp:Button Text="Add User" class="btn text-white userBut2"  runat="server" ID="btnAdd" />--%>
                                                <%--<asp:Button Text="Add User" class="btn text-white userBut2" OnClientClick="this.disabled = true; this.value = 'Submitting...';" UseSubmitBehavior="false" OnClick="Submit_Click" runat="server" ID="btnAdd" />--%>
                                                <asp:Button Text="Add Card" class="btn text-white userBut2"  runat="server" ID="btnAddFlash" OnClientClick="return dynamicCtrlbtnValidation();" OnClick="btnAddFlash_Click" />
                                            </div>
                                        </div>
                                        <!--back to user ends here-->
                                    </div>
                                    <%-- <asp:DropDownList ID="ResourceTrack" AutoPostBack="true" OnSelectedIndexChanged="ResourceTrack_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Track</asp:ListItem>
                                                </asp:DropDownList>
                                     <asp:DropDownList ID="ResourceCurriculum" AutoPostBack="true" OnSelectedIndexChanged="ResourceCurriculum_SelectedIndexChanged" runat="server" class="form-control form-control-lg formTxtopt">
                                                    <asp:ListItem Value="0">--Select Curriculum</asp:ListItem>
                                                </asp:DropDownList>--%>
                                </form>
                                <!--forms ends here-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/jquery.validate.js"></script>

    <script src="js/jquery.form.js"></script>
    <script type="text/javascript">
        function showAlert(msg, type) {
            $("#resourceAlert").text(msg);
            $("#resourceAlert").show();
        }

        function hideAlert() {
            $("#resourceAlert").hide();
        }


    </script>



    <style type="text/css">
        label.error {
            color: red;
        }

        label.error1 {
            color: red;
        }

        .resourcethumb {
            margin-top: -50px;
        }
    </style>
    <script type="text/javascript">
        function LabelTextChange() {
            document.getElementById("Label4").innerText = 'Question4';
        }


    </script>

    <style type="text/css">
        label.error {
            color: red;
        }

        label.error1 {
            color: red;
        }

        .resourcethumb {
            margin-top: -50px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ResourceList").select2({
                placeholder: "--Select Resource Type--",
                allowClear: true
            });
        });

        function LabelTextChange() {
            document.getElementById("Label4").innerText = 'Question4';
        }

        $(function () {
            $("#ResourceList").change(function () {
                var selectedVal = "";
                //  alert($('option:selected', this).val());
                selectedVal = $('option:selected', this).val();

                var fields = selectedVal.split('=');
                var val1 = fields[0];
                var val2 = fields[1];
                document.getElementById("hidResourceSelectedType").value = val1;
                document.getElementById("hidresourceURL").value = val2;
                // alert(document.getElementById("hidresourceURL").value);
            });
        });

        function dynamicCtrlbtnValidation() {
            var resourceValue = document.getElementById("ResourceList");
            var ctrlCardNameValue = document.getElementById("FlashCardName").value;
            var dropresourcevlaue = resourceValue.options[resourceValue.selectedIndex].value;
            if (dropresourcevlaue == 0) {
                document.getElementById("lblResourceName").style.display = "block";
                return false;
            }
            else {
                document.getElementById("lblResourceName").style.display = "none";
            }
            if (ctrlCardNameValue.trim() == null || ctrlCardNameValue.trim() == "") {
                document.getElementById("lblCardName").style.display = "block";
                return false;
            }
            else {
                document.getElementById("lblCardName").style.display = "none";
                this.value = $.trim(this.value);
            }
        }
        function myFunction() {
           // alert();
            var resourceValue = document.getElementById("ResourceList");
            var ctrlCardNameValue = document.getElementById("FlashCardName").value;
            var dropresourcevlaue = resourceValue.options[resourceValue.selectedIndex].value;
            if (dropresourcevlaue == 0) {
                document.getElementById("lblResourceName").style.display = "block";
                return false;
            }
            else {
                document.getElementById("lblResourceName").style.display = "none";
            }
            if (ctrlCardNameValue.trim() == null || ctrlCardNameValue.trim() == "") {
                document.getElementById("lblCardName").style.display = "block";
                return false;
            }
            else {
                document.getElementById("lblCardName").style.display = "none";
                this.value = $.trim(this.value);
            }
        }
        $("#FlashCardName").focusout(function () {
            var ctrlCardNameValue = document.getElementById("FlashCardName").value;
            if (ctrlCardNameValue.trim() == null || ctrlCardNameValue.trim() == "") {
                document.getElementById("lblCardName").style.display = "block";
                return false;
            }
            else {
                document.getElementById("lblCardName").style.display = "none";
                this.value = $.trim(this.value);
            }
        });
        $("#ResourceList").focusout(function () {
            alert();
            var resourceValue = document.getElementById("ResourceList");
            var dropresourcevlaue = resourceValue.options[resourceValue.selectedIndex].value;
            if (dropresourcevlaue == 0) {
                document.getElementById("lblResourceName").style.display = "block";
                return false;
            }
            else {
                document.getElementById("lblResourceName").style.display = "none";
            }
        });
    </script>
</body>

</html>
