﻿using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FlashCardQuestionMigration : System.Web.UI.Page
{

    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected string Values;
    SqlConnection conn;
    public string connS = ConfigurationManager.AppSettings["AppDB"];
    public string user_id = "";
    public static DataTable daObj = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //  BindData();
     
            try
            {
                UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
                user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
                string selectedQstID = Request.QueryString["FlashCardQuestionId"];
                BindData();
                GetSelectedFlashcardQuestion(selectedQstID);
             
            }
            catch (UnauthorizedAccessException)
            {
                Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
                return;
            }
        }
    }

    private void BindData()
    {
        //FlashCourse.DataSource = GetCourseList().Tables[0];
        //FlashCourse.DataValueField = "Course_Id";
        //FlashCourse.DataTextField = "Name";
        //FlashCourse.DataBind();
        //FlashCourse.Items.Insert(0, new ListItem("--Select Course--", "0"));

        ResourceTrack.DataSource = GetResourceTrackList().Tables[0];
        ResourceTrack.DataValueField = "TrackId";
        ResourceTrack.DataTextField = "TrackName";
        ResourceTrack.DataBind();
        ResourceTrack.Items.Insert(0, new ListItem("--Select Track--", "0"));
        ResourceTrack.SelectedIndex = ResourceTrack.Items.IndexOf(ResourceTrack.Items.FindByText("Global"));
        BindCurriculumDropdown(0);
        FlashCurriculum.SelectedIndex = FlashCurriculum.Items.IndexOf(FlashCurriculum.Items.FindByText("Global"));
        ResourceList.DataSource = GetResourceFlashCardListPDF().Tables[0];
        ResourceList.DataValueField = "Namedrp";
        ResourceList.DataTextField = "Name";
        ResourceList.DataBind();
        ResourceList.Items.Insert(0, new ListItem("--Select Resource Type--", "0"));
    }
    private void BindCurriculumDropdown(long id)
    {
        FlashCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        FlashCurriculum.DataValueField = "CurriculumId";
        FlashCurriculum.DataTextField = "CurriculumName";
        FlashCurriculum.DataBind();
        FlashCurriculum.Items.Insert(0, new ListItem("--Select  Curriculum--", "0"));
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }

    public DataSet GetResourceTrackList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceTrack", parameters.ToArray());
    }
    public DataSet GetCourseList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("SP_LMS_GET_COURSE_LIST", parameters.ToArray());
    }

    public DataSet GetResourceFlashCardListPDF()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetFlashCardResourcePDF", parameters.ToArray());
    }

    public DataSet GetResourceCurriculumList(long trackId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@TrackId", trackId, ParameterDirection.Input));
        return dataManager.Execute("GetTrackCurriculumByTrackId", parameters.ToArray());
    }
    public DataSet GetResourceCourseList(long curriculumId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@CurriculumId", curriculumId, ParameterDirection.Input));
        return dataManager.Execute("GetCurriculumCourseByCurriculumId", parameters.ToArray());
    }

    public static string GetSelectedFlashcardQuestion(string flashCardQuestionID)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            daObj = LearningManagementSystem.Components.FlashCard.GetFlashCardQuestionByID(flashCardQuestionID);
            var objData = DataAccessManager.DataTableToJSON(dataTable);
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    protected void btnAddFlash_Click(object sender, EventArgs e)
    {
        try
        {
            loader.Style.Add("display", "block");
            FlashCard fCard = new FlashCard();
            fCard.SelectedResourceTypeId = int.Parse(hidResourceSelectedType.Value);
            fCard.CardTitle = FlashCardName.Text.Trim();
            fCard.Type = 7;
            fCard.FlashCardTrack = (Int64)ResourceTrack.SelectedIndex;
            fCard.FlashCardCurriculam = FlashCurriculum.SelectedIndex;
            fCard.FlashCardCourse = 0;
            fCard.AdminId = user_Id;
            int falshCardID = LearningManagementSystem.Components.FlashCard.AddFlashCard(fCard);//calling addfalshSP
            string resourcePath = System.Configuration.ConfigurationManager.AppSettings["ResourcePath"];
            var listofQuestion = from Questionslst in daObj.AsEnumerable()
                                 select new
                                 {
                                     QstID = Questionslst.Field<int>("FlashCardQuestionID"),
                                     Question = Questionslst.Field<string>("Question"),
                                     Answer = Questionslst.Field<string>("Answer"),
                                     KnowMore = Questionslst.Field<string>("KnowMore"),
                                     File = Questionslst.Field<string>("FlashFile"),
                                 };

            foreach (var questionObj in listofQuestion)
            {
                List<FlashCard> flashListObj = new List<FlashCard>();
                FlashCard flashObj = new FlashCard();
                flashObj.FlashCardID = falshCardID;
                flashObj.Question = questionObj.Question;
                flashObj.Answer = questionObj.Answer;
                flashObj.KnowMore = questionObj.KnowMore;
                flashObj.File = questionObj.File;
                flashObj.FlashCardQuestionParentID = questionObj.QstID;
                flashListObj.Add(flashObj);
                LearningManagementSystem.Components.FlashCard.AddQuestions(flashListObj);
            }
            
            Response.Redirect("flashCard.aspx?IsSuccessflashCard=true", false);
        }
        catch (Exception ex)
        {

            Console.WriteLine(ex.StackTrace);
        }
    }
}