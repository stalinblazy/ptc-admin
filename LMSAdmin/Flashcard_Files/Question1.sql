


 /*Flash manager detail*/
 create table FlashManager(Flash_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,Card_Id int,FlashTrack_Id int,FlashCurriculum_Id int,FlashCourse_Id int,Question_Id int,Answer_Id int,KnowMore_Id int , File_Id int ,IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)

 /*Question details*/
  create table QuestionDetails(Question_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, Question Varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
  create table AnswerDetails(Answer_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, Answer Varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
  create table KnowDetails(KnowMore_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, KnowMore Varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
  create table FileDetails(File_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, FileName Varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)



 /*card details*/
  create table CardDetails(Card_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, CardTitle Varchar(200),FlashTrack_Id int ,FlashCourse_Id int, FlashCurriculum_Id int, IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
  
  /*Flash Tables*/
  create table FlashTrack(FlashTrack_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, FlashTrackName Varchar(200),FlashTrack varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
  create table FlashCurriculum(FlashCurriculum_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, FlashCurriculumName Varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
  create table FlashCourse(FlashCourse_Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, FlashCourseName Varchar(200), IsDeleted int ,Active int,CreatedBy varchar(200),ModifiedBy varchar(200), CreatedDate datetime, ModifiedDate datetime)
