﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupUserListDetail.ascx.cs" Inherits="GroupUserListDetail" %>

<div class="table-top-action">
    <div class="bulkActionbtn">
        <select class="browser-default custom-select" id="ddlbulk">
            <option selected="selected">Bulk Action</option>
            <option value="0">Bulk Inactive</option>
            <option value="1">Bulk Active</option>
            <option value="2">Bulk Delete</option>
        </select>
        <button type="submit" class="btn btn-light applyBtn" id="btnApply">Apply</button>
    </div>
    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
    <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
</div>

<!--table starts-->
<table id="pageUsers" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 29px; padding: 0px 13px !important;">
                <div class="checkbox">
                    <input type="checkbox" id="chkUsersList" class="dt-checkboxes" /><label></label>
                </div>
            </th>
            <th>First Name<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>LAST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>EMAIL ADDRESS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>DESIGNATION<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>DEPARTMENT<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>MANAGER<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec"></span>
                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data" class="page-numb">1 of 1</span>
                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div class="col-md-6"></div>
    <div class="col-md-3 guserBut1">
        <a href="UserManagement.aspx?IsFromGroupPage=true" id="backToUserPage"><span class="fas fa-arrow-left guserIcon"></span>&nbsp;&nbsp;&nbsp;Back to groups</a>
    </div>
    <div class="col-md-3">
        <a class="btn text-white guserBut2" href="#" id="btnCreateGroup" role="button">ASSIGN GROUP</a>
    </div>
</div>

<!--table ends-->