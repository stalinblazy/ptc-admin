﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuestionCommentList.ascx.cs" Inherits="QuestionCommentList" %>

<div class="col-sm-10 adflashcardTxt" style="margin-top: 17px">
    <label>Question <span id="QuestionNumber"></span>:</label>
    <p class="flashQunModal" id="qstflashQunModal"></p>

</div>
<div class="table-top-action">
    <input class="input-search" oninput="searchTableQstComments()" id="searchTableQst" type="text" placeholder="Search" style="margin-right: 100px" />
    <%--  <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>--%>
</div>
<!--table starts-->
<table id="pageQuestionFlashCard" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 165px;">FIRST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 165px;">LAST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 120px;">EMAIL ADDRESS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 120px;">COMMENTS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>

<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-recqstCommt"></span>
                <button id="firstBtnqstCommt" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtnqstCommt" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-dataqstCommt" class="page-numb">1 of 1</span>
                <button id="nextBtnqstCommt" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtnqstCommt" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
