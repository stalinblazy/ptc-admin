﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceList.ascx.cs" Inherits="ResourceList" %>

<div class="table-top-action">
    <div class="rmbulkActionbtn">
        <select class="browser-default custom-select" id="ddlbulk">
           <option disabled selected="selected">Select Action</option>
            <option value="0"> Inactive</option>
            <option value="1"> Active</option>
            <option value="2"> Delete</option>
        </select>
        <button type="submit" id="btnApply" class="btn btn-light rmapplyBtn">Apply</button>
        <a href="AddResource.aspx">
            <button type="button" class="rmcreateUser" id="rmuserbtn"><i class='far fa-calendar-check'></i>&nbsp; Upload Resource </button>
        </a>
    </div>
    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
    <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
</div>
<!--table starts-->
<table id="pageResource" class="" style="width: 100%">
    <thead class="tableHeader">
        <tr>
            <th style="width: 29px; padding: 0px 13px !important;"></th>
            <th>RESOURCE NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 70px;">RESOURCE TYPE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>DESCRIPTION<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 100px;">DATE CREATED<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th style="width: 100px;">COURSE NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
            <th>STATUS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
        </tr>
    </thead>
    <tbody class="tableBody">
    </tbody>
</table>
<div class="clearfix"></div>
<div class="custom-pagination">
    <div class="row">
        <div class="col-md-6 col-lg-6">
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="datatable-pagination pull-right">
                <span id="total-rec"></span>
                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                <span id="page-data" class="page-numb">1 of 1</span>
                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
<!--table ends-->
