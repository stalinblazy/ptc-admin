﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResourceManagement.aspx.cs" Inherits="ResourceManagement" %>

<%@ Register TagPrefix="LMS" TagName="ResourceList" Src="ResourceList.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Resource Manager</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
     <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Resource Manager     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Resource Manager - <span id="spanHeader">Overview</span>
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="rmlistofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row rmlistofusers">
                                    <div class="col-md-6 rmleftlist" style="padding-left: 0px;">
                                        <ul class="rmuserlistleft">
                                            <li>
                                                <h1 class="active">List of Resources</h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_ResourceList">
                                        <LMS:ResourceList ID="ResourceList1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>
            <!-- End Page content -->
        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/custom/ResourceManagement.js"></script>
</body>
</html>
