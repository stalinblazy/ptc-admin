﻿using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ResourceManagement : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }

    }

    [WebMethod]
    public static string BullkResourceActions(string ResourceIds, string state)
    {
        string res = "";
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        var result = false;
        try
        {
            List<string> ResourceId = ResourceIds.Split(',').ToArray().ToList();
            if (state == "2")
            {
                if (ResourceId != null && ResourceId.Count > 0)
                {
                    foreach (string item in ResourceId)
                    {
                        int ID = Convert.ToInt32(item);
                        if (ID > 0)
                        {
                            result = DeleteResourceRecord(ID);
                        }

                    }
                }
                if (result == true)
                {
                    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Deleted the Resource Id's:" + ResourceIds + "", "Delete Operation", user_Id);
                    res = "Resource Deleted Successfully..";
                }
                else
                {
                    res = "Failed to delete Resource!!";
                }
            }
            else
            {
                if (ResourceId != null && ResourceId.Count > 0)
                {
                    foreach (string item in ResourceId)
                    {
                        int ID = Convert.ToInt32(item);
                        if (ID > 0)
                        {
                            result = UpdateResourceActive(ID, state);
                        }

                    }
                }
                if (result == true)
                {
                    string status;
                    if (state == "0")
                    {
                        status = "Inactive";
                    }
                    else
                    {
                        status = "Active";
                    }
                    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin " + status + " the Resource from Resource Management Resource Id's" + ResourceIds + "", "Update Operation", user_Id);
                    res = "Resource Update SuccessFully..";
                }
                else
                {
                    res = "Failed to update Resource!!";
                }
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            res = "Failed to update Resource!!";
        }

        return res;
    }


    [WebMethod]
    public static string GetResourceData(string fromDate , string endDate)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.Resource.GetResource(fromDate, endDate);

            var objData = DataAccessManager.DataTableToJSON(dataTable);

            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    [WebMethod]
    public static bool UpdateResourceActive(int resourceId, string IsActive)
    {
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Active", IsActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@ResourceID", Convert.ToInt32(resourceId), ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_UPDATE_FLASHCARD_ACTIVE_STATUS", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
        finally
        {
            auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Updated the Resource status to "+ IsActive + " from Resource Management Resource Id's" + resourceId + "", "Update Operation", user_Id);
        }
    }


    private static bool DeleteResourceRecord(int resourceId)
    {
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@resourceId", resourceId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_DELETE_Resource", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
        finally
        {
           // auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Delete the Resource from Resource Management Resource Id's" + resourceId + "", "Delete Operation", user_Id);
        }
    }
}