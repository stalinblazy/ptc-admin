﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LearningManagementSystem.Service;
using LearningManagementSystem.WebServices;

public partial class Service_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String reqPacket = System.Text.Encoding.UTF8.GetString(Request.BinaryRead(Request.ContentLength));
        String resPacket = ServiceFactory.GetService(ServiceType.Admin).DoService(reqPacket, this);
        Response.Write(resPacket);
    }
}