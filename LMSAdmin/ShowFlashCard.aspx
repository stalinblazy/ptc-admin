﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowFlashCard.aspx.cs" Inherits="ShowFlashCard" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modifiy Flash Card</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="js/croppie/croppie.css" />
    <link rel="stylesheet" href="css/styledev.css" />
    <link rel="stylesheet" href="css/custom-dev.css" />
    <style type="text/css">
        .upload-demo .upload-demo-wrap,
        .upload-demo .upload-result,
        .upload-demo.ready .upload-msg {
            display: none;
        }

        .upload-demo.ready .upload-demo-wrap {
            display: block;
        }

        .upload-demo.ready .upload-result {
            display: inline-block;
        }

        .upload-demo-wrap {
            width: 750px;
            height: 450px;
            margin: 0 auto;
        }

        .selectFile {
            background-color: #f7f7f7;
            font-size: 12px !important;
            color: #29295b !important;
            box-shadow: 0 3px 6px 0 rgba(0,0,0,0.2);
        }
    </style>

        <script type="text/javascript">
            var serialNumber = "";
            var questionID = 1;
            function GetDynamicTextBox(value)
            {
                serialNumber = parseInt(document.getElementById("HidtotalQstCount").value) + questionID;
                return '<div class="row"><div class="col-sm-12 formTxt"><label>Question ' + serialNumber + ' </label><input name="txtQst' + serialNumber + '" type="text" value = "' + value + '" id="txtQst' + serialNumber + '"  class=form-control  data-prompt-position=bottomRight:-100,3 /></div><span id="errortxtQst' + serialNumber + '" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px;"></span>  </div>' +
                    '<div class="row"><div class="col-sm-12 formTxt"><label>Answer ' + serialNumber + '</label><input name="txtAns' + serialNumber + '" type="text" value = "' + value + '" id="txtAns' + serialNumber + '"  class=form-control  data-prompt-position=bottomRight:-100,3 /></div><span id="errortxtAns' + serialNumber + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:22px"></span> </div>' +
                    '<div class="row"><div class="col-sm-12 formTxt"><label>Resource ' + serialNumber + '</label><input name="file' + serialNumber + '" type="file" accept="image/x-png,image/gif,image/jpeg" id="file' + serialNumber + '" name = "file' + serialNumber + '"   class=form-control  data-prompt-position=bottomRight:-100,3 /></div><div class=col-sm-4></div></div>' +
                    '<div class="row"><div class="col-sm-4 formTxt"><label>Know More(Page No) ' + serialNumber + '</label><input name = "txtKnowmore' + serialNumber + '" type="text"  id="txtKnowmore' + serialNumber + '"  name = "txtKnowmore' + serialNumber + '"  class=form-control  data-prompt-position=bottomRight:-100,3 onkeypress="return isNumber(event)" maxlength="2" /><span id="errortxtKnow' + serialNumber + '" style="display: none; color:red;font:bolder;font-size:12px;font-family:Arial;font-weight:700;padding-left:9px;margin-top:10px"></span></div></div>' +
                    '<br>' +
                    '<input type="button" value="Remove Card" class="btn text-white userBut2" onclick = "RemoveTextBox(this)"  id="remove' + serialNumber + '" /></div><br><br>'
               
            }


            /*button submit validation*/

            function dynamicCtrlbtnValidation()
            {
               
                debugger;
                var loopFlag = true;
                var ctrlCardNameValue = document.getElementById("CardTitle").value;
                if (ctrlCardNameValue == null || ctrlCardNameValue == "") {
                    document.getElementById("lblCardName").style.display = "block";
                    loopFlag = false;
                }
                var trackValue = document.getElementById("FlashTrack");
                var droptrackvlaue = trackValue.options[trackValue.selectedIndex].value;

                if (droptrackvlaue == 0) {
                    document.getElementById("lblflashtrack").style.display = "block";
                    loopFlag = false;
                }
                var resourceValue = document.getElementById("ResourceListUpdate");
                var dropresourcevlaue = resourceValue.options[resourceValue.selectedIndex].value;

                if (dropresourcevlaue == 0) {
                    document.getElementById("lblResourceName").style.display = "block";
                    loopFlag = false;
                }
                var curriValue = document.getElementById("FlashCurriculum");
                
                var dropCurrivlaue = curriValue.options[curriValue.selectedIndex].value;

                if (dropCurrivlaue == 0) {
                    document.getElementById("lblcurriculam").style.display = "block";
                    loopFlag = false;
                }

                var repLength = parseInt(document.getElementById("HidtotalQstCount").value)
               
                var globalqscount = 0;
                for (var item = 0; item < repLength; item++)
                {
                    var   y = item + 1;
                    var txtQst = document.getElementById("FlashRep_txtQst_" + item).value;
                    if (txtQst == "") 
                    {
                        document.getElementById("FlashRep_errortxtQst_" + item).style.display = "block";
                        document.getElementById("FlashRep_errortxtQst_" + item).innerText = "Please enter question " + y;
                        document.getElementById("FlashRep_txtQst_" + item).focus();
                        return false;
                    }
                    else
                    {
                        ShowHideError(item, "qst");
                    }
                    var txtAns = document.getElementById("FlashRep_txtAns_" + item).value;
                    if (txtAns == "") {
                        document.getElementById("FlashRep_errortxtAns_" + item).style.display = "block";
                        document.getElementById("FlashRep_errortxtAns_" + item).innerText = "Please Enter Answer " + y;
                        document.getElementById("FlashRep_txtAns_" + item).focus();
                        return false;
                    }
                    else {
                        ShowHideError(item, "ans");
                    }
                    var txtknow = document.getElementById("FlashRep_txtKnowmore_" + item).value;
                    if (txtknow == "") {
                        document.getElementById("FlashRep_errortxtKnowMore_" + item).style.display = "block";
                        document.getElementById("FlashRep_errortxtKnowMore_" + item).innerText = "Please Enter KnowMore  " + y;
                        document.getElementById("FlashRep_txtKnowmore_" + item).focus();
                        return false;
                    }
                    else {
                        ShowHideError(item, "knowmore");
                    }
                   
                    if (loopFlag == false) {
                        continue;
                    }
                    globalqscount = y;
                 
                }

                var empTable = document.getElementById('TextBoxContainer').childElementCount;
                for (var i = 0; i < empTable; i++)
                {
                     var j=1;
                    var x = globalqscount + 1;
                    var txtQst = document.getElementById("txtQst" + x).value;
                    if (txtQst == "") {
                        document.getElementById("errortxtQst" + x).style.display = "block";
                        document.getElementById("errortxtQst" + x).innerText = "Please Enter Question " + x;
                        document.getElementById("txtQst" + x).focus();
                        return false;
                    }
                    else
                    {
                        ShowHideError(x, "qstdynamic");
                    }

                    var txtAns = document.getElementById("txtAns" + x).value;
                    if (txtAns == "") {
                        document.getElementById("errortxtAns" + x).style.display = "block";
                        document.getElementById("errortxtAns" + x).innerText = "Please Enter Answer " + x;
                        document.getElementById("txtAns" + x).focus();
                        return  false;
                    }
                    else
                        {
                            ShowHideError(x, "ansdynamic");
                        }
                    
                    var txtknow = document.getElementById("txtKnowmore" + x).value;
                    if (txtknow == "") {
                        document.getElementById("errortxtKnow" + x).style.display = "block";
                        document.getElementById("errortxtKnow" + x).innerText = "Please Enter KnowMore " + x;
                        document.getElementById("txtKnowmore" + x).focus()
                        return false;
                    }
                    else
                    {
                        ShowHideError(x, "knowmoredynamic");
                    }
                    if (loopFlag == false) {
                        continue;   
                    }
                    globalqscount++;
                }
            }

            function ShowHideError(textboxId, fieldType) {
                debugger;
                if (fieldType == "qst")
                {
                    document.getElementById("FlashRep_errortxtQst_" + textboxId).style.display = "none";
                    document.getElementById("FlashRep_errortxtQst_" + textboxId).innerText = "";
                   
               }
                else if (fieldType == "ans")
                {
                    document.getElementById("FlashRep_errortxtAns_" + textboxId).style.display = "none";
                    document.getElementById("FlashRep_errortxtAns_" + textboxId).innerText = "";
                  
                }
                else if (fieldType == "knowmore") {
                    document.getElementById("FlashRep_errortxtKnowMore_" + textboxId).style.display = "none";
                    document.getElementById("FlashRep_errortxtKnowMore_" + textboxId).innerText = "";
                   
                }
                else if (fieldType == "qstdynamic")
                {
                    document.getElementById("errortxtQst" + textboxId).style.display = "none";
                    document.getElementById("errortxtQst" + textboxId).innerText = "";
                }
                else if (fieldType == "ansdynamic")
                {
                    document.getElementById("errortxtAns" + textboxId).style.display = "none";
                    document.getElementById("errortxtAns" + textboxId).innerText = "";
                }
                else if (fieldType == "knowmoredynamic")
                {
                    document.getElementById("errortxtKnow" + textboxId).style.display = "none";
                    document.getElementById("errortxtKnow" + textboxId).innerText = "";
                }
                    
            };

            function Trim(value) {
                return value.replace(/^\s+|\s+$/g, '');
            };

            /*dynamic control click validation*/

            function dynamicCtrlValidation(ctrlId) {
                
                var isvalid = true;
                if (ctrlId == "CardTitle") {
                    var ctrlValue = document.getElementById(ctrlId).value;
                    if (ctrlValue == null || ctrlValue == "") {

                        document.getElementById("lblCardName").style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("lblCardName").style.display = "none";

                    }
                }

                else if (ctrlId == "FlashTrack") {
                    var trackValue = document.getElementById(ctrlId);
                    var droptrackvlaue = trackValue.options[trackValue.selectedIndex].value;
                    if (droptrackvlaue == 0) {
                        document.getElementById("lblflashtrack").style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("lblflashtrack").style.display = "none";
                    }
                }

                else if (ctrlId == "ResourceListUpdate") {
                    var resourceValue = document.getElementById(ctrlId);
                    var dropresourcevlaue = resourceValue.options[resourceValue.selectedIndex].value;
                    if (dropresourcevlaue == 0) {
                        document.getElementById("lblResourceName").style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("lblResourceName").style.display = "none";
                    }
                }

                else if (ctrlId == "FlashCurriculum") {
                    if (dropCurrivlaue == 0) {

                        document.getElementById("lblcurriculam").style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("lblcurriculam").style.display = "none";
                    }
                }

                var ctrlValue = document.getElementById(ctrlId).value;
                var txt = ctrlId;
                var serialNum = txt.match(/\d/g);
                serialNum = serialNum.join("");
                var ctrlIdNew = ctrlId.split(serialNum);
                var removeserctrlid = ctrlIdNew[0];
                if (removeserctrlid == "txtQst") {
                    if (ctrlValue == null || ctrlValue == "") {
                        document.getElementById("errortxtQst" + serialNum).style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("errortxtQst" + serialNum).style.display = "none";
                    }
                }

                if (removeserctrlid == "txtAns") {
                    if (ctrlValue == null || ctrlValue == "") {
                        document.getElementById("errortxtAns" + serialNum).style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("errortxtAns" + serialNum).style.display = "none";
                    }

                }

                if (removeserctrlid == "txtKnowmore") {
                    if (ctrlValue == null || ctrlValue == "") {
                        document.getElementById("errortxtKnow" + serialNum).style.display = "block";
                        isvalid = false;
                    }
                    else {
                        document.getElementById("errortxtKnow" + serialNum).style.display = "none";
                    }
                }

                return isvalid;
            }

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            function AddTextBox() {
                debugger;
                var div = document.createElement('DIV');
                div.innerHTML = GetDynamicTextBox("");
                document.getElementById("TextBoxContainer").appendChild(div);
                serialNumber++;
                questionID++;
            }

            function RemoveTextBox(div) {
                debugger;
                if (div["id"] != "remove1") {
                    document.getElementById("TextBoxContainer").removeChild(div.parentNode);
                }
            }

            function RecreateDynamicTextboxes() {
                // alert();
                var values = eval('<%=Values%>');
                //alert(values);
                if (values != null) {
                    var html = "";
                    for (var i = 0; i < values.length; i++) {
                        //  alert(i);
                        html += "<div>" + GetDynamicTextBox(values[i]) + "</div>";
                    }
                    document.getElementById("TextBoxContainer").innerHTML = html;
                }
            }
            $(document).ready(function () {
                $("#ResourceListUpdate").select2({
                    placeholder: "--Select Resource Type--",
                    allowClear: true
                });
            });
            $(function () {
                $("#ResourceListUpdate").change(function () {
                    var selectedVal = "";
                    //  alert($('option:selected', this).val());
                    selectedVal = $('option:selected', this).val();

                    var fields = selectedVal.split('=');
                    var val1 = fields[0];
                    var val2 = fields[1];
                    document.getElementById("HidOrgResource").value = val2;
                    // alert(document.getElementById("hidresourceURL").value);
                });
            });

            //    window.onload = AddTextBox;
    </script>
  
</head>

<body >

    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Flash Card
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Flash Card  - Show FlashCard
                        </div>
                    </div>
                </div>

                <div class="row" id="pageBody">
                    <div class="col-md-12">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxtResource">
                                    <h1>Flash Card Details</h1>
                                </div>
                                <!--forms starts here-->
                                <form id="addCourse" runat="server">
                                    <asp:HiddenField ID="HidtotalQstCount" runat="server" Value="0" />
                                    <asp:HiddenField ID="HidUpdatedResourceURL" runat="server" Value="0" />
                                       <asp:HiddenField ID="HidOrgResource" runat="server" Value="0" />
                                    <div class="userForm">
                                        <div class="row">
                                            <div class="col-sm-7 ">
                                                <!-- first row starts here-->
                                                <div class="row">
                                                 <div class="col-sm-12 formTxt">
                                                <label>Resource Type</label>
                                                <asp:DropDownList ID="ResourceListUpdate" onclick="dynamicCtrlValidation('ResourceList')"   AutoPostBack="true" runat="server" class="form-control">
                                                </asp:DropDownList>
                                                <span id="lblResourceName" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px"">Please select resource name</span>
                                                </div>
                                                    <asp:HiddenField ID="hdnCardID" runat="server" Value="0" />
                                                    <%--<asp:HiddenField ID="hidresourceURL" runat="server" />--%>
                                                    <div class="col-sm-12 formTxt">
                                                        <label>Card Title</label>
                                                        <asp:HiddenField ID="hdnFlashID" runat="server" Value="0" />
                                                        <asp:TextBox runat="server" ID="CardTitle" class="form-control"   onchange="dynamicCtrlValidation('CardTitle')" required="true" data-prompt-position="bottomRight:-100,3"></asp:TextBox>
                                                         <span id="lblCardName" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700; padding-top: 11px">Please enter flash card name</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6 formTxt">
                                                        <label>Flash Track</label>
                                                        <asp:DropDownList ID="FlashTrack" AutoPostBack="true"     onclick="dynamicCtrlValidation('ResourceTrack')"   runat="server" OnSelectedIndexChanged="FlashTrack_SelectedIndexChanged" class="form-control form-control-lg formTxtopt">
                                                        </asp:DropDownList>
                                                          <span id="lblflashtrack" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px"">Please select track</span>

                                                    </div>
                                                    <div class="col-sm-6 formTxt">
                                                        <label>Flash Curriculum</label>
                                                        <asp:DropDownList ID="FlashCurriculum"    onclick="dynamicCtrlValidation('FlashCurriculum')" AutoPostBack="true" runat="server" class="form-control form-control-lg formTxtopt">
                                                        </asp:DropDownList>
                                                         <span id="lblcurriculam" style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px"">Please select curriculum</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 formTxt">
                                                         <asp:HiddenField ID="flashcardQId" Value='<%# Eval("FlashCardQuestionID") %>' runat="server" />
                                                        <asp:Repeater ID="FlashRep" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <label>Question <%#Container.ItemIndex+1 %></label>
                                                                           <asp:HiddenField ID="HidCardcount" Value='<%# Eval("CardCount") %>' runat="server" />
                                                                        <asp:HiddenField ID="flashcardQId" Value='<%# Eval("FlashCardQuestionID") %>' runat="server" />
                                                                        <asp:HiddenField ID="flashcardId" Value='<%# Eval("FlashCard_Id") %>' runat="server" />
                                                                        <asp:HiddenField ID="flashCardParentID" Value='<%# Eval("ParentQuestionID") %>' runat="server" />
                                                                        <asp:TextBox ID="txtQst" class="form-control"   data-prompt-position="bottomRight:-100,3" runat="server" Text='<%# Eval("Question") %>' />
                                                                        <span id="errortxtQst"  style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px" runat="server"></span>
                                                                           <br />
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label>Answer <%#Container.ItemIndex+1 %></label>
                                                                        <asp:TextBox ID="txtAns" class="form-control" runat="server" Text='<%# Eval("Answer") %>' />
                                                                         <span id="errortxtAns"  style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px" runat="server"></span>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label>FlashFile <%#Container.ItemIndex+1 %></label>
                                                                        <br />
                                                                         <%--Make sure the path Client Path Given here--%>
                                                                       <%-- <img src="Resources/Thumbnailimages"+<%#Eval("FlashFile")%>  />--%>
                                                                        <input type="file" accept=".png,.gif,.jpeg,.jpg"  runat="server" id="file"/>
                                                                         <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="row">
                                                                            <div class="col-sm-4 formTxt">
                                                                        <label>KnowMore <%#Container.ItemIndex+1 %></label>
                                                                                <asp:TextBox ID="txtKnowmore" onkeypress="return isNumber(event)" MaxLength="2"   class="form-control" runat="server" Text='<%#GetPDFPageNumber(Eval("KnowMore").ToString()) %>' />
                                                      
                                                                           <span id="errortxtKnowMore"  style="display: none; color: red; font: bolder; font-size: 12px; font-family: Arial; font-weight: 700;padding-top: 11px" runat="server"></span>       
                                                                                
                                                                                 </div>
                                                                            </div>
                                                                    </td>
                                                                </tr>
                                                                    
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                   <div id="TextBoxContainer">
                                            <!--Textboxes will be added here -->
                                        </div>
                                                  <div class="row">
                                       <div class="col-sm-12 formTxt">
                                                <p class="addQun-flashcard" id="btnAdd" onclick="AddTextBox()" style="cursor:pointer"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Question</p>
                                            </div>
                                        </div>

                                            </div>
                                        </div>

                                        <!--back to user starts here-->
                                        <div class="row userBut">
                                            <div class="col-md-6 aeformTxt">
                                            </div>
                                            <div class="col-md-3 userBut1">
                                                <a href="flashCard.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to Flash Card</a>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Button ID="btnUpdate" Text="Update FlashCard" OnClientClick="return dynamicCtrlbtnValidation();" class="btn text-white userBut2" runat="server" OnClick="btnUpdate_Click" />
                                            </div>
                                        </div>
                                        <!--back to user ends here-->

                                    </div>

                                </form>
                                <!--forms ends here-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="imageModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crop Image</h5>
                </div>
                <div class="modal-body">
                    <div class="upload-demo-wrap">
                        <div id="upload-demo"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <LMS:CommonScripts ID="CommonScripts1" runat="server" />
    <link rel="stylesheet" href="js/datetime/bootstrap-datetimepicker.css" />

</body>

</html>
