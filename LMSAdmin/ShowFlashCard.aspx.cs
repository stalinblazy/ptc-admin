﻿using LearningManagementSystem.Components;
using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShowFlashCard : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected static string flashCard_Id;
    protected string Values;
    public string resourcepdf = string.Empty;

    public string FileName { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        try
        {
            string hdnFlashID = Request.QueryString["id"];
            if (!IsPostBack)
            {
                BindData();
                GetDataByFlashID(hdnFlashID);
                // Response.Write(hdnFlashID);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }

    public DataSet GetResourceTrackList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceTrack", parameters.ToArray());
    }

    public DataSet GetCurriculamList()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetResourceCurriculum", parameters.ToArray());
    }

    private void BindData()
    {

        FlashTrack.DataSource = GetResourceTrackList().Tables[0];
        FlashTrack.DataValueField = "TrackId";
        FlashTrack.DataTextField = "TrackName";
        FlashTrack.DataBind();
        FlashTrack.Items.Insert(0, new ListItem("--Select Track--", "0"));


        FlashCurriculum.DataSource = GetCurriculamList().Tables[0];
        FlashCurriculum.DataValueField = "CurriculumId";
        FlashCurriculum.DataTextField = "CurriculumName";
        FlashCurriculum.DataBind();
        BindCurriculumDropdown(0);
        FlashCurriculum.Items.Insert(0, new ListItem("--Select Curriculam--", "0"));


        ResourceListUpdate.DataSource = GetResourceFlashCardListPDF().Tables[0];
        ResourceListUpdate.DataValueField = "Namedrp";
        ResourceListUpdate.DataTextField = "Name";
        ResourceListUpdate.DataBind();
        BindResourceList(0);


    }

    private void BindResourceList(long id)
    {
        FlashCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        FlashCurriculum.DataValueField = "CurriculumId";
        FlashCurriculum.DataTextField = "CurriculumName";
        FlashCurriculum.DataBind();
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }

    public DataSet GetResourceFlashCardListPDF()
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        return dataManager.Execute("GetFlashCardResourcePDF", parameters.ToArray());
    }

    public DataSet GetResourceCurriculumList(long trackId)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(dataManager.CreateParam("@TrackId", trackId, ParameterDirection.Input));
        return dataManager.Execute("GetTrackCurriculumByTrackId", parameters.ToArray());
    }

    private void BindCurriculumDropdown(long id)
    {
        FlashCurriculum.DataSource = GetResourceCurriculumList(id).Tables[0];
        FlashCurriculum.DataValueField = "CurriculumId";
        FlashCurriculum.DataTextField = "CurriculumName";
        FlashCurriculum.DataBind();
        // ResourceCurriculum.SelectedItem.Text = "Global";
    }


    protected void FlashTrack_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCurriculumDropdown(Convert.ToInt64(FlashTrack.SelectedItem.Value));
    }

    protected void GetDataByFlashID(string flashCardId)
    {

        if (flashCardId != null && flashCardId != string.Empty)
        {
            try
            {

                DataTable dt = LearningManagementSystem.Components.FlashCard.GetFlashCardDetailsById(int.Parse(flashCardId));
                if (dt.Rows.Count > 0)
                {
                    HidtotalQstCount.Value = dt.Rows.Count.ToString();
                    resourcepdf = GetResourceURL(dt.Rows[0]["KnowMore"].ToString());
                    HidOrgResource.Value = resourcepdf;
                    FlashRep.DataSource = dt;
                    FlashRep.DataBind();
                }


                hdnFlashID.Value = dt.Rows[0]["FlashCard_Id"].ToString();
                CardTitle.Text = dt.Rows[0]["CardTitle"].ToString();
                string trackID = dt.Rows[0]["TrackId"].ToString();
                string CurriculumId = dt.Rows[0]["CurriculumId"].ToString();
                string resourcetype = dt.Rows[0]["RESOURCELIBRARYTYPEID"].ToString();
                string resourcetypeID = dt.Rows[0]["FlashCardSelectedResourceTypeId"].ToString();

                if (trackID != string.Empty)
                {
                    FlashTrack.SelectedValue = dt.Rows[0]["TrackId"].ToString();
                    BindCurriculumDropdown(Convert.ToInt64(FlashTrack.SelectedValue));
                }
                if (CurriculumId != string.Empty)
                {
                    FlashCurriculum.SelectedValue = dt.Rows[0]["CurriculumId"].ToString();
                }

                if (resourcetypeID != string.Empty)
                {
                    ResourceListUpdate.SelectedValue = resourcetypeID + "=" + resourcepdf;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }

    public string GetPDFPageNumber(string knowMore)
    {
        string[] pdfPageno = knowMore.Split('=');
        return pdfPageno[1];
    }



    public string GetResourceURL(string knowMore)
    {
        string[] resourcePdf = knowMore.Split('#');
        return resourcePdf[0];
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            FlashCard fCard = new FlashCard();
            fCard.CardTitle = CardTitle.Text;
            string[] resutypeid = ResourceListUpdate.SelectedValue.Split('=');
            fCard.SelectedResourceTypeId = Convert.ToInt64(resutypeid[0]);
            HidUpdatedResourceURL.Value = resutypeid[1];
            fCard.Type = 7;
            fCard.FlashCardTrack = Convert.ToInt64(FlashTrack.SelectedValue);
            fCard.FlashCardCurriculam = Convert.ToInt64(FlashCurriculum.SelectedValue);
            fCard.AdminId = user_Id;
            fCard.FlashCardID = Convert.ToInt64(hdnFlashID.Value);
            int falshCardID = LearningManagementSystem.Components.FlashCard.UpdateFlashCard(fCard);//calling uppfalshSP
            string resourcePath = System.Configuration.ConfigurationManager.AppSettings["ResourcePath"];
            for (int item = 0; item < this.FlashRep.Items.Count; item++)
            {
                TextBox question = this.FlashRep.Items[item].FindControl("txtQst") as TextBox;
                TextBox anaswer = this.FlashRep.Items[item].FindControl("txtAns") as TextBox;
                TextBox knowmore = this.FlashRep.Items[item].FindControl("txtKnowMore") as TextBox;
                string KnowmorepdfValue = HidUpdatedResourceURL.Value + "#page=" + knowmore.Text;
                string flashfile = this.FlashRep.Items[item].FindControl("file").ToString();
                HiddenField flashCardID = this.FlashRep.Items[item].FindControl("flashcardId") as HiddenField;
                HiddenField flashCardIDQiestion = this.FlashRep.Items[item].FindControl("flashcardQId") as HiddenField;
                HiddenField FlashCardParentQId = this.FlashRep.Items[item].FindControl("flashCardParentID") as HiddenField;
                List<FlashCard> flashListObj = new List<FlashCard>();
                FlashCard flashObj = new FlashCard();
                flashObj.FlashCardID = falshCardID;
                flashObj.Question = question.Text;
                flashObj.Answer = anaswer.Text;

                int y = item + 1;
                HttpPostedFile PostedFile = Request.Files[item];
                string savePath = resourcePath + "/Resources/FlashCardQimage/";
                string extension = "";
                string fileNameForDB = "";

                string txtQst = this.FlashRep.Items[item].FindControl("txtQst").ToString();
                string path = " ";
                if (PostedFile.ContentLength > 0)
                {
                    FileName = System.IO.Path.GetFileName(PostedFile.FileName);
                    extension = System.IO.Path.GetExtension(PostedFile.FileName);
                    fileNameForDB = "Qst" + y + "_" + falshCardID + extension;
                    // PostedFile.SaveAs(Server.MapPath(resourcePath + "\\Resources\\FlashCardQimage") + txtQst + extension);
                    path = savePath + fileNameForDB;
                    PostedFile.SaveAs(path);
                    flashObj.File = fileNameForDB;
                }
                if (FlashCardParentQId.Value == "" || FlashCardParentQId.Value == null)
                {
                    FlashCardParentQId.Value = "0";
                }
                flashObj.KnowMore = KnowmorepdfValue;
                flashObj.FlashCardQuestionParentID = Convert.ToInt64(FlashCardParentQId.Value);
                flashObj.FlashCardQuestionID = Convert.ToInt64(flashCardIDQiestion.Value);
                flashListObj.Add(flashObj);
                LearningManagementSystem.Components.FlashCard.UpdateQuestions(flashListObj);//calling updateFlahQuestions

            }
            List<FlashCard> addflashListObj = new List<FlashCard>();
            FlashCard flashCardObj = new FlashCard();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                int y = i + 1;
                string txtQst = Request.Form["txtQst" + y];
                string path = " ";
                string txtAns = Request.Form["txtAns" + y];
                string txtKnowmore = HidUpdatedResourceURL.Value + "#page=" + Request.Form["txtKnowmore" + y];
                string FileName = string.Empty;
                HttpPostedFile PostedFile = Request.Files[i];
                string savePath = resourcePath + "/Resources/FlashCardQimage/";
                string extension = "";
                string fileNameForDB = "";
                if (PostedFile.ContentLength > 0)
                {
                    FileName = System.IO.Path.GetFileName(PostedFile.FileName);
                    extension = System.IO.Path.GetExtension(PostedFile.FileName);
                    fileNameForDB = "Qst" + y + "_" + falshCardID + extension;
                    // PostedFile.SaveAs(Server.MapPath(resourcePath + "\\Resources\\FlashCardQimage") + txtQst + extension);
                    path = savePath + fileNameForDB;
                    PostedFile.SaveAs(path);
                }


                flashCardObj.FlashCardID = falshCardID;
                flashCardObj.Question = txtQst;
                flashCardObj.Answer = txtAns;
                flashCardObj.File = fileNameForDB;
                flashCardObj.KnowMore = txtKnowmore;
                addflashListObj.Add(flashCardObj);
            }
            LearningManagementSystem.Components.FlashCard.UpdateFlashCard(flashCardObj);
            Response.Redirect("flashCard.aspx?IsSuccessRescource=true", false);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.StackTrace);
        }
    }
}
