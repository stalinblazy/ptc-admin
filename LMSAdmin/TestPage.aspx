﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TestPage.aspx.vb" Inherits="TestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Using Select2</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">



        <div class="jumbotron">
            <div class="container bg-danger">
                <div class="col-md-6">
                    <label>Single Select2</label>
                    <asp:DropDownList ID="ResourceList" onclick="dynamicCtrlValidation('ResourceList')" runat="server" class="form-control form-control-lg formTxtopt js-states">
                                                    <asp:ListItem Value="0">--Select Track--</asp:ListItem>
                                                </asp:DropDownList>
                </div>
                <div class="col-md-6">
                    <label>Multiple Select2</label>
                    <select id="multiple" class="js-states form-control" multiple>
                        <option>Java</option>
                        <option>Javascript</option>
                        <option>PHP</option>
                        <option>Visual Basic</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- jQuery -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Select2 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script>
            $("#ResourceList").select2({
                placeholder: "Select a programming language",
                allowClear: true
            });
            $("#multiple").select2({
                placeholder: "Select a programming language",
                allowClear: true
            });
        </script>
    </form>
</body>
</html>
