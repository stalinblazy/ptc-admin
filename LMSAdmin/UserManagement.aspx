﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserManagement.aspx.cs" Inherits="Admin_UserManagement" %>

<%@ Register TagPrefix="LMS" TagName="UserList" Src="UserList.ascx" %>
<%@ Register TagPrefix="LMS" TagName="GroupList" Src="GroupList.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <link rel="stylesheet" href="css/styledev.css">
    <link rel="stylesheet" href="css/custom-dev.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <style>
        .dt-buttons {
            margin-left: 24px !important;
            margin-bottom: 10px !important;
            display: block !important;
            
        }
        .dt-button {
            background: #F9A05E !important;
            color: #FFFFFF !important;
            font-size: 12px !important;
            padding: 4px 8px !important;
            margin-left: 10px !important;
            border:none !important;
            cursor:pointer;
           
        }
    </style>
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
</head>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Users and Groups
     
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Users and Groups - <span id="spanHeader">Overview</span>
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="listofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row listofusers">
                                    <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                        <ul class="userlistleft nav nav-tabs" id="userlistleft">
                                            <li><a href="#tab_UserList" data-toggle="tab" id="tabUser">
                                                <h1 class="active">List of Users</h1>
                                            </a></li>
                                            <li><a href="#tab_GroupList" data-toggle="tab" id="tabGroup">
                                                <h1>List of Groups</h1>
                                            </a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 rightlist" id="rightlist">
                                        <ul class="userlistright" id="userTypes">
                                            <li class="active" data-val="Registered">Registered Users</li>
                                            <li data-val="Active">Active Users</li>
                                            <li data-val="Authorized">Authorized Users</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_UserList">
                                        <LMS:UserList ID="UserList1" runat="server" />
                                    </div>

                                    <div class="tab-pane" id="tab_GroupList">
                                        <LMS:GroupList ID="GroupList1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>
            <!-- End Page content -->
        </div>
    </div>


    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/custom/UserManagement.js?v2"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
    <script type="text/javascript" src="js/tableToExcel.js"></script>
    <%--    <script src="../scripts/app.js"></script>--%>
</body>
</html>
