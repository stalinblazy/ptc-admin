﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Medtrix.DataAccessControl;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using LearningManagementSystem.UserManager;

public partial class Admin_UserManagement : System.Web.UI.Page
{
    #region Page Load
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        try
        {
            if (!IsPostBack)
            {

            }
        }
        catch (Exception)
        {

        }
    }
    #endregion

    #region Users

    [WebMethod]
    public static string BindUserList(string fromDate, string endDate, string userType)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.User.GetUserListTypeWise(fromDate, endDate, userType);

            var objData = DataAccessManager.DataTableToJSON(dataTable);

            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }
    #endregion

    #region UserBullkDelete

    [WebMethod]
    public static string BullkUserActions(string userIds, string state)
    {
        string res = "";
        var result = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        bool auditDetails = false;
        try
        {
            List<string> userId = userIds.Split(',').ToArray().ToList();
            if (state == "2")
            {
                if (userId != null && userId.Count > 0)
                {
                    foreach (string item in userId)
                    {
                        int ID = Convert.ToInt32(item);
                        if (ID > 0)
                        {
                            result = DeleteUserRecord(ID);
                        }

                    }
                }
                if (result == true)
                {
                    res = "User Deleted Successfully..";
                    //    auditDetails = dataManager.ExecuteAuditTrailDetails("Admin deleted User from User Management User Id's" + userIds + "", "Delete Operation", user_Id);
                }
                else
                {
                    res = "Failed to delete User!!";
                }
            }
            else
            {
                if (userId != null && userId.Count > 0)
                {
                    foreach (string item in userId)
                    {
                        int ID = Convert.ToInt32(item);
                        if (ID > 0)
                        {
                            result = UpdateUserActive(ID, state);
                        }

                    }
                }
                if (result == true)
                {
                    //string status;
                    //if (state == "0")
                    //{
                    //    status = "Inactive";
                    //}
                    //else
                    //{
                    //    status = "Active";
                    //}
                    //auditDetails = dataManager.ExecuteAuditTrailDetails("Admin " + status + " the User from User Management User Id's" + userIds + "", "Update Operation", user_Id);
                    res = "User Update SuccessFully..";
                }
                else
                {
                    res = "Failed to update User!!";
                }
            }
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            res = "Failed to update User!!";
        }
        return res;
    }

    private static bool DeleteUserRecord(int userId)
    {
        bool auditDetails = false;
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", userId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Desc", "Admin Delete the user " + userId + " from User Management", ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminID", user_Id, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_Delete_USER", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
        finally
        {
            // auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Delete the User from User Management User Id's" + userId + "", "Delete Operation", user_Id);
        }
    }

    [WebMethod]
    public static bool UpdateUserActive(int userId, string IsActive)
    {
        DataAccessManager dataManager = DataAccessManager.GetInstance();
        bool auditDetails = false;
        try
        {
            String referenceCode = Guid.NewGuid().ToString();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Active", IsActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", Convert.ToInt32(userId), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Desc", "Admin changed user to " + IsActive + " from User Management for the User Id's='" + userId + "'", ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminID", user_Id, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery("SP_LMS_Update_ActiveUSER", parameters.ToArray());
        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            return true;
        }
        finally
        {
            // auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Changed Status to " + IsActive + " of the User from User Management User Id's" + userId + "", "Update Operation", user_Id);
        }
    }

    #endregion

    #region Groups
    [WebMethod]
    public static string BindGroupList(string fromDate, string endDate)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.User.GetGroupDetails(fromDate, endDate);

            var objData = DataAccessManager.DataTableToJSON(dataTable);

            JavaScriptSerializer js = new JavaScriptSerializer();
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }
    #endregion

}