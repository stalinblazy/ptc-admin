﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="analytics.aspx.cs" Inherits="analytics" %>

<!DOCTYPE html>
<html lang="en">
<title>Analytics Report</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" href="css/styledev.css">
<link rel="stylesheet" href="css/custom-dev.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" href="css/progressbar.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', { 'packages': ['corechart'] });
</script>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />

        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />

            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Analytics Report
     
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Analytics Report - Overview
     
                       
                        </div>
                    </div>
                </div>

                <div class="row pageBody">

                    <!-- Create Group -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 cardCustom">
                                <div class="row arlistofusers">
                                    <div class="col-md-6" style="padding-left: 0px;">
                                        <ul class="arUserGroup">
                                            <li>
                                                <h1>Learning Progress</h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>

                                <div class="table-top-action" style="padding-top: 10px;">
                                    <div class="arRadioList">
                                        <ul>
                                            <li>
                                                <input type="radio" onchange="onchangeReport()" name="report" id="crs" value="course" checked><label style="padding-left: 5px;" for="crs"> Course-wise Report</label>
                                            </li>
                                            <li>
                                                <input type="radio" onchange="onchangeReport()" name="report" id="urs" value="user"><label style="padding-left: 5px;" for="urs">User Enrollment Report</label></li>
                                            <!--  <li><input type="radio" name="" value=""><label>Module Report</label></li>-->
                                            <li>
                                                <input type="radio" onchange="onchangeReport()" name="report" id="mrs" value="module"><label style="padding-left: 5px;" for="mrs">Module Report</label></li>

                                        </ul>
                                        <div class="arformTxt course-report user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-user"></i></span>
                                                <input type="text" class="form-control border-left-0 searchText" data-column="0" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="arformTxt course-report user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-user"></i></span>
                                                <input type="text" class="form-control border-left-0 searchText" data-column="1" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="arformTxt course-report user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-envelope"></i></span>
                                                <input type="text" class="form-control border-left-0 searchText" data-column="2" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="arformTxt user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fas fa-users"></i></span>
                                                <select id="deplist" data-column="5" class="browser-default border-left-0 custom-select selectFilter">
                                                </select>
                                            </div>
                                        </div>
                                         <div class="arformTxt user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fas fa-users"></i></span>
                                                <select id="designationlist" data-column="4" class="browser-default border-left-0 custom-select selectFilter">
                                                </select>
                                            </div>
                                        </div>
                                         <div class="arformTxt user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fas fa-business-time"></i></span>
                                                <select id="regionlist" data-column="3" class="browser-default border-left-0 custom-select selectFilter">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="arformTxt course-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-folder-open-o"></i></span>
                                                <select id="statuslist" data-column="5" class="browser-default border-left-0 custom-select selectFilter">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="arformTxt course-report" style="width: 500px;">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-folder-open-o"></i></span>
                                                <select id="courselist" data-column="3" class="browser-default border-left-0 custom-select selectFilter">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="arformTxt course-report user-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-folder-open-o"></i></span>
                                                <select id="grouplist" data-column="6" class="browser-default border-left-0 custom-select selectFilter">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="arformTxt module-report" style="width: 500px;">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-folder-open-o"></i></span>
                                                <select id="modcourselist" class="browser-default border-left-0 custom-select modfilter">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="arformTxt module-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-folder-open-o"></i></span>
                                                <select id="modgrouplist" class="browser-default border-left-0 custom-select modfilter">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="arformTxt module-report">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-white border-right-0 arcalendarIcon"><i class="fa fa-folder-open-o"></i></span>
                                                <select id="countrylist" class="browser-default border-left-0 custom-select modfilter">
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <span id="cal-range" class="cal-range  course-report user-report"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
                                </div>

                                <div>
                                    <!--table starts-->
                                    <table id="pageUsers" class="course-report" style="width: 100%">
                                        <thead class="tableHeader">
                                            <tr>
                                                <th style="width: 95px;">FIRST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th style="width: 95px;">LAST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>EMAIL ADDRESS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>COURSE NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>DUE DATE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>

                                                <th style="width: 65px">STATUS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th style="width: 65px">GROUP<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>


                                                <th style="width: 99px">PERCENTAGE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th style="width: 136px">COMPLETION DATE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>

                                            </tr>
                                        </thead>
                                        <tbody class="tableBody">
                                            <asp:Repeater ID="serviceRep" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("First_Name")%></td>
                                                        <td><%# Eval("Last_Name")%></td>
                                                        <td><%#Eval("Email_ID") %> </td>
                                                        <td><%#Eval("Name")%></td>
                                                        <td><%#(Boolean)Eval("DateNotRequired")  ? "NA" : Eval("DueDate") %></td>

                                                        <td><%#Eval("Course_Status")%></td>
                                                        <td><%#Eval("LMSGroupName")%></td>

                                                        <td><%#Eval("Percentage")%></td>
                                                        <td><%#Eval("Completion_Date")%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                    <table id="userWise" class="user-report" style="width: 100%">
                                        <thead class="tableHeader">
                                            <tr>
                                                <th style="min-width: 95px;">FIRST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th style="min-width: 95px;">LAST NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>EMAIL ADDRESS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>REGION<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>DESIGNATION<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>DEPARTMENT<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th>Group<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th style="min-width: 100px">MANAGER<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                <th style="min-width: 120px">REG DATE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>

                                            </tr>
                                        </thead>
                                        <tbody class="tableBody">
                                            <asp:Repeater ID="userRepeater" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("First_Name")%></td>
                                                        <td><%# Eval("Last_Name")%></td>
                                                        <td><%#Eval("Email_ID") %> </td>
                                                        <td><%#Eval("RegionID")%></td>
                                                        <td><%#Eval("Designation")%></td>
                                                        <td><%#Eval("DepartmentID")%></td>
                                                        <td><%#Eval("LMSGroupName")%></td>
                                                        <td><%#Eval("ReportingManagerID")%></td>
                                                        <td><%#Eval("Created_Date","{0:MM/dd/yy}")%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 course-report user-report" style="text-align: right; display: inline-block;">
                                        <button type="button" onclick="downloadReport()" class="assgnUserBtn fa-pull-right" id="arassignUserBtn"><i class="fas fa-download"></i>&nbsp; Download Report </button>
                                    </div>
                                    <div class="custom-pagination  course-report user-report">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="datatable-pagination pull-right">
                                                    <span id="total-rec"></span>
                                                    <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                                                    <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                                                    <span id="page-data" class="page-numb">1 of 1</span>
                                                    <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                                                    <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="module-report" style="margin-top: 10px;">
                                    <div style="display: flex; padding-left: 8px;">
                                        <ul class="arRadioList">
                                            <li>
                                                <input type="radio" onchange="onchangeModuleRpt()" name="mreport" id="mq" value="qwise" checked><label style="padding-left: 5px;" for="mq"> Overall Response</label>
                                            </li>
                                            <li>
                                                <input type="radio" onchange="onchangeModuleRpt()" name="mreport" id="maq" value="owise"><label style="padding-left: 5px;" for="maq">Option Wise Response</label></li>

                                        </ul>
                                    </div>
                                    <table id="myTable" style="display: none" border="1" data-cols-width="50,100">
                                        <thead>
                                            <tr id="myThead"></tr>
                                        </thead>
                                        <tbody id="myTbody"></tbody>
                                    </table>
                                    <div class="overall">
                                        <div id="chartContainer" style="width: 100%; height: 430px; margin: 0; overflow-x: auto; overflow-y: hidden;">
                                            <div id="chartt" style="width: 100%; height: 400px; margin: 0;">
                                            </div>

                                        </div>

                                        <div class="col-md-12" style="text-align: right; display: inline-block;">
                                            <button type="button" onclick="downloadModReport(event)" class="assgnUserBtn dButton fa-pull-right"><i class="fas fa-download"></i>&nbsp; Download Report </button>
                                            <button type="button" onclick="downloadOverallChart()" class="assgnUserBtn dButton fa-pull-right"><i class="fas fa-download"></i>&nbsp; Download Chart </button>
                                        </div>
                                        <div id="questionContainer">
                                            <table class="table table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Question</th>
                                                        <th style='text-align: center;'>Correct</th>
                                                        <th style='text-align: center;'>Incorrect</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="quesBody">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="optionwise">
                                        <div id="questionOpts">
                                        </div>
                                        <div class="col-md-12" style="text-align: right; display: inline-block; margin-bottom: 20px; margin-right: 5px;">
                                            <button type="button" onclick="downloadOverallReport(event)" class="assgnUserBtn dButton fa-pull-right"><i class="fas fa-download"></i>&nbsp; Download Report </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
    <script type="text/javascript" src="js/tableToExcel.js"></script>
    <script src="js/html2canvas.min.js"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/adminservice.js"></script>
    <script src="js/analytics.js?v1"></script>
</body>
</html>
