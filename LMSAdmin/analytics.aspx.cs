﻿using LearningManagementSystem.UserManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class analytics : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        DataTable dt = LearningManagementSystem.Components.Reports.GetCourseWiseReport();
        serviceRep.DataSource = dt;
        serviceRep.DataBind();

        DataTable udt = LearningManagementSystem.Components.Reports.GetUserWiseReport();
        userRepeater.DataSource = udt;
        userRepeater.DataBind();
    }
}