﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="dashboard" %>

<!DOCTYPE html>

<html lang="en">
<title>Dashboard</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="css/styledev.css">
<link rel="stylesheet" href="css/custom-dev.css">
<!-- MAP -->
<link href="css/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/jquery.vmap.min.js"></script>
<script type="text/javascript" src="js/jquery.vmap.world.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.vmap.sampledata.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', { 'packages': ['corechart'] });
</script>
<!--END MAP-->
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-9 pageHeaderTitle">
                            Dashboard
                        </div>
                        <div class="col-md-3 pageHeaderDesc">
                            Dashboard - Overview
                        </div>
                    </div>
                </div>
                <div class="row" id="pageBody">
                    <div class="col-md-4">
                        <!-- Activity -->
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="activitytxt">
                                    <h1>Activity</h1>
                                </div>
                                <div class="rectPub">
                                    Recently Published
                                    <div class="row" style="margin-right: 0px;">
                                        <div class="noti-container">
                                            <table class="noti-table">
                                                <tbody id="notiTable">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Total Active and Autorized Users -->
                        <div id="totalUsers" class="row">
                            <div class="col-md-6" style="padding-right: 5px;">
                                <div class="TAVcardCustom row" style="margin-right: 0px">
                                    <div class="col-md-4 usersIcon">
                                        <img src="images/users.png" alt="Users" width="45" height="45">
                                    </div>
                                    <div class="col-md-8 padReduce">
                                        <div id="activeusers" class="totalUsersnum">
                                            0
                                        </div>
                                        <div class="totalUsersdes">
                                            Total Active
                                            <br>
                                            Users
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 5px;">
                                <div class="TAVcardCustom row" style="margin-left: 0px;">
                                    <div class="col-md-4 usersIcon">
                                        <img src="images/user.png" alt="Users" width="45" height="45">
                                    </div>
                                    <div class="col-md-8 padReduce">
                                        <div id="totalusers" class="totalUsersnum">
                                            0
                                        </div>
                                        <div class="totalUsersdes">
                                            Total Authorized
                                            <br>
                                            Users
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- map -->
                    <div class="col-md-8">
                        <div class="row map">
                            <div class="col-md-9" style="padding: 0px;">
                                <div id="vmap" style="width: 100%; height: 340px;"></div>
                            </div>
                            <div class="col-md-3 map-legend-container" style="margin-top: 20px;">
                                <p class="map-legend">Country-wise Users</p>
                                <table class="map-legend-table">
                                    <tbody id="legend">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="listOfUsers">
                        <div class="cardCustom">
                            <div class="row" style="padding-top: 10px;">
                                <div class="col-md-6">
                                    <h1>List of Users</h1>
                                </div>
                                <div class="col-md-6">
                                    <div style="text-align: right;">
                                        <button onclick="downloadChart()" style="margin-top: 5px;" class="btn btn-light applyBtn"><i class="fa fa-download" aria-hidden="true"></i></button>

                                        <select id="countryList" class="browser-default  custom-select selectFilter">
                                        </select>
                                        <select id="groupList" class="browser-default  custom-select selectFilter">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="chartContainer" style="width: 100%; height: 430px; margin: 0; overflow-x: auto; overflow-y: hidden;">
                                <div id="chartt" style="width: 100%; height: 400px; margin: 0;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/adminservice.js"></script>
    <script>
        $(document).ready(function () {
            activeLeftMenu("dashboard");
            $("#bar-chart > div > div > div:nth-child(2)").mouseover(function () {
                $("#module4Hvr").css("display", "block");
            });
            $("#bar-chart > div > div > div:nth-child(2)").mouseout(function () {
                $("#module4Hvr").css("display", "none");
            });

            let cb = new CallBack();
            cb.func = "AfterDataLoaded";

            _adminService.GetDashboard({}, cb, true);

        });
        var charData;
        let totalUsers = 0;
        function AfterDataLoaded(res) {
            console.log(res);
            if (res) {
                let uReport = res.userreport[0];
                $("#activeusers").html(uReport.activeusers);
                totalUsers = uReport.activeusers;
                $("#totalusers").html(uReport.totalusers - uReport.activeusers);
                loadMap(res.mapreport);
                charData = res.modulereport;
                google.charts.setOnLoadCallback(drawChart);
                generateNotification(res.notifications);
                $('#groupList').append('<option value="" selected>All Group</option>');
                if (res.groups) {
                    res.groups.forEach(function (group) {
                        $('#groupList').append('<option value="' + group.ID + '">' + group.Name + '</option>');
                    });
                };
            }
            $(".loader").fadeOut();

        }

        function generateNotification(notifications) {
            let notiTable;
            notifications.forEach(function (noti) {
                let date = new Date(parseInt(noti.date.substr(6)));
                notiTable += '<tr><td>' + moment(date).format('MMM Do YYYY h:mm a') + '</td>' +
                    '<td>' + noti.notification.replace("<strong>", "").replace("</strong>", "") + '</td> </tr>';
            });
            $("#notiTable").html(notiTable);
        }

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function loadMap(mapreport) {
            var colors = [];
            var legendHtml = "";
            $('#countryList').append('<option value="" selected>All Country</option>');
            var gdpData = {};
            mapreport.forEach(function (map) {
                let code = map.code.toLowerCase();
                let color = getRandomColor();
                $('#countryList').append('<option value="' + map.country + '">' + map.country + '</option>');

                colors[code] = color;
                gdpData[code] = parseInt(map.users);
                legendHtml += '<tr><td><div style="background:' + color + ';" class="map-legend-color"></div> </td>' +
                    '<td>' + map.country + '</td><td>' + (map.users ? map.users : 0) + '</td></tr>';
            })
            $("#legend").html(legendHtml);

            jQuery('#vmap').vectorMap({
                colors: colors,
                color: '#ffe6f3',
                borderColor: '#FFFFFF',
                map: 'world_en',
                backgroundColor: '#FFFFFF',
                hoverOpacity: 0.7,
                selectedColor: '#666666',
                enableZoom: true,
                showTooltip: true,
                normalizeFunction: 'polynomial',
                onLabelShow: function (event, el, code) {
                    el.html(el.html() + ' (Users - ' + (gdpData[code] ? gdpData[code] : 0)  + ')');
                }
            });
        }

        let selectedCountry = "";

        $('#countryList').on('change', function () {
            drawChart();
        });

        $('#groupList').on('change', function () {
            drawChart();
        });

        var chart;
        var maxVal = 0;
        function drawChart() {
            let country = $('#countryList').val();
            let group = $('#groupList').val();
            maxVal = 0;
            let gData = [];
            gData.push(['', 'Completed', 'In Progress', 'Yet to Start']);
            let courses = {};
            charData.forEach(function (module) {

                if (country && country != module.country) {
                    return;
                }
                if (group && group != module.groupid) {
                    return;
                }
                if (!courses[module.id]) {
                    courses[module.id] = {
                        completed: 0,
                        inprogress: 0,
                        yettostart: 0
                    };
                }
                let finalName = module.Name;
                let name = module.Name.split(":");
                if (name.length > 1) {
                    finalName = name[0].replace("ule", "");
                }
                courses[module.id].name = finalName;
                if (module.status === 3) {
                    courses[module.id].completed += module.users;
                }
                if (module.status === 2) {
                    courses[module.id].inprogress += module.users;
                }
                if (module.status === 1) {
                    courses[module.id].yettostart += module.users;
                }
            })
            courses = _.orderBy(courses, ['name'], ['asc'])
            for (var key in courses) {
                var value = courses[key];
                let max = Math.max(value.completed, value.inprogress, value.yettostart);
                if (maxVal < max) {
                    maxVal = max;
                }
                gData.push([value.name, value.completed || 0, value.inprogress || 0, value.yettostart || 0]);
            }

            if (gData.length > 1) {
                // Define the chart to be drawn.
                var data = google.visualization.arrayToDataTable(gData);
                var options = {
                    title: '',
                    legend: 'top',
                    colors: ['#6bc04b', '#f9a05e', '#692771'],
                    vAxis: {
                        viewWindow: {
                            max: (maxVal + (maxVal / 10))
                        }
                    },
                    bar: {
                        groupWidth: "75",
                        gap: 0
                    },

                    'chartArea': { 'width': '93%', 'height': '80%', },

                };


                let gap = 18;
                // Instantiate and draw the chart.
                let fullWidth = $("#chartContainer").width();
                let percentage = (fullWidth * 10) / 100;
                let finalWidth = fullWidth - percentage;
                let oneModule = (finalWidth / 10);
                let allModWidth = oneModule * (gData.length - 1);
                allModWidth += (gap * 10);
                if (allModWidth < fullWidth) {
                    allModWidth = fullWidth;
                }
                $("#chartt").css({ "width": allModWidth + "px" });


                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                    {
                        calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation"
                    },
                    2,
                    {
                        calc: "stringify",
                        sourceColumn: 2,
                        type: "string",
                        role: "annotation"
                    },
                    3,
                    {
                        calc: "stringify",
                        sourceColumn: 3,
                        type: "string",
                        role: "annotation"
                    }
                ]);

                chart = new google.visualization.ColumnChart(document.getElementById('chartt'));
                chart.draw(view, options);
            } else {
                $("#chartt").css({ "width": 100 + "%" });
                $("#chartt").html("<div class='no-records'>No records found</div>");
            }
        }

        function downloadChart() {
            let fName = $("#countryList").val();
            saveAs(chart.getImageURI(), (fName ? fName : "AllCountry") + ".png");
        }


    </script>
</body>
</html>
