--------LMS ADMIN Scripts-----------------------------
---Note: Som are new and some are altered
------------------------------------------------------
Alter table LMS_Groups
Add CreatedDate datetime
Go
CREATE procedure [dbo].[CreateGroup]   
 @GroupName varchar(50) =null,  
 @status varchar(50) = null,  
 @id bigint output  
as  
begin  
 set nocount on;  
  
 insert into LMS_Groups   
 (  
  [Name],  
  [Status],  
  [CreatedDate]  
 )  
 values  
 (  
  @GroupName,  
  @status,  
  GETDATE()  
 )  
  
 set @id = @@IDENTITY  
  
   select ID as GroupId from LMS_Groups where ID = @id  
end  
--------------------------------------------------------
Go
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SP_LMS_Update_GroupName]
@UserID int,
@GroupId int = null,
@GroupName varchar(50) = null

AS
BEGIN
	
UPDATE [dbo].[Users] set 
LMSGroupID = @GroupId, 
LMSGroupName = @GroupName 
WHERE User_Id = @UserID

END
Go

Alter table Users
Add IsDeleted bit
Go
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--exec SP_LMS_GET_USER_LIST_GROUP_WISE 'Authorized',''  
CREATE procedure SP_LMS_GET_USER_LIST_GROUP_WISE  
 @UserType nvarchar(50),  
 @GroupName nvarchar(50)=null  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET @GroupName = NULLIF(@GroupName,'NoGroup')  
 SET @GroupName = ISNULL(@GroupName,'')  
 if(@UserType='Active')  
 begin  
  select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID,   
        U.ReportingManagerID,U.LMSGroupName  
        from Users U   
		Outer APPLY (  
				Select top 1 C.*  From CourseUserMapping C   
                Where C.User_Id = U.User_Id AND C.Active = 1 
		)C  
  Where  C.User_Id IS NOT NULL AND ISNULL(U.LMSGroupName,'') = @GroupName  AND ISNULL(U.IsDeleted,0)=0  
 end  
 else   
 begin  
  select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID  
  ,U.LMSGroupName  
        from Users U   
   LEFT JOIN CourseUserMapping C ON C.User_Id = U.User_Id AND C.Active = 1   
  Where C.User_Id IS NULL AND ISNULL(U.IsDeleted,0)=0  AND ISNULL(U.LMSGroupName,'') = @GroupName  
 end  
END

Go

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetUserByEMailId] 
(
	@UserId varchar(260)
)
AS
BEGIN
	select  u.User_Id, u.First_Name,u.Last_Name,u.Email_ID,Region.ID as RegionId,dept.ID as DepartmentId,
	u.LMSGroupID,u.Designation,	rm.ID as ManagerId from Users u left join Region on Region.Name = u.RegionID
	left join Department dept on dept.Name = u.DepartmentID
	left join ReportingManager rm on rm.Name = u.ReportingManagerID 
	 where u.User_Id = @UserId
END
Go
------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ALter procedure [dbo].[CreateUser] 
 @firstname varchar(50) =null,
 @lastname varchar(50)= null,
 @email varchar(260) =null,
 @designation varchar(260) = null,
 @password varchar(20) =null,
 @isdeleted varchar(20) = null,
 @usertype int  = NULL,
 @referencecode nvarchar(250)  = NULL,
 @active bit  = NULL,
 @accesscode varchar(50)  = NULL,
 @department varchar(50),
 @region varchar(50),
 @reportingmanger varchar(50),
 @lmsgroup varchar(50) = Null,
 @lmsgroupId varchar(50) = Null,
 @userid bigint output
as
begin
 set nocount on;

 delete from Users where Email_ID = @email and Role = 4

 insert into Users 
 (
  [First_Name],
  [Last_Name],
  [Email_ID],
  [Designation],
  [Password],
  [Role],
  [Active], 
  [AccessCode],
  [Access_Token],
  [Created_Date],
  [LastModifiedDate],
  [DepartmentID],
  [RegionID],
  [ReportingManagerID],
  [LMSGroupName],
  [LMSGroupID],
  [IsDeleted]
 )
 values
 (
  @firstname,
  @lastname,
  @email,
  @designation,
  @password,
  @usertype,
  @active,
  @accesscode,
  @referencecode,
  GETDATE(),
  GETDATE(),
  @department,
  @region,
  @reportingmanger,
  @lmsgroup,
  @lmsgroupId,
  @isdeleted
 )

 set @userid = @@IDENTITY
   select User_Id as userid, First_Name as firstname, Last_Name as lastname from Users where User_Id = @userid
end

Go
----------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Exec SP_LMS_GET_NOT_ASSIGNED_USER_LIST 3
CREATE procedure [dbo].[SP_LMS_GET_NOT_ASSIGNED_USER_LIST]  
@courseId int  
as  
begin  
 Select Isnull(CM.Active,0) as IsActiveCourseUser,U.* from Users U
 Left Join CourseUserMapping CM ON CM.User_Id = U.User_Id AND Course_Id=@courseId 
end

Go

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE procedure [dbo].[SP_LMS_GET_NOT_ASSIGNED_GROUP_LIST]
@courseId int,
@fromDate DateTime = null,    
@endDate DateTime = null    
as
begin
SET @fromDate = NULLIF(@fromDate,'')    
 SET @endDate = NULLIF(@endDate,'')   
 
 IF @fromDate IS NOT NULL    
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));    
 IF @endDate IS NOT NULL    
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));   

 Select Isnull(CM.Active,0) as IsActiveCourseGroup,U.NoOfUser,GU.*,
 STUFF((SELECT ', ' + CAST(Course_Id AS VARCHAR(10)) [text()]    
    FROM CourseGroupMapping     
    WHERE Group_Id = GU.ID    
    FOR XML PATH(''), TYPE)    
   .value('.','NVARCHAR(MAX)'),1,2,' ') ModulesAssigned
  from LMS_Groups GU
 Left Join CourseGroupMapping CM ON CM.Group_Id = GU.ID AND Course_Id=@courseId 
 Outer Apply(    
   Select Count(*) as NoOfUser From Users U    
   Where U.LMSGroupID = Gu.ID  AND ISNULL(U.IsDeleted,0)=0   
  )U where @fromDate IS NULL OR (GU.CreatedDate >= @fromDate AND GU.CreatedDate <= @endDate) 
end

Go
ALter table Courses
Add IsDeleted bit

GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--EXEC SP_LMS_GET_COURSE_LIST '2018-05-01 00:00:00','2018-05-01 23:59:00'
--EXEC SP_LMS_GET_COURSE_LIST '2018-05-01','2018-05-01'
ALter PROCEDURE [dbo].[SP_LMS_GET_COURSE_LIST] 
@fromdate DateTime = null,
@todate DateTime = null
AS
BEGIN
	SET @fromdate = NULLIF(@fromdate,'')
	SET @todate = NULLIF(@todate,'')

	IF @fromdate IS NOT NULL
		SET @fromdate =  DATEADD(hh,24,DATEADD(d,-1,@fromdate));
	IF @todate IS NOT NULL
		set @todate = DATEADD(s,-1,DATEADD(d,1,@todate));
	SELECT * FROM Courses LC where ISNULL(LC.IsDeleted,0)=0
	AND (@fromdate IS NULL OR (LC.Created_Date >= @fromdate AND LC.Created_Date <= @todate))
	--LEFT JOIN LMS_SUB_COURSE LSC ON LC.Course_Id = LSC.Course_Id
	ORDER BY LC.Course_id asc

END

Go

Create PROCEDURE [dbo].[SP_LMS_Update_ActiveCOURSE] 	
	@Active	        VARCHAR(100) = NULL,
	@CourseID			int
AS
BEGIN
		UPDATE Courses SET Active = @Active
		WHERE Course_Id = @CourseID	
END

Go
-------------------------------------
--EXEC [SP_LMS_GET_RESOURCE_LIST] '2018-07-01 00:00:00','2018-08-01 23:59:00'
Alter PROCEDURE [dbo].[SP_LMS_GET_RESOURCE_LIST]
	@fromDate DateTime = null,  
	@endDate DateTime = null
AS
BEGIN
	SET NOCOUNT ON;  
	SET @fromDate = NULLIF(@fromDate,'')  
	SET @endDate = NULLIF(@endDate,'')  
  
	IF @fromDate IS NOT NULL  
		SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));  
	IF @endDate IS NOT NULL  
		set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));

	begin
		SELECT RL.ID, RL.Name,RL.Description, RL.CreatedDate,RL.ModifiedDate,
		RL.IsActive,RL.UploadURL, RLT.Name,RLT.ID,RL.ThumbnailImgURL FROM ResourceLibrary RL Join ResourceLibraryType RLT on RLT.ID=RL.ResourceLibraryTypeID
		where @fromDate IS NULL OR (RL.CreatedDate >= @fromDate AND RL.CreatedDate <= @endDate)		
		ORDER BY RL.ID desc
	end
END

Go
CREATE PROCEDURE [dbo].[SP_LMS_UPDATE_RESOURCE_ACTIVE_STATUS]
	@Active VARCHAR(100) = NULL,
	@ResourceID int
AS
BEGIN	
		UPDATE ResourceLibrary
			SET IsActive = @Active
		WHERE ID = @ResourceID	
END

Go

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[SP_LMS_GET_USER_LIST_TYPE_WISE]
	@UserType nvarchar(15)
AS
BEGIN
	SET NOCOUNT ON;

	if(@UserType='Active')
	begin
		select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, 
        U.ReportingManagerID, U.Active 
        from Users U 
			Outer APPLY (
				Select top 1 C.*  From CourseUserMapping C 
                Where C.User_Id = U.User_Id AND C.Active = 1 AND ISNULL(U.IsDeleted,0)=0
			)C
		Where C.User_Id IS NOT NULL
	end

	else if(@UserType='Registered')
	begin
		select U.User_Id,U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID, U.Active from Users U where ISNULL(U.IsDeleted,0)=0
	end

	else if(@UserType='Authorized')
	begin
		select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID, U.Active
        from Users U 
			LEFT JOIN CourseUserMapping C ON C.User_Id = U.User_Id AND C.Active = 1 
		Where C.User_Id IS NULL AND ISNULL(U.IsDeleted,0)=0
	end
END


Go
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SP_LMS_Delete_USER]
@UserID int
AS
Begin
UPDATE [dbo].[Users] set IsDeleted = 1 WHERE User_Id = @UserID
End

Go
--------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SP_LMS_Update_ActiveUSER] 	
	@Active	        VARCHAR(100) = NULL,
	@UserID			int
AS
BEGIN	
		UPDATE USERS SET
						Active = @Active
		WHERE USER_ID = @UserID	
END
-----------------------------------------------------------
Go
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--EXEC [SP_LMS_GET_GROUP_DETAILS] '2019-07-30 00:00:00','2019-07-30 23:59:00'  
CREATE PROCEDURE [dbo].[SP_LMS_GET_GROUP_DETAILS]  
@fromDate DateTime = null,  
@endDate DateTime = null  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET @fromDate = NULLIF(@fromDate,'')  
 SET @endDate = NULLIF(@endDate,'')  
  
 IF @fromDate IS NOT NULL  
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));  
 IF @endDate IS NOT NULL  
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));  
  
 begin  
  Select G.Name, G.Id as GroupId, U.NoOfUser, G.CreatedDate,  
  STUFF((SELECT ', ' + CAST(Course_Id AS VARCHAR(10)) [text()]  
    FROM CourseGroupMapping   
    WHERE Group_Id = G.ID  
    FOR XML PATH(''), TYPE)  
   .value('.','NVARCHAR(MAX)'),1,2,' ') ModulesAssigned  
  from LMS_Groups G  
  Outer Apply(  
   Select Count(*) as NoOfUser From Users U  
   Where U.LMSGroupID = G.ID  AND ISNULL(U.IsDeleted,0)=0 
  )U where @fromDate IS NULL OR (G.CreatedDate >= @fromDate AND G.CreatedDate <= @endDate)  
 end  
END

Go
----------------------------------------------
--------------------------------------------
ALTER TABLE Track
ADD PRIMARY KEY (TrackId);
GO
--------------------------------------------
ALTER TABLE Curriculum
ADD PRIMARY KEY (CurriculumId);
GO
--------------------------------------------
ALTER TABLE Courses
ADD PRIMARY KEY (Course_Id);
GO
--------------------------------------------
Alter table ResourceLibrary
Add TrackId bigint FOREIGN KEY REFERENCES Track(TrackId),
    CurriculumId bigint FOREIGN KEY REFERENCES Curriculum(CurriculumId),
    CoursesId bigint FOREIGN KEY REFERENCES Courses(Course_Id),
    WebLink nvarchar(MAX),
	VolPage nvarchar(MAX),
	Author nvarchar(MAX),
	Publishor nvarchar(MAX)
Go
--EXEC [SP_LMS_ADD_RESOURCE] '2019-08-06 00:00:00','2019-08-06 '  
ALTER PROCEDURE [dbo].[SP_LMS_ADD_RESOURCE] 
	@Name		VARCHAR(256),
	@Description VARCHAR(256),
	@TrackID BIGINT,
	@CurriculumID BIGINT,
	@CoursesID BIGINT,
	@TypeID BIGINT,
	@UploadURL VARCHAR(256),
	@ResourceID	BIGINT OUTPUT,
	@WebLink nvarchar(256) = null,
	@Author nvarchar(256) = null,
	@Vol_Iss_Pg nVARCHAR(256) = null,
	@Publication nVARCHAR(256) = null,
	--@CategoryName VARCHAR(100),
	--@SubCategoryName VARCHAR(100),
	@ThumbnailImgURL VARCHAR(100)


AS
BEGIN
	IF @ResourceID = 0
	BEGIN
		INSERT INTO ResourceLibrary(
						ResourceLibraryTypeID,
						Name,
						Description,
						CreatedDate,
						ModifiedDate,
						IsActive,
						UploadURL,
						--CategoryName,
						--SubCategoryName,
						ThumbnailImgURL,
						TrackId,
						CurriculumId,
						CoursesId,
						WebLink,
						VolPage,
						Author,
						Publication
						)
					VALUES (
						@TypeID,
						@Name,
						@Description,
						GETDATE(),
						GETDATE(),
						1,
						@UploadURL,
						--@CategoryName,
						--@SubCategoryName,
						@ThumbnailImgURL,
						@TrackID,
						@CurriculumID,
						@CoursesID,
						@WebLink,
						@Author,
						@Vol_Iss_Pg,
						@Publication
					)
		SELECT @ResourceID = @@IDENTITY
	END
	ELSE
	BEGIN			
		UPDATE ResourceLibrary SET ResourceLibraryTypeID =@TypeID, Name = @Name, Description = @Description, ModifiedDate = GETDATE(), UploadURL = @UploadURL,ThumbnailImgURL=@ThumbnailImgURL
		WHERE ID = @ResourceID
	END
END
Go
-------------------------------------------------------------------
ALTER PROCEDURE [dbo].[SP_LMS_GET_USER_LIST_TYPE_WISE]
	@UserType nvarchar(15),
	@fromdate DateTime = null,
    @todate DateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	SET @fromdate = NULLIF(@fromdate,'')
	SET @todate = NULLIF(@todate,'')

	IF @fromdate IS NOT NULL
		SET @fromdate =  DATEADD(hh,24,DATEADD(d,-1,@fromdate));
	IF @todate IS NOT NULL
		set @todate = DATEADD(s,-1,DATEADD(d,1,@todate));

	if(@UserType='Active')
	begin
		select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, 
        U.ReportingManagerID, U.Active 
        from Users U 
			Outer APPLY (
				Select top 1 C.*  From CourseUserMapping C 
                Where C.User_Id = U.User_Id AND C.Active = 1 AND ISNULL(U.IsDeleted,0)=0
			)C
		Where C.User_Id IS NOT NULL AND (@fromdate IS NULL OR (U.Created_Date >= @fromdate AND U.Created_Date <= @todate))
	end

	else if(@UserType='Registered')
	begin
		select U.User_Id,U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID, U.Active from Users U where ISNULL(U.IsDeleted,0)=0 AND (@fromdate IS NULL OR (U.Created_Date >= @fromdate AND U.Created_Date <= @todate))
	end

	else if(@UserType='Authorized')
	begin
		select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID, U.Active
        from Users U 
			LEFT JOIN CourseUserMapping C ON C.User_Id = U.User_Id AND C.Active = 1 
		Where C.User_Id IS NULL AND ISNULL(U.IsDeleted,0)=0 AND (@fromdate IS NULL OR (U.Created_Date >= @fromdate AND U.Created_Date <= @todate))
	end
END
Go
-------------------------------------------------------------------------------------

ALTER procedure [dbo].[GetResourceTrack]   
as  
begin  
 select * from Track order by TrackId;
end 
Go
-----------------------------------------------------------------------------------

Create procedure [dbo].[GetResourseById]
     @ResourseId bigint
as
begin
   select rl.Name,rl.Description,rl.UploadURL,rl.ThumbnailImgURL,rl.TrackId,rl.CurriculumId,
   rl.CoursesId,rl.VolPage,rl.WebLink,rl.Author,rl.Publication,rlt.Name , rl.ID, rl.ResourceLibraryTypeID, rlt.ID
   from ResourceLibrary rl
   right join ResourceLibraryType rlt on rl.ResourceLibraryTypeID = rlt.ID where rl.ID = @ResourseId
end
Go
-------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[SP_LMS_ADD_RESOURCE] 
	@Name		VARCHAR(256),
	@Description VARCHAR(256),
	@TrackID BIGINT,
	@CurriculumID BIGINT,
	@CoursesID BIGINT,
	@TypeID BIGINT,
	@UploadURL VARCHAR(256),
	@ResourceID	BIGINT OUTPUT,
	@WebLink nvarchar(256) = null,
	@Author nvarchar(256) = null,
	@Vol_Iss_Pg nVARCHAR(256) = null,
	@Publication nVARCHAR(256) = null,
	--@CategoryName VARCHAR(100),
	--@SubCategoryName VARCHAR(100),
	@ThumbnailImgURL VARCHAR(100)


AS
BEGIN
	IF @ResourceID = 0
	BEGIN
		INSERT INTO ResourceLibrary(
						ResourceLibraryTypeID,
						Name,
						Description,
						CreatedDate,
						ModifiedDate,
						IsActive,
						UploadURL,
						--CategoryName,
						--SubCategoryName,
						ThumbnailImgURL,
						TrackId,
						CurriculumId,
						CoursesId,
						WebLink,
						VolPage,
						Author,
						Publication
						)
					VALUES (
						@TypeID,
						@Name,
						@Description,
						GETDATE(),
						GETDATE(),
						1,
						@UploadURL,
						--@CategoryName,
						--@SubCategoryName,
						@ThumbnailImgURL,
						@TrackID,
						@CurriculumID,
						@CoursesID,
						@WebLink,
						@Vol_Iss_Pg,
						@Author,
						@Publication
					)
		SELECT @ResourceID = @@IDENTITY
	END
	ELSE
	BEGIN			
		UPDATE ResourceLibrary SET ResourceLibraryTypeID =@TypeID, Name = @Name, Description = @Description, ModifiedDate = GETDATE(), UploadURL = @UploadURL,ThumbnailImgURL=@ThumbnailImgURL, WebLink = @WebLink, VolPage = @Vol_Iss_Pg, Author = @Author, Publication = @Publication, TrackId = @TrackID, CurriculumId = @CurriculumID, CoursesId = @CoursesID
		WHERE ID = @ResourceID
	END
END
Go
----------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[SP_LMS_CREATE_COURSE] 
	@Name		VARCHAR(200),
	@Description NVARCHAR(MAX),
	@Image NVARCHAR(MAX),
	@CompletionDate datetime2(7) = null,
	@DueDate datetime2(7) = null,
	@Duration BIGINT,
	@CourseID	BIGINT OUTPUT

AS
BEGIN
	IF @CourseID = 0
	BEGIN
		INSERT INTO Courses(
						Name,
						Description,
						Created_Date,
						Modified_Date,
						Active,
						Course_Status,
						Course_Image,
						IsLaunched,
						CompletionDate,
						Duration,
						DueDate
						)
					VALUES (
						@Name,
						@Description,
						GETDATE(),
						GETDATE(),
						1,
						1,
						@Image,
						0,
						@CompletionDate,
						@Duration,
						@DueDate
					)
		SELECT @CourseID = @@IDENTITY
	END
	ELSE
	BEGIN
		
		IF @Image IS NULL
			select @Image = Course_Image from Courses where Course_Id = @CourseID

		UPDATE Courses SET Name = @Name, Description = @Description, Modified_Date = GETDATE(), Course_Image = @Image,CompletionDate=@CompletionDate,Duration=@Duration,DueDate=@DueDate
		WHERE Course_Id = @CourseID

		UPDATE CourseUserMapping SET Completion_Date=@CompletionDate
		WHERE Course_Id = @CourseID

	END
END
Go
--------------------------------------------------------------------------------
CREATE procedure [dbo].[GetResourceLibraryType] 
as
begin
	SELECT * FROM ResourceLibraryType order by ID desc;
end
Go
CREATE procedure [dbo].[GetResourceCurriculum]   
as  
begin  
 select * from Curriculum order by CurriculumId;
end  
 GO
 CREATE procedure [dbo].[GetResourceCourses]   
as  
begin  
 select Course_Id,Name from Courses order by Course_Id;
end  
Go
Create procedure [dbo].[SP_LMS_DELETE_Resource] 
	@ResourceID bigint
as
begin
	Delete from ResourceLibrary where ID=@ResourceID;
end
Go

--EXEC [SP_LMS_GET_Assign_Resources_LIST] '',''
CREATE PROCEDURE [dbo].[SP_LMS_GET_Assign_Resources_LIST]
	@fromDate DateTime = null,  
	@endDate DateTime = null
AS
BEGIN
	SET NOCOUNT ON;  
	SET @fromDate = NULLIF(@fromDate,'')  
	SET @endDate = NULLIF(@endDate,'')  
  
	IF @fromDate IS NOT NULL  
		SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));  
	IF @endDate IS NOT NULL  
		set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));

	begin
		SELECT RL.ID,
		 RL.Name,
		 RL.Description, 
		 RL.CreatedDate,
		 RL.ModifiedDate,
		RL.IsActive,
		RL.UploadURL,
		 RLT.Name,
		 RLT.ID,
		 RL.ThumbnailImgURL,
		 CRM.isRequired
		  FROM ResourceLibrary RL 
		  Join ResourceLibraryType RLT on RLT.ID=RL.ResourceLibraryTypeID
		    inner join CourseResourceMapping CRM on CRM.ResourceId = RL.ID
		where @fromDate IS NULL OR (RL.CreatedDate >= @fromDate AND RL.CreatedDate <= @endDate)
		--LEFT JOIN LMS_SUB_COURSE LSC ON LC.Course_Id = LSC.Course_Id
		ORDER BY RL.ID desc
	end
END
Go
Create procedure GetTrackCurriculumByTrackId     
@TrackId bigint
as    
begin    
 select C.CurriculumId,C.CurriculumName from TrackCurriculumMapping TM
 Inner Join [dbo].[Curriculum] C on C.CurriculumId = TM.CurriculumId
 Where TM.TrackId=@TrackId;  
end    
Go

Create procedure GetCurriculumCourseByCurriculumId    
@CurriculumId bigint
as    
begin     
 select C.Course_Id,C.Name from CurriculumCourseMapping CM
 Inner Join Courses C on C.Course_Id = CM.CourseId
 Where CM.CurriculumId=@CurriculumId; 
end
Go
--------------------------------------------------------------------------------

ALTER procedure [dbo].[CreateGroup]   
 @GroupName varchar(50) =null,  
 @status varchar(50) = null,  
 @id bigint output  
as  
begin  
 set nocount on;  
  IF @id = 0
	BEGIN
 insert into LMS_Groups   
 (  
  [Name],  
  [Status],  
  [CreatedDate]  
 )  
 values  
 (  
  @GroupName,  
  @status,  
  GETDATE()  
 )  
  
	set @id = @@IDENTITY    
  
   END
	ELSE
	BEGIN	
			UPDATE [dbo].[LMS_Groups] set Name = @GroupName, Status = @status 
		    WHERE ID = @id
			
END
 select ID from LMS_Groups where ID = @id  
END
Go
GO
Create PROCEDURE [dbo].[SP_LMS_ASSIGN_RESOURCE_TO_COURSE]  
 @CourseId  BIGINT,  
 @ResourceID BIGINT,  
 @isRequired bit 
   
AS  
BEGIN  
	IF NOT EXISTS(SELECT 1 FROM CourseResourceMapping WITH(NOLOCK)
	          WHERE CourseId = @CourseId AND ResourceId = @ResourceId)
	    BEGIN
	        INSERT INTO [dbo].[CourseResourceMapping]
				([CourseId]
				,[ResourceId]
				,[isRequired])
					VALUES
				(@CourseId
				,@ResourceId
				,@isRequired)
	    END	   
END  
Go
--EXEC [SP_LMS_GET_Assign_Resources_LIST] '',''    
Alter PROCEDURE [dbo].[SP_LMS_GET_Assign_Resources_LIST]     
 @fromDate DateTime = null,      
 @endDate DateTime = null    
AS    
BEGIN    
 SET NOCOUNT ON;      
 SET @fromDate = NULLIF(@fromDate,'')      
 SET @endDate = NULLIF(@endDate,'')      
      
 IF @fromDate IS NOT NULL      
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));      
 IF @endDate IS NOT NULL      
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));    
    
 begin    
  SELECT RL.ID,    
   RL.Name,    
   RL.Description,     
   RL.CreatedDate,    
   RL.ModifiedDate,    
  RL.IsActive,    
  RL.UploadURL,    
   RLT.Name,    
   RLT.ID,    
   RL.ThumbnailImgURL,    
   CRM.isRequired   ,
   CourseId
    FROM ResourceLibrary RL     
    Join ResourceLibraryType RLT on RLT.ID=RL.ResourceLibraryTypeID    
    Left join CourseResourceMapping CRM on CRM.ResourceId = RL.ID    
  where @fromDate IS NULL OR (RL.CreatedDate >= @fromDate AND RL.CreatedDate <= @endDate)    
  --LEFT JOIN LMS_SUB_COURSE LSC ON LC.Course_Id = LSC.Course_Id    
  ORDER BY RL.ID desc    
 end    
END  

GO
Create PROCEDURE [dbo].[SP_LMS_UNASSIGN_RESOURCE_TO_COURSE]  
 @CourseId  BIGINT,  
 @ResourceID BIGINT   
AS  
BEGIN  
	Delete FROM CourseResourceMapping  WHERE CourseId = @CourseId AND ResourceId = @ResourceId	    
END  
Go

Alter PROCEDURE [dbo].[SP_LMS_Update_GroupName]    
@UserID int,    
@GroupId int = null,    
@GroupName varchar(50) = null,    
@OldGroupId int = null  
AS    
BEGIN    
     
UPDATE [dbo].[Users] set     
LMSGroupID = @GroupId,     
LMSGroupName = @GroupName     
WHERE User_Id = @UserID    
  
  IF NULLIF(@OldGroupId,0) IS NOT NULL 
	SET @GroupId=@OldGroupId 
   

 INSERT INTO CourseUserMapping (    
     Course_Id,    
     User_Id,    
     Active,    
     Assign_Date,    
     Completion_Date,    
     User_Course_Status,    
     LMSGroupID    
     )    
 Select CG.Course_Id,@UserID,1, GETDATE(), NULL, 1,@GroupId    
 from [dbo].[CourseGroupMapping] CG   
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=@UserID  
 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL  
  
  
 Insert into UserSubCourseDetails (    
     [User_Id]    
       ,[Subcourse_Id]    
      ,[Percentage]    
      ,[Sub_Course_Status]    
      ,[Completion_Date]    
      ,[LastModifiedDate]    
      ,[TimeSpentInSeconds]    
      ,[isAlreadyCompleted]    
      ,[Attempts])      
      Select @UserID, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0   
   from SubCourse SC   
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID  
   where CG.Group_Id = @GroupId  
    
END  
   Go 
    
    
Alter PROCEDURE [dbo].[SP_LMS_EDIT_USER]   
 @FirstName  VARCHAR(200) = NULL,  
 @LastName  VARCHAR(200) = NULL,  
 @Designation VARCHAR(100) = NULL,  
 @Region  VARCHAR(200) = NULL,  
 @GroupName  VARCHAR(200) = NULL,  
 @GroupId  VARCHAR(200) = NULL,  
 @Department VARCHAR(100) = NULL,  
 @Manager VARCHAR(100) = NULL,  
 @UserID   BIGINT  
AS  
BEGIN  

	Declare @Old@GroupId VARCHAR(200);
	Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;
   
  UPDATE USERS SET     
      First_Name = @FirstName,   
      Last_Name = @LastName,   
      Designation = @Designation,  
      DepartmentID = @Department,  
      RegionID = @Region,  
      LMSGroupID = @GroupId,  
      LMSGroupName = @GroupName,  
      ReportingManagerID = @Manager  
  WHERE USER_ID = @UserID  
  

  IF @Old@GroupId IS NOT NULL AND @Old@GroupId <> @GroupId
  BEGIN
		Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid 
		Delete U from UserSubCourseDetails U 
			 Inner Join SubCourse SC ON SC.Sub_Course_Id = U.Subcourse_Id 
			 Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID    		
			Where U.User_Id=@userid AND CG.Group_Id = @Old@GroupId 
  END

   INSERT INTO CourseUserMapping (      
     Course_Id,      
     User_Id,      
     Active,      
     Assign_Date,      
     Completion_Date,      
     User_Course_Status,      
     LMSGroupID      
     )      
 Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@GroupId      
 from [dbo].[CourseGroupMapping] CG     
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=@userid    
 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL    
    
    
 Insert into UserSubCourseDetails (      
     [User_Id]      
       ,[Subcourse_Id]      
      ,[Percentage]      
      ,[Sub_Course_Status]      
      ,[Completion_Date]      
      ,[LastModifiedDate]      
      ,[TimeSpentInSeconds]      
      ,[isAlreadyCompleted]      
      ,[Attempts])        
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0     
   from SubCourse SC     
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID    
   where CG.Group_Id = @GroupId    
   
END
Go

Alter PROCEDURE [dbo].[SP_LMS_EDIT_USER]   
 @FirstName  VARCHAR(200) = NULL,  
 @LastName  VARCHAR(200) = NULL,  
 @Designation VARCHAR(100) = NULL,  
 @Region  VARCHAR(200) = NULL,  
 @GroupName  VARCHAR(200) = NULL,  
 @GroupId  VARCHAR(200) = NULL,  
 @Department VARCHAR(100) = NULL,  
 @Manager VARCHAR(100) = NULL,  
 @UserID   BIGINT  
AS  
BEGIN  

	Declare @Old@GroupId VARCHAR(200);
	Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;
   
  UPDATE USERS SET     
      First_Name = @FirstName,   
      Last_Name = @LastName,   
      Designation = @Designation,  
      DepartmentID = @Department,  
      RegionID = @Region,  
      LMSGroupID = @GroupId,  
      LMSGroupName = @GroupName,  
      ReportingManagerID = @Manager  
  WHERE USER_ID = @UserID  
  

  IF @Old@GroupId IS NOT NULL AND @Old@GroupId <> @GroupId
  BEGIN
		Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid 
		Delete U from UserSubCourseDetails U 
			 Inner Join SubCourse SC ON SC.Sub_Course_Id = U.Subcourse_Id 
			 Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID    		
			Where U.User_Id=@userid AND CG.Group_Id = @Old@GroupId 
  END

   INSERT INTO CourseUserMapping (      
     Course_Id,      
     User_Id,      
     Active,      
     Assign_Date,      
     Completion_Date,      
     User_Course_Status,      
     LMSGroupID      
     )      
 Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@GroupId      
 from [dbo].[CourseGroupMapping] CG     
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=@userid    
 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL    
    
    
 Insert into UserSubCourseDetails (      
     [User_Id]      
       ,[Subcourse_Id]      
      ,[Percentage]      
      ,[Sub_Course_Status]      
      ,[Completion_Date]      
      ,[LastModifiedDate]      
      ,[TimeSpentInSeconds]      
      ,[isAlreadyCompleted]      
      ,[Attempts])        
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0     
   from SubCourse SC     
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID    
   where CG.Group_Id = @GroupId    
   
END
Go

Alter PROCEDURE [dbo].[SP_LMS_ASSIGN_RESOURCE_TO_COURSE]    
 @CourseId  BIGINT,    
 @ResourceID BIGINT,    
 @isRequired bit   
     
AS    
BEGIN    
	IF NOT EXISTS(SELECT 1 FROM CourseResourceMapping WITH(NOLOCK)  
           WHERE CourseId = @CourseId AND ResourceId = @ResourceId)  
     BEGIN  
         INSERT INTO [dbo].[CourseResourceMapping]  
			([CourseId]  
			,[ResourceId]  
			,[isRequired])  
			 VALUES  
			(@CourseId  
			,@ResourceId  
			,@isRequired)  
     END   
	 ELSE
	 BEGIN
		Update [CourseResourceMapping] SET [isRequired]=@isRequired WHERE CourseId = @CourseId AND ResourceId = @ResourceId
	 END
END    
Go
Alter procedure [dbo].[SP_LMS_DELETE_COURSE]   
 @CourseID int  
as  
begin  
 --UPDATE [dbo].[Courses] set IsDeleted = 1 WHERE Course_Id = @CourseID  
 Delete from Courses  WHERE Course_Id = @CourseID  
 Delete from CourseUserMapping where Course_Id = @CourseID 
 Delete from UserSubCourseDetails where Subcourse_Id in (Select Sub_Course_Id from SubCourse where Course_ID = @CourseID) 
end
Go


---------------------------------------

INSERT INTO [dbo].[LMS_Groups]
           ([Name]
           ,[Status]
           ,[CreatedDate])
     VALUES
           ('NoGroup'
           ,1
           ,GETDATE())
GO

ALter PROCEDURE [dbo].[SP_LMS_IS_EMAIL_EXIST] 
	-- Add the parameters for the stored procedure here
	@EMailID VARCHAR(260),
	@Status INT OUTPUT 
AS
BEGIN
	SET @Status = 1 -- Set default to not exits
    SELECT @Status = 0 FROM Users
    WHERE Email_ID = @EMailID AND ISNULL(IsDeleted,0)=0

END
Go
--exec SP_LMS_GET_USER_LIST_GROUP_WISE 'Authorized',''  
Alter procedure SP_LMS_GET_USER_LIST_GROUP_WISE  
 @UserType nvarchar(50),  
 @GroupName nvarchar(50)=null  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET @GroupName = NULLIF(@GroupName,'NoGroup')  
 SET @GroupName = ISNULL(@GroupName,'')  
 if(@UserType='Active')  
 begin  
  select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID,   
        U.ReportingManagerID,U.LMSGroupName  
        from Users U   
		Outer APPLY (  
				Select top 1 C.*  From CourseUserMapping C   
                Where C.User_Id = U.User_Id AND C.LMSGroupID=C.LMSGroupID AND C.Active = 1 
		)C  
  Where  C.User_Id IS NOT NULL AND ISNULL(U.LMSGroupName,'') = @GroupName  AND ISNULL(U.IsDeleted,0)=0  
 end  
 else   
 begin  
  select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID  
  ,U.LMSGroupName  
        from Users U   
   LEFT JOIN CourseUserMapping C ON C.User_Id = U.User_Id AND C.LMSGroupID=C.LMSGroupID AND C.Active = 1   
  Where C.User_Id IS NULL AND ISNULL(U.IsDeleted,0)=0  AND ISNULL(U.LMSGroupName,'') = @GroupName  
 end  
END
Go
--exec SP_LMS_GET_USER_LIST_GROUP_WISE '','Group_C'      
--exec SP_LMS_GET_USER_LIST_GROUP_WISE '','Group_B',1      
Alter procedure SP_LMS_GET_USER_LIST_GROUP_WISE      
 @UserType nvarchar(50),      
 @GroupName nvarchar(50)=null  ,    
 @IsFromAssign bit = null  
AS      
BEGIN      
 SET NOCOUNT ON;      
 SET @isFromAssign = NULLIF(@isFromAssign,0);  
 print @GroupName
 if(@UserType='Active')      
 begin      
  select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID,       
        U.ReportingManagerID,U.LMSGroupName      
        from Users U       
  Outer APPLY (      
    Select top 1 C.*  From CourseUserMapping C       
                Where C.User_Id = U.User_Id AND C.LMSGroupID=U.LMSGroupID AND C.Active = 1     
  )C      
  Where  C.User_Id IS NOT NULL AND (ISNULL(U.LMSGroupName,'') = @GroupName OR (@isFromAssign=1 AND (ISNULL(U.LMSGroupName,'')='' OR U.LMSGroupName='NoGroup' )))  
  AND ISNULL(U.IsDeleted,0)=0      
 end      
 else       
 begin      
  select U.User_Id, U.First_Name,U.Last_Name,U.Email_ID, U.Designation, U.DepartmentID, U.ReportingManagerID      
  ,U.LMSGroupName      
        from Users U       
   LEFT JOIN CourseUserMapping C ON C.User_Id = U.User_Id AND C.LMSGroupID=U.LMSGroupID AND C.Active = 1          
  Where C.User_Id IS NULL AND ISNULL(U.IsDeleted,0)=0     
   AND (ISNULL(U.LMSGroupName,'') = @GroupName OR (@isFromAssign=1 AND (ISNULL(U.LMSGroupName,'')='' OR U.LMSGroupName='NoGroup' )))   
 end      
END 

Go
---Assign Users to Course With Group
ALter PROCEDURE [dbo].[SP_LMS_ASSIGN_COURSE_TO_GROUP]  
 @CourseID BIGINT,  
 @GroupID BIGINT   
AS  
BEGIN  
 DECLARE @Status BIGINT  
 SET @Status = 0  
   
 SELECT @Status = Course_Group_Id FROM CourseGroupMapping WHERE  Group_id=@GroupID AND Course_id=@CourseID  
 print @Status;  
 IF ISNULL(@Status,0) = 0  
 BEGIN  
  INSERT INTO CourseGroupMapping(  
     Course_Id,  
     Group_Id,  
     Active,  
     Assign_Date  
     )  
     VALUES (  
     @CourseID,  
     @GroupID,  
     1,  
     GETDATE()  
     )  
		 INSERT INTO CourseUserMapping (    
		 Course_Id,    
		 User_Id,    
		 Active,    
		 Assign_Date,    
		 Completion_Date,    
		 User_Course_Status,    
		 LMSGroupID    
		 )    
		 Select CG.Course_Id,U.User_Id,1, GETDATE(), NULL, 1,@GroupId    
		 from [dbo].[CourseGroupMapping] CG   
		 Left Join Users U on U.LMSGroupID = CG.Group_Id
		 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=U.User_Id  
		 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL  
  
  
		 Insert into UserSubCourseDetails (    
			 [User_Id]    
			   ,[Subcourse_Id]    
			  ,[Percentage]    
			  ,[Sub_Course_Status]    
			  ,[Completion_Date]    
			  ,[LastModifiedDate]    
			  ,[TimeSpentInSeconds]    
			  ,[isAlreadyCompleted]    
			  ,[Attempts])      
			  Select U.User_Id, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0   
		   from SubCourse SC   
		   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID  
		   Left Join Users U on U.LMSGroupID = CG.Group_Id
		   where CG.Group_Id = @GroupId  

 END  
 ELSE  
 BEGIN  
  UPDATE CourseGroupMapping SET  
    Active = 1  
    WHERE @Status = Course_Group_Id  
 END  
END  
Go
ALter procedure [dbo].[CreateGroup]   
 @GroupName varchar(50) =null,  
 @status varchar(50) = null,  
 @id bigint output  
as  
begin  
 set nocount on;  
  IF @id = 0
	BEGIN
		insert into LMS_Groups   
		(  
		 [Name],  
		 [Status],  
		 [CreatedDate]  
		)  
		values  
		(  
		 @GroupName,  
		 @status,  
		 GETDATE()  
		)    
		set @id = @@IDENTITY      
   END
	ELSE
	BEGIN	
			UPDATE [dbo].[LMS_Groups] set Name = @GroupName, Status = @status 
		    WHERE ID = @id			

			UPDATE [dbo].[Users]
			SET [LMSGroupName] = @GroupName      
			WHERE LMSGroupID=@id
	END
 select ID from LMS_Groups where ID = @id  
END  
 
Go
Alter proc [dbo].[RemoveCourseFromGroup]    
(    
 @groupId bigint,    
 @CourseID bigint    
)    
as    
Begin    
 Delete from CourseGroupMapping where Group_Id=@groupId and Course_Id=@CourseID;    
     
 Delete CM from CourseUserMapping CM    
 Left Join CourseGroupMapping CG on CG.Course_Id=CM.Course_Id  AND CG.Group_Id = CM.LMSGroupID    
 Where CM.LMSGroupID=@groupId AND CM.Course_Id = @CourseID     
     
 Delete from UserSubCourseDetails    
 where Subcourse_Id in (    
  Select Sub_Course_Id from SubCourse SC    
  Inner Join CourseGroupMapping CG on CG.Course_Id=SC.Course_Id       
  where CG.Group_Id=@groupId AND CG.Course_ID = @CourseID    
 )     
End   
Go
Create PROCEDURE [dbo].[SP_LMS_IS_GroupName_EXIST] 
	-- Add the parameters for the stored procedure here
	@GroupName VARCHAR(260),
	@Status INT OUTPUT 
AS
BEGIN
	SET @Status = 1 -- Set default to not exits
    SELECT @Status = 0 FROM LMS_Groups
    WHERE Name = @GroupName 

END
Go
ALter PROCEDURE [dbo].[SP_LMS_ASSIGN_COURSE_TO_GROUP]  
 @CourseID BIGINT,  
 @GroupID BIGINT   
AS  
BEGIN  
 DECLARE @Status BIGINT  
 SET @Status = 0  
   
 SELECT @Status = Course_Group_Id FROM CourseGroupMapping WHERE  Group_id=@GroupID AND Course_id=@CourseID  
 print @Status;  
 IF ISNULL(@Status,0) = 0  
 BEGIN  
  INSERT INTO CourseGroupMapping(  
     Course_Id,  
     Group_Id,  
     Active,  
     Assign_Date  
     )  
     VALUES (  
     @CourseID,  
     @GroupID,  
     1,  
     GETDATE()  
     )  
		 INSERT INTO CourseUserMapping (    
		 Course_Id,    
		 User_Id,    
		 Active,    
		 Assign_Date,    
		 Completion_Date,    
		 User_Course_Status,    
		 LMSGroupID    
		 )    
		 Select CG.Course_Id,U.User_Id,1, GETDATE(), NULL, 1,@GroupId    
		 from [dbo].[CourseGroupMapping] CG   
		 Left Join Users U on U.LMSGroupID = CG.Group_Id
		 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=U.User_Id  
		 Where CG.Group_Id = @GroupId AND CM.Course_User_Id IS NULL  
  
  
		 Insert into UserSubCourseDetails (    
			 [User_Id]    
			   ,[Subcourse_Id]    
			  ,[Percentage]    
			  ,[Sub_Course_Status]    
			  ,[Completion_Date]    
			  ,[LastModifiedDate]    
			  ,[TimeSpentInSeconds]    
			  ,[isAlreadyCompleted]    
			  ,[Attempts])      
			  Select U.User_Id, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0   
		   from SubCourse SC   
		   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID  
		   Left Join Users U on U.LMSGroupID = CG.Group_Id
		   where CG.Group_Id = @GroupId  

 END  
 ELSE  
 BEGIN  
  UPDATE CourseGroupMapping SET  
    Active = 1  
    WHERE @Status = Course_Group_Id  
 END  
END
  
Go
ALter PROCEDURE [dbo].[SP_LMS_EDIT_USER]     
 @FirstName  VARCHAR(200) = NULL,    
 @LastName  VARCHAR(200) = NULL,    
 @Designation VARCHAR(100) = NULL,    
 @Region  VARCHAR(200) = NULL,    
 @GroupName  VARCHAR(200) = NULL,    
 @GroupId  VARCHAR(200) = NULL,    
 @Department VARCHAR(100) = NULL,    
 @Manager VARCHAR(100) = NULL,    
 @UserID   BIGINT    
AS    
BEGIN    
  
 Declare @Old@GroupId VARCHAR(200);  
 Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;  
     
  UPDATE USERS SET       
      First_Name = @FirstName,     
      Last_Name = @LastName,     
      Designation = @Designation,    
      DepartmentID = @Department,    
      RegionID = @Region,    
      LMSGroupID = @GroupId,    
      LMSGroupName = @GroupName,    
      ReportingManagerID = @Manager    
  WHERE USER_ID = @UserID    
    
  
  IF @Old@GroupId IS NOT NULL AND @Old@GroupId <> @GroupId  
  BEGIN  
		Delete U from UserSubCourseDetails  U  
		where Subcourse_Id in (    
			Select Sub_Course_Id from SubCourse SC    
			Inner Join CourseUserMapping CG on CG.Course_Id=SC.Course_Id       
			where CG.LMSGroupID=@Old@GroupId  AND CG.User_Id = @UserID    
		)  
		AND User_Id=@UserID
		Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid   	
		
  END  
  
   INSERT INTO CourseUserMapping (        
     Course_Id,        
     User_Id,        
     Active,        
     Assign_Date,        
     Completion_Date,        
     User_Course_Status,        
     LMSGroupID        
     )        
 Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@GroupId        
 from [dbo].[CourseGroupMapping] CG       
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=@userid      
 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL      
      
      
 Insert into UserSubCourseDetails (        
     [User_Id]        
       ,[Subcourse_Id]        
      ,[Percentage]        
      ,[Sub_Course_Status]        
      ,[Completion_Date]        
      ,[LastModifiedDate]        
      ,[TimeSpentInSeconds]        
      ,[isAlreadyCompleted]        
      ,[Attempts])          
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0       
   from SubCourse SC       
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID      
   where CG.Group_Id = @GroupId      
     
END  

GO
ALter PROCEDURE [dbo].[SP_LMS_EDIT_USER]     
 @FirstName  VARCHAR(200) = NULL,    
 @LastName  VARCHAR(200) = NULL,    
 @Designation VARCHAR(100) = NULL,    
 @Region  VARCHAR(200) = NULL,    
 @GroupName  VARCHAR(200) = NULL,    
 @GroupId  VARCHAR(200) = NULL,    
 @Department VARCHAR(100) = NULL,    
 @Manager VARCHAR(100) = NULL,    
 @UserID   BIGINT    
AS    
BEGIN    
  
 Declare @Old@GroupId VARCHAR(200);  
 Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;  

 IF @Old@GroupId IS NOT NULL AND @Old@GroupId <> @GroupId  
  BEGIN  
		Delete U from UserSubCourseDetails  U  
		where Subcourse_Id in (    
			Select Sub_Course_Id from SubCourse SC    
			Inner Join CourseUserMapping CG on CG.Course_Id=SC.Course_Id       
			where CG.LMSGroupID=@Old@GroupId  AND CG.User_Id = @UserID    
		)  
		AND User_Id=@UserID
		Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid   	
		
  END  
  


 IF @GroupId = 0 
 BEGIN
	Select @GroupId = ID,@GroupName=[Name] FROM [dbo].[LMS_Groups] Where [Name]='NoGroup'	
 END
     
  UPDATE USERS SET       
      First_Name = @FirstName,     
      Last_Name = @LastName,     
      Designation = @Designation,    
      DepartmentID = @Department,    
      RegionID = @Region,    
      LMSGroupID = @GroupId,    
      LMSGroupName = @GroupName,    
      ReportingManagerID = @Manager    
  WHERE USER_ID = @UserID    
    
  
  
   INSERT INTO CourseUserMapping (        
     Course_Id,        
     User_Id,        
     Active,        
     Assign_Date,        
     Completion_Date,        
     User_Course_Status,        
     LMSGroupID        
     )        
 Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@GroupId        
 from [dbo].[CourseGroupMapping] CG       
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=@userid      
 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL      
      
      
 Insert into UserSubCourseDetails (        
     [User_Id]        
       ,[Subcourse_Id]        
      ,[Percentage]        
      ,[Sub_Course_Status]        
      ,[Completion_Date]        
      ,[LastModifiedDate]        
      ,[TimeSpentInSeconds]        
      ,[isAlreadyCompleted]        
      ,[Attempts])          
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0       
   from SubCourse SC       
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID      
   where CG.Group_Id = @GroupId      
     
END  
Go
Alter PROCEDURE [dbo].[SP_LMS_Update_GroupName]      
@UserID int,      
@GroupId int = null,      
@GroupName varchar(50) = null,      
@OldGroupId int = null    
AS      
BEGIN    

	 Declare @Old@GroupId VARCHAR(200);  
	 Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;   
 
	 IF @Old@GroupId IS NOT NULL AND (@Old@GroupId <> @GroupId  OR @Old@GroupId<>@OldGroupId  )
	 BEGIN  
	 	Delete U from UserSubCourseDetails  U  
	 	where Subcourse_Id in (    
	 		Select Sub_Course_Id from SubCourse SC    
	 		Inner Join CourseUserMapping CG on CG.Course_Id=SC.Course_Id       
	 		where CG.LMSGroupID=@Old@GroupId  AND CG.User_Id = @UserID    
	 	)  
	 	AND User_Id=@UserID
	 	Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid   	
	 	
	 END 
 
	 IF @GroupId = 0 
	 BEGIN
		Select @GroupId = ID,@GroupName=[Name] FROM [dbo].[LMS_Groups] Where [Name]='NoGroup'	
	 END
 

	 UPDATE [dbo].[Users] set       
	 LMSGroupID = @GroupId,       
	 LMSGroupName = @GroupName       
	 WHERE User_Id = @UserID      
    
	 INSERT INTO CourseUserMapping (      
		 Course_Id,      
		 User_Id,      
		 Active,      
		 Assign_Date,      
		 Completion_Date,      
		 User_Course_Status,      
		 LMSGroupID      
		 )      
	 Select CG.Course_Id,@UserID,1, GETDATE(), NULL, 1,@GroupId      
	 from [dbo].[CourseGroupMapping] CG     
	 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.LMSGroupID=CG.Group_Id AND CM.User_Id=@UserID    
	 Where CG.Group_Id = @GroupId AND CM.Course_Id IS NULL    
    
    
	 Insert into UserSubCourseDetails (      
		 [User_Id]      
		   ,[Subcourse_Id]      
		  ,[Percentage]      
		  ,[Sub_Course_Status]      
		  ,[Completion_Date]      
		  ,[LastModifiedDate]      
		  ,[TimeSpentInSeconds]      
		  ,[isAlreadyCompleted]      
		  ,[Attempts])        
		  Select @UserID, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0     
	   from SubCourse SC     
	   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID    
	   where CG.Group_Id = @GroupId    
      
END    
Go

Alter table ResourceLibrary
drop constraint FK_ResourceLibrary_Track
GO
Alter table ResourceLibrary
drop constraint FK_ResourceLibrary_Curriculum
Go
Alter table ResourceLibrary
drop constraint FK_ResourceLibrary_Courses
Go
Alter table ResourceLibrary
drop constraint FK_ResourceLibrary_ResourceLibraryType
Go
Alter table ResourceLibrary
drop constraint FK_ResourceLibrary_ResourceLibrary

GO
--EXEC [SP_LMS_GET_Assign_Resources_LIST] '',''        
Alter PROCEDURE [dbo].[SP_LMS_GET_Assign_Resources_LIST]         
 @fromDate DateTime = null,          
 @endDate DateTime = null        
AS        
BEGIN        
 SET NOCOUNT ON;          
 SET @fromDate = NULLIF(@fromDate,'')          
 SET @endDate = NULLIF(@endDate,'')          
          
 IF @fromDate IS NOT NULL          
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));          
 IF @endDate IS NOT NULL          
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));        
        
 begin        
  SELECT distinct RL.ID,        
   RL.Name,        
   RL.Description,         
   RL.CreatedDate,        
   RL.ModifiedDate,        
  RL.IsActive,        
  RL.UploadURL,        
   RLT.Name,        
   RLT.ID,        
   RL.ThumbnailImgURL,        
   CRM.isRequired   ,    
   CourseId    
    FROM ResourceLibrary RL         
    Inner Join ResourceLibraryType RLT on RLT.ID=RL.ResourceLibraryTypeID        
   OUter Apply (
		Select top 1 CourseId,isRequired FROM 
		CourseResourceMapping CRM Where CRM.ResourceId = RL.ID    
) CRM    
  where @fromDate IS NULL OR (RL.CreatedDate >= @fromDate AND RL.CreatedDate <= @endDate)        
  --LEFT JOIN LMS_SUB_COURSE LSC ON LC.Course_Id = LSC.Course_Id        
  ORDER BY RL.ID desc        
 end        
END   
Go
GO
ALTER PROCEDURE [dbo].[SP_LMS_ADD_RESOURCE] 
	@Name		VARCHAR(256),
	@Description VARCHAR(256),
	@TrackID BIGINT = 0,
	@CurriculumID BIGINT = 0,
	@CoursesID BIGINT = 0,
	@TypeID BIGINT,
	@UploadURL VARCHAR(256),
	@ResourceID	BIGINT OUTPUT,
	@WebLink nvarchar(256) = null,
	@Author nvarchar(256) = null,
	@Vol_Iss_Pg nVARCHAR(256) = null,
	@Publication nVARCHAR(256) = null,
	--@CategoryName VARCHAR(100),
	--@SubCategoryName VARCHAR(100),
	@ThumbnailImgURL VARCHAR(100)


AS
BEGIN
	IF @ResourceID = 0
	BEGIN
		INSERT INTO ResourceLibrary(
						ResourceLibraryTypeID,
						Name,
						Description,
						CreatedDate,
						ModifiedDate,
						IsActive,
						UploadURL,
						--CategoryName,
						--SubCategoryName,
						ThumbnailImgURL,
						TrackId,
						CurriculumId,
						CoursesId,
						WebLink,
						VolPage,
						Author,
						Publication
						)
					VALUES (
						@TypeID,
						@Name,
						@Description,
						GETDATE(),
						GETDATE(),
						1,
						@UploadURL,
						--@CategoryName,
						--@SubCategoryName,
						@ThumbnailImgURL,
						@TrackID,
						@CurriculumID,
						@CoursesID,
						@WebLink,
						@Vol_Iss_Pg,
						@Author,
						@Publication
					)
		SELECT @ResourceID = @@IDENTITY
	END
	ELSE
	BEGIN			
		UPDATE ResourceLibrary SET ResourceLibraryTypeID =@TypeID, Name = @Name, Description = @Description, ModifiedDate = GETDATE(), UploadURL = @UploadURL,ThumbnailImgURL=@ThumbnailImgURL, WebLink = @WebLink, VolPage = @Vol_Iss_Pg, Author = @Author, Publication = @Publication, TrackId = @TrackID, CurriculumId = @CurriculumID, CoursesId = @CoursesID
		WHERE ID = @ResourceID
	END
END
Go
ALter PROCEDURE [dbo].[SP_LMS_EDIT_USER]       
 @FirstName  VARCHAR(200) = NULL,      
 @LastName  VARCHAR(200) = NULL,      
 @Designation VARCHAR(100) = NULL,      
 @Region  VARCHAR(200) = NULL,      
 @GroupName  VARCHAR(200) = NULL,      
 @GroupId  VARCHAR(200) = NULL,      
 @Department VARCHAR(100) = NULL,      
 @Manager VARCHAR(100) = NULL,      
 @UserID   BIGINT      
AS      
BEGIN      
    
 Declare @Old@GroupId VARCHAR(200);    
 Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;    
  
 IF @Old@GroupId IS NOT NULL AND @Old@GroupId <> @GroupId    
  BEGIN    
  Delete U from UserSubCourseDetails  U    
  where Subcourse_Id in (      
   Select Sub_Course_Id from SubCourse SC      
   Inner Join CourseUserMapping CG on CG.Course_Id=SC.Course_Id         
   where CG.LMSGroupID=@Old@GroupId  AND CG.User_Id = @UserID     
   AND SC.Course_ID NOT IN(
	 SElect Course_Id from CourseGroupMapping Where Group_Id=@GroupId
   ) 
  )    
  AND User_Id=@UserID  
  Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid AND Course_ID NOT IN(
	 SElect Course_Id from CourseGroupMapping Where Group_Id=@GroupId
   )       
    
  END    
    
  
  
 IF @GroupId = 0   
 BEGIN  
 Select @GroupId = ID,@GroupName=[Name] FROM [dbo].[LMS_Groups] Where [Name]='NoGroup'   
 END  
       
  UPDATE USERS SET         
      First_Name = @FirstName,       
      Last_Name = @LastName,       
      Designation = @Designation,      
      DepartmentID = @Department,      
      RegionID = @Region,      
      LMSGroupID = @GroupId,      
      LMSGroupName = @GroupName,      
      ReportingManagerID = @Manager      
  WHERE USER_ID = @UserID      
      
    
    
   INSERT INTO CourseUserMapping (          
     Course_Id,          
     User_Id,          
     Active,          
     Assign_Date,          
     Completion_Date,          
     User_Course_Status,          
     LMSGroupID          
     )          
	Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@GroupId          
	from [dbo].[CourseGroupMapping] CG         
	Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=@userid --AND CM.LMSGroupID=CG.Group_Id        
	Where CG.Group_Id = @GroupId AND CM.Course_User_Id IS NULL       
        
        
   Insert into UserSubCourseDetails (          
     [User_Id]          
       ,[Subcourse_Id]          
      ,[Percentage]          
      ,[Sub_Course_Status]          
      ,[Completion_Date]          
      ,[LastModifiedDate]          
      ,[TimeSpentInSeconds]          
      ,[isAlreadyCompleted]          
      ,[Attempts])            
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0         
	from SubCourse SC         
	Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID        
	LEft Join UserSubCourseDetails U ON U.Subcourse_Id = SC.Sub_Course_Id AND U.User_Id= @UserID    
	where CG.Group_Id = @GroupId AND U.Subcourse_Id IS NULL         
       
END    
Go
ALter PROCEDURE [dbo].[SP_LMS_Update_GroupName]        
@UserID int,        
@GroupId int = null,        
@GroupName varchar(50) = null,        
@OldGroupId int = null      
AS        
BEGIN      
  
  Declare @Old@GroupId VARCHAR(200);    
  Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;     
   
  IF @Old@GroupId IS NOT NULL AND (@Old@GroupId <> @GroupId  OR @Old@GroupId<>@OldGroupId  )  
  BEGIN    
   Delete U from UserSubCourseDetails  U    
   where Subcourse_Id in (      
    Select Sub_Course_Id from SubCourse SC      
    Inner Join CourseUserMapping CG on CG.Course_Id=SC.Course_Id         
    where CG.LMSGroupID=@Old@GroupId  AND CG.User_Id = @UserID     
	AND SC.Course_ID NOT IN(
	 SElect Course_Id from CourseGroupMapping Where Group_Id=@GroupId
   )  
   )    
   AND User_Id=@UserID  
   Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid  AND Course_ID NOT IN(
	 SElect Course_Id from CourseGroupMapping Where Group_Id=@GroupId
   )        
     
  END   
   
  IF @GroupId = 0   
  BEGIN  
  Select @GroupId = ID,@GroupName=[Name] FROM [dbo].[LMS_Groups] Where [Name]='NoGroup'   
  END  
   
  
  UPDATE [dbo].[Users] set         
  LMSGroupID = @GroupId,         
  LMSGroupName = @GroupName         
  WHERE User_Id = @UserID        
      
  INSERT INTO CourseUserMapping (        
   Course_Id,        
   User_Id,        
   Active,        
   Assign_Date,        
   Completion_Date,        
   User_Course_Status,        
   LMSGroupID        
   )        
  Select CG.Course_Id,@UserID,1, GETDATE(), NULL, 1,@GroupId        
  from [dbo].[CourseGroupMapping] CG       
  Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=@UserID   -- AND CM.LMSGroupID=CG.Group_Id    
  Where CG.Group_Id = @GroupId AND CM.Course_User_Id IS NULL      
      
      
  Insert into UserSubCourseDetails (        
   [User_Id]        
     ,[Subcourse_Id]        
    ,[Percentage]        
    ,[Sub_Course_Status]        
    ,[Completion_Date]        
    ,[LastModifiedDate]        
    ,[TimeSpentInSeconds]        
    ,[isAlreadyCompleted]        
    ,[Attempts])          
    Select @UserID, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0       
    from SubCourse SC       
    Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID  
	LEft Join UserSubCourseDetails U ON U.Subcourse_Id = SC.Sub_Course_Id AND U.User_Id= @UserID    
    where CG.Group_Id = @GroupId  AND U.Subcourse_Id IS NULL            
END      
Go
--EXEC [SP_LMS_GET_Assign_Resources_LIST] '','',3            
Alter PROCEDURE [dbo].[SP_LMS_GET_Assign_Resources_LIST]             
 @fromDate DateTime = null,              
 @endDate DateTime = null,  
 @courseId bigint = null  
AS            
BEGIN            
 SET NOCOUNT ON;              
 SET @fromDate = NULLIF(@fromDate,'')              
 SET @endDate = NULLIF(@endDate,'')              
 SET @CourseId = NULLIF(@CourseId,0);             
 IF @fromDate IS NOT NULL              
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));              
 IF @endDate IS NOT NULL              
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));            
            
 begin            
  SELECT distinct RL.ID,            
   RL.Name,            
   RL.Description,             
   RL.CreatedDate,            
   RL.ModifiedDate,            
   RL.IsActive,            
   RL.UploadURL,            
   RLT.Name,            
   RLT.ID,            
   RL.ThumbnailImgURL,            
   CRM.isRequired   ,        
   CourseId        
    FROM ResourceLibrary RL             
    Inner Join ResourceLibraryType RLT on RLT.ID=RL.ResourceLibraryTypeID            
   OUter Apply (    
  Select top 1 CourseId,isRequired FROM     
  CourseResourceMapping CRM Where CRM.ResourceId = RL.ID  AND (NULLIF(@courseId,'') IS NULL OR CourseId=@courseId )      
) CRM        
  where @fromDate IS NULL OR (RL.CreatedDate >= @fromDate AND RL.CreatedDate <= @endDate)            
  --LEFT JOIN LMS_SUB_COURSE LSC ON LC.Course_Id = LSC.Course_Id            
  ORDER BY RL.ID desc            
 end            
END 
Go
----------------------------------------------------------------------------------------------------------------
ALTER procedure [dbo].[CreateUser]       
 @firstname varchar(50) =null,      
 @lastname varchar(50)= null,      
 @email varchar(260) =null,      
 @designation varchar(260) = null,      
 @password varchar(20) =null,      
 @isdeleted varchar(20) = null,      
 @usertype int  = NULL,      
 @referencecode nvarchar(250)  = NULL,      
 @active bit  = NULL,      
 @accesscode varchar(50)  = NULL,      
 @department varchar(50),      
 @country varchar(50),   
 @countryid bigint,     
 @region varchar(50),    
 @reportingmanger varchar(50),      
 @lmsgroup varchar(50) = Null,      
 @lmsgroupId varchar(50) = Null,      
 @userid bigint output      
as      
begin      
 set nocount on;      
      
 --delete from Users where Email_ID = @email and Role = 4 and IsDeleted = 1  
      
 insert into Users       
 (      
  [First_Name],      
  [Last_Name],      
  [Email_ID],      
  [Designation],      
  [Password],      
  [Role],      
  [Active],       
  [AccessCode],      
  [Access_Token],      
  [Created_Date],      
  [LastModifiedDate],      
  [DepartmentID],      
  Country,  
  countryid,      
  [RegionID],
  [ReportingManagerID],      
  [LMSGroupName],      
  [LMSGroupID],      
  [IsDeleted]      
 )      
 values      
 (      
  @firstname,      
  @lastname,      
  @email,      
  @designation,      
  @password,      
  @usertype,      
  @active,      
  @accesscode,      
  @referencecode,      
  GETDATE(),      
  GETDATE(),      
  @department,      
  @country,  
  @countryid,
  @region,      
  @reportingmanger,      
  @lmsgroup,      
  @lmsgroupId,      
  @isdeleted      
 )      
      
 set @userid = @@IDENTITY      
      
   select User_Id as userid, First_Name as firstname, Last_Name as lastname from Users where User_Id = @userid      
    
    
   INSERT INTO CourseUserMapping (          
     Course_Id,          
     User_Id,          
     Active,          
     Assign_Date,          
     Completion_Date,          
     User_Course_Status,          
     LMSGroupID          
     )          
 Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@lmsgroupId          
 from [dbo].[CourseGroupMapping] CG         
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=@userid      --AND CM.LMSGroupID=CG.Group_Id   
 Where CG.Group_Id = @lmsgroupId AND CM.Course_User_Id IS NULL        
        
        
 Insert into UserSubCourseDetails (          
     [User_Id]          
       ,[Subcourse_Id]          
      ,[Percentage]          
      ,[Sub_Course_Status]          
      ,[Completion_Date]          
      ,[LastModifiedDate]          
      ,[TimeSpentInSeconds]          
      ,[isAlreadyCompleted]          
      ,[Attempts])            
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0         
   from SubCourse SC         
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID        
   LEft Join UserSubCourseDetails U ON U.Subcourse_Id = SC.Sub_Course_Id AND U.User_Id= @userid     
   where CG.Group_Id = @lmsgroupId     AND U.Subcourse_Id IS NULL     
    
    
end    
Go
create PROCEDURE [dbo].[SP_LMS_ADD_NOTIFICATION]
@notificationid BIGINT OUTPUT ,
	@notification NVARCHAR(500) = null,
	@userid BIGINT = 0
AS
BEGIN
SET @notificationid = 0;
	SELECT @notificationid = notificationid FROM Notification WHERE @notification = notification
IF @notificationid <> 0
	BEGIN
	 
  select notificationid from Notification where notificationid = @notificationid  
   END
ELSE
   BEGIN
     INSERT INTO Notification (notification, date) VALUES (@notification, GETDATE())
   END
  
END
Go
Create PROCEDURE [dbo].[SP_LMS_ADD_UserNOTIFICATION]
@notificationid BIGINT ,
	@userid BIGINT
AS
BEGIN
INSERT INTO UserNotificationMapping (notificationid, userid,isread) VALUES (@notificationid, @userid,'false')
 
END
Go
--Exec SP_LMS_GET_NOT_ASSIGNED_USER_LIST 3  
ALter procedure [dbo].[SP_LMS_GET_NOT_ASSIGNED_USER_LIST]    
@courseId int    
as    
begin    
 Select Isnull(CM.Active,0) as IsActiveCourseUser,U.* from Users U  
 Left Join CourseUserMapping CM ON CM.User_Id = U.User_Id AND Course_Id=@courseId
 Where ISNULL(U.IsDeleted,0)=0 
end 
Go
---------------------------------------------------------------------------------

Create PROCEDURE [dbo].[GetResourceByCourseId]
	@courseId bigint
AS
BEGIN
	select r.Name from Courses c
    left join CourseResourceMapping cm on c.Course_Id = cm.CourseId 
    left join ResourceLibrary r on cm.ResourceId = r.ID
    where cm.CourseId = @courseId and r.Name IS Not Null
END
Go
-----------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[SP_LMS_EDIT_USER]       
 @FirstName  VARCHAR(200) = NULL,      
 @LastName  VARCHAR(200) = NULL,      
 @Designation VARCHAR(100) = NULL,    
 @Region  VARCHAR(200) = NULL,    
 @Country  VARCHAR(200) = NULL,
 @countryid  bigint = 250,       
 @GroupName  VARCHAR(200) = NULL,      
 @GroupId  VARCHAR(200) = NULL,      
 @Department VARCHAR(100) = NULL,      
 @Manager VARCHAR(100) = NULL,      
 @UserID   BIGINT      
AS      
BEGIN      
    
 Declare @Old@GroupId VARCHAR(200);    
 Select @Old@GroupId=LMSGroupID FROM USERS Where USER_ID = @UserID ;    
  
 IF @Old@GroupId IS NOT NULL AND @Old@GroupId <> @GroupId    
  BEGIN    
  Delete U from UserSubCourseDetails  U    
  where Subcourse_Id in (      
   Select Sub_Course_Id from SubCourse SC      
   Inner Join CourseUserMapping CG on CG.Course_Id=SC.Course_Id         
   where CG.LMSGroupID=@Old@GroupId  AND CG.User_Id = @UserID     
   AND SC.Course_ID NOT IN(
	 SElect Course_Id from CourseGroupMapping Where Group_Id=@GroupId
   ) 
  )    
  AND User_Id=@UserID  
  Delete from CourseUserMapping Where LMSGroupID=@Old@GroupId AND User_Id=@userid AND Course_ID NOT IN(
	 SElect Course_Id from CourseGroupMapping Where Group_Id=@GroupId
   )       
    
  END    
    
  
  
 IF @GroupId = 0   
 BEGIN  
 Select @GroupId = ID,@GroupName=[Name] FROM [dbo].[LMS_Groups] Where [Name]='NoGroup'   
 END  
       
  UPDATE USERS SET         
      First_Name = @FirstName,       
      Last_Name = @LastName,       
      Designation = @Designation,      
      DepartmentID = @Department,      
      Country = @Country, 
	  countryid = @countryid,
	  RegionID = @Region,     
      LMSGroupID = @GroupId,      
      LMSGroupName = @GroupName,      
      ReportingManagerID = @Manager      
  WHERE USER_ID = @UserID      
      
    
    
   INSERT INTO CourseUserMapping (          
     Course_Id,          
     User_Id,          
     Active,          
     Assign_Date,          
     Completion_Date,          
     User_Course_Status,          
     LMSGroupID          
     )          
	Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@GroupId          
	from [dbo].[CourseGroupMapping] CG         
	Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=@userid --AND CM.LMSGroupID=CG.Group_Id        
	Where CG.Group_Id = @GroupId AND CM.Course_User_Id IS NULL       
        
        
   Insert into UserSubCourseDetails (          
     [User_Id]          
       ,[Subcourse_Id]          
      ,[Percentage]          
      ,[Sub_Course_Status]          
      ,[Completion_Date]          
      ,[LastModifiedDate]          
      ,[TimeSpentInSeconds]          
      ,[isAlreadyCompleted]          
      ,[Attempts])            
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0         
	from SubCourse SC         
	Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID        
	LEft Join UserSubCourseDetails U ON U.Subcourse_Id = SC.Sub_Course_Id AND U.User_Id= @UserID    
	where CG.Group_Id = @GroupId AND U.Subcourse_Id IS NULL         
       
END 
GO
------------------------------------------------------------
 insert into Track values('Global');
 GO
 --------------------------------------------------
insert into Curriculum values('Global')
Go
--------------------------------------------------
  insert into TrackCurriculumMapping values(3,4)
  Go
-----------------------------------------------


Alter procedure [dbo].[GetTrackCurriculumByTrackId]     
@TrackId bigint = 0
as    
begin 
   select C.CurriculumId,C.CurriculumName from Curriculum C where CurriculumName = 'Global'
   Union 
 select C.CurriculumId,C.CurriculumName from TrackCurriculumMapping TM
 Inner Join [dbo].[Curriculum] C on C.CurriculumId = TM.CurriculumId
 Where @TrackId =0 Or  TM.TrackId=@TrackId;  
end    
Go
Alter procedure [dbo].[CreateUser]       
 @firstname varchar(50) =null,      
 @lastname varchar(50)= null,      
 @email varchar(260) =null,      
 @designation varchar(260) = null,      
 @password varchar(20) =null,      
 @isdeleted varchar(20) = null,      
 @usertype int  = NULL,      
 @referencecode nvarchar(250)  = NULL,      
 @active bit  = NULL,      
 @accesscode varchar(50)  = NULL,      
 @department varchar(50),      
 @country varchar(50),   
 @countryid bigint,     
 @region varchar(50),    
 @reportingmanger varchar(50),      
 @lmsgroup varchar(50) = Null,      
 @lmsgroupId varchar(50) = Null,      
 @userid bigint output      
as      
begin      
 set nocount on;      
      
 --delete from Users where Email_ID = @email and Role = 4 and IsDeleted = 1  
      
 insert into Users       
 (      
  [First_Name],      
  [Last_Name],      
  [Email_ID],      
  [Designation],      
  [Password],      
  [Role],      
  [Active],       
  [AccessCode],      
  [Access_Token],      
  [Created_Date],      
  [LastModifiedDate],      
  [DepartmentID],      
  Country,  
  countryid,      
  [RegionID],
  [ReportingManagerID],      
  [LMSGroupName],      
  [LMSGroupID],      
  [IsDeleted]      
 )      
 values      
 (      
  @firstname,      
  @lastname,      
  @email,      
  @designation,      
  @password,      
  @usertype,      
  @active,      
  @accesscode,      
  @referencecode,      
  GETDATE(),      
  GETDATE(),      
  @department,      
  @country,  
  @countryid,
  @region,      
  @reportingmanger,      
  @lmsgroup,      
  @lmsgroupId,      
  @isdeleted      
 )      
      
 set @userid = @@IDENTITY      
      
   select User_Id as userid, First_Name as firstname, Last_Name as lastname from Users where User_Id = @userid      
    
    
   INSERT INTO CourseUserMapping (          
     Course_Id,          
     User_Id,          
     Active,          
     Assign_Date,          
     Completion_Date,          
     User_Course_Status,          
     LMSGroupID          
     )          
 Select CG.Course_Id,@userid,1, GETDATE(), NULL, 1,@lmsgroupId          
 from [dbo].[CourseGroupMapping] CG         
 Left Join CourseUserMapping CM on CM.Course_Id = CG.Course_Id AND CM.User_Id=@userid      --AND CM.LMSGroupID=CG.Group_Id   
 Where CG.Group_Id = @lmsgroupId AND CM.Course_User_Id IS NULL        
        
        
 Insert into UserSubCourseDetails (          
     [User_Id]          
       ,[Subcourse_Id]          
      ,[Percentage]          
      ,[Sub_Course_Status]          
      ,[Completion_Date]          
      ,[LastModifiedDate]          
      ,[TimeSpentInSeconds]          
      ,[isAlreadyCompleted]          
      ,[Attempts])            
      Select @userid, SC.Sub_Course_Id, 0, 1, NULL, GETDATE(), 0, 0, 0         
   from SubCourse SC         
   Inner Join CourseGroupMapping CG ON CG.Course_Id = SC.Course_ID        
   LEft Join UserSubCourseDetails U ON U.Subcourse_Id = SC.Sub_Course_Id AND U.User_Id= @userid     
   where CG.Group_Id = @lmsgroupId     AND U.Subcourse_Id IS NULL     
    
    
end 

Go
CREATE PROCEDURE [dbo].[SP_LMS_ADD_RESOURCE] 
	@Name		VARCHAR(256),
	@Description VARCHAR(256),
	@TrackID BIGINT = 0,
	@CurriculumID BIGINT = 0,
	@CoursesID BIGINT = 0,
	@TypeID BIGINT,
	@UploadURL VARCHAR(256),
	@ResourceID	BIGINT OUTPUT,
	@WebLink nvarchar(256) = null,
	@Author nvarchar(256) = null,
	@Vol_Iss_Pg nVARCHAR(256) = null,
	@Publication nVARCHAR(256) = null,
	--@CategoryName VARCHAR(100),
	--@SubCategoryName VARCHAR(100),
	@ThumbnailImgURL VARCHAR(100)


AS
BEGIN
	IF @ResourceID = 0
	BEGIN
		INSERT INTO ResourceLibrary(
						ResourceLibraryTypeID,
						Name,
						Description,
						CreatedDate,
						ModifiedDate,
						IsActive,
						UploadURL,
						--CategoryName,
						--SubCategoryName,
						ThumbnailImgURL,
						TrackId,
						CurriculumId,
						CoursesId,
						WebLink,
						VolPage,
						Author,
						Publication
						)
					VALUES (
						@TypeID,
						@Name,
						@Description,
						GETDATE(),
						GETDATE(),
						1,
						@UploadURL,
						--@CategoryName,
						--@SubCategoryName,
						@ThumbnailImgURL,
						@TrackID,
						@CurriculumID,
						@CoursesID,
						@WebLink,
						@Vol_Iss_Pg,
						@Author,
						@Publication
					)
		SELECT @ResourceID = @@IDENTITY
	END
	ELSE
	BEGIN			
		UPDATE ResourceLibrary SET ResourceLibraryTypeID =@TypeID, Name = @Name, Description = @Description, ModifiedDate = GETDATE(), UploadURL = @UploadURL,ThumbnailImgURL=@ThumbnailImgURL, WebLink = @WebLink, VolPage = @Vol_Iss_Pg, Author = @Author, Publicat
ion = @Publication, TrackId = @TrackID, CurriculumId = @CurriculumID, CoursesId = @CoursesID
		WHERE ID = @ResourceID
	END
END
Go
-----------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[SP_LMS_GET_GROUP_DETAILS]  
@fromDate DateTime = null,  
@endDate DateTime = null  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET @fromDate = NULLIF(@fromDate,'')  
 SET @endDate = NULLIF(@endDate,'')  
  
 IF @fromDate IS NOT NULL  
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));  
 IF @endDate IS NOT NULL  
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));  
  
 begin  
  Select G.Name, G.Id as GroupId, U.NoOfUser, G.CreatedDate,  
  STUFF((SELECT ', ' + CAST(CM.Course_Id AS VARCHAR(10)) + '|' + C.Name [text()]  
    FROM CourseGroupMapping CM
	Inner Join Courses C ON C.Course_Id= CM.Course_Id  
    WHERE CM.Group_Id = G.ID  
    FOR XML PATH(''), TYPE)  
   .value('.','NVARCHAR(MAX)'),1,2,' ') ModulesAssigned  
  from LMS_Groups G  
  Outer Apply(  
   Select Count(*) as NoOfUser From Users U  
   Where U.LMSGroupID = G.ID  AND ISNULL(U.IsDeleted,0)=0 
  )U where @fromDate IS NULL OR (G.CreatedDate >= @fromDate AND G.CreatedDate <= @endDate)  
 end  
END
Go
---------------------------------------------------------------------------------------------------------
Alter procedure [dbo].[SP_LMS_GET_NOT_ASSIGNED_GROUP_LIST]    
@courseId int,    
@fromDate DateTime = null,        
@endDate DateTime = null        
as    
begin    
SET @fromDate = NULLIF(@fromDate,'')        
 SET @endDate = NULLIF(@endDate,'')       
     
 IF @fromDate IS NOT NULL        
  SET @fromDate =  DATEADD(hh,24,DATEADD(d,-1,@fromDate));        
 IF @endDate IS NOT NULL        
  set @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate));       
    
 Select Isnull(CM.Active,0) as IsActiveCourseGroup,U.NoOfUser,GU.*,    
 STUFF((SELECT ', ' + CAST(CM.Course_Id AS VARCHAR(10)) + '|' + C.Name   [text()]   
    FROM CourseGroupMapping CM        
	Inner Join Courses C ON C.Course_Id= CM.Course_Id
    WHERE CM.Group_Id = GU.ID        
    FOR XML PATH(''), TYPE)        
   .value('.','NVARCHAR(MAX)'),1,2,' ') ModulesAssigned    
  from LMS_Groups GU    
 Left Join CourseGroupMapping CM ON CM.Group_Id = GU.ID AND Course_Id=@courseId     
 Outer Apply(        
   Select Count(*) as NoOfUser From Users U        
   Where U.LMSGroupID = Gu.ID  AND ISNULL(U.IsDeleted,0)=0       
  )U where @fromDate IS NULL OR (GU.CreatedDate >= @fromDate AND GU.CreatedDate <= @endDate)     
end    
Go