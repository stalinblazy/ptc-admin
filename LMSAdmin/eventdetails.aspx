﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eventdetails.aspx.cs" Inherits="eventdetails" %>

<!DOCTYPE html>

<html lang="en">
<title>Add Event</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="js/datetime/bootstrap-datetimepicker.css" />
<link rel="stylesheet" href="js/toast/jquery.toast.min.css">
<link rel="stylesheet" href="js/croppie/croppie.css">
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<link rel="stylesheet" href="css/styledev.css">
<link rel="stylesheet" href="css/custom-dev.css">
<style>
    .upload-demo .upload-demo-wrap,
    .upload-demo .upload-result,
    .upload-demo.ready .upload-msg {
        display: none;
    }

    .upload-demo.ready .upload-demo-wrap {
        display: block;
    }

    .upload-demo.ready .upload-result {
        display: inline-block;
    }

    .upload-demo-wrap {
        width: 750px;
        height: 450px;
        margin: 0 auto;
    }

    .error {
        color: red !important;
    }
</style>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />

        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />

            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Upcoming Events
                       
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Upcoming Events - <span class="eName">Add Event</span>
                        </div>
                    </div>
                </div>
                <div class="row" id="pageBody">
                    <div class="col-md-12">
                        <div class="row" id="activity">
                            <div class="col-md-12 cardCustom">
                                <div class="aeactivitytxtUser">
                                    <h1 class="eName">Add Event</h1>
                                </div>
                                <form id="AddeventManager" runat="server">

                                    <div class="eveForm">
                                        <input type="hidden" id="eId" runat="server" />
                                         <input type="hidden" id="AdminId" runat="server" />
                                        <div class="row">
                                            <div class="col-sm-7 ">
                                                <div class="row">
                                                    <div class="col-sm-12 aeformTxt">
                                                        <label>Event Name</label>
                                                        <input runat="server" id="eName" type="text" class="form-control" placeholder="Event Name">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-6 aeformTxt">
                                                        <label>Start Date</label>
                                                        <input runat="server" id="sDate" oninput="validator.form()" type="text" class="form-control datepicker" placeholder="From Date">
                                                    </div>
                                                    <div class="col-sm-6 aeformTxt">
                                                        <label>End Date</label>
                                                        <input runat="server" id="eDate" oninput="validator.form()" type="text" class="form-control datepicker" placeholder="To Date">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-6 aeformTxt">
                                                        <label>Time</label>
                                                        <input runat="server" id="eTime" type="text" class="form-control timepicker" placeholder="Time">
                                                    </div>
                                                    <div class="col-sm-6 aeformTxt">
                                                        <label>Venue</label>
                                                        <input runat="server" id="eVenue" type="text" class="form-control" placeholder="Location">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-12 aeformTxt">
                                                        <label>Event Link</label>
                                                        <input runat="server" id="eLink" type="text" class="form-control" placeholder="Url">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <img id="eventimg" runat="server" class="img-fluid event-img" src="" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-8">
                                                <label style="margin-bottom: 0px;" class="btn btn-default selectFile">
                                                    Select Image
                                                   
                                                    <input accept="image/x-png,image/gif,image/jpeg" id="upload" type="file" hidden>
                                                </label>
                                                <input type="button" class="btn  btn-default selectFile" onclick="clearImage()" value="Clear Image" />
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-6 aeformTxt">
                                                <label id="eventAlert" class="error">
                                                </label>
                                            </div>
                                            <div class="col-md-3 userBut1">
                                                <a href="events.aspx"><span class="fas fa-arrow-left userIcon"></span>&nbsp;&nbsp;&nbsp;Back to upcoming events</a>
                                            </div>
                                            <div class="col-md-3">
                                                <a class="btn text-white userBut2" id="subBtn" runat="server" onclick="addEvent()" href="#" role="button">ADD EVENT</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="imageModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crop Image</h5>
                </div>
                <div class="modal-body">
                    <div class="upload-demo-wrap">
                        <div id="upload-demo"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Clear</button>
                    <button type="button" class="btn btn-primary save-btn" data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="js/toast/jquery.toast.min.js"></script>
    <script src="scripts/jquery.validate.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>

    <script src="js/datetime/bootstrap-datetimepicker.min.js"></script>
    <script src="js/croppie/croppie.js"></script>
    <script src="js/adminservice.js"></script>
    <script src="js/event.js"></script>

    <script>

        function validateKeyStrokes(event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        var validator;
        $(document).ready(function () {
            validator = $("#AddeventManager").validate({
                rules: {
                    eName: {
                        required: true
                    }, sDate: {
                        required: true
                    }, eDate: {
                        required: true
                    }, eVenue: {
                        required: true
                    }, eLink: {
                        required: true,
                        url: true
                    }, eTime: {
                        required: true
                    }, upload: {
                        required: true
                    }
                }
            });
            activeLeftMenu("events");

            $(".eName").html(parseInt($("#eId").val()) == 0 ? 'Add Event' : "Update Event")
            $(".loader").fadeOut();
        });

        $('.datepicker').datetimepicker({
            format: 'L',
            minDate: moment()
        });
        $('.timepicker').datetimepicker({
            format: 'LT',
            format: 'HH:mm'
        });

        $("#sDate").on('dp.change', function (e) { $('#eDate').data("DateTimePicker").minDate(e.date) })

        var input = document.getElementsByTagName('input')[0];

        $('#upload')[0].onchange = function (e) {
            const file = this.files[0];
            const fileType = file['type'];
            const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
            if (validImageTypes.indexOf(fileType) == -1) {
                alert("Select valid image.");
                return;
            }
            $("#imageModal").modal("show");

        };

        $('#imageModal').on('shown.bs.modal', function () {
            var input = $('#upload');
            readFile(input[0]);
        });


        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 566,
                height: 313
            },
            enableExif: true,
            enforceBoundary: true
        });

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });

                }

                reader.readAsDataURL(input.files[0]);
            }
            else {
                alert("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        function clearImage() {
            $("#eventimg").attr("src", "");
            $("#eventimg").hide();
            $("#upload").val('');
        }

        $('.save-btn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                if (resp && resp != "data:,") {
                    hideAlert();
                    $("#eventimg").attr("src", resp);
                    $("#eventimg").fadeIn();
                    $('#imageModal').modal("hide");
                }
            });
        });

        function addEvent() {
            hideAlert();

            //   msg = !$("#eventimg")[0].src ? 'Event image should not be empty!' : msg;
           
            if ($('#AddeventManager').valid()==true) {
                if (document.getElementById("eventimg").getAttribute("src") == null || document.getElementById("eventimg").getAttribute("src") == "") {
                    showAlert("Event image should not be empty!");
                    return false;
                }
                var eve = new UpcomingEvent();
                eve.EventData = $("#eName").val();
                eve.fromDate = $("#sDate").val();
                eve.toDate = $("#eDate").val();
                eve.Venue = $("#eVenue").val();
                eve.EventLink = $("#eLink").val();
                eve.Time = $("#eTime").val();
                eve.Img = $("#eventimg").attr("src");


                eve.Time = $("#eTime").data('DateTimePicker').viewDate().format("HH:mm:00.000")
                eve.fromDate = new Date(eve.fromDate);
                eve.toDate = new Date(eve.toDate);
                eve.ID = parseInt($("#eId").val());
                eve.AdminId = parseInt($("#AdminId").val());
                let cb = new CallBack();
                cb.func = "AfterUpdateEvent";

                _adminService.UpdateEvent(eve, cb, true);
                $(".loader").show();
                return true;
            }
            
        }

        function AfterUpdateEvent(res) {
            if (res) {
                $.toast({
                    heading: 'Success',
                    text: parseInt($("#eId").val()) == 0 ? 'Event added successful. Redirecting...' : 'Event updated successful. Redirecting...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    afterHidden: function () {
                        window.location.href = "events.aspx";
                    }
                })

            } else {
                $(".loader").hide();
            }
        }

        function showAlert(msg, type) {
            $("#eventAlert").text(msg);
            $("#eventAlert").show();
        }

        function hideAlert() {
            $("#eventAlert").hide();
        }


    </script>
</body>
</html>
