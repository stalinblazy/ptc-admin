﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LearningManagementSystem.Components;
using LearningManagementSystem.Data;
using LearningManagementSystem.UserManager;

public partial class eventdetails : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
            AdminId.Value = user_Id.ToString();
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        String Id = Request.QueryString["id"];
        Id = String.IsNullOrEmpty(Id) ? "0" : Id;
        eId.Value = Id.ToString();

        if(Id != "0")
        {
            subBtn.InnerText = "Update Event";
            DataTable dt = LearningManagementSystem.Components.Events.GetEvent(Convert.ToInt64(Id));
            if(dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                eName.Value = dr["EventData"].ToString();
                sDate.Value = DateTime.Parse(dr["fromDate"].ToString()).ToString("MM/dd/yyyy");
                eDate.Value = DateTime.Parse(dr["toDate"].ToString()).ToString("MM/dd/yyyy");
                eVenue.Value = dr["Venue"].ToString();
                eTime.Value = dr["Time"].ToString();
                eLink.Value = dr["EventLink"].ToString();
                eventimg.Attributes.Add("src", dr["ImgPath"].ToString());
                eventimg.Style.Add("display", "block");
            }
        }

    }
}