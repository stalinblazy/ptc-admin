﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="events.aspx.cs" Inherits="events" %>

<html lang="en">
<head runat="server">
    <title>Upcoming Events</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css" />
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="js/toast/jquery.toast.min.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">
    <link rel="stylesheet" href="css/dataTables.checkboxes.css">
    <link rel="stylesheet" href="css/styledev.css">
    <link rel="stylesheet" href="css/custom-dev.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <style>
        .checkbox label::after {
            margin-top: -4px !important;
        }

        .checkbox input[type="checkbox"]:indeterminate + label::after, .checkbox input[type="radio"]:indeterminate + label::after {
            margin-top: 2px !important;
        }
    </style>
</head>
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />

        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />

            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Upcoming Events
     
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Upcoming Events - Overview
     
                        </div>
                    </div>
                </div>

                <div class="row pageBody">

                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="uplistofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row uplistofusers">
                                    <div class="col-md-6 upleftlist" style="padding-left: 0px;">
                                        <ul class="upuserlistleft">
                                            <li>
                                                <h1 class="active">Upcoming Events</h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>


                                <div class="table-top-action">
                                    <div class="upbulkActionbtn">
                                        <select class="browser-default custom-select" id="bulkAction">
                                            <option disabled selected value="0">Select Action</option>
                                            <option value="1">Active</option>
                                            <option value="2">InActive</option>
                                            <option value="3">Delete</option>
                                        </select>
                                        <button type="button" onclick="bulkAction(this)" class="btn btn-light upapplyBtn">Apply</button>
                                        <a href="eventdetails.aspx?id=0">
                                            <button type="button" class="upcreateUser" id="upuserbtn"><i class='far fa-calendar-check'></i>&nbsp; Create Event </button>
                                        </a>
                                    </div>
                                    <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
                                    <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
                                </div>

                                <!--table starts-->
                                <table id="pageUsers" class="" style="width: 100%">
                                    <thead class="tableHeader">
                                        <tr>
                                            <th style="width: 29px; padding: 0px 13px !important;"></th>
                                            <th>EVENT NAME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                            <th>FROM<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                            <th>TO<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                            <th>VENUE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                            <th style="min-width: 50px;">TIME<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                            <th>EVENT LINK<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                            <th style="min-width: 75px;">STATUS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody class="tableBody">
                                        <asp:Repeater ID="serviceRep" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("ID")%></td>
                                                    <td class="eventName"><a href="eventdetails.aspx?id=<%# Eval("ID")%>"><%# Eval("EventData")%></a></td>
                                                    <td><%#Eval("fromDate") %> </td>
                                                    <td><%#Eval("toDate")%></td>
                                                    <td><%#Eval("Venue")%></td>
                                                    <td><%#Eval("Time")%></td>
                                                    <td><%#Eval("EventLink")%></td>
                                                    <td><%# Eval("isActive") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                                 <input type="hidden" id="AdminId" runat="server" />
                                <div class="clearfix"></div>
                                <div class="custom-pagination">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="datatable-pagination pull-right">
                                                <span id="total-rec"></span>
                                                <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                                                <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                                                <span id="page-data" class="page-numb">1 of 1</span>
                                                <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                                                <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--table ends-->
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="js/toast/jquery.toast.min.js"></script>
    <script src="scripts/jquery-confirm.min.js"></script>
    <script src="js/dataTables.checkboxes.min.js"></script>
    <script src="js/adminservice.js"></script>
    <script src="js/event.js"></script>
    <script>


        var fromDate = {
            'targets': [2, 3, 5, 7],
            'render': function (data, type, row, meta) {
                if (meta.col == 5) {
                    let time = data.split(".");
                    data = moment(new Date("01/01/1990 " + time[0])).format("hh:mm A");
                } else if (meta.col == 2 || meta.col == 3) {
                    data = moment(new Date(data)).format("MM/DD/YYYY");
                }

                if (meta.col == 7) {
                    data = generateAction(row, data);
                }

                return data;
            }
        }

        function generateAction(row, data) {
            let cls = (data === 'True' ? 'btn-active' : 'btn-inactive');
            return '<div class="btn-group status-btn">' +
                '<button type="button" class="btn ' + cls + ' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (data === 'True' ? 'Active' : 'Inactive') + '</button>' +
                '<div class="dropdown-menu  dropdown-menu-right">' +
                '<a class="dropdown-item" data-val="True" data-current="' + data + '" onclick="toggleStatus(this,' + row[0] + ')" href="javascript:void(0)">Active</a>' +
                '<a class="dropdown-item" data-val="False"data-current="' + data + '" onclick="toggleStatus(this,' + row[0] + ')" href="javascript:void(0)">Inactive</a>' +
                '<div class="dropdown-divider"></div>' +
                '<a class="dropdown-item" onclick="deleteEvent(' + row[0] + ')" href="javascript:void(0)">Delete</a>' +
                '</div>' +
                '</div>';
        }

        var sEle;
        function toggleStatus(ele, id) {
            let status = $(ele).data("current");
            sEle = ele;
            let val = $(ele).data("val");
            let btn = $(sEle).parents(".status-btn").find("button");

            if (val == status) {
                return;
            }
            $(btn).attr("disabled", true);
            let ev = new UpcomingEvent();
            ev.ID = id;
            ev.IsActive = val == 'True' ? true : false;
            ev.AdminId = parseInt($("#AdminId").val());
            let cb = new CallBack();
            cb.func = "AfterSetStatus";
            cb.data = ev.IsActive;

            _adminService.SetEventStatus([ev], cb, true);
        }

        function AfterSetStatus(res, data) {
            let cls = data ? 'btn-active' : 'btn-inactive';
            let btn = $(sEle).parents(".status-btn").find("button");
            if (res) {
                $(btn).removeClass('btn-active').removeClass('btn-inactive');
                $(btn).addClass(cls);
                $(btn).text(data ? 'Active' : 'Inactive');
                $(sEle).parent().find("a").data("current", data ? 'True' : 'False');
                $.toast({
                    heading: 'Success',
                    text: 'Event status updated successfully.',
                    showHideTransition: 'slide',
                    icon: 'success'
                })
            }
            $(btn).attr("disabled", false);
        }

        function deleteEvent(id) {
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure you want to delete this event?',
                buttons: {
                    confirm: function () {
                        let ev = new UpcomingEvent();
                        ev.ID = parseInt(id);
                        ev.AdminId = parseInt($("#AdminId").val());
                        let cb = new CallBack();
                        cb.func = "AfterDelete";
                        $(".loader").show();
                        _adminService.DeleteEvents([ev], cb, true);
                    },
                    cancel: function () {

                    }
                }
            });


        }

        function AfterDelete(res) {

            if (res) {
                $.toast({
                    heading: 'Success',
                    text: 'Event Deleted. Updating Data.',
                    showHideTransition: 'slide',
                    icon: 'success',
                    afterHidden: function () {
                        window.location.href = "events.aspx";
                    }
                })
            } else {
                $.toast({
                    heading: 'Error',
                    text: 'Something went wrong',
                    showHideTransition: 'slide',
                    icon: 'error'
                })
                $(".loader").hide();
            }
        }

        var serviceTable;

        let startDate;
        let endDate;

        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                var dateStart = startDate;
                var dateEnd = endDate;
                if (dateStart && dateStart) {
                    // aData represents the table structure as an array of columns, so the script access the date value
                    // in the first column of the table via aData[0]
                    var evalDate = new Date(aData[2]);
                    if (evalDate >= dateStart && evalDate <= dateEnd) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                return true;
            });


        $(document).ready(function () {
            activeLeftMenu("events");
            $('#cal-range').daterangepicker({
                opens: 'left',
                locale: { cancelLabel: 'Clear' }
            });

            $('#cal-range').on('apply.daterangepicker', function (ev, picker) {
                startDate = picker.startDate.toDate();
                endDate = picker.endDate.toDate();
                serviceTable && serviceTable.draw()
                $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + picker.startDate.format('MMM DD') + ' - ' + picker.endDate.format('MMM DD'));
            });

            $('#cal-range').on('cancel.daterangepicker', function (ev, picker) {
                startDate = '';
                endDate = '';
                //do something, like clearing an input
                $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
                serviceTable && serviceTable.draw();
            });


            serviceTable = $('#pageUsers').DataTable({
                "bLengthChange": false,
                "searching": true,
                "paging": true,
                "info": false,
                "lengthChange": false,
                "autoWidth": true,
                dom: 't',
                'columnDefs': [
                    {
                        'targets': 0,
                        'render': function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                            }

                            return data;
                        },
                        'checkboxes': {
                            'selectRow': true,
                            'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                        }
                    }, fromDate

                ],
                'select': {
                    'style': 'multi'
                },
                "aoColumns": [
                    null,
                    null,
                    { "sType": "date" },
                    { "sType": "date" },
                    null,
                    null,
                    null,
                    null
                ],
                'order': [[2, 'desc']],
                "initComplete": function (settings, json) {
                    $(".loader").fadeOut();
                }
            });

            initPagination();

            $('#pageUsers').on('draw.dt', function (e, settings, len) {
                initPagination();
            });

            $("#prevBtn").on("click", function () {
                serviceTable.page('previous').draw('page');
                initPagination();
            });
            $("#nextBtn").on("click", function () {
                serviceTable.page('next').draw('page');
                initPagination();
            });
            $("#firstBtn").on("click", function () {
                serviceTable.page('first').draw('page');
                initPagination();
            });
            $("#lastBtn").on("click", function () {
                serviceTable.page('last').draw('page');
                initPagination();
            });

        });

        function initPagination() {
            if (serviceTable) {
                $(".pagg-btn").attr("disabled", false);
                let page = serviceTable.page.info();
                $("#total-rec").html(page.recordsDisplay + " Events");
                if (page.pages == 1) {
                    $(".pagg-btn").attr("disabled", true);
                }
                let cPage = page.page + 1;
                let lPage = page.pages;

                if (lPage == 0) {
                    cPage = 0;
                }
                $("#page-data").html(cPage + " of " + lPage);
                if (cPage == 1) {
                    $("#firstBtn,#prevBtn").attr("disabled", true);
                }
                if (cPage == lPage) {
                    $("#lastBtn,#nextBtn").attr("disabled", true);
                }

            }
        }

        function searchTable() {
            serviceTable && serviceTable.search($('#searchTable').val()).draw();
        }


        function bulkAction(ele) {
            let option = $("#bulkAction option:selected").val();
            let selectedRow = serviceTable.column(0).checkboxes.selected().toArray();
            let isActive = false;
            if (selectedRow.length <= 0) {
                alert("Please select atleast one row.");
                return;
            }
            if (option == "0") {
                alert("Please select bulk option.");
                return;
            } else if (option == "1") {
                isActive = true;
            }
            let selectedEvents = [];
            selectedRow.forEach(function (id) {
                let ev = new UpcomingEvent();
                ev.ID = parseInt(id);
                ev.IsActive = isActive;
                ev.AdminId = parseInt($("#AdminId").val());
                selectedEvents.push(ev);
            })

            $.confirm({
                title: 'Confirm!',
                content: 'Apply the bulk action?',
                buttons: {
                    confirm: function () {
                        let cb = new CallBack();
                        cb.func = "AfterBulkUpdate";
                        $(".loader").show();
                        if (option != "3") {
                            _adminService.SetEventStatus(selectedEvents, cb, true);
                        } else {
                            _adminService.DeleteEvents(selectedEvents, cb, true);
                        }
                    },
                    cancel: function () {

                    }
                }
            });


        }

        function AfterBulkUpdate(res) {
            if (res) {
                $.toast({
                    heading: 'Success',
                    text: 'Bulk action successful. Updating Data.',
                    showHideTransition: 'slide',
                    icon: 'success',
                    afterHidden: function () {
                        window.location.href = "events.aspx";
                    }
                })

            } else {
                $(".loader").hide();
            }
        }

    </script>
</body>
</html>
