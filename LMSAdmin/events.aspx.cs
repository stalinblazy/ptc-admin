﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Medtrix.DataAccessControl;
using Medtrix.WebServices;
using LearningManagementSystem.Data;
using LearningManagementSystem.UserManager;

public partial class events : System.Web.UI.Page
{
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
            AdminId.Value = user_Id.ToString();
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }
        DataTable dt = LearningManagementSystem.Components.Events.GetAllEvents();
        serviceRep.DataSource = dt;
        serviceRep.DataBind();
        
    }
}