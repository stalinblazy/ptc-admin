﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="flashCard.aspx.cs" Inherits="flashCard" %>

<%@ Register TagPrefix="LMS" TagName="FlashCardList" Src="FlashCardList.ascx" %>
<%@ Register TagPrefix="LMS" TagName="FlashCardQstCommentList" Src="QuestionCommentList.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flash Card Page</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <LMS:CommonStyles ID="CommonStyles" runat="server" />
    <style>
        .hyper {
            cursor: pointer;
            text-decoration-line: underline;
            text-decoration-style: solid;
        }

        .adflashcardTxt input {
            color: #29295B;
            font-family: Arial;
            font-size: 12px;
            padding: 8px 10px 8px 10px;
        }

        .flashQunModal {
            background-color: #9e9e9e4d;
            background-clip: padding-box;
            border: 1px solid #9e9e9e9e;
            padding: 12px;
            border-radius: 7px;
            font-size: 13px;
        }
        .fa fa-caret-up {
            content: none;
        }
    </style>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css" />
    <link rel="stylesheet" href="css/w3.css" />
</head>
<body>
     <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />
        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />
            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Flash Card                      
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Flash Card - <span id="spanHeader">Overview</span>
                        </div>
                    </div>
                </div>
                <div class="row pageBody">
                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="listofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row listofusers">
                                    <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                        <ul class="guserlistleft">
                                            <li class="activitytxtCourse1">
                                                <h1 class="active">List of Flash Card</h1>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_CourseList">
                                        <LMS:FlashCardList ID="FlashCardList1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>
            <!-- End Page content -->
        </div>
    </div>

    <!--modal-->
    <!-- The Modal -->
    <div id="id01" class="w3-modal">
        <div class="w3-modal-content">
            <div class="w3-container">
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <LMS:FlashCardQstCommentList ID="FlashCardList2" runat="server" />
            </div>
        </div>
    </div>


    <!--modal ends-->
    <LMS:CommonScripts ID="CommonScripts" runat="server" />
    <script src="scripts/custom/FlashCard.js"></script>
</body>
</html>
