﻿using LearningManagementSystem.UserManager;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class flashCard : System.Web.UI.Page
{
    public static object LearningManagemenctSystem { get; private set; }
    protected static UserType defined = UserType.Admin;
    protected static int user_Id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            UserSession.IsAuthorizedsup(defined, Session["User"] as UserSession);
            user_Id = int.Parse(((UserSession)Session["User"]).UserID.ToString());
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect(UserManager.GetDefaultPage(UserType.Invalid));
            return;
        }

    }

    [WebMethod]
    public static string GetFlashcardData(string fromdate, string enddate)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.FlashCard.GetFlashCard(fromdate, enddate);
            var objData = DataAccessManager.DataTableToJSON(dataTable);
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    [WebMethod]
    public static string GetFlashcardQstCount(int flashcard_Id)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.FlashCard.GetFlashCardQstCount(flashcard_Id);
            var objData = DataAccessManager.DataTableToJSON(dataTable);
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }

    [WebMethod]
    public static string GetFlashCardQstComments(int flashqstId)
    {
        string response = string.Empty;
        try
        {
            var dataTable = new DataTable();
            dataTable = LearningManagementSystem.Components.FlashCard.GetFlashCardQstComments(flashqstId);
            var objData = DataAccessManager.DataTableToJSON(dataTable);
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            response = js.Serialize(objData);

        }
        catch (Exception ex)
        {
            Medtrix.Trace.Logger.Log(ex.Message.ToString());
            response = "[]";
        }
        return response;
    }





    //private static bool DeleteFlashCardRecord(int resourceId)
    //{
    //    bool auditDetails = false;
    //    DataAccessManager dataManager = DataAccessManager.GetInstance();
    //    try
    //    {
    //        String referenceCode = Guid.NewGuid().ToString();
    //        List<SqlParameter> parameters = new List<SqlParameter>();
    //        parameters.Add(dataManager.CreateParam("@FlashCard_Id", resourceId, ParameterDirection.Input));
    //        return dataManager.ExecuteNonQuery("SP_LMS_DELETE_FLASHCARD", parameters.ToArray());
    //    }
    //    catch (Exception ex)
    //    {
    //        Medtrix.Trace.Logger.Log(ex.Message.ToString());
    //        return true;
    //    }
    //    finally
    //    {
    //        // auditDetails = dataManager.ExecuteAuditTrailDetails("Admin Delete the Resource from Resource Management Resource Id's" + resourceId + "", "Delete Operation", user_Id);
    //    }

    //    return auditDetails;
    //}






}