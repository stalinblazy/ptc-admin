﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommonHeader.ascx.cs" Inherits="headers_CommonHeader" %>

<!-- header -->
<div class="row" id="header">
    <div class="col-md-2">
        <img src="images/logo.png" alt="Logo" width="50" height="50">
    </div>
    <div class="col-md-10 pgTitle">
        Hello <b>Admin</b> <a href="default.aspx?type=logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
    </div>
</div>
