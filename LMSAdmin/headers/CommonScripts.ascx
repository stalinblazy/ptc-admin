﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommonScripts.ascx.cs" Inherits="headers_CommonScripts" %>

<script src="scripts/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="scripts/dataTables.checkboxes.min.js"></script>
<script src="js/toast/jquery.toast.min.js"></script>
<script src="scripts/jquery-confirm.min.js"></script>
<script src="scripts/custom/Common.js"></script>
