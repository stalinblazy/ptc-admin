﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommonStyles.ascx.cs" Inherits="headers_CommonStyles" %>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link href="css/awesome-bootstrap-checkbox.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="css/style.css?6.8.2019" />
<link href="css/custom-dev.css" rel="stylesheet" />
<link rel="stylesheet" href="css/dataTables.checkboxes.css" />
<link rel="stylesheet" href="css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="css/toastr.min.css" />
<link rel="stylesheet" href="css/jquery-confirm.min.css" />
<link rel="stylesheet" href="js/toast/jquery.toast.min.css">
