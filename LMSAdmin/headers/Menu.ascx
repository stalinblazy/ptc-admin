﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu.ascx.cs" Inherits="headers_Menu" %>
<!-- left Navigation -->
<div class="col-md-2" id="leftNavigation">
    <ul>
        <li id="dashboard">
            <a href="dashboard.aspx"><i>
                <img src="images/icon-home.png" width="28" height="25"></i>
                <div class="">
                    Dashboard
                </div>
            </a>
        </li>
        <li id="UserManagement">
            <a href="UserManagement.aspx"><i>
                <img src="images/icon-userandgroups.png" width="30" height="27"></i>
                <div class="">
                    Users and Groups
                </div>
            </a>
        </li>
        <li id="CourseManagement">
            <a href="CourseManagement.aspx"><i>
                <img src="images/icon-course.png" width="28" height="24"></i>
                <div class="">
                    Course Manager
                </div>
            </a>
        </li>
        <li id="ResourceManagement">
            <a href="ResourceManagement.aspx"><i>
                <img src="images/icon-resources.png" width="28" height="30"></i>
                <div class="">
                    Resource Manager
                </div>
            </a>
        </li>
        <li id="flashCard">
            <a href="flashCard.aspx"><i>
                <img src="images/icon-flashcard.png" width="30" height="30"></i>
                <div class="">
                    Flash Cards
                </div>
            </a>
        </li>

         <li id="assessment">
            <a href="Assessment.aspx"><i>
                <img src="images/taxes.svg" width="30" height="30"></i>
                <div class="">
                   Assessment
                </div>
            </a>
        </li>
        <li id="events">
            <a href="events.aspx"><i>
                <img src="images/icon-events.png" width="26" height="30"></i>
                <div class="">
                    Upcoming Events
                </div>
            </a>
        </li>
        <li id="analytics">
            <a href="analytics.aspx"><i>
                <img src="images/icon-atics.png" width="30" height="30"></i>
                <div class="">
                    Analytics Reports
                </div>
            </a>
        </li>
        <li id="quickReport">
            <a href="quickReport.aspx"><i>
                <img src="images/quick.png" width="30" height="30"></i>
                <div class="">
                    Quick Reports
                </div>
            </a>
        </li>
        <li id="serviceRequest">
            <a href="serviceRequest.aspx"><i>
                <img src="images/icon-technicalsupport.png" width="30" height="30"></i>
                <div class="">
                    Technical Support
                </div>
            </a>
        </li>

    </ul>
</div>
