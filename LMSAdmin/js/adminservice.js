﻿function CallBack() {
    this.func = "";
    this.data = "";
}

function RqHeader(Command, DataPacket, Callback) {
    this.Command = Command;
    this.Data = DataPacket;
    this.callback = Callback;
}

function WebService() {
    this.XmlHttp = new XMLHttpRequest();
    this.Url = "service/AdminService.aspx";
}

// Ajax data request post to server
WebService.prototype.PostRequest = function (PostData, async) {
    try {
        PostData.service = this.service;
        console.log("Request: " + JSON.stringify(PostData));
        this.XmlHttp.open("POST", this.Url, async);
        this.XmlHttp.send(JSON.stringify(PostData));
        if (!async) {
            return JSON.parse(this.XmlHttp.responseText).result;
        }
        else {
            var me = this;
            var callback = PostData.callback;
            var timer = window.setInterval(function (callback) {
                if (me.XmlHttp.readyState == 4) {
                    window.clearInterval(timer);
                    if (me.XmlHttp.status != 200) {
                        return;
                    }
                    //console.log("Response: " + me.XmlHttp.responseText);
                    var data = JSON.parse(me.XmlHttp.responseText);
                    if (data.iserror) {
                        if (data.code == 764) {
                            alert("Session Expired !");
                            location.reload();
                        }
                        else
                            alert(data.message);
                    }

                    var result = data.Result;
                    if (callback != null)
                        window[callback.func](result, callback.data);
                }
            }, 1000, PostData.callback);
        }
    }
    catch (ex) {
        alert(ex);
    }
}



function AdminService() {
}
AdminService.prototype = new WebService();
AdminService.prototype.SetEventStatus = function (PostData, callback, async) {
    var request = new RqHeader("SetEventStatus", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.UpdateEvent = function (PostData, callback, async) {
    var request = new RqHeader("UpdateEvent", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.GetDashboard = function (PostData, callback, async) {
    var request = new RqHeader("GetDashboard", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.GetFilters = function (PostData, callback, async) {
    var request = new RqHeader("GetFilters", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.DeleteEvents = function (PostData, callback, async) {
    var request = new RqHeader("DeleteEvent", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.SetRequestStatus = function (PostData, callback, async) {
    var request = new RqHeader("SetRequestStatus", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.GetQuickReport = function (PostData, callback, async) {
    var request = new RqHeader("GetQuickReport", PostData, callback);
    return this.PostRequest(request, async);
}
AdminService.prototype.SetDueDate = function (PostData, callback, async) {
    var request = new RqHeader("SetDueDate", PostData, callback);
    return this.PostRequest(request, async);
}

var _adminService = new AdminService();

function activeLeftMenu(menuId) {
    $("#leftNavigation li.active").removeClass("active");
    $("#leftNavigation li#" + menuId).addClass("active");
}