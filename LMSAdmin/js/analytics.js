﻿
var serviceTable;
var userTable;
var _type = "course";
let startDate;
let endDate;

$.fn.dataTableExt.afnFiltering.push(
    function (oSettings, aData, iDataIndex) {
        var dateStart = startDate;
        var dateEnd = endDate;
        if (dateStart && dateStart) {
            // aData represents the table structure as an array of columns, so the script access the date value
            // in the first column of the table via aData[0]
            var evalDate = new Date(aData[_type == "course" ? 8 : 8]);
            if (evalDate >= dateStart && evalDate <= dateEnd) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    });

$(document).ready(function () {
    activeLeftMenu("analytics");
    $(".user-report").hide();
    $(".module-report").hide();
    $(".course-report").show();

    let cb = new CallBack();
    cb.func = "AfterFilterLoaded";
    _adminService.GetFilters({}, cb, true);

    $('#cal-range').daterangepicker({
        opens: 'left',
        locale: { cancelLabel: 'Clear' }
    });

    $('#cal-range').on('apply.daterangepicker', function (ev, picker) {
        startDate = picker.startDate.toDate();
        endDate = picker.endDate.toDate();
        _type == "course" ? serviceTable && serviceTable.draw() : userTable && userTable.draw();
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + picker.startDate.format('MMM DD') + ' - ' + picker.endDate.format('MMM DD'));
    });

    $('#cal-range').on('cancel.daterangepicker', function (ev, picker) {
        picker.setStartDate(moment());
        picker.setEndDate(moment());
        startDate = '';
        endDate = '';
        //do something, like clearing an input
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
        _type == "course" ? serviceTable && serviceTable.draw() : userTable && userTable.draw();
    });

    userTable = $('#userWise').DataTable({
        "bLengthChange": false,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        "bPaginate": false,
        dom: 'Bt',
        'select': {
            'style': 'multi'
        },
        "aaSorting": [],
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            { "sType": "date" }
        ],
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'User Enrollment Report'
            },
            {
                extend: 'csvHtml5',
                title: 'User Enrollment Report'
            }
        ],
        "order": [[8, "desc"]]
    });

    serviceTable = $('#pageUsers').DataTable({
        "bLengthChange": false,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        "bPaginate": false,
        dom: 'Bt',
        'select': {
            'style': 'multi'
        },
        "aaSorting": [],
        "aoColumns": [
            null,
            null,
            null,
            null,
            { "sType": "date" },
            null,
            null,
            null,
            { "sType": "date" }
        ],
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Course Wise Report'
            },
            {
                extend: 'csvHtml5',
                title: 'Course Wise Report'
            }
        ]
    });
    initPagination();

    $('#pageUsers,#userWise').on('draw.dt', function (e, settings, len) {
        initPagination();
    });

    $("#prevBtn").on("click", function () {
        let table = _type == "course" ? serviceTable : userTable;
        table.page('previous').draw('page');
        initPagination();
    });
    $("#nextBtn").on("click", function () {
        let table = _type == "course" ? serviceTable : userTable;
        table.page('next').draw('page');
        initPagination();
    });
    $("#firstBtn").on("click", function () {
        let table = _type == "course" ? serviceTable : userTable;
        table.page('first').draw('page');
        initPagination();
    });
    $("#lastBtn").on("click", function () {
        let table = _type == "course" ? serviceTable : userTable;
        table.page('last').draw('page');
        initPagination();
    });

    $('.searchText').on('keyup', function () {
        let table = _type == "course" ? serviceTable : userTable;
        table && table.columns(this.dataset.column).search(this.value).draw();
    });

    $('.selectFilter').on('change', function () {
        let table = _type == "course" ? serviceTable : userTable;
        table && table.columns(this.dataset.column).search(this.value).draw();
    });

    $('.modfilter').on('change', function () {
        onchangeModuleRpt();
    });


});

function initPagination() {
    if (serviceTable) {
        $(".pagg-btn").attr("disabled", false);
        let page = _type == "course" ? serviceTable.page.info() : userTable.page.info();
        $("#total-rec").html(page.recordsDisplay + " Reports");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }
    }
}

var resultObj;

function AfterFilterLoaded(res) {
    if (res) {
        
        resultObj = res;
        if (res.department) {
            $('#deplist').append('<option value="" selected>All Department</option>');
            res.department.forEach(function (dep) {
                $('#deplist').append('<option value="' + dep.Name + '">' + dep.Name + '</option>');
            })
        }
        if (res.course) {
            $('#courselist').append('<option value="" selected>All Course</option>');
            res.course.forEach(function (course) {
                $('#courselist').append('<option value="' + course.Name + '">' + course.Name + '</option>');
                $('#modcourselist').append('<option value="' + course.Course_Id + '">' + course.Name + '</option>');
            })
        }
        if (res.status) {
            $('#statuslist').append('<option value="" selected>All Status</option>');
            res.status.forEach(function (status) {
                $('#statuslist').append('<option value="' + status.Course_Status + '">' + status.Course_Status + '</option>');
            })
        }
        if (res.groups) {
            $('#grouplist').append('<option value="" selected>All Groups</option>');
            $('#modgrouplist').append('<option value="" selected>All Groups</option>');
            res.groups.forEach(function (group) {
                $('#grouplist').append('<option value="' + group.Name + '">' + group.Name + '</option>');
                $('#modgrouplist').append('<option value="' + group.ID + '">' + group.Name + '</option>');
            });
        }

        if (res.countries) {
            $('#countrylist').append('<option value="" selected>All Country</option>');
            res.countries.forEach(function (status) {
                $('#countrylist').append('<option value="' + status.countryid + '">' + status.Country + '</option>');
            })
        }
        if (res.designation) {
            $('#designationlist').append('<option value="" selected>All Designation</option>');
            res.designation.forEach(function (designation) {
                $('#designationlist').append('<option value="' + designation.Designation + '">' + designation.Designation + '</option>');
            })
        }
        if (res.region) {
            $('#regionlist').append('<option value="" selected>All Region</option>');
            res.region.forEach(function (region) {
                $('#regionlist').append('<option value="' + region.Name + '">' + region.Name + '</option>');
            })
        }
    }
    $(".loader").fadeOut();
}

function onchangeReport() {
    _type = $("input[name='report']:checked").val();
    if (_type == "course") {
        serviceTable.search('').columns().search('').draw();
        $(".user-report").hide();
        $(".module-report").hide();
        $(".course-report").show();
    } else if (_type == "user") {
        userTable.search('').columns().search('').draw();
        $(".course-report").hide();
        $(".module-report").hide();
        $(".user-report").show();
    } else if (_type = "module") {
        $(".course-report").hide();
        $(".user-report").hide();
        $(".module-report").show();
        $('.modfilter').val('');
        $('#modcourselist').val('1');
        onchangeModuleRpt();
        return;
    }
    if ($('#cal-range').data('daterangepicker')) {
        $('#cal-range').data('daterangepicker').setStartDate(moment());
        $('#cal-range').data('daterangepicker').setEndDate(moment());
        startDate = '';
        endDate = '';
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
    }
    $('.searchText').val('');
    $('.selectFilter').val('');
    $('.modfilter').val('');

    initPagination();
}

function downloadReport() {
    if (_type == 'course') { $('#pageUsers_wrapper .buttons-excel').click(); }
    else { $('#userWise_wrapper .buttons-excel').click(); }
}




var chart;
var modTable = [];
function drawChart() {
    modTable = [];
    $("#quesBody").html("");
    let courseid = $('#modcourselist').val();
    let country = $('#countrylist').val();
    let group = $('#modgrouplist').val();
    let gData = [];
    gData.push(['', 'Correct', 'Incorrect']);
    let maxQuestion = 15;
    modTable.push({ row1: "Module Title", row2: $('#modcourselist option:selected').text() });
    if (resultObj.mreport && resultObj.mreport.modgraph) {
        let filter = { courseId: parseInt(courseid) };
        if (country) {
            filter['countryid'] = parseInt(country);
        }
        if (group) {
            filter['groupid'] = group;
        }
        let courseData = _.filter(resultObj.mreport.modgraph, filter);
        for (var i = 1; i <= maxQuestion; i++) {
            let correctQuestionData = _.filter(courseData, { qId: i, iscorrect: true });
            let incorrectQuestionData = _.filter(courseData, { qId: i, iscorrect: false });

            let totalCorrect = correctQuestionData.length;
            let totalIncorrect = incorrectQuestionData.length;
            let total = totalCorrect + totalIncorrect;
            if (correctQuestionData.length > 0 || incorrectQuestionData.length > 0) {
                let question = correctQuestionData.length > 0 ? correctQuestionData[0].question : (incorrectQuestionData.length > 0 ? incorrectQuestionData[0].question : "");
                modTable.push({ row1: "", row2: "" });
                modTable.push({ row1: "Question " + i, row2: question });
                modTable.push({ row1: "Correct responses", row2: Math.round((totalCorrect / total) * 100) + "%(" + totalCorrect + ")" });
                modTable.push({ row1: "Incorrect responses", row2: Math.round((totalIncorrect / total) * 100) + "%(" + totalIncorrect + ")" });

                $("#quesBody").append("<tr><td><div class='mquestion'><b>" + 'Q' + i + "</b>: " + question + "</div></td><td style='text-align:center;'>" + totalCorrect + "</td><td style='text-align:center;'>" + totalIncorrect +"</td></tr>");
                gData.push(['Q' + i, (totalCorrect / total) * 100, (totalIncorrect / total) * 100]);
            }
        }
    }

    if (gData.length > 1) {
        // Define the chart to be drawn.
        var data = google.visualization.arrayToDataTable(gData);
        var options = {
            title: '',
            legend: 'top',
            colors: ['#6bc04b', '#692771'],
            vAxis: {
                viewWindow: {
                    max: 100
                },
                format: '#\'%\''
            },
            bar: {
                gap: 0
            },

            'chartArea': { 'width': '90%', 'height': '80%', },

        };


        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: function (dataTable, column) {
                    return dataTable.getFormattedValue(column, 1);
                },
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2,
            {
                calc: function (dataTable, column) {
                    return dataTable.getFormattedValue(column, 2);
                },
                sourceColumn: 2,
                type: "string",
                role: "annotation"
            }
        ]);

        chart = new google.visualization.ColumnChart(document.getElementById('chartt'));
        chart.draw(view, options);
    } else {
        $("#chartt").css({ "width": 100 + "%" });
        $("#chartt").html("<div class='no-records'>No records found</div>");
    }
}

function downloadOverallChart() {
    let fName = $('#modcourselist option:selected').text() + "_" + $('#modgrouplist option:selected').text() + "_" + $('#countrylist option:selected').text();
    saveAs(chart.getImageURI(), fName + ".png");
}


function onchangeModuleRpt() {
    let cVal = $("input[name='mreport']:checked").val();
    if (cVal == "owise") {
        $(".overall").hide();
        $(".optionwise").show();
        drawResByOpt();
    } else {
        $(".optionwise").hide();
        $(".overall").show();
        drawChart();
    }
   
}

var overAllTable = [];
function drawResByOpt() {
    overAllTable = [];
    let courseid = $('#modcourselist').val();
    let country = $('#countrylist').val();
    let group = $('#modgrouplist').val();
    overAllTable.push({ row1: "Module Title", row2: $('#modcourselist option:selected').text() });
    overAllTable.push({ row1: "", row2: "" });
    if (resultObj.mreport && resultObj.mreport.modquestions) {
        let filter = { courseId: parseInt(courseid) };
        if (country) {
            filter['countryid'] = parseInt(country);
        }
        if (group) {
            filter['groupid'] = group;
        }
        let modQues = _.filter(resultObj.mreport.modquestions, { courseId: parseInt(courseid) });
        modQues.forEach(function (ques) {
            ques.options = _.filter(resultObj.mreport.modqopts, { qId: ques.qId, subcourseId: ques.subcourseId });
            ques.options.forEach(function (option) {
                option.ansCount = 0;
                let modFilter = _.clone(filter);
                modFilter["qId"] = ques.qId;
                modFilter["subcourseId"] = ques.subcourseId;
                let userAnswers = _.filter(resultObj.mreport.modgraph, modFilter);
                userAnswers.forEach(function (uAns) {
                    let ansList = uAns.answers.split(":::");
                    if (_.includes(ansList, option.options)) {
                        option.ansCount++;
                    }
                });

            });
        });

        let resHtml = "";
        modQues.forEach(function (Que) {
            resHtml += "<div style='position:relative;'> " +
                "<button type='button' data-qid='" + Que.qId + "' onclick='downloadOptChart(this)' class='assgnUserBtn dButton tblDown fa-pull-right'><i class='fas fa-download'></i> </button>" +
                "<table border='1'>";
            resHtml += generateModQues(Que.qId + ". " + Que.question);
            overAllTable.push({ row1: "Question " + Que.qId, row2: Que.question });

            let total = _.sumBy(Que.options, "ansCount");
            Que.options.forEach(function (Opt) {
                let per = Math.round((Opt.ansCount / total) * 100);
                overAllTable.push({ row1: Opt.options, row2: per + "%(" + Opt.ansCount + ")", isCorrect: Opt.isCorrect });

                resHtml += generateModOption(Opt.options, per, Opt.isCorrect, Opt.ansCount);
            });
            overAllTable.push({ row1: "" , row2: "" });
            resHtml += "</table></div>";
        })
        if (resHtml) {
            $("#questionOpts").html(resHtml);
        } else {
            $("#questionOpts").html("<div class='no-records'>No records found</div>");
        }
    } 
}

function generateModQues(question) {
    return "<tr>" +
        "<td colspan='3'>" + question +
          "</td>" +
        "</tr>" + generateModOpHeader();
}

function generateModOpHeader() {
    return "<tr>" +
        "<td>Response</td>" +
        "<td>%</td>" +
        "<td>Percentage of total response</td>" +
        "</tr>";
}

function generateModOption(opt, percentage, isCorrect, count) {
    percentage = isNaN(percentage) ? 0 : percentage;
    percentage = Math.round(percentage * 100) / 100;
    return "<tr>" +
        "<td>" + opt + "</td>" +
        "<td>" + percentage + "%("+count+")</td>" +
        "<td>" +
            "<div class='cssProgress'>" +
                "<div class='progress1'>" +
        "<div class='cssProgress-bar " + (isCorrect ? 'correctBg' : 'incorrectBg') + "' data-percent='" + percentage + "' style='width: " + percentage +"%;'>" +
                    "</div>" +
                "</div>" +
            "</div>" +
        "</td>" +
        "</tr>";
}


function downloadOptChart(ele) {
    let fName = $('#modcourselist option:selected').text() + "_" + $('#modgrouplist option:selected').text() + "_" + $('#countrylist option:selected').text() + "_Q" + $(ele).data("qid");

    html2canvas($(ele).parent().find("table"), {//give the div id whose image you want in my case this is #cont
        onrendered: function (canvas) {
            saveAs(canvas.toDataURL(), fName + ".png");
        }
    });
}


function downloadModReport(e) {
    let fName = $('#modcourselist option:selected').text() + "_" + $('#modgrouplist option:selected').text() + "_" + $('#countrylist option:selected').text();

    $('#myThead, #myTbody').html('');

    if (modTable) {
        // modTable has the JSON
        // download excel logic here

        // $$$ Converting JSON to table in template $$$
        var obj = modTable[0];
        let header = Object.keys(obj);
        for (var i = 0; i < header.length; i++) {
            document.getElementById('myThead').innerHTML += "<th data-a-v='top' data-a-wrap='true' data-fill-color='6FA8DC' id='th-" + i + "'>" + obj[header[i]] + "</th>";

        }
        for (var j = 1; j < modTable.length; j++) {
            document.getElementById('myTbody').innerHTML += "<tr id='cell" + j + "'>" + "</tr>";
            for (var i = 0; i < header.length; i++) {
                document.getElementById('cell' + j).innerHTML += "<td data-a-wrap='true' data-a-v='top' id='tableCells" + j + i + "'>" + modTable[j][header[i]] + "</td>";
            }
        }
        // $$$ Adding background to the question row $$$
        for (var j = 2; j < modTable.length; j++) {
            if (document.getElementById('cell' + j).previousElementSibling.firstChild.innerHTML == "") {
                var questionTd = document.getElementById('cell' + j);
                questionTd.lastChild.setAttribute('data-fill-color', '93C47D');
                questionTd.firstChild.setAttribute('data-fill-color', '93C47D');
                //questionTd.firstChild.style.backgroundColor = "#93C47D";
            }
        }
    }
    var table = document.getElementById("myTable");
    TableToExcel.convert(table, {
        name: fName + ".xlsx",
        sheet: {
            name: "ModuleQuestionWise"
        }
    });
}

function downloadOverallReport(e) {
    let fName = $('#modcourselist option:selected').text() + "_" + $('#modgrouplist option:selected').text() + "_" + $('#countrylist option:selected').text();

    $('#myThead, #myTbody').html('');
    if (overAllTable) {
        // modTable has the JSON
        // $$$ Converting JSON to table in template $$$
        var tableObj = overAllTable[0];
        console.log(tableObj);
        var header = Object.keys(tableObj);
        for (var i = 0; i < header.length; i++) {
            document.getElementById('myThead').innerHTML += "<th data-a-v='top' data-a-wrap='true' data-fill-color='6FA8DC' id='th-" + i + "'>" + tableObj[header[i]] + "</th>";
        }

        for (var j = 1; j < overAllTable.length; j++) {
            document.getElementById('myTbody').innerHTML += "<tr id='cell" + j + "'>" + "</tr>";
            for (var i = 0; i < header.length; i++) {
                document.getElementById('cell' + j).innerHTML += "<td  data-a-wrap='true' data-a-v='top'  id='tableCells" + j + i + "'>" + overAllTable[j][header[i]] + "</td>";

                if (overAllTable[j].isCorrect) {
                    document.getElementById("tableCells" + j + i).setAttribute('data-f-bold', 'true');
                }
            }
        }
        // $$$ Adding background to the question row $$$
        for (var j = 2; j < overAllTable.length; j++) {
            if (document.getElementById('cell' + j).previousElementSibling.firstChild.innerHTML == "") {
                var questionTd = document.getElementById('cell' + j);
                questionTd.lastChild.setAttribute('data-fill-color', '93C47D');
                questionTd.firstChild.setAttribute('data-fill-color', '93C47D');
                //questionTd.firstChild.style.backgroundColor = "#93C47D";
            }
        }
    }
    var table = document.getElementById("myTable");
    TableToExcel.convert(table, {
        name: fName + ".xlsx",
        sheet: {
            name: "ModuleOptionWise"
        }
    });
}


