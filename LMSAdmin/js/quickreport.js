﻿$(document).ready(function () {
    activeLeftMenu("quickReport");
    let cb = new CallBack();
    cb.func = "AfterReport";
    _adminService.GetQuickReport({}, cb, true);
});

var repRes;
var repRes1;
function AfterReport(res) {
    $(".loader").hide();
    repRes1 = res;
    repRes = res;

}
// Read Report
function downloadReadReport(e) {
    $('#myThead', '#myTbody').html('');
    $('#myThead').html('<th data-a-wrap="true" data-f-color="FFFFFF" data-fill-color="692771">  S No.</th ><th data-a-wrap="true"  data-f-color="FFFFFF" data-fill-color="692771"> Resource Name</th ><th  data-f-color="FFFFFF" data-a-wrap="true" data-fill-color="692771">Resource Type</th ><th  data-f-color="FFFFFF" data-a-wrap="true" data-fill-color="692771"> Number of Unique Reads </th><th></th>');
    if (repRes1.readreport) {
        var readreport = repRes1.readreport;
        // $$$ Converting JSON to table in template $$$
        var obj = readreport[0];
        var header = Object.keys(obj);
        // for aligning text in right side in excel use data-a-h='right' align='right' data-a-rtl='true'
        for (var j = 0; j < readreport.length; j++) {
            document.getElementById('myTbody').innerHTML += "<tr id='cell" + j + "'>" + "</tr>";
            for (var i = 0; i < header.length; i++) {
                document.getElementById('cell' + j).innerHTML += "<td data-a-wrap='true' data-a-v='top'  id='tableCells" + j + i + "'>" + readreport[j][header[i]] + "</td>";

            }
        }
        
    }
    var table = document.getElementById("quickreportTable");
    TableToExcel.convert(table, {
        name: "ResourceReadReport.xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
}

// Resource Report
function downloadResourceReport(e) {
    $('#myThead,#myTbody').html('');
    $('#myThead').html('<th data-a-wrap="true"  data-f-color="FFFFFF" data-fill-color="692771"> Module/Curriculum/Track </th > <th data-a-wrap="true"  data-f-color="FFFFFF" data-fill-color="692771"> Resource</th ><th data-a-wrap="true"  data-f-color="FFFFFF" data-fill-color="692771"> Required/Optional </th >');
    if (repRes) {
        delete repRes.readreport;
        // $$$ Converting JSON to table in template $$$
        var repResKeys = Object.keys(repRes);
        for (var i = 0; i < repResKeys.length; i++) {
            var modKey = Object.keys(repRes[repResKeys[i]][i]);
            var modReportData = repRes[repResKeys[i]];
            for (var j = 0; j < modReportData.length; j++) {
                document.getElementById('myTbody').innerHTML += "<tr  id='cell" + i + j + "'>" + "</tr>";
                for (var k = 0; k < modKey.length; k++) {
                    document.getElementById('cell' + i + j).innerHTML += "<td data-a-wrap='true' data-a-v='top'  id='tableCells" + j + k + "'>" + (k == 2 ? modReportData[j][modKey[k]] ? "Required" : "Optional" :  modReportData[j][modKey[k]]) + "</td>";
                }
            }
            document.getElementById("cell" + i + (modReportData.length - 1)).insertAdjacentHTML("afterend", "<tr><td data-fill-color='692771'></td><td data-fill-color='692771'></td></tr>");
        }
    }
    var table = document.getElementById("quickreportTable");
    TableToExcel.convert(table, {
        name: "ResourceMappingReport.xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
}
