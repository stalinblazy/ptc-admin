﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="quickReport.aspx.cs" Inherits="quickReport" %>

<!DOCTYPE html>

<html lang="en">
<title>Quick Reports</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css" />
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="js/tableToExcel.js"></script>
<link rel="stylesheet" href="css/styledev.css">
<link rel="stylesheet" href="css/custom-dev.css">
<body>
    <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />

        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />

            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Quick Reports
                        </div>
                    </div>
                </div>

                <div class="row pageBody">

                   
                    <!-- two div boxes -->
                    <div class="col-md-12" id="quickDiv">
                        <div class="row">
                            <div class="col-md-6 quickContent" style="padding: 0; padding-right: 5px;">
                                <div class="box col-md-12">
                                    <div class="col-md-12" style="padding: 0;">
                                        <h1>Resource Read Report</h1>
                                          <div class="pull-right" style="text-align: right; display: inline-block; margin-right: 5px;border-bottom: 1px solid #EBEDF2;">
                                            <button class="assgnUserBtn dButton fa-pull-right quickBtn" id="readBtn" onclick="downloadReadReport(event)"  type="button"><i class="fas fa-download"></i>&nbsp;Quick Download</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 quickContent" style="padding: 0; padding-left: 5px;">
                                <div class="box col-md-12">
                                    <div class="col-md-12" style="padding: 0;">
                                        <h1>Resource Mapping Report</h1>
                                        <div class="pull-right" style="text-align: right; display: inline-block; margin-right: 5px;border-bottom: 1px solid #EBEDF2;">
                                            <button class="assgnUserBtn dButton fa-pull-right quickBtn" id="resourceBtn" onclick="downloadResourceReport(event)" type="button"><i class="fas fa-download"></i>&nbsp; Quick Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table style="display: none;" id="quickreportTable" border="1" data-cols-width="50,50,50">
                    <thead>
                        <tr id="myThead">
                        </tr>
                    </thead>
                    <tbody id="myTbody"></tbody>
                </table>

            </div>
        </div>

    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="js/adminservice.js"></script>
    <script src="js/quickreport.js"></script>
</body>
</html>
