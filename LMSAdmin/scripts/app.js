var labels123 = ['0', '20', '40', '60', '120'];

Highcharts.chart('listOfUsersChart', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'List of Users'
  },
  // subtitle: {
  //   text: 'Source: WorldClimate.com'
  // },
  xAxis: {
    categories: [
      'Module 1',
      'Module 2',
      'Module 3',
      'Module 4',
      'Module 5',
      'Module 6'
    ],
    crosshair: true
  },
  yAxis: {
    title: {
      text: ''
    },
    labels: {
     formatter: function() {
       if (this.isFirst) {
         i = -1
       }
       i++;
       return labels123[i]
     }
   }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },

  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'Example 1',
    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0]

  }, {
    name: 'Example 2',
    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]

  }, {
    name: 'Example 3',
    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3]

  }]
});
