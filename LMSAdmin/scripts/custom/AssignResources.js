﻿var fromDate = "", endDate = "", courseId;
var resourceTable;
$(document).ready(function () {


    $(".rmbulkActionbtn").hide();
    activeLeftMenu("CourseManagement");
    var moduleName = sessionStorage.getItem("moduleNameResources");
    courseId = sessionStorage.getItem("courseIdResources");
    $("#lblModuleName").text(moduleName);
    BindAssignResourceList(fromDate, endDate);
    //BindCourceUserListWise(courseId);

    $("#prevBtn").on("click", function () {
        resourceTable.page('previous').draw('page');
        initPagination();
    });

    $("#nextBtn").on("click", function () {
        resourceTable.page('next').draw('page');
        initPagination();
    });

    $("#firstBtn").on("click", function () {
        resourceTable.page('first').draw('page');
        initPagination();
    });

    $("#lastBtn").on("click", function () {
        resourceTable.page('last').draw('page');
        initPagination();
    });

});


function BindAssignResourceList(fromDate, endDate) {
    try {
        var params = JSON.stringify({ "fromDate": fromDate, "endDate": endDate, "courseId": courseId });
        $.ajax({
            type: "POST",
            url: "AssignResources.aspx/GetAssignResourceData",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initResourceData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {

        Console.log(ex);
    }
}
var activeResourceId = "";
var dataMap = {};
function initResourceData(data) {

    var filteredSub = data.filter(function (subc) {
        dataMap[subc.ID] = subc.isRequired;
        if (subc.CourseId == courseId) {
            return subc.ID;
        }
    });
    activeResourceId = filteredSub.map(function (a) { return a.ID }).join(',');
    resourceTable = $('#pageResource').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mData": "ID" },
            { "mData": "Name" },
            { "mData": "Name1" },
            { "mData": "Description" },
            {
                "mData": "CreatedDate",
                "type": "date",
                "render": function (data) {
                    if (data === null) return "";

                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");
                    return data;
                }
            },
            {
                "mData": "ModifiedDate",
                "type": "date",
                "render": function (data) {
                    if (data === null) return "";
                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");
                    return data;
                }
            },
            {
                "mData": "IsActive",
                "render": function (data, type, row, meta) {
                    if (type === 'display') {
                        data = '<select onchange="updateDataMap(' + row.ID + ',this.value)" class="mdb-select md-form resource' + row.ID + '"  id="acivebtn">';
                        if (row.isRequired == true) {
                            data += '<option value="0" data-id=' + row.ID + ' >Optional</option>';
                            data += '<option value="1" data-id=' + row.ID + ' selected="selected">Required</option>';
                        }
                        else {
                            data += '<option value="0" data-id=' + row.ID + ' selected="selected">Optional</option>';
                            data += '<option value="1" data-id=' + row.ID + ' >Required</option>';
                        }
                        data += '</select>';
                    }
                    return data;
                }
            }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "stateSave": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'initComplete': function (settings) {
            var api = this.api();
            api.cells(api.rows(function (idx, data, node) {
                return (data.CourseId == courseId) ? true : false;
            }).indexes(), 0).checkboxes.select();
        },
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    return '<div class="checkbox"><input type="checkbox"  data-id =' + row.ID + ' class="dt-checkboxes"><label></label></div>';
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': 'multi',
        'order': [[1, 'asc']]
    });

    initPagination();
    $('#pageResource').on('draw.dt', function (e, settings, len) {
        initPagination();
    });
}

function updateDataMap(id, value) {
    dataMap[id] = value == "1" ? true : false;
}

function initPagination() {
    if (resourceTable) {
        $(".pagg-btn").attr("disabled", false);
        let page = resourceTable.page.info();
        $("#total-rec").html(page.recordsTotal + " Resources");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

function searchTable() {
    resourceTable && resourceTable.search($('#searchTable').val()).draw();
}


$(document).on('click', '#btnAssignCourseResource', function (e) {
    let jsonData = [];
    let selectedResources = resourceTable.column(0).checkboxes.selected().toArray();
    selectedResources.forEach(function (Id) {
        let obj = {};
        obj.IsRequired = dataMap[Id] || false;
        obj.ResourceID = Id;
        jsonData.push(obj);
    });
    var params = JSON.stringify({ "courseId": courseId, "resourceIds": jsonData, "activeResourceId": "" });
    $.ajax({
        type: "POST",
        url: "AssignResources.aspx/BullkAssignResources",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location.href = "CourseManagement.aspx";
            toastr.success("Resource Update Successfully..");
        },
        error: function (res) {
            console.log(res);
        }
    });

})
