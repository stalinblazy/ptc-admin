﻿function activeLeftMenu(menuId) {
    $("#leftNavigation li.active").removeClass("active");
    $("#leftNavigation li#" + menuId).addClass("active");
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getUnmatchedElementsFromArray(array1, array2) {
    var result = array2.filter(function (a) {
        return array1.indexOf(a) === -1;
    });
    return result;
}

function toastWrapper() { };
toastWrapper.prototype.success = function(message) {
    $.toast({
        heading: 'Success',
        text: message,
        showHideTransition: 'slide',
        icon: 'success'
    })
}
toastWrapper.prototype.info = function (message) {
    $.toast({
        heading: 'Info',
        text: message,
        showHideTransition: 'slide',
        icon: 'info'
    })
}
toastWrapper.prototype.error = function (message) {
    $.toast({
        heading: 'Error',
        text: message,
        showHideTransition: 'slide',
        icon: 'error'
    })
}

var toastr = new toastWrapper();