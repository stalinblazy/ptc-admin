﻿var tblUsers;
var tblGroups;
var fromDate = "";
var endDate = "";
var moduleName, courseId;
var activeUserId, activeGroupId;
var isUserOptionSelected = true;
$(document).ready(function () {
    activeLeftMenu("CourseManagement");
    $(".loader").fadeOut();
    $('#cal-range').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
    });

    moduleName = sessionStorage.getItem("moduleName");
    courseId = sessionStorage.getItem("courseId");
    $("#lblModuleName").text(moduleName);

    BindCourceUserListWise(courseId);
    BindGroupList(courseId);

    $("#prevBtn").on("click", function () {
        tblUsers.page('previous').draw('page');
        initPaginationUser();
        
    });

    $("#nextBtn").on("click", function () {
        tblUsers.page('next').draw('page');
        initPaginationUser();
        
    });

    $("#firstBtn").on("click", function () {
        tblUsers.page('first').draw('page');
        initPaginationUser();
        
    });

    $("#lastBtn").on("click", function () {
        tblUsers.page('last').draw('page');
        initPaginationUser();
         
    });

    $("#prevBtn1").on("click", function () {
        tblGroups.page('previous').draw('page');
        initGroupPagination();
        
    });

    $("#nextBtn1").on("click", function () {
        tblGroups.page('next').draw('page');
        initGroupPagination();
        $(".tooltipdata").tooltip();
    });

    $("#firstBtn1").on("click", function () {
        tblGroups.page('first').draw('page');
        initGroupPagination();
        $(".tooltipdata").tooltip();
    });

    $("#lastBtn1").on("click", function () {
        tblGroups.page('last').draw('page');
        initGroupPagination();
        $(".tooltipdata").tooltip();
    });


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href"); // activated tab
        $('#searchTable').val('');
        $('[name="gender"]').removeAttr('checked');
        if (target == "#tab_GroupList") {
            $("input[name=gender][value=Groups]").prop('checked', 'checked');
            isUserOptionSelected = false;
            BindGroupList(courseId);
        }
        else {
            $("input[name=gender][value=Users]").prop('checked', 'checked');
            isUserOptionSelected = true;
            BindCourceUserListWise(courseId);
        }
    });

    $('.datepicker').datetimepicker({
        format: 'L'
    });
});
var arrUsers = [];
var arrGroups = [];

function addSelectedItemsToUl(ele, values, id) {
    $("#" + ele).append('<li>' + values + ' <i class="fa fa-times-circle" id="cancelSelectedUser" data-name="' + values + '"  data-id ="' + id + '"></i></li>');
}
function removeSelectedItemsToUl(ele, values) {

    $("#" + ele + " li:contains('" + values + "')").remove();
}

// for Group
function addSelectedItemsToUlForGroup(ele, values, id) {

    $("#" + ele).append('<li>' + values + ' <i class="fa fa-times-circle" id="cancelSelectedGroup" data-name="' + values + '"  data-id ="' + id + '"></i></li>');
}
function removeSelectedItemsToUlForGroup(ele, values) {
    $("#" + ele + " li:contains('" + values + "')").remove();
}

// Array holding selected row IDs
var rows_selected = [];


function initUsersData(data) {
    $("#selectedCourseUsers").empty();
    var filteredSub = data.filter(function (subc) {
        if (subc.IsActiveCourseUser == true) {
            arrUsers.push(subc.First_Name + ' ' + subc.Last_Name);
            addSelectedItemsToUl("selectedCourseUsers", subc.First_Name + ' ' + subc.Last_Name, subc.User_Id);
            return subc.User_Id;
        }
    });
    rows_selected = filteredSub.map(function (a) { return a.User_Id });
    activeUserId = filteredSub.map(function (a) { return a.User_Id }).join(',');
    tblUsers = $('#pageUsers').DataTable({
        "aaData": data,
        "aoColumns": [
            //{
            //    "render": function (data, type, row) {
            //        if (type === 'display') {
            //            if (row.IsActiveCourseUser == false) {
            //                data = '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes" name="UserIdChk" onclick="updateSelectedUserList(\'' + row.First_Name + ' ' + row.Last_Name + '\',this,' + row.User_Id + ')"><label></label></div>';
            //            }
            //            else {
            //                data = '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes" name="UserIdChk" onclick="updateSelectedUserList(\'' + row.First_Name + ' ' + row.Last_Name + '\',this,' + row.User_Id + ')" checked><label></label></div>';
            //            }
            //        }
            //        return data;
            //    }
            //},
            { "mData": "User_Id" },
            { "mData": "First_Name" },
            { "mData": "Last_Name" },
            {
                "mData": "Email_ID"
                //"render": function (data, type, row, meta) {
                //    data = '<a href="AddUser.aspx?id=' + row.User_Id + '">' + data + '</a>';
                //    return data;
                //}
            },
            { "mData": "Designation" },
            { "mData": "DepartmentID" },
            { "mData": "ReportingManagerID" },
            {
                "mData": "DueDate",
                "type": "date",
                "render": function (data, type, row, table) {
                    let date = "";
                    if (data !== null) {

                        var pattern = /Date\(([^)]+)\)/;
                        var results = pattern.exec(data);
                        var dt = new Date(parseFloat(results[1]));
                        date = moment(new Date(dt)).format("MM/DD/YYYY");
                    }
                    if (type !== 'display') {
                        return date;
                    }
                    return '<div><button data-row="' + table.row +'" data-userid="' + row.User_Id + '" data-duedate="' + row.DueDate + '" data-courseid="' + courseId + '" data-email="' + row.Email_ID + '" data-isrequired="' + row.DateNotRequired + '" onclick="openDueDateModal(this)" class="btn btn-custom btn-icon-small btn-sm mr-1"><span class="fa fa-pencil"></span></button><span>' + (row.DateNotRequired ? "Not Required" : (date ? date : "Course")) + '</span></div>';
                }
            }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "stateSave": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    if (type === 'display') {
                        if (row.IsActiveCourseUser == false) {
                            data = '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes" name="UserIdChk" onclick="updateSelectedUserList(\'' + row.First_Name + ' ' + row.Last_Name + '\',this,' + row.User_Id + ')"><label></label></div>';
                        }
                        else {
                            data = '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes" name="UserIdChk" onclick="updateSelectedUserList(\'' + row.First_Name + ' ' + row.Last_Name + '\',this,' + row.User_Id + ')" checked><label></label></div>';
                        }
                    }
                    return data;
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" name="select_all" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        //'rowCallback': function(row, data, dataIndex){
        //    // Get row ID
        //    var rowId = row.User_Id;

        //    // If row ID is in the list of selected row IDs
        //    if($.inArray(rowId, rows_selected) !== -1){
        //        $(row).find('input[type="checkbox"]').prop('checked', true);
        //        $(row).addClass('selected');
        //    }
        //},
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
            
        }
    });

    initPaginationUser();
    $('#pageUsers').on('draw.dt', function (e, settings, len) {
        initPaginationUser();
    });
    //updateDataTableSelectAllCtrl(tblUsers);
    // Handle click on checkbox
    $('#pageUsers tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = tblUsers.row($row).data();

        // Get row ID
        var rowId = data.User_Id;

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        // updateDataTableSelectAllCtrl(tblUsers);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });
}

function openDueDateModal(ele) {
   
    let data = $(ele).data();

    if (data.duedate) {
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(data.duedate);
        var dt = new Date(parseFloat(results[1]));
        $("#selDueDate").datetimepicker({
            date: new Date(dt)
        });
        $('#selDueDate').datetimepicker({});
        $('#selDueDate').data("DateTimePicker").date(moment(new Date(dt)).format('MM/DD/YYYY'));
    } else {
        $('#selDueDate').datetimepicker({});
        $('#selDueDate').data("DateTimePicker").clear();
    }
    $("#selUserId").val(data.userid);
    $("#selrow").val(data.row);
    $("#selCourseId").val(data.courseid)
    $("#selEmail").html(data.email);

    $('#dateReq').prop('checked', data.isrequired);

    $("#setDateModal").modal('show');
}

function onSaveDueDate(ele) {
    $('.dudabtn').attr('disabled', 'disabled');
    let obj = {
        CourseId: parseInt($("#selCourseId").val()),
        UserId: parseInt($("#selUserId").val()),
        DueDate: $('#selDueDate').val(),
        DateNotRequired: $('#dateReq').is(":checked"),
        Row: $("#selrow").val()
    }

    let cb = new CallBack();
    cb.func = "AfterSetDueDate";
    cb.data = obj;

    _adminService.SetDueDate(obj, cb, true);
}

function AfterSetDueDate(res, obj) {
    $('.dudabtn').removeAttr('disabled');
    if (res) {
        var temp = tblUsers.row(obj.Row).data();
        temp.DateNotRequired = obj.DateNotRequired;
        if (obj.DueDate) {
            temp.DueDate = "/Date(" + moment(new Date(obj.DueDate)).valueOf() + ")/";
        } else {
            temp.DueDate = null;
        }
        tblUsers.row(parseInt(obj.Row)).data(temp).invalidate();
        $("#setDateModal").modal('hide');
        $.toast({
            heading: 'Success',
            text: 'Duedate updated successfully.',
            showHideTransition: 'slide',
            icon: 'success'
        })

    }
}

function updateSelectedUserList(values, chkObj, Id) {
    var index = arrUsers.indexOf(values);
    if (chkObj.checked) {
        if (index === -1) {
            arrUsers.push(values)
            addSelectedItemsToUl("selectedCourseUsers", values, Id);
        }
    }
    else {
        if (index !== -1) {
            arrUsers.splice(index, 1); //Remove
            removeSelectedItemsToUl("selectedCourseUsers", values);
        }
    }
}

function updateSelectedGroupList(values, chkObj, Id) {
    var index = arrGroups.indexOf(values);
    if (chkObj.checked) {
        if (index === -1) {
            arrGroups.push(values)
            addSelectedItemsToUlForGroup("selectedCourseGroups", values, Id);
        }
    }
    else {
        if (index !== -1) {
            arrGroups.splice(index, 1); //Remove
            removeSelectedItemsToUlForGroup("selectedCourseGroups", values);
        }
    }
}

//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table) {

    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

//function InitCheckboxes() {
//    var checkboxes = document.querySelectorAll('input.dt-checkboxes');
//    var checkall = document.getElementsByClassName('dt-checkboxes---');

//    for (var i = 0; i < checkboxes.length; i++) {
//        checkboxes[i].onclick = function () {
//            var checkedCount = document.querySelectorAll('input.dt-checkboxes:checked').length;

//            checkall.checked = checkedCount > 0;
//            checkall.indeterminate = checkedCount > 0 && checkedCount < checkboxes.length;
//        }
//    }

//    checkall.onclick = function () {
//        for (var i = 0; i < checkboxes.length; i++) {
//            checkboxes[i].checked = this.checked;
//        }
//    }
//}

function changeState(el) {
    //    if (el.readOnly) el.checked = el.readOnly = false;
    //   else if (!el.checked) el.readOnly = el.indeterminate = true;
}

function initPaginationUser() {

    if (tblUsers) {
        $(".pagg-btn").attr("disabled", false);
        let page = tblUsers.page.info();
        $("#total-rec").html(page.recordsTotal + " Users");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }

        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

function searchTable() {
    if (isUserOptionSelected) {
        tblUsers && tblUsers.search($('#searchTable').val()).draw();
        $("#total-rec").html(tblUsers.page.info().recordsDisplay + " Users");
    }
    else {
        tblGroups && tblGroups.search($('#searchTable').val()).draw();
        $("#total-rec1").html(tblGroups.page.info().recordsDisplay + " Groups");
    }
}

function BindCourceUserListWise(courseId) {
    try {
        var params = JSON.stringify({ "courseId": courseId });
        $.ajax({
            type: "POST",
            url: "AssignUsers.aspx/BindUnAssignedCourseUserList",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initUsersData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

$('#btnAssignCourseUser').on("click", function () {
    try {
        var userId = "";
        var newUsers = "";
        userId = rows_selected.toString();
        var list = getUnmatchedElementsFromArray(userId.split(","), activeUserId.split(","));
        var arrnewUsers = tblUsers.column(0).checkboxes.selected().toArray();
        // var newUsers = [];
        if (arrnewUsers.length > 0) {
            newUsers = arrnewUsers.join(',');
        }
        //var params = JSON.stringify({ "courseId": courseId, "userIds": userId, "activeUserIds": list.join(","), "courseName": moduleName, "newUsers": newUsers });
        var params = JSON.stringify({ "courseId": courseId, "userIds": userId, "activeUserIds": "", "courseName": moduleName, "newUsers": newUsers });
       // alert(params);
        $.ajax({
            type: "POST",
            url: "AssignUsers.aspx/BullkAsignUser",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                BindCourceUserListWise(courseId);
                window.location = 'CourseManagement.aspx?IsFromCourse=true';
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
});

function BindGroupList(courseId) {
    try {
        var range = $('.drp-selected').text();
        if (range) {
            var dateRange = range.split('-');
            fromDate = dateRange[0].trim();
            endDate = dateRange[1].trim();
        }
        else {
            fromDate = "";
            endDate = "";
        }
        var params = JSON.stringify({ "fromDate": fromDate, "endDate": endDate, "courseId": courseId });
        $.ajax({
            type: "POST",
            url: "AssignUsers.aspx/BindUnAssignedCourseGroupList",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                console.log(aaData);
                initGroupData(aaData);
                $(".tooltipdata").tooltip();
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

// Array holding selected row IDs
var rows_selected_group = [];


function initGroupData(data) {
    //alert(data);
    $("#selectedCourseGroups").empty();
    //var filteredSub = data.filter(function (subc) {
    //    if (subc.IsActiveCourseGroup == true) {
    //        return subc.ID;
    //    }
    //});
    var filteredSub = data.filter(function (subc) {
        if (subc.IsActiveCourseGroup == true) {
            arrGroups.push(subc.Name);
            addSelectedItemsToUlForGroup("selectedCourseGroups", subc.Name, subc.ID);
            return subc.ID;
        }
    });
    //alert(filteredSub);
    activeGroupId = filteredSub.map(function (a) { return a.ID }).join(',');
    tblGroups = $('#pageGroups').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mData": "ID" },
            { "mData": "Name" },
            { "mData": "NoOfUser" },
            {
                "mData": "CreatedDate",
                "type": "date",
                "render": function (data) {
                    if (data === null) return "";

                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");
                    return data;
                }
            },
            {
                "mData": "ModulesAssigned",
                "type": "date",
                "render": function (data) {
                    if (data === null) return "";
                    var str = "";
                    var arr = data.split(",");
                    $.each(arr, function (index, item) {
                        var temp = item.split("|");
                        str += "<span class='tooltipdata' title='" + temp[1] + "'>" + temp[0] + "</span>"
                        // do something with `item` (or `this` is also `item` if you like)
                    });
                    data = str;
                    return data;
                }
            }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    if (type === 'display') {
                        if (row.IsActiveCourseGroup == false) {
                            data = '<div class="checkbox"><input type="checkbox" data-id =' + row.ID + ' class="dt-checkboxes"  onclick="updateSelectedGroupList(\'' + row.Name + '\',this,' + row.ID + ')"><label></label></div>';
                        }
                        else {
                            data = '<div class="checkbox"><input type="checkbox" data-id =' + row.ID + ' class="dt-checkboxes"  onclick="updateSelectedGroupList(\'' + row.Name + '\',this,' + row.ID + ')" checked><label></label></div>';
                        }
                    }
                    return data;
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" name="select_all" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data.ID;

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected_group) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        }
    });

    //tblGroups.on("click", "#chkGroupsList", function () {
    //    
    //    var cells = tblGroups.cells().nodes();
    //    $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    //});

    initGroupPagination();
    $('#pageGroups').on('draw.dt', function (e, settings, len) {
        initGroupPagination();
    });
    // Handle click on checkbox
    $('#pageGroups tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = tblGroups.row($row).data();

        // Get row ID
        debugger
        var rowId = data.ID;

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected_group);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected_group.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected_group.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        debugger;
        // Update state of "Select all" control
        //updateDataTableSelectAllCtrl(tblGroups);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on "Select all" control
    $('#pageGroups thead input[name="select_all"]', tblGroups.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#pageGroups tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#pageGroups tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    //tblGroups.on('draw', function(){
    //    debugger;
    //    // Update state of "Select all" control
    //    //updateDataTableSelectAllCtrl(tblGroups);
    //});

}

function initGroupPagination() {
    if (tblGroups) {
        $(".group-pagg-btn").attr("disabled", false);
        let page = tblGroups.page.info();
        $("#total-rec1").html(page.recordsTotal + " Groups");
        if (page.pages == 1) {
           // $(".group-pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data1").html(cPage + " of " + lPage);
        if (cPage == 1) {
           // $("#firstBtn1,#prevBtn1").attr("disabled", true);
        }
        if (cPage == lPage) {
           // $("#lastBtn1,#nextBtn1").attr("disabled", true);
        }
    }
}

$('.applyBtn').on("click", function () {
    BindGroupList(courseId);
});


$('#btnAssignCourseGroup').on("click", function () {
    try {
        var groupId = tblGroups.$("input:checkbox:checked").map(function () { return $(this).attr('data-id'); }).get().join(',');
        var list = getUnmatchedElementsFromArray(groupId.split(","), activeGroupId.split(","));
        //var params = JSON.stringify({ "courseId": courseId, "groupIds": groupId, "activeGroupIds": list.join(",") });
        var params = JSON.stringify({ "courseId": courseId, "groupIds": groupId, "activeGroupIds": "" });
       // alert(params);
        $(".loader").show();
        $.ajax({
            type: "POST",
            url: "AssignUsers.aspx/BullkAsignGroup",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                BindGroupList(courseId);

                window.location = 'CourseManagement.aspx?IsFromCourse=true';
            },
            error: function (res) {
                $(".loader").hide();
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
});

function compare(string1, string2) {
    var arr1 = string1.split(",");
    var arr2 = string2.split(",");
    var mergedArray = $.merge(arr1, arr2);

    var tt = [];

    for (var i = 0; i < mergedArray.length; i++) {
        var equal = false;
        for (var j = 0; j < mergedArray.length; j++) {
            if (i != j) {
                if (mergedArray[i] == mergedArray[j]) {
                    equal = true;
                    break;
                }
            }
            else {
                equal = false;
            }
        }
        if (equal == false) {
            tt.push(mergedArray[i]);
        }
    }
    return tt.toString();
}

$(document).on("click", "#cancelSelectedUser", function (e) {
    var User_Id = $(this).attr("data-id");
    var values = $(this).attr("data-name");
    tblUsers.$('input:checkbox[name=UserIdChk]').map(function () {
        if (User_Id == $(this).attr('data-id')) {
            $(this).prop('checked', false)
            var index = arrUsers.indexOf(values);
            arrUsers.splice(index, 1); //Remove
            removeSelectedItemsToUl("selectedCourseUsers", values);
        }
    })
});

$(document).on("click", "#cancelSelectedGroup", function (e) {
    var Group_Id = $(this).attr("data-id");
    var values = $(this).attr("data-name");
    tblGroups.$('input:checkbox[name=GroupIdChk]').map(function () {
        if (Group_Id == $(this).attr('data-id')) {
            $(this).prop('checked', false)
            var index = arrGroups.indexOf(values);
            arrGroups.splice(index, 1); //Remove
            removeSelectedItemsToUlForGroup("selectedCourseGroups", values);
        }
    })
});

function onchangeTab() {
    var name = $("input[name='gender']:checked").val();
    if (name == "Users") {
        $("#tab_UserList").show();
        $("#tab_GroupList").hide();
    } else {
        $("#tab_UserList").hide();
        $("#tab_GroupList").show();
    }

}