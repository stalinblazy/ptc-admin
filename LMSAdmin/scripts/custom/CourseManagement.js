﻿var tblCourse;
var fromDate = "", endDate = "";

$(document).ready(function () {
    activeLeftMenu("CourseManagement");
    
    $('#cal-range').daterangepicker({
        //startDate: '2005/05/03',
        //endDate: '2005/06/03',
        locale: {
            format: 'YYYY/MM/DD'
        },
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
        // $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
    var IsSuccessCourse = getParameterByName("IsFromCourse");
    if (IsSuccessCourse == "true") {
        toastr.success("Course saved successfully.");
        if (window.location.href.indexOf('?') > -1) {
            history.pushState('', document.title, window.location.pathname);
        }
    }
    BindCourseList(fromDate, endDate);

    $("#prevBtn").on("click", function () {
        tblCourse.page('previous').draw('page');
        initPagination();
    });

    $("#nextBtn").on("click", function () {
        tblCourse.page('next').draw('page');
        initPagination();
    });

    $("#firstBtn").on("click", function () {
        tblCourse.page('first').draw('page');
        initPagination();
    });

    $("#lastBtn").on("click", function () {
        tblCourse.page('last').draw('page');
        initPagination();
    });
    
});


function searchTable() {
    tblCourse && tblCourse.search($('#searchTable').val()).draw();
    $("#total-rec").html(tblCourse.page.info().recordsDisplay + " Courses");   
}

function BindCourseList(fromDate, endDate) {
    debugger;
    try {
        $("#searchTable").val("");
        var params = JSON.stringify({ "fromdate": fromDate, "enddate": endDate});
        $.ajax({
            type: "POST",
            url: "CourseManagement.aspx/BindCourseList",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {               
                var aaData = JSON.parse(data.d);
                initCourseData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}


function generateAction(row, data) {
    let cls = (row.Active === true ? 'btn-active' : 'btn-inactive');
    return '<div class="btn-group status-btn">' +
        '<button type="button" class="btn ' + cls + ' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (data === true ? 'Active' : 'Inactive') + '</button>' +
        '<div class="dropdown-menu  dropdown-menu-right">' +
        '<a class="dropdown-item" data-id=' + row.Course_Id + ' data-val="1"  value="1" data-current="' + data + '" onclick="toggleStatus(this)" href="javascript:void(0)">Active</a>' +
        '<a class="dropdown-item" data-id=' + row.Course_Id + '  data-val="0" value="0" data-current="' + data + '" onclick="toggleStatus(this)" href="javascript:void(0)">Inactive</a>' +
        '<div class="dropdown-divider"></div>' +
        '<a class="dropdown-item" onclick="deleteCourse(' + row.Course_Id + ')" href="javascript:void(0)">Delete</a>' +
        '</div>' +
        '</div>';
}

var sEle;
function toggleStatus(ele) {
    let status = $(ele).data("current");
    sEle = ele;
    let val = $(ele).data("val");
    let btn = $(sEle).parents(".status-btn").find("button");
    let res = val == '1' ? true : false;
    if (val == status) {
        return;
    }
    let Id = $(ele).data("id");
    $(btn).attr("disabled", true);
    //var params = JSON.stringify({ "resourceId": Id, "IsActive": val });
    $(".loader").show();
    var params = JSON.stringify({ "courseId": Id, "IsActive": val });
    $.ajax({
        type: "POST",
        url: "CourseManagement.aspx/UpdateCourseActive",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            toastr.success("Course Update Successfully..");
            //var aaData = JSON.parse(data.d);
            BindCourseList("", "");
        },
        error: function (res) {
            console.log(res);
        }
    });
}


function AfterSetStatus(res, data) {
    let cls = data ? 'btn-active' : 'btn-inactive';
    let btn = $(sEle).parents(".status-btn").find("button");
    if (res) {
        $(btn).removeClass('btn-active').removeClass('btn-inactive');
        $(btn).addClass(cls);
        $(btn).text(data ? 'Active' : 'InActive');
        $(sEle).parent().find("a").data("current", data ? true : false);
        $.toast({
            heading: 'Success',
            text: 'Course status updated successfully.',
            showHideTransition: 'slide',
            icon: 'success'
        })
    }
    $(btn).attr("disabled", false);
}


function deleteCourse(CourseId) {
    $.confirm({
        title: 'Delete Confirm!',
        content: 'Are you sure you want to delete this course?',
        buttons: {
            confirm: function () {
                $(".loader").show();
                $.ajax({
                    type: "POST",
                    url: "CourseManagement.aspx/BullkCourseActions",
                    data: JSON.stringify({ "courseIds": CourseId, "state": 2 }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "Failed to delete course!!")
                            AfterDelete(false);
                        else
                            AfterDelete(true);                       
                    },
                    error: function (res) {
                        toastr.error("Failed to update user!!");
                        console.log(res);
                    }
                });
            },
            cancel: function () {

            }
        }
    });
}

function AfterDelete(res) {
    if (res) {
        $.toast({
            heading: 'Success',
            text: 'Course Deleted. Updating Data.',
            showHideTransition: 'slide',
            icon: 'success',
            afterHidden: function () {
                BindCourseList("", "");
                $(".loader").hide();
            }
        });
    } else {
        $.toast({
            heading: 'Error',
            text: 'Something went wrong',
            showHideTransition: 'slide',
            icon: 'error'
        });
        $(".loader").hide();
    }
}


function initCourseData(data) {
   
    debugger;
    tblCourse = $('#pageCourse').DataTable({
        "aaData": data,
        "aoColumns": [
            //{
            //    "render": function (data, type, row) {
            //        return '<div class="checkbox"><input type="checkbox" data-id =' + row.Course_Id + ' class="dt-checkboxes"><label></label></div>';
            //    }
            //},
            null,
            { "mData": "Course_Id" },
            {
                "mData": "Name",
                "render": function (data, type, row, meta) {
                    data = '<a href="AddCourse.aspx?id=' + row.Course_Id + '">' + data + '</a>';
                    return data;
                }
            },
            { "mData": "Description" },
            {
                "mData": "Created_Date",
                "type": "date",
                "render": function (data) {
                    //var dateObj = new Date(parseInt(data.substr(6)));
                    //return (dateObj.getMonth() + 1) + "/" + dateObj.getDate() + "/" + dateObj.getFullYear();
                    if (data === null) return "";
                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");                   
                    return data;
                }
            },
            {
                "mData": "Active",
                "render": function (data, type, row, meta) {
                    //data = '<select class="mdb-select md-form" id="acivebtn">';
                    //if (row.Active == true) {
                    //    data += '<option value="1" data-id=' + row.Course_Id + ' selected>Active</option>';
                    //    data += '<option value="0" data-id=' + row.Course_Id + '>Inactive</option>';
                    //}
                    //else {
                    //    data += '<option value="1" data-id=' + row.Course_Id + '>Active</option>';
                    //    data += '<option value="0" data-id=' + row.Course_Id + ' selected>Inactive</option>';
                    //}
                    //data += '</select>';
                    data = generateAction(row, data);
                    return data;
                }
            },
            {
                "render": function (data, type, row) {
                    
                    //return '<div class="linkresource"><button data-id =' + row.Course_Id + ' class="btnlinkresource"><i class="fa fa-file-o" style="font-size:12px"></i> Link Resources</button>';
                    return '<a id="AssignResources" class="btnlinkresource" style="padding: 8px;" href="AssignResources.aspx"><i class="fa fa-file-o" style="font-size: 14px"></i> Link Resources </a>';
                }
            },
            {
                "render": function (data, type, row) {
                    //return '<div class="linkresource"><button data-id =' + row.Course_Id + ' class="btnlinkresource"><i class="fa fa-users" style="font-size:12px"></i>  Assign Users</button>';
                    return '<a id="assignUser" class="btnlinkresource" style="padding: 8px;" href="AssignUsers.aspx"><i class="fa fa-users" style="font-size: 14px"></i> Assign Users </a>';
                }
            },
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "stateSave": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    if (type === 'display') {
                        return '<div class="checkbox"><input type="checkbox" data-id =' + row.Course_Id + ' class="dt-checkboxes"><label></label></div>';
                    }
                    return data;
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });
    //tblCourse.on("click", "#chkUsersList", function () {
    //    var cells = tblCourse.cells().nodes();
    //    $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    //});

    initPagination();
    $('#pageCourse').on('draw.dt', function (e, settings, len) {
        initPagination();
    });
}

function initPagination() {
    if (tblCourse) {
        $(".pagg-btn").attr("disabled", false);
        let page = tblCourse.page.info();
        $("#total-rec").html(page.recordsTotal + " Courses");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

$(document).on('change', '#acivebtn', function (e) {
    try {
        var IsActive = $(this).val();
        var Id = $(this).find(':selected').attr('data-id');
        var params = JSON.stringify({ "courseId": $(this).find(':selected').attr('data-id'), "IsActive": $(this).val() });
        $.ajax({
            type: "POST",
            url: "CourseManagement.aspx/UpdateCourseActive",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                toastr.success("Course Update Successfully..");
                //var aaData = JSON.parse(data.d);
                BindCourseList("", "");
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
    }
});

$(document).on('click', '#btnApply', function (e) {
    var state = $("#ddlbulk option:selected").val();
    var CourseId = $("input:checkbox:checked").map(function () { return $(this).attr('data-id'); }).get().join(',');
    if (CourseId == "") {
        toastr.info("Please Select at least one course..");
    } else {
        if (state == "2") {
            $.confirm({
                title: 'Delete Confirm!',
                content: 'Are you sure you want to delete Course? ',
                buttons: {
                    confirm: function () {
                        // $.alert('Delete Confirm!');
                        $.ajax({
                            type: "POST",
                            url: "CourseManagement.aspx/BullkCourseActions",
                            data: JSON.stringify({ "courseIds": CourseId, "state": state }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                //  var aaData = JSON.parse(data.d);
                                BindCourseList("", "");
                                if (data.d == "Failed to delete course!!") {
                                    toastr.error(data.d);
                                } else if (data.d == "Failed to delete course!!") {
                                    toastr.error(data.d);
                                } else {
                                    toastr.success(data.d);
                                }
                            },
                            error: function (res) {
                                toastr.error("Failed to update user!!");
                                console.log(res);
                            }
                        });
                    },
                    cancel: function () {
                        //  $.alert('Canceled!');
                    }
                }
            });
        } else if (state == "0" || state == "1") {
            $.confirm({
                title: 'Update Confirm!',
                content: 'Are you sure you want to Update Coures? ',
                buttons: {
                    confirm: function () {
                        //  $.alert('Update Confirm!');
                        $.ajax({
                            type: "POST",
                            url: "CourseManagement.aspx/BullkCourseActions",
                            data: JSON.stringify({ "courseIds": CourseId, "state": state }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                // var aaData = JSON.parse(data.d);
                                BindCourseList("", "");
                                if (data.d == "Failed to update User!!") {
                                    toastr.error(data.d);
                                } else if (data.d == "Failed to update User!!") {
                                    toastr.error(data.d);
                                } else {
                                    toastr.success(data.d);
                                }
                            },
                            error: function (res) {
                                toastr.error("Failed to update user!!");
                                console.log(res);
                            }
                        });
                    },
                    cancel: function () {
                        //  $.alert('Canceled!');
                    }
                }
            });
        }
    }
    $("#searchTable").val("");

});

$(document).on('click', '.applyBtn', function (e) {
    var range = $('.drp-selected').text();
    if (range) {
        var dateRange = range.split('-');
        fromDate = dateRange[0].trim();
        endDate = dateRange[1].trim();

        BindCourseList(fromDate, endDate);
    }
    else {
        fromDate = "";
        endDate = "";
    }
});

$(document).on('click','.cancelBtn', function (e) {
    fromDate = "";
    endDate = "";
    $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
    $('#cal-range').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'YYYY/MM/DD',
            cancelLabel: 'Clear'
        },
    }, function (start, end, label) {
        $(this).val('');
    });
    BindCourseList(fromDate, endDate);
})


$(document).on('click', '#assignUser', function (e) {
    var data_row = tblCourse.row($(this).closest('tr')).data();
    sessionStorage.setItem("moduleName", data_row.Name);
    sessionStorage.setItem("courseId", data_row.Course_Id);
});

$(document).on('click', '#AssignResources', function (e) {
    var data_row = tblCourse.row($(this).closest('tr')).data();
    sessionStorage.setItem("moduleNameResources", data_row.Name);
    sessionStorage.setItem("courseIdResources", data_row.Course_Id);
});

