﻿var tblFlashCard;
var tblFlashCardCommentQst;
var fromDate = "", endDate = "";

$(document).ready(function () {
    activeLeftMenu("flashCard");

    $('#cal-range').daterangepicker({
        //startDate: '2005/05/03',
        //endDate: '2005/06/03',
        locale: {
            format: 'YYYY/MM/DD'
        },
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
        // $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
    var IsSuccessCourse = getParameterByName("IsSuccessflashCard");
    if (IsSuccessCourse == "true") {
        toastr.success("Flash Card saved successfully.");
        if (window.location.href.indexOf('?') > -1) {
            history.pushState('', document.title, window.location.pathname);
        }
    }

    BindFlashCardList(fromDate, endDate);


    $(document).on("click", '.applyBtn', function () {
        var range = $('.drp-selected').text();
        if (range) {
            var dateRange = range.split('-');
            fromDate = dateRange[0].trim();
            endDate = dateRange[1].trim();

            BindFlashCardList(fromDate, endDate);
        }
        else {
            fromDate = "";
            endDate = "";
        }
    });
    $(document).on("click", '.cancelBtn', function () {
        fromDate = "";
        endDate = "";
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
        $('#cal-range').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD',
                cancelLabel: 'Clear'
            },
        }, function (start, end, label) {
            $(this).val('');
        });
      //  alert(fromDate + "" + endDate);
        BindFlashCardList(fromDate, endDate);
    })


    $("#prevBtn").on("click", function () {
        tblFlashCard.page('previous').draw('page');
        initPagination();
    });

    $("#nextBtn").on("click", function () {
        tblFlashCard.page('next').draw('page');
        initPagination();
    });

    $("#firstBtn").on("click", function () {
        tblFlashCard.page('first').draw('page');
        initPagination();
    });

    $("#lastBtn").on("click", function () {
        tblFlashCard.page('last').draw('page');
        initPagination();
    });

});

function searchTable() {
    tblFlashCard && tblFlashCard.search($('#searchTable').val()).draw();
    $("#total-rec").html(tblFlashCard.page.info().recordsDisplay + " Flash Cards");
}

function searchTableQstComments() {
    tblFlashCardCommentQst && tblFlashCardCommentQst.search($('#searchTableQst').val()).draw();
    $("#total-recqstCommt").html(tblFlashCardCommentQst.page.info().recordsDisplay + " Comments");
}

function BindFlashCardList(fromDate, endDate) {

    try {
        $("#searchTable").val("");
        var params = JSON.stringify({ "fromdate": fromDate, "enddate": endDate });
        $.ajax({
            type: "POST",
            url: "flashCard.aspx/GetFlashcardData",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initFlashData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

function initFlashData(data) {
    // debugger;
    //  alert(JSON.stringify(data));
    tblFlashCard = $('#pageFlashCard').DataTable({
        "destroy": true,
        "aaData": data,

        "aoColumns": [
            
            {
                "mData": "CardTitle",
                "render": function (data, type, row, meta) {
                    data = '<a href="ShowFlashCard.aspx?id=' + row.Flashcard_id + '">' + data + '</a>';
                    return data;
                }
            },
            {
                "mData": "Comments",
                "sClass": "details-control hyper ",
            },

            { "mData": "Agree" },
            { "mData": "GotAnswer" },
            { "mData": "NewLearn" },
            { "mData": "CardCount" },
            { "mData": "CountView" },

            {
                "mData": "CreatedDate",
                "type": "date",
                "render": function (data) {
                    //var dateObj = new Date(parseInt(data.substr(6)));
                    //return (dateObj.getMonth() + 1) + "/" + dateObj.getDate() + "/" + dateObj.getFullYear();
                    if (data === null) return "";
                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");
                    return data;

                }
            }
            //{
            //    "mData": "Active",
            //    "render": function (data, type, row, meta) {
            //        //data = '<select class="mdb-select md-form" id="acivebtn">';
            //        //if (row.Active == true) {
            //        //    data += '<option value="1" data-id=' + row.Flashcard_Id + ' selected>Active</option>';
            //        //    data += '<option value="0" data-id=' + row.Flashcard_Id + '>Inactive</option>';
            //        //}
            //        //else {
            //        //    data += '<option value="1" data-id=' + row.Flashcard_Id + '>Active</option>';
            //        //    data += '<option value="0" data-id=' + row.Flashcard_Id + ' selected>Inactive</option>';
            //        //}
            //        //data += '</select>';
            //        data = generateAction(row, data);
            //        return data;
            //    }
            //}
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'columnDefs': [
            //{
                //"targets": 7,
                //"type": 'datetime-moment',}
                ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });
    //tblFlashCard.on("click", "#chkUsersList", function () {
    //    var cells = tblFlashCard.cells().nodes();
    //    $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    //});

    initPagination();
    $('#pageFlashCard').on('draw.dt', function (e, settings, len) {
        initPagination();
    });
}

function initPagination() {
    if (tblFlashCard) {
        $(".pagg-btn").attr("disabled", false);
        let page = tblFlashCard.page.info();
        $("#total-rec").html(page.recordsTotal + " Flash Cards");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

$('#pageFlashCard tbody').on('click', 'td.details-control', function () {
    debugger;
    var tr = $(this).closest('tr');
    var row = tblFlashCard.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        row.child(format(row.data())).show();
        tr.addClass('shown');
    }
    //alert("hi");
});

function format(rowData) {
    debugger;
    var div = $('<div/>')
        .addClass('loading')
        .text('Loading...');
    var params = JSON.stringify({ "flashcard_Id": rowData["Flashcard_id"] });
    $.ajax({
        type: "POST",
        url: "flashCard.aspx/GetFlashcardQstCount",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //data: {
        //    name: rowData.name
        //},
        //  dataType: 'json',
        success: function (json) {
            var aaData = JSON.parse(json.d);
            //  alert(JSON.stringify(aaData));
            //  debugger
            var output = '';
            var output1 = "<table style='width:80%;margin-left:60px;font-weight:bold' >";
            output = "";
            //    debugger;
            if (aaData.length == 0) {
                output += '<tr><td colospan="2">No Comments for this Flash Card</td></tr>';
            }
            for (var i = 0; i < aaData.length; i++) {
                var quote_str = "'" + aaData[i]['Question'] + "'";
                output += '<tr><td>' + aaData[i]['Question'] + '</td><td style="cursor:pointer;text-decoration-line:underline;text-decoration-style:solid;" onclick="showQuestionDetails(' + aaData[i]['FlashCardQuestionID'] + ',' + quote_str + ')" class="w3-button w3-black">' + aaData[i]['Comments'] + ' Comments</td></tr>';

                console.log(output);
                // document.getElementById('tables').innerHTML = output;
            }
            output1 += output + "</table>";

            // var a = "<table><tr><td>" + aaData["Question"] + "</td><td>" + aaData["Comments"]+"</td><tr><table>"
            //   div.html(aaData.html)
            div.css("margin:left", "100px")
            div.html(output1).removeClass('loading');
        }
    });
    return div;
}

function showQuestionDetails(qestionId, qstStr) {
    document.getElementById("id01").style.display = "block";
    document.getElementById("QuestionNumber").innerText = qestionId;
    document.getElementById("qstflashQunModal").innerText = qstStr;
    //  alert(qestionId);
    var params = JSON.stringify({ "flashqstId": qestionId });
    $.ajax({
        type: "POST",
        url: "flashCard.aspx/GetFlashCardQstComments",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //data: {
        //    name: rowData.name
        //},
        //  dataType: 'json',
        success: function (json) {
            var aaData = JSON.parse(json.d);
            //  alert(JSON.stringify(aaData));
            tblFlashCardCommentQst = $('#pageQuestionFlashCard').DataTable({
                "aaData": aaData,

                "aoColumns": [
                    {
                        "mData": "First_Name",
                    },

                    { "mData": "Last_Name" },
                    { "mData": "Email_ID" },
                    { "mData": "Comments" },
                ],
                "bLengthChange": false,
                "bDestroy": true,
                "searching": true,
                "paging": true,
                "info": false,
                "lengthChange": false,
                "autoWidth": true,
                deferRender: true,
                dom: 't',
                'columnDefs': [
                    
                ],
                'select': {
                    'style': 'multi'
                },
                'order': [[1, 'asc']],
                "initComplete": function (settings, json) {
                    $(".loader").fadeOut();
                }
            });
        }
    });
}






