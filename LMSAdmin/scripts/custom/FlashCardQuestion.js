﻿var tblFlashCard;

$(document).ready(function () {
    BindFlashCardQuestionList("", "")
    $("#prevBtn").on("click", function () {
        tblFlashCard.page('previous').draw('page');
        initPagination();
    });

    $("#nextBtn").on("click", function () {
        tblFlashCard.page('next').draw('page');
        initPagination();
    });

    $("#firstBtn").on("click", function () {
        tblFlashCard.page('first').draw('page');
        initPagination();
    });

    $("#lastBtn").on("click", function () {
        tblFlashCard.page('last').draw('page');
        initPagination();
    });
});

function BindFlashCardQuestionList(fromDate, endDate) {
    try {
        $("#searchTable").val("");
        var params = JSON.stringify({ "fromdate": fromDate, "enddate": endDate });
        $.ajax({
            type: "POST",
            url: "FlashCardQuestion.aspx/GetFlashcardQuestion",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initFlashData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

function searchTable() {
    tblFlashCard && tblFlashCard.search($('#searchTable').val()).draw();
    $("#total-rec").html(tblFlashCard.page.info().recordsDisplay + " Flash Cards");
}

function initFlashData(data) {
    tblFlashCard = $('#pageFlashCardQuestion').DataTable({
        "destroy": true,
        "aaData": data,
        "aoColumns": [
            { "mData": "FlashCardQuestionID" },
            { "mData": "Question" },
            { "mData": "Answer" },
            { "mData": "Active" }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    if (type === 'display') {
                        return '<div class="checkbox"><input type="checkbox" data-id =' + row.FlashCardQuestionID + ' class="dt-checkboxes"><label></label></div>';
                    }
                    return data;
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });
    initPagination();
    $('#pageFlashCardQuestion').on('draw.dt', function (e, settings, len) {
        initPagination();
    });

   

    function initPagination() {
        if (tblFlashCard) {
            $(".pagg-btn").attr("disabled", false);
            let page = tblFlashCard.page.info();
            $("#total-rec").html(page.recordsTotal + " Questions");
            if (page.pages == 1) {
                $(".pagg-btn").attr("disabled", true);
            }
            let cPage = page.page + 1;
            let lPage = page.pages;

            if (lPage == 0) {
                cPage = 0;
            }
            $("#page-data").html(cPage + " of " + lPage);
            if (cPage == 1) {
                $("#firstBtn,#prevBtn").attr("disabled", true);
            }
            if (cPage == lPage) {
                $("#lastBtn,#nextBtn").attr("disabled", true);
            }

        }
    }


}

$(document).on('click', '#btnFlashCard', function (e) {

    //var rows = $(tblFlashCard.$('input:checkbox:checked').map(function () {
    //    return $(this).attr('data-id');
    //}).get().join(','));
    

   // alert(JSON.stringify(rows));
    debugger;
    var state = $("#ddlbulk option:selected").val();
    var FlashCardQuestionId = $(tblFlashCard.$("input:checkbox:checked")).map(function () { return $(this).attr('data-id'); }).get().join(',');
    
   // alert("FlashCardQuestion" + FlashCardQuestionId);
    //  document.getElementById("hidQstid").value = FlashCardQuestionId;

    if (FlashCardQuestionId == "") {

        document.getElementById("lblError").innerText = 'Please select atleast one question';
        return false;
    }
    else {

        document.getElementById("lblError").innerText = "";
        document.getElementById("hidQstid").value = FlashCardQuestionId;
        var url = "FlashCardQuestionMigration.aspx?FlashCardQuestionId=" + encodeURIComponent(FlashCardQuestionId);
        window.location.href = url;
    }



});




function MergeFlashCardQuestionList(questionID) {
    try {
        debugger;
        $("#searchTable").val("");
        var params = JSON.stringify({ "fromdate": fromDate, "enddate": endDate });
        $.ajax({
            type: "POST",
            url: "FlashCardQuestionMigration.aspx/GetFlashcardQuestion",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initFlashData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}
