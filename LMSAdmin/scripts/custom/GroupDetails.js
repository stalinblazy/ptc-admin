﻿var tblUsers;
var userType;
var groupName;
var groupId;
var activeUserCId;
var IsFromAssign = false;
var isGroupNameExist = false;
var IsFromCreate = false;
$(document).ready(function () {
    activeLeftMenu("UserManagement");
    userType = "Active";
    $('#cal-range').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
    });

    if (document.location.search.length) {

        var queryString = document.location.search;
        //  var queryParams = new URLSearchParams(queryString);
        groupName = getParameterByName('Name');
        groupId = getParameterByName('id');
        IsFromAssign = getParameterByName('IsFromAssign');
        IsFromCreate = getParameterByName('IsFromCreate');
        if (groupName != "" && groupName != undefined) {
            BindUserListTypeWise(userType, groupName);

            $("#txtGroupName").val(groupName);
            $("#btnCreateGroup").html('UPDATE GROUP');
            $("#lblgroupupdate").text('Update Group');
            if (groupName == "NoGroup") {
                $('#txtGroupName').attr('readonly', true);
            }
        }
        else {
            groupName = "";
            BindUserListTypeWise(userType, "");
        }

        //if (IsFromAssign == "true") {
        //    $("#btnCreateGroup").html('ASSIGN GROUP');
        //    $("#lblgroupupdate").text('Assign Group');
        //    if (IsFromCreate == "true") {
        //        $('#txtGroupName').attr('readonly', false);
        //        $("#btnCreateGroup").html('CREATE GROUP');
        //        $("#lblgroupupdate").text('Create Group');
        //    }
        //    else
        //        $('#txtGroupName').attr('readonly', true);
        //}

        if (IsFromAssign == "true") {
            if (IsFromCreate == "true") {
                $("#btnCreateGroup").html('CREATE GROUP');
                $("#lblgroupupdate").text('Create Group');
            } else {
                $('#txtGroupName').attr('readonly', true);
                $("#btnCreateGroup").html('ASSIGNS GROUP');
                $("#lblgroupupdate").text('Assign Group');
            }

        } else {
            if (groupName != "" && groupName != undefined) {
                $("#btnCreateGroup").html('UPDATE GROUP');
                $("#lblgroupupdate").text('Update Group');
            } else {
                $('#txtGroupName').attr('readonly', true);
                $("#btnCreateGroup").html('ASSIGN GROUP');
                $("#lblgroupupdate").text('Assign Group');
            }

        }
    }
    else {

        groupName = "";
        BindUserListTypeWise(userType, "");
    }

    $("#prevBtn").on("click", function () {
        tblUsers.page('previous').draw('page');
        initPagination();
    });

    $("#nextBtn").on("click", function () {
        tblUsers.page('next').draw('page');
        initPagination();
    });

    $("#firstBtn").on("click", function () {
        tblUsers.page('first').draw('page');
        initPagination();
    });

    $("#lastBtn").on("click", function () {
        tblUsers.page('last').draw('page');
        initPagination();
    });

    $('#userTypes').on('click', 'li', function (e) {
        e.preventDefault();
        BindUserListTypeWise($(this).attr("data-val"), groupName);
        $('#userTypes li.active').removeClass('active');
        $(this).addClass('active');

        $('#chkUsersList').prop('checked', false);
    });
});

function BindUserListTypeWise(userType, grpName) {
    try {
        var params = JSON.stringify({ "userType": userType, "groupName": grpName, "isFromAssign": IsFromAssign });
        $.ajax({
            type: "POST",
            url: "AddGroup.aspx/BindUserListGroupWise",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initUsersData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

// Array holding selected row IDs
var rows_selectedUser = [];
function initUsersData(data) {
    var filteredSub = data.filter(function (subc) {
        if (groupName !== "" && groupName == subc.LMSGroupName) {
            return subc.User_Id;
        }
    });
    rows_selectedUser = filteredSub.map(function (a) { return a.User_Id });
    activeUserCId = filteredSub.map(function (a) { return a.User_Id }).join(',');
    tblUsers = $('#pageUsers').DataTable({
        "aaData": data,
        "aoColumns": [
            {
                "render": function (data, type, row) {
                    if (groupName !== "" && groupName == row.LMSGroupName) {
                        return '<div class="checkbox"><input type="checkbox" checked="checked" data-id =' + row.User_Id + ' class="dt-checkboxes"><label></label></div>';
                    }
                    else {
                        return '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes"><label></label></div>';
                    }
                }
            },
            { "mData": "First_Name" },
            { "mData": "Last_Name" },
            {
                "mData": "Email_ID",
                "render": function (data, type, row, meta) {
                    data = '<a href="AddUser.aspx?id=' + row.User_Id + '">' + data + '</a>';
                    return data;
                }
            },
            { "mData": "Designation" },
            { "mData": "DepartmentID" },
            { "mData": "ReportingManagerID" }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        dom: 't',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']]
    });
    tblUsers.on("click", "#chkUsersList", function () {
        var cells = tblUsers.cells().nodes();

        $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    });

    initPagination();
    $('#pageUsers').on('draw.dt', function (e, settings, len) {
        initPagination();
    });

    $('#pageUsers tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = tblUsers.row($row).data();

        // Get row ID
        var rowId = data.User_Id;

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selectedUser);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selectedUser.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selectedUser.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        // Update state of "Select all" control
        // updateDataTableSelectAllCtrl(tblUsers);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });
}

function initPagination() {
    if (tblUsers) {
        $(".pagg-btn").attr("disabled", false);
        let page = tblUsers.page.info();
        $("#total-rec").html(page.recordsTotal + " Emails");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

function searchTable() {
    tblUsers && tblUsers.search($('#searchTable').val()).draw();
}

$(document).on('click', '#btnCreateGroup', function (e) {

    if (IsFromCreate == "true") {
        checkIsGroupNameAvilable();
        if (isGroupNameExist) {
            return false;
        }
    }
    var groupName = $("#txtGroupName").val();
    if (groupName == "") {
        $("#InvalidGroupMsg").css("display", "block");
        $("#errorg").css("display", "block");
        $("#txtGroupName").addClass("errorg");
    } else {
        $("#InvalidGroupMsg").css("display", "none");
        $("#errorg").css("display", "none");
        $("#txtGroupName").addClass("Valid");

        if (groupId == undefined) {
            var id = "";
        } else {
            var id = groupId;
        }
        var UserId = "";
        //  var UserId = $("input:checkbox:checked").map(function () { return $(this).attr('data-id'); }).get().join(',');
        UserId = rows_selectedUser.toString();
        var AllUserId = $("input:checkbox").map(function () { return $(this).attr('data-id'); }).get().join(',');
        var list = getUnmatchedElementsFromArray(UserId.split(","), activeUserCId.split(","));
        var newUsers = compare(UserId, activeUserCId);
        newUsers = compare(newUsers, list.toString());
        $.ajax({
            type: "POST",
            url: "AddGroup.aspx/CreateAndAssignGroups",
            data: JSON.stringify({ "userIds": UserId, "groupName": groupName, "groupId": id, "UnchkUserId": list.join(","), "newUsers": newUsers, "isFromCreate": IsFromCreate }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                BindUserListTypeWise(userType, groupName);
                window.location = 'UserManagement.aspx?IsFromGroupPageSuccess=true';
                // initUsersData(aaData);
            },
            error: function (res) {
                console.log(res);
            }
        });
    }
})

$(document).on('keyup', '#txtGroupName', function (e) {
    var groupName = $("#txtGroupName").val();
    if (groupName == "") {
        $("#InvalidGroupMsg").css("display", "block");
        $("#errorg").css("display", "block");
        $("#txtGroupName").addClass("errorg");
    } else {
        $("#InvalidGroupMsg").css("display", "none");
        $("#errorg").css("display", "none");
        $("#txtGroupName").addClass("Valid");
    }
});

function compare(string1, string2) {
    var arr1 = string1.split(",");
    var arr2 = string2.split(",");
    var mergedArray = $.merge(arr1, arr2);

    var tt = [];

    for (var i = 0; i < mergedArray.length; i++) {
        var equal = false;
        for (var j = 0; j < mergedArray.length; j++) {
            if (i != j) {
                if (mergedArray[i] == mergedArray[j]) {
                    equal = true;
                    break;
                }
            }
            else {
                equal = false;
            }
        }
        if (equal == false) {
            tt.push(mergedArray[i]);
        }
    }
    return tt.toString();
}

var checkIsGroupNameAvilable = function () {
    var groupNamechk = $("#txtGroupName").val();
    if ($.trim(groupNamechk) == "" || $("#txtGroupName").length == 0) {
        $("#InvalidGroupMsg").css("display", "block");
        $("#errorg").text("Group Name is requried");
        $("#errorg").css("display", "block");
        $("#txtGroupName").addClass("errorg");
        isGroupNameExist = true;
        return false;
    }
    if (groupName == $("#txtGroupName").val()) {
        isGroupNameExist = false;
        return false;
    }
    $.ajax({
        type: "POST",
        url: "AddGroup.aspx/IsGroupNameAvailable",
        contentType: "application/json; charset=utf-8",
        async: false,
        dataType: "json",
        data: JSON.stringify({ groupName: $("#txtGroupName").val(), IsUpdate: $("#btnCreateGroup").text() }),
        success: function (data) {
            var msg = data.d;
            isGroupNameExist = data.d;
            if (msg == true) {
                $("#InvalidGroupMsg").css("display", "block");
                $("#errorg").text("Group Name already exist");
                $("#errorg").css("display", "block");
                $("#txtGroupName").addClass("errorg");
                isGroupNameExist = true;
            }
            //else {
            //    $("#InvalidGroupMsg").css("display", "block");
            //    $("#errorg").text("Group Name is already Exist..");
            //    $("#errorg").css("display", "block");
            //    $("#txtGroupName").addClass("errorg");
            //}
            //else {
            //    $("#InvalidGroupMsg").css("display", "none");
            //    $("#errorg").text("Group Name is requried");
            //    $("#errorg").css("display", "none");
            //    $("#txtGroupName").addClass("Valid");
            //}
            return msg;
        },
        error: function (res) {
            console.log(res);
            isGroupNameExist = false;
        }
    });
    return false;
}

