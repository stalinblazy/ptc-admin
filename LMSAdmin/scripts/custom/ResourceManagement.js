﻿var resourceTable;
var fromDate = "", endDate = "";

$(document).ready(function () {
    activeLeftMenu("ResourceManagement");

    $('#cal-range').daterangepicker({
        locale: {
            format: 'YYYY/MM/DD'
        },
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
    });

    var IsSuccessRescource = getParameterByName("IsSuccessRescource");
    if (IsSuccessRescource == "true") {
        toastr.success("Resource saved successfully.");
        if (window.location.href.indexOf('?') > -1) {
            history.pushState('', document.title, window.location.pathname);
        }
    }

    BindResourceList(fromDate, endDate);

    $('.applyBtn').on("click", function () {
        var range = $('.drp-selected').text();
        if (range) {
            var dateRange = range.split('-');
            fromDate = dateRange[0].trim();
            endDate = dateRange[1].trim();

            BindResourceList(fromDate, endDate);
        }
        else {
            fromDate = "";
            endDate = "";
        }
    });
    $('.cancelBtn').on("click", function () {
        fromDate = "";
        endDate = "";
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
        $('#cal-range').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD',
                cancelLabel: 'Clear'
            },
        }, function (start, end, label) {
            $(this).val('');
        });
        BindResourceList(fromDate, endDate);
    })

    $("#prevBtn").on("click", function () {
        resourceTable.page('previous').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $("#nextBtn").on("click", function () {
        resourceTable.page('next').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
        
    });

    $("#firstBtn").on("click", function () {
        resourceTable.page('first').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $("#lastBtn").on("click", function () {
        resourceTable.page('last').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $(document).on('change', '#acivebtn', function (e) {
        try {
            var IsActive = $(this).val();

            var tr = $(this).closest('tr');
            var row = resourceTable.row(tr);
            var Id = row.data().ID;

            var params = JSON.stringify({ "resourceId": Id, "IsActive": $(this).val() });
            $.ajax({
                type: "POST",
                url: "ResourceManagement.aspx/UpdateResourceActive",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    toastr.success("Resource Update Successfully..");
                    BindResourceList(fromDate, endDate);
                    //var aaData = JSON.parse(data.d);
                },
                error: function (res) {
                    console.log(res);
                }
            });
        } catch (ex) {
        }
    });

});

function BindResourceList(fromDate, endDate) {
    $("#searchTable").val("");
    try {
        var params = JSON.stringify({ "fromDate": fromDate, "endDate": endDate });
        $.ajax({
            type: "POST",
            url: "ResourceManagement.aspx/GetResourceData",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                var aaData = JSON.parse(data.d);
                initResourceData(aaData);
                $(".tooltipdata").tooltip();
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

function generateAction(row, data) {
    let cls = (row.IsActive === true ? 'btn-active' : 'btn-inactive');
    return '<div class="btn-group status-btn">' +
        '<button type="button" class="btn ' + cls + ' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (data === true ? 'Active' : 'Inactive') + '</button>' +
        '<div class="dropdown-menu  dropdown-menu-right">' +
        '<a class="dropdown-item" data-id=' + row.ID + ' data-val="1"  value="1" data-current="' + data + '" onclick="toggleStatus(this)" href="javascript:void(0)">Active</a>' +
        '<a class="dropdown-item" data-id=' + row.ID + '  data-val="0" value="0" data-current="' + data + '" onclick="toggleStatus(this)" href="javascript:void(0)">Inactive</a>' +
        '<div class="dropdown-divider"></div>' +
        '<a class="dropdown-item" onclick="deleteResource(' + row.ID + ')" href="javascript:void(0)">Delete</a>' +
        '</div>' +
        '</div>';
}

var sEle;
function toggleStatus(ele) {
    let status = $(ele).data("current");
    sEle = ele;
    let val = $(ele).data("val");
    let btn = $(sEle).parents(".status-btn").find("button");
    let res = val == '1' ? true : false;
    if (val == status) {
        return;
    }
    let Id = $(ele).data("id");
    $(btn).attr("disabled", true);
    var params = JSON.stringify({ "resourceId": Id, "IsActive": val });
    $(".loader").show();
    $.ajax({
        type: "POST",
        url: "ResourceManagement.aspx/UpdateResourceActive",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            BindResourceList(fromDate, endDate);
            AfterSetStatus(data, res);
            $(".loader").hide();
        },
        error: function (res) {
            console.log(res);
            $(".loader").hide();
        }
    });
}

function AfterSetStatus(res, data) {
    let cls = data ? 'btn-active' : 'btn-inactive';
    let btn = $(sEle).parents(".status-btn").find("button");
    if (res) {
        $(btn).removeClass('btn-active').removeClass('btn-inactive');
        $(btn).addClass(cls);
        $(btn).text(data ? 'Active' : 'InActive');
        $(sEle).parent().find("a").data("current", data ? true : false);
        $.toast({
            heading: 'Success',
            text: 'Resource status updated successfully.',
            showHideTransition: 'slide',
            icon: 'success'
        })
    }
    $(btn).attr("disabled", false);
}

function deleteResource(ResourceId) {    
    $.confirm({
        title: 'Delete Confirm!',
        content: 'Are you sure you want to delete this resource?',
        buttons: {
            confirm: function () {
                $(".loader").show();
                $.ajax({
                    type: "POST",
                    url: "ResourceManagement.aspx/BullkResourceActions",
                    data: JSON.stringify({ "ResourceIds": ResourceId, "state": 2 }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "Failed to delete Resource!!")
                            AfterDelete(false);
                        else
                            AfterDelete(true);                       
                    },
                    error: function (res) {
                        toastr.error("Failed to update Resource!!");
                        console.log(res);
                        $(".loader").hide();
                    }
                });
            },
            cancel: function () {

            }
        }
    });
}

function AfterDelete(res) {
    if (res) {
        $.toast({
            heading: 'Success',
            text: 'Resource Deleted. Updating Data.',
            showHideTransition: 'slide',
            icon: 'success',
            afterHidden: function () {
                BindResourceList("", "");
                $(".loader").hide();
            }
        })
    } else {
        $.toast({
            heading: 'Error',
            text: 'Something went wrong',
            showHideTransition: 'slide',
            icon: 'error'
        })
        $(".loader").hide();
    }
}

function initResourceData(data) {
    debugger;
    resourceTable = $('#pageResource').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mData": "ID" },
            {
                "mData": "Name",
                "render": function (data, type, row, meta) {
                    data = '<a href="AddResource.aspx?id=' + row.ID + '">' + data + '</a>';
                    return data;
                }
            },
            { "mData": "Name1" },
            { "mData": "Description" },
            {
                "mData": "CreatedDate",
                "type": "date",
                "render": function (data) {
                    if (data === null) return "";

                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");                   
                    return data;
                }
            },
            {
                "mData": "CourseName",
                "render": function (data) {
                    if (data === null) return "";
                    var str = "";
                    var arr = data.split(",");
                    $.each(arr, function (index, item) {
                        var temp = data.split(":");
                        str += "<span class='tooltipdata' title='" + temp[1] + "'>" + temp[0] + "</span>"
                        // do something with `item` (or `this` is also `item` if you like)
                    });
                   
                    data = str;
                    return data;
                }
            },
            //{
            //    "mData": "ModifiedDate",
            //    "type": "date",
            //    "render": function (data) {
            //        if (data === null) return "";

            //        var pattern = /Date\(([^)]+)\)/;
            //        var results = pattern.exec(data);
            //        var dt = new Date(parseFloat(results[1]));
            //        data = moment(new Date(dt)).format("MM/DD/YYYY");                   
            //        return data;
            //    }
            //},
            {
                "mData": "IsActive",
                "render": function (data, type, row, meta) {
                    //data = '<select class="mdb-select md-form" id="acivebtn">';
                    //if (row.IsActive == true) {
                    //    data += '<option value="1" data-id=' + row.ID + ' selected>Active</option>';
                    //    data += '<option value="0" data-id=' + row.ID + '>Inactive</option>';
                    //}
                    //else {
                    //    data += '<option value="1" data-id=' + row.ID + '>Active</option>';
                    //    data += '<option value="0" data-id=' + row.ID + ' selected>Inactive</option>';
                    //}
                    //data += '</select>';
                    data = generateAction(row, data);
                    return data;
                }
            }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "stateSave": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 't',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    return '<div class="checkbox"><input type="checkbox" data-id =' + row.ID + ' class="dt-checkboxes"><label></label></div>';
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });

    //resourceTable.on("click", "#chkUsersList", function () {
    //    var cells = resourceTable.cells().nodes();
    //    $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    //});

    initPagination();
    $('#pageResource').on('draw.dt', function (e, settings, len) {
        initPagination();
    });
}

function initPagination() {

    if (resourceTable) {
        $(".pagg-btn").attr("disabled", false);
        let page = resourceTable.page.info();
        $("#total-rec").html(page.recordsTotal + " Resources");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

function searchTable() {
    resourceTable && resourceTable.search($('#searchTable').val()).draw();
    $("#total-rec").html(resourceTable.page.info().recordsDisplay + " Resources");
}


$(document).on('click', '#btnApply', function (e) {
    var state = $("#ddlbulk option:selected").val();
    var ResourceId = $("input:checkbox:checked").map(function () { return $(this).attr('data-id'); }).get().join(',');
    if (ResourceId == "") {
        toastr.info("Please Select at least one resource..");
    } else {
        if (state == "2") {
            $.confirm({
                title: 'Delete Confirm!',
                content: 'Are you sure you want to delete Resource? ',
                buttons: {
                    confirm: function () {
                        // $.alert('Delete Confirm!');
                        $.ajax({
                            type: "POST",
                            url: "ResourceManagement.aspx/BullkResourceActions",
                            data: JSON.stringify({ "ResourceIds": ResourceId, "state": state }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if (data.d == "Failed to update Resource!!") {
                                    toastr.error(data.d);
                                } else if (data.d == "Failed to delete Resource!!") {
                                    toastr.error(data.d);
                                } else {
                                    toastr.success(data.d);
                                }
                                BindResourceList("", "");
                            },
                            error: function (res) {
                                toastr.error("Failed to update Resource!!");
                                console.log(res);
                            }
                        });

                    },
                    cancel: function () {
                        //  $.alert('Canceled!');
                    }
                }
            });
        }
        else if (state == "0" || state == "1") {
            $.confirm({
                title: 'Update Confirm!',
                content: 'Are you sure you want to Update Resource? ',
                buttons: {
                    confirm: function () {
                        //  $.alert('Update Confirm!');
                        $.ajax({
                            type: "POST",
                            url: "ResourceManagement.aspx/BullkResourceActions",
                            data: JSON.stringify({ "ResourceIds": ResourceId, "state": state }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if (data.d == "Failed to update Resource!!") {
                                    toastr.error(data.d);
                                } else if (data.d == "Failed to delete Resource!!") {
                                    toastr.error(data.d);
                                } else {
                                    toastr.success(data.d);
                                }
                                BindResourceList("", "");
                            },
                            error: function (res) {
                                toastr.error("Failed to update Resource!!");
                                console.log(res);
                            }
                        });
                    },
                    cancel: function () {
                        //  $.alert('Canceled!');
                    }
                }
            });
        }
    }
    $("#searchTable").val("");
});