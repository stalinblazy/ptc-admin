﻿var tblUsers;
var tblGroups;
var userType;
var fromDate = "";
var endDate = "";
//var isUserOptionSelected = true;

$(document).ready(function () {
    //$("#groupFooter").hide();   
    userType = "Registered";
    //$("#leftNavigation li.active").removeClass("active");  
    //$("#leftNavigation li#UserManagement").addClass("active");  
    activeLeftMenu("UserManagement");
    $('#cal-range').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'YYYY/MM/DD',
            //  cancelLabel: 'Clear'
        },
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
        // $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    $('#cal-range-group').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        $("#cal-range-group").html('<i class="fa fa-calendar"></i>&nbsp;' + start.format('MMM DD') + ' - ' + end.format('MMM DD'));
    });

    try {
        var params = getParameterByName("IsFromGroupPage");
        var IsFromGroupPageSuccess = getParameterByName("IsFromGroupPageSuccess");
        var isFromGRoupPage = getParameterByName("IsFromGroupPage");
        var IsSuccessUserSave = getParameterByName("IsSuccess");
        if (IsSuccessUserSave == "true") {
            toastr.success("User data saved successfully.");
            if (window.location.href.indexOf('?') > -1) {
                history.pushState('', document.title, window.location.pathname);
            }
        }
        if (isFromGRoupPage == "true") {
            $('#userlistleft h1.active').removeClass('active');
            $("#tabGroup").find("h1").addClass("active");
            $('#userlistleft > li > a#tabGroup').trigger('click');
            $("#spanHeader").html("Overview");
            $("#rightlist").hide();
            BindGroupList(fromDate, endDate);
            if (window.location.href.indexOf('?') > -1) {
                history.pushState('', document.title, window.location.pathname);
            }
        }
        else {
            BindUserListTypeWise(userType);

        }

        if (IsFromGroupPageSuccess == "true") {
            $('#userlistleft h1.active').removeClass('active');
            $("#tabGroup").find("h1").addClass("active");
            $('#userlistleft > li > a#tabGroup').trigger('click');
            $("#spanHeader").html("Overview");
            $("#rightlist").hide();
            BindGroupList(fromDate, endDate);
            if (window.location.href.indexOf('?') > -1) {
                history.pushState('', document.title, window.location.pathname);
            }
            toastr.success("Group Save Successfully");
        }
    } catch (e) {

    }

    $('.applyBtn').on("click", function () {
        var range = $('.drp-selected').text();
        if (range) {
            var dateRange = range.split('-');
            fromDate = dateRange[0].trim();
            endDate = dateRange[1].trim();

            BindGroupList(fromDate, endDate);
            BindUserListTypeWise(userType);
        }
        else {
            fromDate = "";
            endDate = "";
        }
    });

    $('.cancelBtn').on("click", function (e) {

        fromDate = "";
        endDate = "";
        $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
        $('#cal-range').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD',
                cancelLabel: 'Clear'
            },
        }, function (start, end, label) {
            $(this).val('');
        });

        BindUserListTypeWise(userType);
        BindGroupList(fromDate, endDate);
    });

    //For Group
    $('.applyBtn').on("click", function () {

        var range = $('.drp-selected').text();
        if (range) {
            var dateRange = range.split('-');
            fromDate = dateRange[0].trim();
            endDate = dateRange[1].trim();

            BindGroupList(fromDate, endDate);
            BindUserListTypeWise(userType);
        }
        else {
            fromDate = "";
            endDate = "";
        }
    });

    $('.cancelBtn').on("click", function () {

        fromDate = "";
        endDate = "";
        $("#cal-range-group").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
        $('#cal-range-group').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD',
                cancelLabel: 'Clear'
            },
        }, function (start, end, label) {
            $(this).val('');
        });
        BindUserListTypeWise(userType);
        BindGroupList(fromDate, endDate);
    });


    // BindUserListTypeWise(userType);

    $("#prevBtn").on("click", function () {
        tblUsers.page('previous').draw('page');
        initPagination();

    });

    $("#nextBtn").on("click", function () {
        tblUsers.page('next').draw('page');
        initPagination();

    });

    $("#firstBtn").on("click", function () {
        tblUsers.page('first').draw('page');
        initPagination();

    });

    $("#lastBtn").on("click", function () {
        tblUsers.page('last').draw('page');
        initPagination();

    });

    //Group next ,pre
    $("#prevBtng").on("click", function () {
        tblGroups.page('previous').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $("#nextBtng").on("click", function () {
        tblGroups.page('next').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $("#firstBtng").on("click", function () {
        tblGroups.page('first').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $("#lastBtng").on("click", function () {
        tblGroups.page('last').draw('page');
        initPagination();
        $(".tooltipdata").tooltip();
    });

    $('#userTypes').on('click', 'li', function (e) {
        e.preventDefault();
        BindUserListTypeWise($(this).attr("data-val"));
        $('#userTypes li.active').removeClass('active');
        $(this).addClass('active');

        $('#chkUsersList').prop('checked', false);
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#userlistleft h1.active').removeClass('active');
        $(this).find("h1").addClass("active");
        var target = $(e.target).attr("href"); // activated tab
        var rightListDiv = document.getElementById("rightlist");

        if (target == "#tab_GroupList") {
            $("#spanHeader").html("Overview");
            rightListDiv.style.display = "none";
            //$("#btnCreateUser").hide();
            //  isUserOptionSelected = false;
            BindGroupList(fromDate, endDate);
        }
        else {
            $("#spanHeader").html("Overview");
            rightListDiv.style.display = "block";
            //$("#btnCreateUser").show();
            // isUserOptionSelected = true;
            BindUserListTypeWise(userType);
        }
    });

});


$(document).on('change', '#acivebtn', function (e) {
    try {
        var IsActive = $(this).val();
        var Id = $(this).find(':selected').attr('data-id');
        var params = JSON.stringify({ "userId": $(this).find(':selected').attr('data-id'), "IsActive": $(this).val() });
        $.ajax({
            type: "POST",
            url: "UserManagement.aspx/UpdateUserActive",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                toastr.success("User Update Successfully..");
                // var aaData = JSON.parse(data.d);
                BindUserListTypeWise(userType);
                // initUsersData(aaData);
            },
            error: function (res) {
                console.log(res);
                toastr.error(res);
            }
        });
    } catch (ex) {
    }
});

$(document).on('click', '#btnApply', function (e) {

    var state = $("#ddlbulk option:selected").val();
    var UserId = $("input:checkbox:checked").map(function () { return $(this).attr('data-id'); }).get().join(',');
    if (UserId == "") {
        toastr.info("Please Select at least one user..");
    } else {
        if (state == "2") {
            $.confirm({
                title: 'Delete Confirm!',
                content: 'Are you sure you want to delete Users? ',
                buttons: {
                    confirm: function () {
                        // $.alert('Delete Confirm!');
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/BullkUserActions",
                            data: JSON.stringify({ "userIds": UserId, "state": state }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if (data.d == "Failed to update User!!") {
                                    toastr.error(data.d);
                                } else if (data.d == "Failed to delete User!!") {
                                    toastr.error(data.d);
                                } else {
                                    toastr.success(data.d);
                                }

                                //var aaData = JSON.parse(data.d);
                                BindUserListTypeWise(userType);

                                // initUsersData(aaData);
                            },
                            error: function (res) {
                                toastr.error("Failed to update user!!");
                                console.log(res);
                            }
                        });
                    },
                    cancel: function () {
                        //  $.alert('Canceled!');
                    }
                }
            });
        } else if (state == "0" || state == "1") {
            $.confirm({
                title: 'Update Confirm!',
                content: 'Are you sure you want to Update Users? ',
                buttons: {
                    confirm: function () {
                        //  $.alert('Update Confirm!');
                        $.ajax({
                            type: "POST",
                            url: "UserManagement.aspx/BullkUserActions",
                            data: JSON.stringify({ "userIds": UserId, "state": state }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if (data.d == "Failed to update User!!") {
                                    toastr.error(data.d);
                                } else if (data.d == "Failed to delete User!!") {
                                    toastr.error(data.d);
                                } else {
                                    toastr.success(data.d);
                                }

                                //var aaData = JSON.parse(data.d);
                                BindUserListTypeWise(userType);

                                // initUsersData(aaData);
                            },
                            error: function (res) {
                                toastr.error("Failed to update user!!");
                                console.log(res);
                            }
                        });
                    },
                    cancel: function () {
                        //  $.alert('Canceled!');
                    }
                }
            });
        }
    }
    $("#searchTable").val("");
});

function generateAction(row, data) {
    let cls = (row.Active === true ? 'btn-active' : 'btn-inactive');
    return '<div class="btn-group status-btn">' +
        '<button type="button" class="btn ' + cls + ' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (data === true ? 'Active' : 'Inactive') + '</button>' +
        '<div class="dropdown-menu  dropdown-menu-right">' +
        '<a class="dropdown-item" data-id=' + row.User_Id + ' data-val="1"  value="1" data-current="' + data + '" onclick="toggleStatus(this)" href="javascript:void(0)">Active</a>' +
        '<a class="dropdown-item" data-id=' + row.User_Id + '  data-val="0" value="0" data-current="' + data + '" onclick="toggleStatus(this)" href="javascript:void(0)">Inactive</a>' +
        '<div class="dropdown-divider"></div>' +
        '<a class="dropdown-item" onclick="deleteUser(' + row.User_Id + ')" href="javascript:void(0)">Delete</a>' +
        '</div>' +
        '</div>';
}

var sEle;
function toggleStatus(ele) {
    let status = $(ele).data("current");
    sEle = ele;
    let val = $(ele).data("val");
    let btn = $(sEle).parents(".status-btn").find("button");
    let res = val == '1' ? true : false;
    if (val == status) {
        return;
    }
    let Id = $(ele).data("id");
    $(btn).attr("disabled", true);
    $(".loader").show();
    var params = JSON.stringify({ "userId": Id, "IsActive": val });
    $.ajax({
        type: "POST",
        url: "UserManagement.aspx/UpdateUserActive",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            BindUserListTypeWise(userType);
            AfterSetStatus(data, res);
            $(".loader").hide();
        },
        error: function (res) {
            console.log(res);
            toastr.error(res);
        }
    });
}

function AfterSetStatus(res, data) {
    let cls = data ? 'btn-active' : 'btn-inactive';
    let btn = $(sEle).parents(".status-btn").find("button");
    if (res) {
        $(btn).removeClass('btn-active').removeClass('btn-inactive');
        $(btn).addClass(cls);
        $(btn).text(data ? 'Active' : 'Inactive');
        $(sEle).parent().find("a").data("current", data ? true : false);
        $.toast({
            heading: 'Success',
            text: 'User status updated successfully.',
            showHideTransition: 'slide',
            icon: 'success'
        })
    }
    $(btn).attr("disabled", false);
}

function deleteUser(UserId) {
    $.confirm({
        title: 'Delete Confirm!',
        content: 'Are you sure you want to delete this user?',
        buttons: {
            confirm: function () {
                $(".loader").show();
                $.ajax({
                    type: "POST",
                    url: "UserManagement.aspx/BullkUserActions",
                    data: JSON.stringify({ "userIds": UserId, "state": 2 }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "Failed to delete User!!")
                            AfterDelete(false);
                        else
                            AfterDelete(true);
                    },
                    error: function (res) {
                        toastr.error("Failed to update user!!");
                        console.log(res);
                    }
                });
            },
            cancel: function () {

            }
        }
    });
}

function AfterDelete(res) {
    if (res) {
        $.toast({
            heading: 'Success',
            text: 'User Deleted. Updating Data.',
            showHideTransition: 'slide',
            icon: 'success',
            afterHidden: function () {
                BindUserListTypeWise(userType);
                $(".loader").hide();
            }
        })
    } else {
        $.toast({
            heading: 'Error',
            text: 'Something went wrong',
            showHideTransition: 'slide',
            icon: 'error'
        })
        $(".loader").hide();
    }
}
function initUsersData(data) {
    tblUsers = $('#pageUsers').DataTable({
        "aaData": data,
        "aoColumns": [
            //{
            //    "render": function (data, type, row) {
            //        return '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes"><label></label></div>';
            //    }
            //},
            { "mData": "User_Id" },
            { "mData": "First_Name" },
            { "mData": "Last_Name" },
            {
                "mData": "Email_ID",
                "render": function (data, type, row, meta) {
                    data = '<a href="AddUser.aspx?id=' + row.User_Id + '">' + data + '</a>';
                    return data;
                }
            },
            { "mData": "Designation" },
            { "mData": "DepartmentID" },
            { "mData": "Country" },
            {
                "mData": "Active",
                "render": function (data, type, row, meta) {
                    //data = '<select class="mdb-select md-form" id="acivebtn">';
                    //if (row.Active == true) {
                    //    data += '<option value="1" data-id=' + row.User_Id + ' selected>Active</option>';
                    //    data += '<option value="0" data-id=' + row.User_Id + '>Inactive</option>';
                    //}
                    //else {
                    //    data += '<option value="1" data-id=' + row.User_Id + '>Active</option>';
                    //    data += '<option value="0" data-id=' + row.User_Id + ' selected>Inactive</option>';
                    //}
                    //data += '</select>';
                    data = generateAction(row, data);
                    return data;
                }
            }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        "stateSave": true,
        deferRender: true,
        dom: 'Bt',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    return '<div class="checkbox"><input type="checkbox" data-id =' + row.User_Id + ' class="dt-checkboxes"><label></label></div>';
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        buttons: [
            {
                extend: 'excel',
                text: 'Click here to download',
              
            }

        ],
        'select': {
            'style': 'multi'
        },
        'order': [[2, 'asc']],
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });
    //tblUsers.on("click", "#chkUsersList", function () {
    //    var cells = tblUsers.cells().nodes();
    //    $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    //});

    initPagination();
    $('#pageUsers').on('draw.dt', function (e, settings, len) {
        initPagination();
    });
}

function initPagination() {
    if (tblUsers) {
        $(".pagg-btn").attr("disabled", false);
        let page = tblUsers.page.info();
        $("#total-rec").html(page.recordsTotal + " Users");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-data").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtn,#prevBtn").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtn,#nextBtn").attr("disabled", true);
        }

    }
}

function searchTable() {
    tblUsers && tblUsers.search($('#searchTable').val()).draw();
    $("#total-rec").html(tblUsers.page.info().recordsDisplay + " Emails");
}
function searchGroupTable() {
    tblGroups && tblGroups.search($('#searchGroupTable').val()).draw();
    $("#total-rec-group").html(tblGroups.page.info().recordsDisplay + " Groups");
}

function BindUserListTypeWise(userListType) {

    try {
        //var range = $('.drp-selected').text();
        //if (range) {
        //    var dateRange = range.split('-');
        //    fromDate = dateRange[0].trim();
        //    endDate = dateRange[1].trim();
        //}
        //else {
        //    fromDate = "";
        //    endDate = "";
        //}
        $('#searchTable').val();
        var params = JSON.stringify({ "fromDate": fromDate, "endDate": endDate, "userType": userListType });
        $.ajax({
            type: "POST",
            url: "UserManagement.aspx/BindUserList",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initUsersData(aaData);
                initPagination();
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

function BindGroupList(fromDate, endDate) {
    try {
        var params = JSON.stringify({ "fromDate": fromDate, "endDate": endDate });
        $.ajax({
            type: "POST",
            url: "UserManagement.aspx/BindGroupList",
            data: params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var aaData = JSON.parse(data.d);
                initGroupData(aaData);
                $(".tooltipdata").tooltip();
            },
            error: function (res) {
                console.log(res);
            }
        });
    } catch (ex) {
        Console.log(ex);
    }
}

function initGroupData(data) {
    tblGroups = $('#pageGroups').DataTable({
        "aaData": data,
        "aoColumns": [
            //{
            //    "render": function (data, type, row) {
            //        return '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
            //    }
            //},
            { "mData": "GroupId" },
            {
                "mData": "Name",
                "render": function (data, type, row, meta) {
                    //data = "<a href='AddGroup.aspx?id=" + row.GroupId + "&Name=" + row.Name + '" data-id= '" + row.Name + "'>" + data + "</a>";
                    return "<a data-id= '" + row.Name + "' href='AddGroup.aspx?Name=" + row.Name + "&id=" + row.GroupId + "&IsFromCreate=true'>" + data + "</a > ";
                    return data;
                }
            },
            { "mData": "NoOfUser" },
            {
                "mData": "CreatedDate",
                "type": "date ",
                'render': function (data, type, row, meta) {
                    if (data === null) return "";

                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(data);
                    var dt = new Date(parseFloat(results[1]));
                    data = moment(new Date(dt)).format("MM/DD/YYYY");
                    return data;
                }
            },
            {
                "mData": "ModulesAssigned",
                "render": function (data) {
                    if (data === null) return "";
                    var str = "";
                    var arr = data.split(",");
                    $.each(arr, function (index, item) {
                        var temp = item.split("|");
                        str += "<span class='tooltipdata' title='" + temp[1] + "'>" + temp[0] + "</span>"
                        // do something with `item` (or `this` is also `item` if you like)
                    });
                    data = str;
                    return data;
                }
            },
            {
                className: "text-right",
                "render": function (data, type, row, meta) {
                    return "<a class='assgnUserBtn' id='assignUserBtn' data-id= '" + row.Name + "'href='AddGroup.aspx?Name=" + row.Name + "&IsFromAssign=true&id=" + row.GroupId + "'><i class='fas fa-users'></i> Assign Users </a > ";
                }
            }
        ],
        "bLengthChange": false,
        "bDestroy": true,
        "searching": true,
        "paging": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,
        deferRender: true,
        dom: 'Bt',
        'columnDefs': [
            {
                'targets': 0,
                "orderable": false,
                "defaultContent": "-",
                "render": function (data, type, row) {
                    return '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        buttons: [
            {
                extend: 'excel',
                text: 'Click here to download',
                filename: 'ListOfGroup'

            }

        ],
        'select': {
            'style': 'multi'
        },
        'order': [[1, 'asc']],
        "initComplete": function (settings, json) {
            $(".loader").fadeOut();
        }
    });
    //tblGroups.on("click", "#chkUsersList", function () {
    //    var cells = tblGroups.cells().nodes();
    //    $(cells).find('.dt-checkboxes').prop('checked', $(this).is(':checked'));
    //});

    initGroupPagination();
    $('#pageGroups').on('draw.dt', function (e, settings, len) {
        initGroupPagination();
    });
}

function initGroupPagination() {
    if (tblGroups) {
        $(".pagg-btn").attr("disabled", false);
        let page = tblGroups.page.info();
        $("#total-rec-group").html(page.recordsTotal + " Groups");
        if (page.pages == 1) {
            $(".pagg-btn").attr("disabled", true);
        }
        let cPage = page.page + 1;
        let lPage = page.pages;

        if (lPage == 0) {
            cPage = 0;
        }
        $("#page-datag").html(cPage + " of " + lPage);
        if (cPage == 1) {
            $("#firstBtng,#prevBtng").attr("disabled", true);
        }
        if (cPage == lPage) {
            $("#lastBtng,#nextBtng").attr("disabled", true);
        }
    }
}

