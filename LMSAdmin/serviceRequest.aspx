﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="serviceRequest.aspx.cs" Inherits="serviceRequest" %>

<!DOCTYPE html>

<html lang="en">
<title>User Page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css" />
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="js/toast/jquery.toast.min.css">
<link rel="stylesheet" href="css/jquery-confirm.min.css">
<link rel="stylesheet" href="css/dataTables.checkboxes.css">
<link rel="stylesheet" href="css/styledev.css">
<link rel="stylesheet" href="css/custom-dev.css">
<link rel="stylesheet" href="css/jquery.dataTables.min.css">

<body>
     <div class="loader">
        <img src="images/loaderp.svg" />
    </div>
    <div id="app" class="container-fluid">
        <!-- header -->
        <LMS:CommonHeader ID="CommonHeader" runat="server" />

        <div class="row">
            <!-- left Navigation -->
            <LMS:Menu ID="Menu" runat="server" />

            <!-- Page content -->
            <div class="col-md-10" id="pageHeader">
                <!-- Header Title -->
                <div class="pageheaderbar">
                    <div class="row">
                        <div class="col-md-8 pageHeaderTitle">
                            Technical Support
                        </div>
                        <div class="col-md-4 pageHeaderDesc">
                            Technical Support - Users Overview
                        </div>
                    </div>
                </div>

                <div class="row pageBody">

                    <!-- Activity -->
                    <div class="col-md-12">
                        <div class="row" id="listofusers">
                            <div class="col-md-12 cardCustom">
                                <div class="row listofusers">
                                    <div class="col-md-6 leftlist" style="padding-left: 0px;">
                                        <ul class="userlistleft">
                                            <li>
                                                <h1>List of Service Requests</h1>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="table-top-action">
                                        <input class="input-search" oninput="searchTable()" id="searchTable" type="text" placeholder="Search" />
                                        <span id="cal-range" class="cal-range"><i class="fa fa-calendar"></i>&nbsp; Select date range</span>
                                    </div>
                                    <form runat="server">
                                        <input type="hidden" id="hidAdmin" runat="server" />
                                        <!--table starts-->
                                        <table id="pageUsers" class="" style="width: 100%">
                                            <thead class="tableHeader">
                                                <tr>
                                                    <th></th>
                                                    <th>EMAIL ADDRESS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                    <th style="min-width: 120px;">SUBJECT<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                    <th>DESCRIPTION<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                    <th style="min-width: 140px;">REQUESTED DATE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                    <th style="min-width: 140px;">RESOLVED DATE<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>
                                                    <th style="min-width: 75px;">STATUS<i class="fa fa-sort"></i><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></th>

                                                </tr>
                                            </thead>
                                            <tbody class="tableBody">
                                                <asp:Repeater ID="serviceRep" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Eval("contactid")%></td>
                                                            <td><%#Eval("email")%></td>
                                                            <td><%#Eval("subject")%></td>
                                                            <td><%#Eval("description")%></td>
                                                            <td><%#Eval("createddatetime")%></td>
                                                            <td><%#Eval("resolveddate")%></td>
                                                            <td><%# Eval("isResolved") %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>


                                    </form>
                                    <div class="clearfix"></div>
                                    <div class="custom-pagination">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6">
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="datatable-pagination pull-right">
                                                    <span id="total-rec"></span>
                                                    <button id="firstBtn" class="pagg-btn btn"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                                                    <button id="prevBtn" class="pagg-btn btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                                                    <span id="page-data" class="page-numb">1 of 1</span>
                                                    <button id="nextBtn" class=" pagg-btn btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                                                    <button id="lastBtn" class=" pagg-btn btn"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--table ends-->
                            </div>
                        </div>
                        <!-- dfdsfsdf -->
                    </div>
                </div>
            </div>

        </div>

    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="js/toast/jquery.toast.min.js"></script>
    <script src="scripts/jquery-confirm.min.js"></script>
    <script src="js/dataTables.checkboxes.min.js"></script>
    <script src="js/adminservice.js"></script>
    <script>
        var serviceTable;
        let startDate;
        let endDate;

        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                var dateStart = startDate;
                var dateEnd = endDate;
                if (dateStart && dateStart) {
                    // aData represents the table structure as an array of columns, so the script access the date value
                    // in the first column of the table via aData[0]
                    var evalDate = new Date(aData[5]);
                    if (evalDate >= dateStart && evalDate <= dateEnd) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                return true;
            });


        var sEle;
        function toggleStatus(ele, id) {
            let status = $(ele).data("current");
            sEle = ele;
            let val = $(ele).data("val");
            let btn = $(sEle).parents(".status-btn").find("button");

            if (val == status) {
                return;
            }
            $(btn).attr("disabled", true);
            let ev = {};
            ev.ID = id;
            ev.AdminId = $("#hidAdmin").val();
            ev.isResolved = val == 'True' ? true : false;
            let cb = new CallBack();
            cb.func = "AfterSetStatus";
            cb.data = ev.isResolved;

            _adminService.SetRequestStatus([ev], cb, true);
        }

        function AfterSetStatus(res, data) {
            let cls = data ? 'btn-active' : 'btn-inactive';
            let btn = $(sEle).parents(".status-btn").find("button");
            if (res) {
                $(btn).removeClass('btn-active').removeClass('btn-inactive');
                $(btn).addClass(cls);
                $(btn).text(data ? 'Resolved' : 'UnResolved');
                $(sEle).parent().find("a").data("current", data ? 'True' : 'False');
                $(".loader").show();
                $.toast({
                     hideAfter : 1000,
                    heading: 'Success',
                    text: 'Event status updated successfully.',
                    showHideTransition: 'slide',
                    icon: 'success',
                     afterHidden: function () {
                        window.location.href = "serviceRequest.aspx";
                    }
                })
            }
            $(btn).attr("disabled", false);
        }

        var fromDate = {
            'targets': [6],
            'render': function (data, type, row, meta) {
                if (meta.col == 6) {
                    data = generateAction(row, data);
                }

                return data;
            }
        }


        function generateAction(row, data) {
            let cls = (data === 'True' ? 'btn-active' : 'btn-inactive');
            return '<div class="btn-group status-btn">' +
                '<button type="button" style="width:100px;" class="btn ' + cls + ' dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + (data === 'True' ? 'Resolved' : 'Unresolved') + '</button>' +
                '<div class="dropdown-menu  dropdown-menu-right">' +
                '<a class="dropdown-item" data-val="True" data-current="' + data + '" onclick="toggleStatus(this,' + row[0] + ')" href="javascript:void(0)">Resolved</a>' +
                '<a class="dropdown-item" data-val="False"data-current="' + data + '" onclick="toggleStatus(this,' + row[0] + ')" href="javascript:void(0)">Unresolved</a>' +
                '</div>';
        }

        $(document).ready(function () {
            activeLeftMenu("serviceRequest");
            $('#cal-range').daterangepicker({
                opens: 'left',
                locale: { cancelLabel: 'Clear' }
            });

            $('#cal-range').on('apply.daterangepicker', function (ev, picker) {
                startDate = picker.startDate.toDate();
                endDate = picker.endDate.toDate();
                serviceTable && serviceTable.draw()
                $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp;' + picker.startDate.format('MMM DD') + ' - ' + picker.endDate.format('MMM DD'));
            });

            $('#cal-range').on('cancel.daterangepicker', function (ev, picker) {
                startDate = '';
                endDate = '';
                //do something, like clearing an input
                $("#cal-range").html('<i class="fa fa-calendar"></i>&nbsp; Select date range');
                serviceTable && serviceTable.draw();
            });


            serviceTable = $('#pageUsers').DataTable({
                "bLengthChange": false,
                "searching": true,
                "paging": true,
                "info": false,
                "lengthChange": false,
                "autoWidth": true,
                'columnDefs': [{
                    "targets": [0],
                    "visible": false
                }, fromDate],
                dom: 't',
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    { "sType": "date" },
                     { "sType": "date" },
                    null
                ],
                'select': {
                    'style': 'multi'
                },
                'order': [[5, 'desc']]
            });

            initPagination();

            $('#pageUsers').on('draw.dt', function (e, settings, len) {
                initPagination();
            });

            $("#prevBtn").on("click", function () {
                serviceTable.page('previous').draw('page');
                initPagination();
            });
            $("#nextBtn").on("click", function () {
                serviceTable.page('next').draw('page');
                initPagination();
            });
            $("#firstBtn").on("click", function () {
                serviceTable.page('first').draw('page');
                initPagination();
            });
            $("#lastBtn").on("click", function () {
                serviceTable.page('last').draw('page');
                initPagination();
            });

            $(".loader").fadeOut();

        });

        function initPagination() {
            if (serviceTable) {
                $(".pagg-btn").attr("disabled", false);
                let page = serviceTable.page.info();
                $("#total-rec").html(page.recordsDisplay + " Emails");
                if (page.pages == 1) {
                    $(".pagg-btn").attr("disabled", true);
                }
                let cPage = page.page + 1;
                let lPage = page.pages;

                if (lPage == 0) {
                    cPage = 0;
                }
                $("#page-data").html(cPage + " of " + lPage);
                if (cPage == 1) {
                    $("#firstBtn,#prevBtn").attr("disabled", true);
                }
                if (cPage == lPage) {
                    $("#lastBtn,#nextBtn").attr("disabled", true);
                }

            }
        }

        function searchTable() {
            serviceTable && serviceTable.search($('#searchTable').val()).draw();
        }
    </script>

</body>
</html>
