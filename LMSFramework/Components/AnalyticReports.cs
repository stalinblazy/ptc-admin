﻿using LearningManagementSystem.Data;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace LearningManagementSystem.Components
{
    public class AnalyticReports
    {
        public static DataTable LearnerProgress()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.LearnerProgress, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }
        public static DataTable LearnerActivity()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.LearnerActivity, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable DepartmentProgress()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.DepartmentProgress, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable CourseActivity()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.CourseActivity, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable CourseAssessment()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.CourseAssessment, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }
        public static DataTable CourseCompletionStatus()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.CourseCompletionStatusReport, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

    }
}
