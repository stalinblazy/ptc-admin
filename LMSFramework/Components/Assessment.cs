using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;
namespace LearningManagementSystem.Components
{
    public class Assessment
    {
        public Int64 AssessmentID
        {
            get;
            set;
        }

        public Int64 Type
        {
            get;
            set;
        }
        public Int64 AssessmentTrackId
        {
            get;
            set;
        }
        public Int64 AssessmentCurriculmId
        {
            get;
            set;
        }

        public Int64 AssessmentCourseId
        {
            get;
            set;
        }

        public Boolean Active
        {
            get;
            set;
        }

        public string AssessmentTitle
        {
            get;
            set;
        }

        public int AdminId
        {
            get;
            set;
        }

        public int QuestionId
        {
            get;
            set;
        }

        public string Question
        {
            get; set;
        }



        public string Options
        {
            get; set;
        }

        public string CorrectAnswer
        {
            get; set;
        }
        public int NoOptions
        {
            get; set;
        }



        public static int AddAssessment(Assessment assessment)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@TypeID", assessment.Type, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AssessmentTitle", assessment.AssessmentTitle, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AssessmentTrackId", assessment.AssessmentTrackId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AssessmentCurriculumId", assessment.AssessmentCurriculmId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AssessmentCourseId", assessment.AssessmentCourseId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", assessment.AdminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Assessment_Id", assessment.AssessmentID, ParameterDirection.Output));
            dataManager.ExecuteNonQuery(StoreProcedure.AddAssessment, parameters.ToArray());
            return Convert.ToInt32(parameters[6].Value);
        }

        public static int AddAssessmentQuestions(Assessment assessment)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Assessment_Id", assessment.AssessmentID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Questions", assessment.Question, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Answer", assessment.CorrectAnswer, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@NoOptions", assessment.NoOptions, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", assessment.AdminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Question_Id", assessment.QuestionId, ParameterDirection.Output));
            dataManager.ExecuteNonQuery(StoreProcedure.AddAddAssessmentQuestions, parameters.ToArray());
            return Convert.ToInt32(parameters[5].Value);
        }

        public static void AddAssessmenOptions(List<Assessment> lstAssessment)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            foreach (Assessment assessment in lstAssessment)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(dataManager.CreateParam("@QuestionId", assessment.QuestionId, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@Options", assessment.Options, ParameterDirection.Input));
                dataManager.ExecuteNonQuery(StoreProcedure.AddAddAssessmentOptions, parameters.ToArray());
            }

        }

        public static DataTable GetAssessmentList(string fromdate = "", string enddate = "")
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@fromdate", Value = fromdate });
            parameters.Add(new SqlParameter { ParameterName = "@todate", Value = enddate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetAssessmentList, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

    }

    //public static void UpdateQuestions(List<FlashCard> lstFlashCard)
    //{
    //    DataAccessManager dataManager = DataAccessManager.GetInstance();
    //    foreach (FlashCard flashcard in lstFlashCard)
    //    {
    //        List<SqlParameter> parameters = new List<SqlParameter>();
    //        parameters.Add(dataManager.CreateParam("@Question", flashcard.Question, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@FlashCard_Id", flashcard.FlashCardID, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@Answer", flashcard.Answer, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@KnowMore", flashcard.KnowMore, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@FlashFile", flashcard.File, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@AdminId", flashcard.AdminId, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@FlashCardQuestionID", flashcard.FlashCardQuestionID, ParameterDirection.Input));
    //        parameters.Add(dataManager.CreateParam("@ParentQuestionID", flashcard.FlashCardQuestionParentID, ParameterDirection.Input));
    //        dataManager.ExecuteNonQuery(StoreProcedure.UpdateFalshCardQuestion, parameters.ToArray());
    //    }

    //}
}

//public class QuestionsOptions
//{
//    private int _OptId;
//    private string _QOption;
//    public int OptId
//    {
//        get
//        {
//            return _OptId;
//        }
//        set
//        {
//            _OptId = value;
//        }
//    }

//    public string QOption
//    {
//        get
//        {
//            return _QOption;
//        }
//        set
//        {
//            _QOption = value;
//        }
//    }
//}
