﻿using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Web;
using System.Web.UI;

namespace LearningManagementSystem.Components
{
    public enum CourseStatus
    {
        Invalid = 0,
        NotStarted = 1,
        InProgress = 2,
        Passed = 3,
        NotPassed = 4
    }

    public class Course
    {
        #region Properties
        public Int64 CourseID
        {
            get;
            set;
        }
        public String Name
        {
            get;
            set;
        }
        public String Description
        {
            get;
            set;
        }
        public Boolean Active
        {
            get;
            set;
        }

        public String Course_Image
        {
            get;
            set;
        }
        public String CompletionDate
        {
            get;
            set;
        }

        public String DueDate
        {
            get;
            set;
        }
        public int Duration
        {
            get;
            set;
        }

        private List<SubCourse> _SubCourses = new List<SubCourse>();
        public List<SubCourse> SubCourses
        {
            get { return _SubCourses; }
            set { _SubCourses = value; }
        }
        #endregion

        #region Static methods

        public static bool SendCourseAssignMail(String eMailID, String UserName, String Courses)
        {

            //Send mail
            XmlDocument mailTemplate = new XmlDocument();
            mailTemplate.Load(System.Configuration.ConfigurationManager.AppSettings["CourseAssignTemplate"]);
            Replace(mailTemplate, "UserName", UserName);
            Replace(mailTemplate, "courses", Courses.ToString());

            return Medtrix.EmailService.SendMail(eMailID, System.Configuration.ConfigurationManager.AppSettings["CourseAssignSubject"], mailTemplate.OuterXml, null, String.Empty, "Info");

        }

        private static void Replace(XmlDocument doc, String id, String value)
        {
            XmlNodeList nl = doc.SelectNodes("//*[@id='" + id + "']");
            foreach (XmlNode node in nl)
            {
                node.InnerText = value;
            }
        }

        public static Course CreateCourse(Course crs)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Name", crs.Name, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Description", crs.Description, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Image", crs.Course_Image, ParameterDirection.Input));
            
            
            parameters.Add(dataManager.CreateParam("@CourseID", crs.CourseID, ParameterDirection.InputOutput));
            if (crs.CompletionDate != null && crs.CompletionDate != "")
            {
                parameters.Add(dataManager.CreateParam("@CompletionDate", crs.CompletionDate, ParameterDirection.Input));
            }

            if (crs.DueDate != null && crs.DueDate != "")
            {
                parameters.Add(dataManager.CreateParam("@DueDate", crs.DueDate, ParameterDirection.Input));
            }

            parameters.Add(dataManager.CreateParam("@Duration", crs.Duration, ParameterDirection.Input));
            if (dataManager.ExecuteNonQuery(StoreProcedure.CreateCourse, parameters.ToArray()))
            {
                Course course = new Course();
                course.Name = crs.Name;
                course.Description = crs.Description;

                course.CourseID = (Int64)parameters[3].Value;
                course.Active = true;
                if (crs.CompletionDate != null && crs.CompletionDate != "")
                {
                    course.CompletionDate = crs.CompletionDate;
                }

                if (crs.DueDate != null && crs.DueDate != "")
                {
                    course.DueDate = crs.DueDate;
                }
                
                course.Duration = crs.Duration;
                return course;
            }
            return null;
        }

        public bool AssignCourseToUsers(Int64 courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseId", courseId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Course_User_Id", 0, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.AssignCourseToUsers, parameters.ToArray());
        }

        public static bool DeleteCourse(Int64 CourseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseID", CourseId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.DeleteCourse, parameters.ToArray());
        }

        public DataTable GetCourseDetailsById(Int64 courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseId", courseId, ParameterDirection.Input));
            return dataManager.Execute(StoreProcedure.GetCourseDetailsById, parameters.ToArray()).Tables[0];
        }

        public DataTable GetLMSUsers(UserRole role)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@role", role, ParameterDirection.Input));
            return dataManager.Execute(StoreProcedure.GetLMSUsers, parameters.ToArray()).Tables[0];
        }

        public DataTable GetLMSCourseUsers(UserRole role, Int64 courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@role", role, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@courseid", courseId, ParameterDirection.Input));
            return dataManager.Execute(StoreProcedure.GetLMSCourseUsers, parameters.ToArray()).Tables[0];
        }

        public Boolean SetCourseInvitationStatus(Int64 userId, Int64 courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@userid", userId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@courseid", courseId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.SetCourseInvitationStatus, parameters.ToArray());
        }


        public static DataTable GetSubCourses(Int64 CourseID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetSubCourseList, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }
        public static DataTable GetCourses(string fromdate, string enddate)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@fromdate", fromdate, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@todate", enddate, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetCourses, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
                return null;
        }
        public static DataTable GetActiveCourses()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetActiveCourses, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }
        public static Course GetCourse(Int64 CourseID)
        {
            List<Course> courses = GetCourseList();
            foreach (Course course in courses)
            {
                if (course.CourseID == CourseID)
                    return course;
            }
            return null;
        }

        public static DataTable GetCoursesByGroupID(long GroupID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@GroupID", GroupID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetCoursesByGroupID, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return ds.Tables[0];
        }

        public static DataTable GetResourceByCourseId(string courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@courseId", Convert.ToInt64(courseId), ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetResourceNameByCourseId, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0];
            return null;
        }

        public static List<Course> GetCourseList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetCourses, parameters.ToArray());
            if(ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0 )
            {
                
                Course LastCourse = null;
                List<Course> courses = new List<Course>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    
                    LastCourse = new Course();
                    LastCourse.Name = dr["Name"].ToString();
                    LastCourse.Description = dr["Description"].ToString();
                    LastCourse.CourseID = (long)dr["Course_ID"];
                    LastCourse.CompletionDate = dr["CompletionDate"].ToString();
                    LastCourse.DueDate = dr["DueDate"].ToString();
                    LastCourse.Active = (Boolean)dr["Active"];
                    LastCourse.Course_Image = dr["Course_Image"].ToString();
                    LastCourse.Duration = Convert.ToInt32(dr["Duration"]);
                    courses.Add(LastCourse);

                    DataTable dt = GetSubCourses(LastCourse.CourseID);
                    if (dt == null)
                        continue;
                    foreach (DataRow drSub in dt.Rows)
                    {
                        SubCourse subCourse = new SubCourse();
                        subCourse.Name = drSub["Name"].ToString();
                        subCourse.Description = drSub["Description"].ToString();
                        subCourse.Duration = (double)drSub["Duration"];
                        subCourse.PassMark = (double)drSub["Pass_Percentage"];
                        subCourse.Type = (CourseType)drSub["Course_Type"];
                        subCourse.SubCourseID = (long)drSub["Sub_Course_Id"];
                        subCourse.CourseID = LastCourse.CourseID;
                        subCourse.Active = (bool)drSub["Active"];
                        LastCourse.SubCourses.Add(subCourse);
                    }
                }
                return courses;
            }
            return null;
        }

        public static bool AssignCoursetoUser(Int64 CourseId, Int64 UserId, CourseStatus status, DateTime? CompletionDate, Int64 LMSGroupID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseID", CourseId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", UserId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CompletionDate", CompletionDate, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseStatus", status, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Course_User_Id", 0, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@LMSGroupID", LMSGroupID, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.AssignCoursetoUser, parameters.ToArray());
        }

        public static DataTable GetScores(Int64 UserID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            DataSet ds =  dataManager.Execute(StoreProcedure.GetScores, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetLearning(Int64 ModuleID, Int64 UserID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ModuleID", ModuleID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetLearning, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetCourseStatusReport()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetCourseStatusReport, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetAssignedCourse(Int64 UserID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetAssignedCourse, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserSubCourse(UInt64 UserID, UInt64 CourseID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserSubCourse, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetSlideCount(Int64 SubCourseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@SubCourseID", SubCourseId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetSlideCount, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetAttemptID(Int64 UserID, Int64 SubCourseID, DateTime Timestamp)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Timestamp", Timestamp, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetAttemptID, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                return ds.Tables[0];
            }
            return null;
        }

        #endregion
        #region public methods
        public SubCourse AddSubCourse(String Name, String Description, CourseType type, float PassMark, float Duration,bool IsActive)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Name", Name, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Description", Description, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Type", type, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@PassMark", PassMark, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Duration", Duration, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Active", IsActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", 0, ParameterDirection.InputOutput));
            if (dataManager.ExecuteNonQuery(StoreProcedure.CreateSubCourse, parameters.ToArray()))
            {
                SubCourse course = new SubCourse();
                course.Name = Name;
                course.Description = Description;
                course.CourseID = CourseID;
                course.Duration = Duration;
                course.PassMark = PassMark;
                course.Type = type;
                course.SubCourseID = (int)parameters[7].Value;
                course.Active = true;
                SubCourses.Add(course);
                return course;
            }
            return null;
        }
        public bool ChangeStatus(bool bActive)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Active", bActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.ChangeCourseStatus, parameters.ToArray());
        }
        #endregion
    }
    public enum CourseType
    {
        PreTest = 1,
        Course = 2,
        PostTest = 3
    }
    public enum Platform
    {
        iOS,
        PC,
		HTML5
    }
    public class SubCourse
    {
        #region Properties
        public Int64 SubCourseID
        {
            get;
            set;
        }
        public CourseType Type
        {
            get;
            set;
        }
        public double Duration
        {
            get;
            set;
        }
        public double PassMark
        {
            get;
            set;
        }
        public String Description
        {
            get;
            set;
        }
        public String Name
        {
            get;
            set;
        }
        public Boolean Active
        {
            get;
            set;
        }
        public Int64 CourseID
        {
            get;
            set;
        }

        #endregion
        public bool ChangeStatus(bool bActive)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Active", bActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", CourseID, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.ChangeSubCourseStatus, parameters.ToArray());
        }
        public bool Update()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Name", Name, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Description", Description, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Type", this.Type, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@PassMark", PassMark, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Duration", Duration, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", SubCourseID, ParameterDirection.InputOutput));
            return dataManager.ExecuteNonQuery(StoreProcedure.CreateSubCourse, parameters.ToArray());

        }

        public bool SaveCourseContent(Platform PlatformID, System.IO.Stream ContentStream)
        {
            byte[] buffer = new byte[ContentStream.Length];
            ContentStream.Read(buffer, 0, (int)ContentStream.Length);
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@PlatformID", PlatformID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseContent", buffer, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", this.SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@ContentID", 0, ParameterDirection.InputOutput));
            return dataManager.ExecuteNonQuery(StoreProcedure.StoreCourseContent, parameters.ToArray());
        }

        public byte[] GetCourseContent(Platform PlatformID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@PlatformID", PlatformID, ParameterDirection.Input));
            //parameters.Add(dataManager.CreateParam("@SubCourseID", PlatformID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", SubCourseID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetCourseContent, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1)
            {
                return (byte[])ds.Tables[0].Rows[0]["Course_Content"];
            }
            return null;
        }

        public static Boolean SendCourseLaunchEmail(String FirstName, String LastName, String email, String CourseId, String CourseName)
        {
            String serverName = System.Configuration.ConfigurationManager.AppSettings["server"];
            String senderMail = System.Configuration.ConfigurationManager.AppSettings["sender"];
            String body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/templates/courseassign.html"));
            body = body.Replace("#YEAR#", DateTime.Now.Year.ToString());
            body = body.Replace("#LASTNAME#", LastName);
            body = body.Replace("#FIRSTNAME#", FirstName);
            body = body.Replace("#MODULETITLE#", CourseName);
            body = body.Replace("#LINK#", serverName + "/login.aspx");
            body = body.Replace("#COURSEIMAGE#", serverName + "/services/GetImage.aspx?id="+ CourseId);

            return Medtrix.MailService.SendInBlue.SendEMail(email, senderMail, "Launch of new course – PTC", body, null, true);

        }

        public static DataTable GetCourseListRemainder()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute("GetCourseListRemainder", parameters.ToArray());
            if(ds != null && ds.Tables[0] != null)
            {
                return ds.Tables[0];
            }
            return new DataTable();
        }

        public static bool SetCourseListRemainderSend(Int32 UserId, Int32 CourseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseId", CourseId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserId", UserId, ParameterDirection.Input));
            return  dataManager.ExecuteNonQuery("SetCourseListRemainderSend", parameters.ToArray());
        }


        

        public static bool SendCoursetoGroupEmail(Int32 GroupId, Int32 CourseId)
        {
            String serverName = System.Configuration.ConfigurationManager.AppSettings["server"];
            String senderMail = System.Configuration.ConfigurationManager.AppSettings["sender"];
            String body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/templates/groupcourseassign.html"));

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CourseId", CourseId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute("GetCourseById", parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1)
            {
                body = body.Replace("#MODULETITLE#", ds.Tables[0].Rows[0]["Name"].ToString());
            }
            
            List<SqlParameter> userParams = new List<SqlParameter>();
            userParams.Add(dataManager.CreateParam("@groupId", GroupId, ParameterDirection.Input));
            DataSet userds = dataManager.Execute("GetLmsUsersByGroupId", userParams.ToArray());
            List<String> userEmails= new List<string>();

            if(userds != null && userds.Tables[0] != null)
            {
                foreach(DataRow dr in userds.Tables[0].Rows)
                {
                    userEmails.Add(dr["Email_ID"].ToString());
                }
            }

            body = body.Replace("#YEAR#", DateTime.Now.Year.ToString());
            body = body.Replace("#COURSEIMAGE#", serverName + "/services/GetImage.aspx?id=" + CourseId);

            return Medtrix.MailService.SendInBlue.SendEMail("support@ptcdegree.com", senderMail, "Launch of new course – PTC", body, null, false, userEmails);
        }
    }
}
