﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;

namespace LearningManagementSystem.Components
{
    public class Events
    {
        public Int64 ID
        {
            get;
            set;
        }
        public bool isActive
        {
            get;
            set;
        }
        public String EventData
        {
            get;
            set;
        }
        public DateTime fromDate
        {
            get;
            set;
        }
        public DateTime toDate
        {
            get;
            set;
        }
        public String Venue
        {
            get;
            set;
        }
        public String EventLink
        {
            get;
            set;
        }
        public String Time
        {
            get;
            set;
        }
        public String Img
        {
            get;
            set;
        }

        public int AdminId
        {
            get;
            set;
        }

        public static DataTable GetAllEvents()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetUpcomingEvents, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetEvent(Int64 ID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ID", ID, ParameterDirection.Input));

            DataSet ds = dataManager.Execute(StoreProcedure.GetEvent, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0];
            return null;
        }

        public static bool DeleteEvent(Int64 ID, int adminId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ID", ID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", adminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Desc", "Admin Deleted the Event EventId:" + ID + "", ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.DeleteEvent, parameters.ToArray());

        }



        public static bool SetEventStatus(Events ev)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ID", ev.ID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@IsActive", ev.isActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", ev.AdminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Desc", "Admin Changed the Event Status to :" + ev.isActive + " Event ID : " + ev.ID + "", ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.SetEventStatus, parameters.ToArray());
        }

        public static bool UpdateEvent(Events ev)
        {
            string even = "";
            even = ev.ID == 0 ? "Admin Create New Event " + ev.EventData + "" : "Admin Modified the Event " + ev.ID + "";
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ID", ev.ID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@IsActive", ev.isActive, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@fromDate", ev.fromDate, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@toDate", ev.toDate, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@EventData", ev.EventData, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@EventLink", ev.EventLink, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Time", ev.Time, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Img", ev.Img, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Venue", ev.Venue, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", ev.AdminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Desc", even, ParameterDirection.Input));

            return dataManager.ExecuteNonQuery(StoreProcedure.CreateEvent, parameters.ToArray());
        }
    }
}
