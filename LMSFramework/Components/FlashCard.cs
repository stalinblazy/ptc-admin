﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;

namespace LearningManagementSystem.Components
{
 public class FlashCard
    {

        public Int64 FlashCardID
        {
            get;
            set;
        }
        public Int64 SelectedResourceTypeId
        {
            get;
            set;
        }
        public Int64 Type
        {
            get;
            set;
        }
        public Int64 FlashCardTrack
        {
            get;
            set;
        }
        public Int64 FlashCardCurriculam
        {
            get;
            set;
        }
        public Int64 FlashCardCourse
        {
            get;
            set;
        }
        public Boolean Active
        {
            get;
            set;
        }

        public string CardTitle
        {
            get;
            set;
        }

        public int AdminId
        {
            get;
            set;
        }

        public string Question
        {
            get;set;
        }

        public string Answer
        {
            get; set;
        }

        public string File
        {
            get; set;
        }


        public string KnowMore
        {
            get; set;
        }
        public Int64 FlashCardQuestionID
        {
            get;
            set;
        }
        public Int64 FlashCardQuestionParentID
        {
            get;
            set;
        }
        public static int AddFlashCard(FlashCard flashcard)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@TypeID",flashcard.Type, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CardTitle", flashcard.CardTitle, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCardTrackId", flashcard.FlashCardTrack, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCardCurriculumId", flashcard.FlashCardCurriculam, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCardCourseId", flashcard.FlashCardCourse, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", flashcard.AdminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SelectedResourceTypeId", flashcard.SelectedResourceTypeId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCard_Id", flashcard.FlashCardID, ParameterDirection.Output));
            dataManager.ExecuteNonQuery(StoreProcedure.AddFalshCard, parameters.ToArray());
            return Convert.ToInt32(parameters[7].Value);
        }
        public static int UpdateFlashCard(FlashCard flashcard)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@TypeID", flashcard.Type, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CardTitle", flashcard.CardTitle, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCardTrackId", flashcard.FlashCardTrack, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCardCurriculumId", flashcard.FlashCardCurriculam, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", flashcard.AdminId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCard_Id", flashcard.FlashCardID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FlashCardSelectedResourceTypeId", flashcard.SelectedResourceTypeId, ParameterDirection.Input));
            dataManager.ExecuteNonQuery(StoreProcedure.UpdateFalshCard, parameters.ToArray());
            return Convert.ToInt32(parameters[5].Value);
        }

        public static void AddQuestions(List<FlashCard> lstFlashCard)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            foreach (FlashCard flashcard in lstFlashCard)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(dataManager.CreateParam("@FlashCard_Id", flashcard.FlashCardID, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@Question", flashcard.Question, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@Answer", flashcard.Answer, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@KnowMore", flashcard.KnowMore, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@FlashFile", flashcard.File, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@AdminId", flashcard.AdminId, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@ParentQuestionID", flashcard.FlashCardQuestionParentID, ParameterDirection.Input));
                dataManager.ExecuteNonQuery(StoreProcedure.AddFalshCardQuestions, parameters.ToArray());
            }
           
        }
        public static void UpdateQuestions(List<FlashCard> lstFlashCard)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            foreach (FlashCard flashcard in lstFlashCard)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(dataManager.CreateParam("@Question", flashcard.Question, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@FlashCard_Id", flashcard.FlashCardID, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@Answer", flashcard.Answer, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@KnowMore", flashcard.KnowMore, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@FlashFile", flashcard.File, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@AdminId", flashcard.AdminId, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@FlashCardQuestionID", flashcard.FlashCardQuestionID, ParameterDirection.Input));
                parameters.Add(dataManager.CreateParam("@ParentQuestionID", flashcard.FlashCardQuestionParentID, ParameterDirection.Input));
                dataManager.ExecuteNonQuery(StoreProcedure.UpdateFalshCardQuestion, parameters.ToArray());
            }

        }
        public static List<FlashCard> GetFlashCardList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetFlashCard, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {

                FlashCard flashCard = null;
                List<FlashCard> listFlashcard = new List<FlashCard>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    flashCard = new FlashCard();
                    flashCard.CardTitle = dr["CardTitle"].ToString();
                    listFlashcard.Add(flashCard);
                }
                return listFlashcard;
            }
            return null;
        }

        public static DataTable GetFlashCardQuestion(string fromdate = "", string enddate = "")
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@fromdate", Value = fromdate });
            parameters.Add(new SqlParameter { ParameterName = "@todate", Value = enddate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetFlashCardQuestion, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetFlashCardDetailsById(Int64 flashCardID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Flashcard_id", flashCardID, ParameterDirection.Input));
            return dataManager.Execute(StoreProcedure.GetFlashCardQuestions, parameters.ToArray()).Tables[0];
        }


        public static FlashCard GetFlashCard(Int64 FlashCardID)
        {
            List<FlashCard> flashcard = GetFlashCardList();
            foreach (FlashCard flashcardobj in flashcard)
            {
                if (flashcardobj.FlashCardID == FlashCardID)
                    return flashcardobj;
            }
            return null;
        }

        public static DataTable GetFlashCard(string fromdate = "", string enddate = "")
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@fromdate", Value = fromdate });
            parameters.Add(new SqlParameter { ParameterName = "@todate", Value = enddate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetFlashCard, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }
        public static DataTable GetFlashCardQuestionByID(string flashCardQuestionID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@FlashCardQuestionID", Value = flashCardQuestionID });
            DataSet ds = dataManager.Execute(StoreProcedure.GetFlashCardMergeQuestion, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }


        public static DataTable GetFlashCardQstCount(int flashcardCount)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@FLASHCARD_ID", Value = flashcardCount });
            DataSet ds = dataManager.Execute(StoreProcedure.GetFlashCardQuestionCount, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetFlashCardQstComments(int flashcardqstId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@FlashQstId", Value = flashcardqstId });
            DataSet ds = dataManager.Execute(StoreProcedure.GetFlashCardQuestionComments, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }








    }
}
