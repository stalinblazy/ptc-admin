﻿using LearningManagementSystem.Data;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningManagementSystem.Components
{
    public class Group
    {
        public static bool IsGroupNameExists(String Groupname)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@GroupName", Groupname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", 1, ParameterDirection.Output));

            dataManager.Execute(StoreProcedure.IsGroupNameExist, parameters.ToArray());

            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }


        public static bool checkCourseInGroup(Int32 GroupId, Int32 CourseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@groupId", GroupId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@courseId", CourseId, ParameterDirection.Input));

            DataSet ds = dataManager.Execute("CheckCourseInGroup", parameters.ToArray());
            return ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0 ? true : false;

        }


        public static bool SendGroupEmailer(String GroupName, Int32 GroupId, Int32 UserId)
        {
            User us = User.GetUser(UserId);

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@groupId", GroupId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute("GetCourseListByGroupId", parameters.ToArray());
            DataTable dt = ds.Tables[0];
            String courses = "<ul>";
            foreach (DataRow dr in dt.Rows)
            {
                courses += "<li>" + dr["Name"].ToString() + "</li>";
            }
            courses += "</ul>";
            String serverName = System.Configuration.ConfigurationManager.AppSettings["server"];
            String senderMail = System.Configuration.ConfigurationManager.AppSettings["sender"];
            String body = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/templates/groupassign.html"));
            body = body.Replace("#YEAR#", DateTime.Now.Year.ToString());
            body = body.Replace("#FULLNAME#", us.FirstName + " " + us.LastName);
            body = body.Replace("#GROUP#", GroupName);
            body = body.Replace("#COURSES#", courses);

            return Medtrix.MailService.SendInBlue.SendEMail(us.EMailID, senderMail, "PTCDegree welcomes you to your new group", body, null, true);
        }

    }
       
}
