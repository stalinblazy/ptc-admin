﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml;

using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;

namespace LearningManagementSystem.Components
{
    public class Reports
    {
        #region Properties
        public Int64 CountryID
        {
            get;
            set;
        }
        public Int64 CityID
        {
            get;
            set;
        }

        public Int64 SubCourseStatus
        {
            get;
            set;
        }
        public Int64 SubCourseType
        {
            get;
            set;
        }
        #endregion

        #region Static methods
        public static DataSet GetRegistrationReport(Int64 year)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@year", year, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetNewRegistrationReport, parameters.ToArray());
            if (ds != null)
            {
                return ds;
            }
            return null;
        }

        public static DataTable GetCourseWiseReport()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
             DataSet ds = dataManager.Execute(StoreProcedure.GetCourseWiseReport, parameters.ToArray());
            if (ds != null)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserWiseReport()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserWiseReport, parameters.ToArray());
            if (ds != null)
            {
                return ds.Tables[0];
            }
            return null;
        }

       

        public static DataTable GetUserDistributionByCountry()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserDistributionByCountryReport, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserDistributionBySalesRep()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserDistributionBySalesRep, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserSubCourseStatus(Int64 SubCourseStatus, Int64 SubCourseType)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@SubCourseStatus", SubCourseStatus, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseType", SubCourseType, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.UserSubCourseStatus, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserSubCourseSeatTime(Int64 SubCourseType)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@SubCourseType", SubCourseType, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.UserSubCourseSeatTime, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserSubCourseScore(Int64 SubCourseType)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@SubCourseType", SubCourseType, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.UserSubCourseScore, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static DataTable GetUserCourseStatus()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.UserCourseStatus, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];

                dt.DefaultView.Sort = "Subcourse_Id asc";
                DataTable courseTable = new DataTable();
                courseTable.Columns.Add("SalesManager", typeof(string));
                courseTable.Columns.Add("SalesRep", typeof(string));
                courseTable.Columns.Add("UserId", typeof(string));
                courseTable.Columns.Add("User", typeof(string));
                courseTable.Columns.Add("CourseId", typeof(string));
                courseTable.Columns.Add("CourseName", typeof(string));
                courseTable.Columns.Add("CourseStatus", typeof(string));

                int count = -1;
                string[] status = new string[3];
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    count++;
                    status[count] = dt.Rows[index]["SubCourseStatus"].ToString();

                    if (count == 2)
                    {
                        count = -1;

                        if (status[0].ToString().Equals("Not Started") &&
                            status[1].ToString().Equals("Not Started") &&
                            status[2].ToString().Equals("Not Started"))
                        {
                            courseTable.Rows.Add(
                                dt.Rows[index]["SalesManager"].ToString(),
                                dt.Rows[index]["SalesRep"].ToString(),
                                dt.Rows[index]["User_Id"].ToString(),
                                dt.Rows[index]["User"].ToString(),
                                dt.Rows[index]["Course_Id"].ToString(),
                                dt.Rows[index]["Name"].ToString(),
                                "Not Started");
                        }

                        else if (status[1].ToString().Equals("Passed") &&
                                 status[2].ToString().Equals("Passed"))
                        {
                            courseTable.Rows.Add(
                                dt.Rows[index]["SalesManager"].ToString(),
                                dt.Rows[index]["SalesRep"].ToString(),
                                dt.Rows[index]["User_Id"].ToString(),
                                dt.Rows[index]["User"].ToString(),
                                dt.Rows[index]["Course_Id"].ToString(),
                                dt.Rows[index]["Name"].ToString(),
                                "Passed");
                        }

                        else if (status[0].ToString().Equals("Not Passed") &&
                                 status[1].ToString().Equals("Not Passed") &&
                                 status[2].ToString().Equals("Not Passed"))
                        {
                            courseTable.Rows.Add(
                                dt.Rows[index]["SalesManager"].ToString(),
                                dt.Rows[index]["SalesRep"].ToString(),
                                dt.Rows[index]["User_Id"].ToString(),
                                dt.Rows[index]["User"].ToString(),
                                dt.Rows[index]["Course_Id"].ToString(),
                                dt.Rows[index]["Name"].ToString(),
                                "Not Passed");
                        }

                        else
                        {
                            courseTable.Rows.Add(
                                dt.Rows[index]["SalesManager"].ToString(),
                                dt.Rows[index]["SalesRep"].ToString(),
                                dt.Rows[index]["User_Id"].ToString(),
                                dt.Rows[index]["User"].ToString(),
                                dt.Rows[index]["Course_Id"].ToString(),
                                dt.Rows[index]["Name"].ToString(),
                                "In Progress");
                        }
                    }
                }

                return courseTable;
            }
            return null;
        }

        //public static DataTable GetUserDistributionByCity(Int64 CountryId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@Country", CountryId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetUserDistributionByCityReport, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetBestScorerInCountry(Int64 CountryId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@CountryID", CountryId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetBestScorerInCountry, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetBestScorerInCity(Int64 CityId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@CityID", CityId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetBestScorerInCity, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetCourseScoreByCountry(Int64 CountryId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@CountryID", CountryId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetCourseScoreByCountry, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetCourseScoreByCity(Int64 CityId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@CityID", CityId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetCourseScoreByCity, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetCourseCompletedByLastMonth()
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@StartDate", DateTime.Now.AddDays(-30), ParameterDirection.Input));
        //    parameters.Add(dataManager.CreateParam("@EndDate", DateTime.Now, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetCourseCompletedByLastMonth, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetSeatTimeByCountry(Int64 CountryId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@CountryID", CountryId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetSeatTimeReportByCountry, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetSeatTimeByCity(Int64 CityId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@CityID", CityId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetSeatTimeReportByCity, parameters.ToArray());
        //    if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetUserCourseStatusReport(Int64 CountryId, Int64 CityId, CourseStatus Status)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@Country", CountryId, ParameterDirection.Input));
        //    parameters.Add(dataManager.CreateParam("@City", CityId, ParameterDirection.Input));
        //    parameters.Add(dataManager.CreateParam("@CourseStatus", Status, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetUserCourseStatusReport, parameters.ToArray());

        //    if (ds != null && ds.Tables.Count == 1)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetDefaultCity(Int64 CountryId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@Country", CountryId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetDefaultCity, parameters.ToArray());

        //    if (ds != null && ds.Tables.Count == 1)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetUserTranscriptReport(Int64 UserId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@userID", UserId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetUserTranscriptReport, parameters.ToArray());

        //    if (ds != null && ds.Tables.Count == 1)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetAggregateStatistics()
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetAggregateStatistcs, parameters.ToArray());

        //    if (ds != null && ds.Tables.Count == 1)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetSummaryReport(Int64 UserId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@UserID", UserId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetSummaryReport, parameters.ToArray());

        //    if (ds != null && ds.Tables.Count == 1)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        //public static DataTable GetDetailedReport(Int64 UserId, Int64 ModuleId)
        //{
        //    DataAccessManager dataManager = DataAccessManager.GetInstance();
        //    List<SqlParameter> parameters = new List<SqlParameter>();
        //    parameters.Add(dataManager.CreateParam("@UserID", UserId, ParameterDirection.Input));
        //    parameters.Add(dataManager.CreateParam("@CourseID", ModuleId, ParameterDirection.Input));
        //    DataSet ds = dataManager.Execute(StoreProcedure.GetDetailedReport, parameters.ToArray());

        //    if (ds != null && ds.Tables.Count == 1)
        //    {
        //        return ds.Tables[0];
        //    }
        //    return null;
        //}

        #endregion
    }
}
