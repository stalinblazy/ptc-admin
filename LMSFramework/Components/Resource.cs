﻿using LearningManagementSystem.Data;
using Medtrix.DataAccessControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningManagementSystem.Components
{
    public enum ResourceType
    {
        Invalid = 0,
        JOURNALS = 1,
        VIDEO = 2,
        WEBLINK = 3,
        SlideDeck = 5,
        Poster = 4,
        iRead=6,
        FlashCard=7,
    }
    public class Resource
    {
        public Int64 ResourceID
        {
            get;
            set;
        }
        public Int64 ResourceLibraryTypeID
        {
            get;
            set;
        }
        public String Name
        {
            get;
            set;
        }
        public String Description
        {
            get;
            set;
        }
        public Boolean Active
        {
            get;
            set;
        }

        public String UploadURL
        {
            get;
            set;
        }

        public String ResourceLibraryType
        {
            get;
            set;
        }
        public Int64 TypeID
        {
            get;
            set;
        }
        public String ThumbnailImgURL
        {
            get;
            set;
        }

        public int AdminId
        {
            get;
            set;
        }
        public Int64 ResourceTrack { get; set; }
        public Int64 ResourceCurriculum { get; set; }
        public Int64 ResourceCourses { get; set; }
        public string Vol_Iss_Pg { get; set; }
        public string Author { get; set; }
        public string Publication { get; set; }
        public string WebLink { get; set; }

        public string uploadURL { get; set; }
        public bool IsRequired { get; set; }

        public static DataTable GetResource(string fromDate = "", string endDate = "")
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@fromDate", Value = fromDate });
            parameters.Add(new SqlParameter { ParameterName = "@endDate", Value = endDate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetResource, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
                return null;
        }

        public static DataTable GetAssignResource(string fromDate = "", string endDate = "", long courseId = 0)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@fromDate", Value = fromDate });
            parameters.Add(new SqlParameter { ParameterName = "@endDate", Value = endDate });
            parameters.Add(new SqlParameter { ParameterName = "@courseId", Value = courseId });
            DataSet ds = dataManager.Execute(StoreProcedure.GetAssignResource, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }



        public static Resource CreateResource(Resource res)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            
            parameters.Add(dataManager.CreateParam("@Name", res.Name, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Description", res.Description, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@TrackID", res.ResourceTrack, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CurriculumID", res.ResourceCurriculum, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CoursesID", res.ResourceCourses, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@TypeID", res.ResourceLibraryTypeID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UploadURL", res.UploadURL, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@ResourceID", res.ResourceID, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@ThumbnailImgURL", res.ThumbnailImgURL, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@WebLink", res.UploadURL, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Author", res.Author, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Vol_Iss_Pg", res.Vol_Iss_Pg, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Publication", res.Publication, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", res.AdminId, ParameterDirection.Input));
            if (dataManager.ExecuteNonQuery(StoreProcedure.AddResource, parameters.ToArray()))
            {
                Resource resource = new Resource();
                resource.ResourceLibraryTypeID = res.ResourceLibraryTypeID;
                resource.Name = res.Name;
                resource.Description = res.Description;
                resource.UploadURL = res.UploadURL;
                resource.ResourceID = (Int64)parameters[4].Value;
                resource.Active = true;
                resource.ThumbnailImgURL = res.ThumbnailImgURL;
                AssignResourcetoCourse(resource.ResourceID, res.ResourceCourses);
                return resource;
            }
            return null;
        }

        public static bool DeleteResource(Int64 ResourceId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ResourceID", ResourceId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.DeleteResource, parameters.ToArray());
        }

        public static bool DeleteEvent(Int64 EventID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EventId", EventID, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.DeleteEvent, parameters.ToArray());
        }

        public static Resource UpdateResource(Resource res)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(dataManager.CreateParam("@Name", res.Name, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Active", res.Active, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Description", res.Description, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@TypeID", res.ResourceLibraryTypeID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UploadURL", res.UploadURL, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@ResourceID", res.ResourceID, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@ThumbnailImgURL", res.ThumbnailImgURL, ParameterDirection.Input));
            if (dataManager.ExecuteNonQuery(StoreProcedure.UpdateResource, parameters.ToArray()))
            {
                Resource resource = new Resource();
                resource.ResourceLibraryTypeID = res.ResourceLibraryTypeID;
                resource.Name = res.Name;
                resource.Active = res.Active;
                resource.Description = res.Description;
                resource.UploadURL = res.UploadURL;
                resource.ResourceID = (Int64)parameters[5].Value;
                //resource.Active = true;
                resource.ThumbnailImgURL = res.ThumbnailImgURL;
                return resource;
            }
            return null;
        }

        public static bool AssignResourcetoUser(Int64 ResourceID, Int64 UserID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ResourceID", ResourceID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@ResourceUserID", 0, ParameterDirection.InputOutput));
            return dataManager.ExecuteNonQuery(StoreProcedure.AssignResourcetoUser, parameters.ToArray());
        }
        public static Resource GetResource(Int64 ResourceID)
        {
            List<Resource> resources = GetResourceList();
            foreach (Resource resource in resources)
            {
                if (resource.ResourceID == ResourceID)
                    return resource;
            }
            return null;
        }

        public static List<Resource> GetResourceList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetResource, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
            {

                Resource LastResource = null;
                List<Resource> resource = new List<Resource>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    LastResource = new Resource();
                    LastResource.Name = dr["Name"].ToString();
                    LastResource.Description = dr["Description"].ToString();
                    LastResource.ResourceID = (long)dr["ID"];
                    LastResource.Active = (Boolean)dr["IsActive"];
                    LastResource.ResourceLibraryType = dr["Name1"].ToString();
                    LastResource.TypeID = Convert.ToUInt32(dr["ID1"]);
                    LastResource.uploadURL = dr["UploadURL"].ToString();
                    LastResource.ThumbnailImgURL = dr["ThumbnailImgURL"].ToString();
                    resource.Add(LastResource);

                   
                }
                return resource;
            }
            return null;
        }
        public static bool AssignResourcetoCourse(Int64 ResourceID, Int64 CourseID, bool isRequired = false, Int64 AdminId=0)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ResourceID", ResourceID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@isRequired", isRequired, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminID", AdminId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.AssignResourcetoCourse, parameters.ToArray());
        }
        public static bool UnAssignResourcetoCourse(Int64 ResourceID, Int64 CourseID,Int64 AdminId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ResourceID", ResourceID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseID", CourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", AdminId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.UnAssignResourcetoCourse, parameters.ToArray());
        }
    }

}
