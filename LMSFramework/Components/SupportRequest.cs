﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;
using System.Xml;

namespace LearningManagementSystem.Components
{
    public enum IssueCategory
    {
        AccessIssue,
        LoginIssue,
        CourseIssue
    }

    public enum IssueStatus
    {
        InProgress = 1,
        Resolved = 2
    }

    public class SupportRequest
    {
        
        public Int64 ID
        {
            get;
            set;
        }
        public bool isResolved
        {
            get;
            set;
        }

        public int AdminId
        {
            get;
            set;
        }


        public static DataTable GetSupportRequests()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetSupportRequests, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0];
            return null;
        }

        public static bool SetRequestStatus(SupportRequest ev)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@ID", ev.ID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@isResolved", ev.isResolved, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AdminId", ev.AdminId, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.SetRequestStatus, parameters.ToArray());
        }
        


    }

}
