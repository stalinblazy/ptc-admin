﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml;

using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;

namespace LearningManagementSystem.Components
{
    public enum UserRole
    {
        Invalid = 0,
        Admin = 1,
        SalesManager = 2,
        SalesRep = 3,
        User = 4,
        UserViewer = 5
    }

    public enum UserCourseStatus
    {
        Invalid = 0,
        NotStarted = 1,
        InProgress = 2,
        Passed = 3,
        NotPassed = 4
    }


    public class TechSupport
    {
        public String Email = String.Empty;
        public String Subject = String.Empty;
        public Int32 IssueID = 0;
        public String IssueString = String.Empty;
        public String Description = String.Empty;
    }


    public class LogData
    {
        public String log = String.Empty;

    }

    public class MailAndCode
    {
        public String EmailId = String.Empty;
        public String AccessCode = String.Empty;
    }


    public class EmailData
    {
        public String Email = String.Empty;

    }
    public class IssueList
    {
        public Int32 IssueID = 0;
        public String Issue = String.Empty;

        public IssueList()
        {
        }

        public IssueList(Int32 issueID, String issue)
        {
            this.IssueID = issueID;
            this.Issue = issue;
        }

    }
    // World DB Class
    public class Countries
    {
        public Int64 CountryId = 0;
        public String CountryName = String.Empty;
    }

    public class States
    {
        public Int64 StateId = 0;
        public Int64 CountryId = 0;
        public String StateName = String.Empty;
    }

    public class Cities
    {
        public Int64 CityId = 0;
        public String CityName = String.Empty;
        public Int64 StateId = 0;
        public Int64 CountryId = 0;
    }

    public class User
    {
        #region Properties
        public Int64 UserID
        {
            get;
            set;
        }
        public String FirstName
        {
            get;
            set;
        }

        public String LastName
        {
            get;
            set;
        }

        public String Title
        {
            get;
            set;
        }

        public String Designation
        {
            set;
            get;
        }

        public String Address
        {
            get;
            set;
        }

        public String LMSGroupId
        {
            get;
            set;
        }

        public String Country
        {
            get;
            set;
        }

        public String State
        {
            get;
            set;
        }

        public String City
        {
            get;
            set;
        }

        public String Username
        {
            get;
            set;
        }

        public String Password
        {
            get;
            set;
        }

        public String AccessToken
        {
            get;
            set;
        }

        public Boolean Active
        {
            get;
            set;
        }

        public UserRole Role
        {
            get;
            set;
        }

        public Int32 InvitesSent
        {
            get;
            set;
        }

        public String EMailID
        {
            get;
            set;
        }

        public String MobileNumber
        {
            get;
            set;
        }

        public String PharmacyName
        {
            get;
            set;
        }

        public String PharmacyChainName
        {
            get;
            set;
        }

        public String AccessCode
        {
            get;
            set;
        }

        public Int64 ParentUserID
        {
            get;
            set;
        }
        #endregion

        #region Static methods

        public static DataTable GetUnAssignedCourseUserList(int courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@courseId", courseId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetUnAssignedCourseUserList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetUnAssignedCourseGroupList(string fromDate, string endDate,int courseId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@courseId", courseId, ParameterDirection.Input));
            parameters.Add(new SqlParameter { ParameterName = "@fromDate", Value = fromDate });
            parameters.Add(new SqlParameter { ParameterName = "@endDate", Value = endDate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetUnAssignedCourseGroupList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetUserList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetUserListTypeWise(string fromDate, string endDate, string userType)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserType", userType, ParameterDirection.Input));
            parameters.Add(new SqlParameter { ParameterName = "@fromdate", Value = fromDate });
            parameters.Add(new SqlParameter { ParameterName = "@todate", Value = endDate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserListTypeWise, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static Int64 CreateUser(String eMailID, String Firstname, String Lastname)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EMailID", eMailID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FirstName", Firstname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@LastName", Lastname, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", 0, ParameterDirection.Output));
            dataManager.ExecuteNonQuery(StoreProcedure.CreateUser, parameters.ToArray());

            Int64 UserId = 0;
            UserId = Convert.ToInt64(parameters[parameters.Count - 1].SqlValue.ToString());
            Medtrix.Trace.Logger.Log("Created UserId: " + UserId);
            return UserId;
        }

        public static Boolean MapCoursetoUser(Int64 UserID, Int64 SubCourseID, String Percentage, DateTime? CompletionDate, UserCourseStatus Status)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Percentage", Percentage, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", Status, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CompletionDate", CompletionDate, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.MapCoursetoUser, parameters.ToArray());
        }

        public static bool IsMailExists(String EMailID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EMailID", EMailID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", 1, ParameterDirection.Output));

            dataManager.Execute(StoreProcedure.IsEmailExist, parameters.ToArray());

            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public static bool IsUsernameExist(String Username)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Username", Username, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", 1, ParameterDirection.Output));

            dataManager.Execute(StoreProcedure.IsUsernameExist, parameters.ToArray());

            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public static DataTable ForgotPassword(String Mail)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EMailID", Mail, ParameterDirection.Input));

            DataSet ds = dataManager.Execute(StoreProcedure.ForgotPassword, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static bool SetPassword(Int64 UserID, String Password)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Password", Password, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));

            return dataManager.ExecuteNonQuery(StoreProcedure.SetPassword, parameters.ToArray());
        }

        public static DataSet AuthenticateUser(String username, String password)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserName", username, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Password", password, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", 0, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@UserID", 0, ParameterDirection.InputOutput));
            DataSet ds = dataManager.Execute(StoreProcedure.AuthenticateUser, parameters.ToArray());

            return ds;
        }

        public static DataSet GetLastSlide(UInt64 UserID, UInt64 SubCourseID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", SubCourseID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserLastSlide, parameters.ToArray());

            return ds;
        }



        public static User Authenticate(String Username, String Password)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserName", Username, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Password", Password, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", 0, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@UserID", 0, ParameterDirection.InputOutput));
            DataSet ds = dataManager.Execute(StoreProcedure.AuthenticateUser, parameters.ToArray());

            if (ds != null && ds.Tables.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                return GetUser(dr);
            }
            UnauthorizedAccessException ex = new UnauthorizedAccessException("Incorrect username or password.");

            ex.Data["ErrorCode"] = 998; // Error to mobile saying invalid parameters.
            ex.Data["result"] = null;
            throw ex;
        }

        public static bool SendInvite(String eMailID, String Firstname, String Lastname)
        {
            String AccessToken = "adcetris2014";
            DateTime TokenExpiry = DateTime.Now.AddHours(48);

            //Send mail
            XmlDocument mailTemplate = new XmlDocument();
            mailTemplate.Load(System.Configuration.ConfigurationManager.AppSettings["InviteMailTemplate"]);
            Replace(mailTemplate, "UserName", Firstname + " " + Lastname);
            Replace(mailTemplate, "token", AccessToken.ToString());

            Medtrix.EmailService.SendMail(eMailID, System.Configuration.ConfigurationManager.AppSettings["InviteMailSubject"], mailTemplate.OuterXml, null, String.Empty, "Info");

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EMailID", eMailID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AccessToken", AccessToken, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@TokenExpiry", TokenExpiry, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", 0, ParameterDirection.InputOutput));

            return dataManager.ExecuteNonQuery(StoreProcedure.InviteSent, parameters.ToArray());

        }

        private static void Replace(XmlDocument doc, String id, String value)
        {
            XmlNodeList nl = doc.SelectNodes("//*[@id='" + id + "']");
            foreach (XmlNode node in nl)
            {
                node.InnerText = value;
            }
        }

        public static DataTable GetAdmin(Int64 UserID, Int64 Role)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Role", Role, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetAdmin, parameters.ToArray());

            if (ds != null && ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public static User GetUser(Int64 UserID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));

            DataSet ds = dataManager.Execute(StoreProcedure.GetUserInfo, parameters.ToArray());

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
            {
                return GetUser(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public static User GetUser(DataRow dr)
        {
            User user = new User();
            user.FirstName = dr["First_Name"].ToString();
            user.LastName = dr["Last_Name"].ToString();
            user.Title = dr["Title"].ToString();
            user.Designation = dr["Designation"].ToString();
            user.Address = dr["Address"].ToString();
            user.Country = dr["Country"].ToString();
            user.State = dr["State"].ToString();
            user.City = dr["City"].ToString();
            user.Username = dr["User_Name"].ToString();
            user.Role = (UserRole)dr["Role"];
            user.LMSGroupId = dr["LMSGroupID"].ToString();
            //user.InvitesSent = (int)dr["Invites_Sent"];
            user.Active = (Boolean)dr["Active"];
            user.EMailID = dr["Email_ID"].ToString();
            user.UserID = (Int64)dr["User_Id"];
            user.AccessToken = dr["Access_Token"].ToString();
            user.Password = dr["Password"].ToString();
            user.MobileNumber = dr["MobileNumber"].ToString();
            //user.PharmacyName = dr["PharmacyName"].ToString();
            //user.PharmacyChainName = dr["PharmacyChainName"].ToString();
            //user.AccessCode = dr["AccessCode"].ToString();
            //user.ParentUserID = (Int64)dr["ParentUserID"];

            return user;
        }

        public static DataTable GetDesignationList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetDesignationList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetCountryList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetCountryList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetRegisteredCountryList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetRegisteredCountryList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetCountryStatesList(Int64 CountryId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CountryId", CountryId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetCountryStatesList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetStatesCityList(Int64 StateId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@StateId", StateId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetStateCityList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetCountryCityList(Int64 CountryId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CountryId", CountryId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetCityofCountry, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetRegisteredCityList(Int64 CountryId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CountryId", CountryId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetRegisteredCityList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetUserfromDesignation(Int64 DesignationID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@DesignationID", DesignationID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserFromDesignation, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetEmailID(Int64 UserID, String accessToken)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@AccessToken", accessToken, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetEmailID, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataSet GetLatestNews()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetLatestNews, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 2)
                return ds;
            return null;
        }

        public static DataTable GetInvitationStatus(Int64 UserID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));

            DataSet ds = dataManager.Execute(StoreProcedure.GetInvitationStatus, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetActiveUsers()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetActiveUsers, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetActiveUsersByGroupID(long GroupID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@GroupID", GroupID, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetActiveUsersByGroupID, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return ds.Tables[0];
        }

        public static DataTable GetInactiveUsers()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetInactiveUsers, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable CheckMailAndCode(String Code, String Mail)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@AccessToken", Code, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@EmailID", Mail, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.CheckMailAndCode, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static Dictionary<string, string> GetAllAdmin()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            //parameters.Add(dataManager.CreateParam("@Role", role, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetAllAdmin, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list.Add(dr["Email_id"].ToString(), dr["First_Name"].ToString());
                }
                return list;
            }
            return null;
        }

        // World DB
        public static Boolean InsertCountries(Int64 countryId, String Name)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CountryID", countryId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Name", Name, ParameterDirection.Input));

            return dataManager.ExecuteNonQuery(StoreProcedure.InsertCountries, parameters.ToArray());
        }

        public static Boolean InsertStates(Int64 stateId, Int64 CountryID, String Name)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@StateID", stateId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CountryID", CountryID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Name", Name, ParameterDirection.Input));

            return dataManager.ExecuteNonQuery(StoreProcedure.InsertStates, parameters.ToArray());
        }

        public static Boolean InsertCity(Int64 cityId, String Name, Int64 stateId, Int64 CountryID)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@CityID", cityId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@City", Name, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@StateID", stateId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CountryID", CountryID, ParameterDirection.Input));

            return dataManager.ExecuteNonQuery(StoreProcedure.InsertCity, parameters.ToArray());
        }

        public static DataTable GetUserByMailId(string emailId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@email", emailId, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserByMailId, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetUserForChat(int role)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@Role", role, ParameterDirection.Input));
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserForChat, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetGroupList()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetGroupList, parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetCoursesForChatByUserId(long userId)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@userId", Value = userId });
            DataSet ds = dataManager.Execute("GetCoursesForChatByUserId", parameters.ToArray());

            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }

        public static bool UpdateTrainerName(long userId, long courseId, string trainerName)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@userId", Value = userId });
            parameters.Add(new SqlParameter { ParameterName = "@CourseId", Value = courseId });
            parameters.Add(new SqlParameter { ParameterName = "@TrainerName", Value = trainerName });

            bool result = dataManager.ExecuteNonQuery("UpdateTrainerName", parameters.ToArray());
            return result;
        }



        public static DataTable GetGroupDetails(string fromDate, string endDate)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@fromDate", Value = fromDate });
            parameters.Add(new SqlParameter { ParameterName = "@endDate", Value = endDate });
            DataSet ds = dataManager.Execute(StoreProcedure.GetGroupDetails, parameters.ToArray());

            if (ds != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;
        }

        public static DataTable GetUserListGroupWise(string userType, string groupName, bool? isFromAssign)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter { ParameterName = "@UserType", Value = userType });
            parameters.Add(new SqlParameter { ParameterName = "@GroupName", Value = groupName });
            parameters.Add(new SqlParameter { ParameterName = "@IsFromAssign", Value = isFromAssign });
            DataSet ds = dataManager.Execute(StoreProcedure.GetUserListGroupWise, parameters.ToArray());
            if (ds != null && ds.Tables.Count == 1)
                return ds.Tables[0];
            return null;
        }
        #endregion


        #region Public methods
        public bool Activate()
        {
            //Send mail

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EMailID", EMailID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@AccessToken", AccessToken, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", 0, ParameterDirection.InputOutput));

            if (dataManager.ExecuteNonQuery(StoreProcedure.ActivateUser, parameters.ToArray()))
            {
                return ((int)parameters[2].Value == 1);
            }

            return false;
        }

        public bool ChangeRole(UserRole newRole)
        {
            Role = newRole;
            return UpdateUser();
        }
        public bool ChangeStatus()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Active", Active, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Role", Role, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.ChangeUserStatus, parameters.ToArray());
        }

        public Boolean UpdateUser()
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@EMailID", EMailID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@FirstName", FirstName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@LastName", LastName, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Title", Title, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Designation", Designation, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Address", Address, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Country", Country, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@State", State, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@City", City, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Username", Username, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Password", Password, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Role", 2, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", 0, ParameterDirection.InputOutput));
            bool bRet = dataManager.ExecuteNonQuery(StoreProcedure.CreateUser, parameters.ToArray());
            try
            {
                if (String.IsNullOrEmpty(parameters[12].Value.ToString()))
                    UserID = 0;
                else
                    UserID = Convert.ToInt64(parameters[12].Value.ToString());
            }
            catch (Exception e) { }
            return bRet;
        }



        #endregion

    }


    public class UserOutput
    {
        public UInt64 UserID = 0;
        public String Email = String.Empty;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
    }

    public class StatusOutput
    {
        public bool status = false;
    }
    public class ContentDetails
    {
        public UInt64 ContentID = 0;
        public String Message = String.Empty;
        public String DateModified = String.Empty;

        public ContentDetails()
        { }

        public ContentDetails(UInt64 contentID, String message, String dateModified)
        {
            this.ContentID = contentID;
            this.Message = message;
            this.DateModified = dateModified;
        }
    }

    public class CourseDetail
    {
        public UInt64 CourseID = 0;
        public String CourseName = String.Empty;
        public String Description = String.Empty;
        public String Status = String.Empty;
        public List<SubCourseDetail> SubCourses = new List<SubCourseDetail>();

    }

    public class SubCourseDetail
    {
        public UInt64 SubCourseID = 0;
        public String SubCourseName = String.Empty;
        public String Description = String.Empty;
        public String SubCourseType;
        public String Status;
        public String Percentage = String.Empty;
        public String Score = String.Empty;
        public String SeatTime = String.Empty;

        public SubCourseDetail()
        { }
        public SubCourseDetail(UInt64 subCourseID, String subCourseName, String subCourseType, String status, String percentage, String score, String description, String seatTime)
        {
            this.SubCourseID = subCourseID;
            this.SubCourseName = subCourseName;
            this.SubCourseType = subCourseType;
            this.Status = status;
            this.Percentage = percentage;
            this.Score = score;
            this.Description = description;
            this.SeatTime = seatTime;
        }

    }

}
