using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.Reflection;
using System.Linq;
namespace Medtrix.Trace
{
    public class Logger
    {
        private static string LogFile;
        private static string LogJs;
        static Logger()
        {
            //LogFile = "c:\\Database\\" +  System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".log";
            LogFile = System.Configuration.ConfigurationManager.AppSettings["LogFile"];
            LogJs = System.Configuration.ConfigurationManager.AppSettings["LogJs"];
        }

        public static void Log(string msg)
        {
            try
            {
                System.IO.File.AppendAllText(LogFile, DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss.fff") + " : " + msg + Environment.NewLine);
            }
            catch (Exception)
            {
            }
        }
        public static void LogDetails(string msg)
        {
            try
            {
                System.IO.File.AppendAllText(LogJs, DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss.fff") + " : " + msg + Environment.NewLine);
            }
            catch (Exception)
            {
            }
        }

    }
}
namespace Medtrix.DataAccessControl
{
    public enum ExecutionStatus
    {
        ExecuteSuccess = 0,
        ExecuteFailed = 1
    }
    public class DataAccessManager
    {
        private String _ConnectionString = String.Empty;
        private static DataAccessManager _dataConn = null;
        private static object _lock = new object();
        private DataAccessManager(String ConnectionString)
        {
            _ConnectionString = ConnectionString;

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
            }
        }

        public static DataAccessManager GetInstance(String ConnectionString)
        {
            lock (_lock)
            {
                if (_dataConn == null)
                {
                    _dataConn = new DataAccessManager(ConnectionString);
                }
            }
            return _dataConn;
        }

        public static DataAccessManager GetInstance()
        {
            lock (_lock)
            {
                if (_dataConn == null)
                {
                    _dataConn = new DataAccessManager(System.Configuration.ConfigurationManager.ConnectionStrings["AppDB"].ConnectionString);
                }
            }
            return _dataConn;
        }

        public SqlParameter CreateParam(String Name, Object value, ParameterDirection pd)
        {
            SqlParameter param = new SqlParameter(Name, value);
            param.Direction = pd;
            if (value is UInt64)
                param.SqlDbType = SqlDbType.BigInt;
            if (value is String && pd == ParameterDirection.Output)
                param.Size = -1;
            return param;
        }

        public DataSet Execute(String StoreProcedure, SqlParameter[] parameters)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(_ConnectionString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand(StoreProcedure, con);
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter param in parameters)
                        command.Parameters.Add(param);

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet retSet = new DataSet();
                    sda.Fill(retSet);

                    return retSet;
                }
            }
            catch (Exception e)
            {
                Trace.Logger.Log("Procedure failed  : " + StoreProcedure);
                Trace.Logger.Log("Failure message   : " + e.Message);
                Trace.Logger.Log("Execute non query : " + e.StackTrace);
                return null;
            }
        }

        public bool ExecuteNonQuery(String StoreProcedure, SqlParameter[] parameters)
        {
            //SqlTransaction transaction = new SqlTransaction();
            try
            {
                using (SqlConnection con = new SqlConnection(_ConnectionString))
                {

                    con.Open();
                    //transaction = con.BeginTransaction();
                    SqlCommand command = new SqlCommand(StoreProcedure, con);
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter param in parameters)
                        command.Parameters.Add(param);

                    command.ExecuteNonQuery();
                    //transaction.Commit();
                    return true;
                }
            }
            catch (Exception e)
            {
                //transaction.Rollback();
                Trace.Logger.Log("Procedure failed  : " + StoreProcedure);
                Trace.Logger.Log("Failure message   : " + e.Message);
                Trace.Logger.Log("Execute non query : " + e.StackTrace);
                return false;
            }
        }

        public DataSet GetDataSet(string Query)
        {
            DataSet ds = null;

            try
            {

                using (SqlConnection myCon = new SqlConnection(_ConnectionString))
                {
                    string query = string.Format(Query);

                    using (SqlDataAdapter da = new SqlDataAdapter(query, _ConnectionString))
                    {
                        ds = new DataSet();
                        da.Fill(ds);
                    }
                }

            }
            catch (Exception e)
            {

                Trace.Logger.Log("Execution Failed failed  : " + "GetDataSet");
                Trace.Logger.Log("Failure message   : " + e.Message);
                Trace.Logger.Log("Execute non query : " + e.StackTrace);
            }
            return ds;
        }

        public string SingleColumnValue(string Query)
        {
            string singleValueData = "";
            using (SqlConnection myCon = new SqlConnection(_ConnectionString))
            {
                SqlCommand command = new SqlCommand(Query, myCon);
                myCon.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        singleValueData = reader.GetString(0);
                    }
                }
                else
                {
                    singleValueData = "-1";
                }
                reader.Close();
            }
            return singleValueData;
        }
        public int InsertSQL(string Query)
        {
            int Status = 0;
            try
            {

                using (SqlConnection myCon = new SqlConnection(_ConnectionString))
                {
                    string query = string.Format(Query);

                    using (SqlCommand mycmd = new SqlCommand(query, myCon))
                    {
                        myCon.Open();
                        Status = mycmd.ExecuteNonQuery();
                        myCon.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Status = 0;
                throw ex;
            }
            return Status;
        }
        public DataTable GetRecordsBySql(string sql)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = null;
            SqlDataAdapter da = null;
            SqlConnection myCon = new SqlConnection(_ConnectionString);
            try
            {
                myCon.Open();
                cmd = new SqlCommand(sql, myCon);
                da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cmd.Dispose();
                da.Dispose();
            }
            catch (Exception ex)
            {
                Trace.Logger.Log("Execution Failed failed  : " + "GetDataSet");
                Trace.Logger.Log("Failure message   : " + ex.Message);
                Trace.Logger.Log("Execute non query : " + ex.StackTrace);
            }
            finally
            {
                da = null;
                cmd = null;
                myCon.Close();
            }
            return dt;
        }
        public static object DataTableToJSON(DataTable dt)
        {
            if (dt == null)
                return null;

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows;
        }

        public static List<T> ToListof<T>(DataTable dt)
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
            var columnNames = dt.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName)
                .ToList();
            var objectProperties = typeof(T).GetProperties(flags);
            var targetList = dt.AsEnumerable().Select(dataRow =>
            {
                var instanceOfT = Activator.CreateInstance<T>();

                foreach (var properties in objectProperties.Where(properties => columnNames.Contains(properties.Name) && dataRow[properties.Name] != DBNull.Value))
                {
                    properties.SetValue(instanceOfT, dataRow[properties.Name], null);
                }
                return instanceOfT;
            }).ToList();

            return targetList;
        }

        public bool ExecuteAuditTrailDetails(string Description,string Activity,int Create_By)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(CreateParam("@Description", Description, ParameterDirection.Input));
            parameters.Add(CreateParam("@Activity", Activity, ParameterDirection.Input));
            parameters.Add(CreateParam("@Create_By", Create_By, ParameterDirection.Input));
            return ExecuteNonQuery("SP_LMS_AuditTrailDetails", parameters.ToArray());
        }
    }
}
