using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml.Serialization;
using Medtrix.DataAccessControl;
using System.Reflection;

namespace LearningManagementSystem.Data
{
    internal class StoreProcedure
    {
        public static readonly string AssignResourcetoCourse = "SP_LMS_ASSIGN_RESOURCE_TO_COURSE";
        public static readonly string UnAssignResourcetoCourse = "SP_LMS_UNASSIGN_RESOURCE_TO_COURSE";
        public static readonly string GetAssignResource = "SP_LMS_GET_Assign_Resources_LIST";
        public static readonly string GetUnAssignedCourseUserList = "SP_LMS_GET_NOT_ASSIGNED_USER_LIST";
        public static readonly string GetUnAssignedCourseGroupList = "SP_LMS_GET_NOT_ASSIGNED_GROUP_LIST";
        public static readonly string CreateUser = "SP_LMS_CREATE_USER";
        public static readonly string AuthenticateUser = "SP_LMS_AUTHENTICATE_USER";
        public static readonly string InviteSent = "SP_LMS_INVITE_SENT";
        public static readonly string ActivateUser = "SP_LMS_ACTIVATE_USER";
        public static readonly string GetUserList = "SP_LMS_GET_USER_LIST";
        public static readonly string GetUserListTypeWise = "SP_LMS_GET_USER_LIST_TYPE_WISE";
        public static readonly string GetUserInfo = "SP_LMS_GET_USER_INFO";
        public static readonly string GetGroupDetails = "SP_LMS_GET_GROUP_DETAILS";
        public static readonly string GetUserListGroupWise = "SP_LMS_GET_USER_LIST_GROUP_WISE";
        public static readonly string CreateCourse = "SP_LMS_CREATE_COURSE";
        public static readonly string CreateSubCourse = "SP_LMS_CREATE_SUB_COURSE";
        public static readonly string GetCourses = "SP_LMS_GET_COURSE_LIST";
        public static readonly string GetActiveUsersByGroupID = "GetActiveUsersByGroupID";
        public static readonly string GetCoursesByGroupID = "GetCourseListByGroupId";
        public static readonly string ChangeCourseStatus = "SP_LMS_CHANGE_COURSE_STATUS";
        public static readonly string ChangeSubCourseStatus = "SP_LMS_CHANGE_SUB_COURSE_STATUS";
        public static readonly string CreateContent = "SP_LMS_CREATE_CONTENT";
        public static readonly string GetContentList = "SP_LMS_GET_CONTENT_LIST";
        public static readonly string CreateSupportRequest = "SP_LMS_CREATE_SUPPORT_REQUEST";
        public static readonly string GetSubCourseList = "SP_LMS_GET_SUB_COURSE";
        public static readonly string StoreCourseContent = "SP_LMS_UPLOAD_SUB_COURSE_CONTENT";
        public static readonly string GetCourseContent = "SP_LMS_GET_SUB_COURSE_CONTENT";
        public static readonly string AssignCoursetoUser = "SP_LMS_ASSIGN_COURSE_TO_USER";
        public static readonly string DeleteCourse = "SP_LMS_DELETE_COURSE";
        public static readonly string ChangeUserStatus = "SP_LMS_CHANGE_USER_STATUS";
        public static readonly string GetDesignationList = "SP_LMS_GET_DESIGNATION_LIST";
        public static readonly string GetUserFromDesignation = "SP_LMS_GET_USERS_DESIGNATION";
        public static readonly string GetIssueList = "SP_LMS_GET_ISSUE_LIST";
        public static readonly string GetEmailID = "SP_LMS_GET_USER_MAIL_ID";
        public static readonly string GetCountryList = "SP_LMS_GET_COUNTRY_LIST";
        
        public static readonly string GetScores = "SP_LMS_GET_SCORES";
        public static readonly string GetLearning = "SP_LMS_GET_LEARNING";
        public static readonly string GetLatestNews = "SP_LMS_GET_NEWS";
        public static readonly string GetAdmin = "SP_LMS_GET_ADMIN_USER";
        public static readonly string IsEmailExist = "SP_LMS_IS_EMAIL_EXIST";
        public static readonly string IsGroupNameExist = "SP_LMS_IS_GroupName_EXIST";
        public static readonly string IsUsernameExist = "SP_LMS_IS_USERNAME_EXIST";
        public static readonly string SetPassword = "SP_LMS_SET_PASSWORD";
        public static readonly string ForgotPassword = "SP_LMS_FORGOT_PASSWORD";
        public static readonly string GetInvitationStatus = "SP_LMS_GET_INVITATION_STATUS";
        public static readonly string MapCoursetoUser = "SP_LMS_MAP_COURSE_TO_USER";
        public static readonly string GetRequestId = "SP_LMS_GET_SUPPORT_REQUEST_ID";
        public static readonly string SetSupportStatus = "SP_LMS_SET_SUPPORT_STATUS";
        public static readonly string GetCityofCountry = "SP_LMS_GET_COUNTRY_CITY_LIST";
        public static readonly string GetDefaultCity = "SP_LMS_GET_DEFAULT_CITY";
        public static readonly string GetCourseStatusReport = "SP_LMS_RPT_COURSE_STATUS";
        public static readonly string GetNewRegistrationReport = "SP_LMS_RPT_NEW_REGISTRATIONS";
        public static readonly string GetUserDistributionByCountryReport = "SP_LMS_RPT_USER_DISTRIBUTION_BY_COUNTRY";
        public static readonly string GetUserDistributionByCityReport = "SP_LMS_RPT_USER_DISTRIBUTION_BY_CITY";
        public static readonly string GetUserDistributionBySalesRep = "SP_LMS_RPT_USER_DISTRIBUTION_BY_SALES_REP";
        public static readonly string GetUserCourseStatusReport = "SP_LMS_RPT_COURSE_STATUS_BY_COUNTRY_CITY";
        public static readonly string GetAssignedCourse = "SP_LMS_GET_ASSIGNED_COURSE";
        public static readonly string GetActiveUsers = "SP_LMS_GET_ACTIVE_USERS";
        public static readonly string AddQuizResult = "SP_LMS_ADD_QUIZ_RESULT";
        public static readonly string SetCourseStatus = "SP_LMS_SET_COURSE_STATUS";
        public static readonly string ChangePostingStatus = "SP_LMS_CHANGE_CONTENT_STATUS";
        public static readonly string UpdateContent = "SP_LMS_UPDATE_CONTENT";
        public static readonly string UpdateContentImage = "SP_LMS_UPDATE_CONTENT_IMAGE";
        public static readonly string GetUserSubCourse = "SP_LMS_GET_USER_SUB_COURSE";
        public static readonly string GetBestScorerInCountry = "SP_LMS_RPT_BEST_SCORER_BY_COUNTRY";
        public static readonly string GetBestScorerInCity = "SP_LMS_RPT_BEST_SCORER_BY_CITY";
        public static readonly string GetCourseScoreByCountry = "SP_LMS_RPT_COURSE_SCORE_BY_COUNTRY";
        public static readonly string GetCourseScoreByCity = "SP_LMS_RPT_COURSE_SCORE_BY_CITY";
        public static readonly string GetCourseCompletedByLastMonth = "SP_LMS_RPT_COURSE_COMPLETED_BY_LAST_MONTH";
        public static readonly string AddSeatTime = "SP_LMS_ADD_SEAT_TIME";
        public static readonly string GetSeatTimeReportByCountry = "SP_LMS_RPT_SEAT_TIME_BY_COUNTRY";
        public static readonly string GetSeatTimeReportByCity = "SP_LMS_RPT_SEAT_TIME_BY_CITY";
        public static readonly string GetCountryStatesList = "SP_LMS_GET_COUNTRY_STATE_LIST";
        public static readonly string GetStateCityList = "SP_LMS_GET_STATE_CITY_LIST";
        public static readonly string GetSlideCount = "SP_LMS_GET_SLIDE_COUNTS";
        public static readonly string AddAttemptEntry = "SP_ADD_ATTEMPT_ENTRY";
        public static readonly string GetAttemptID = "SP_GET_ATTEMPT_ENTRY";
        public static readonly string GetUserTranscriptReport = "SP_LMS_RPT_USER_TRANSCRIPT";
        public static readonly string GetAggregateStatistcs = "SP_LMS_RPT_AGGREGATE_STATISTICS";
        public static readonly string GetSummaryReport = "SP_LMS_RPT_SUMMARY";
        public static readonly string GetDetailedReport = "SP_LMS_RPT_SUMMARY_DETAIL";
        public static readonly string GetUserLastSlide = "SP_LMS_GET_USER_LAST_SLIDE";
        public static readonly string CheckMailAndCode = "SP_LMS_CHECK_MAIL_AND_CODE";
        public static readonly string GetActiveCourses = "SP_LMS_GET_ACTIVE_COURSE_LIST";
        public static readonly string GetRegisteredCountryList = "SP_LMS_GET_REGISTERED_COUNTRY_LIST";
        public static readonly string GetRegisteredCityList = "SP_LMS_GET_REGISTERED_COUNTRY_CITY_LIST";
        public static readonly string GetResource = "SP_LMS_GET_RESOURCE_LIST";
        public static readonly string AddResource = "SP_LMS_ADD_RESOURCE";
        public static readonly string UpdateResource = "SP_LMS_UPDATE_RESOURCE";
        public static readonly string DeleteResource = "SP_LMS_DELETE_Resource";
        public static readonly string AssignResourcetoUser = "SP_LMS_ASSIGN_RESOURCE_TO_USER";
        public static readonly string GetUserForChat = "SP_LMS_GET_USER_FOR_CHAT";
        public static readonly string GetAllAdmin = "SP_LMS_GET_ALL_ADMIN";
        public static readonly string GetGroupList = "GetLMSGroupList";
        public static readonly string AddFalshCard = "SP_LMS_ADD_FlashCard";
        public static readonly string AddFalshCardQuestions = "SP_LMS_ADD_FlashCardQuestions";


        //to implement

        public static readonly string GetFlashCard = "SP_LMS_GET_FLASHCARD_LIST";
        public static readonly string GetFlashCardMergeQuestion = "SP_LMS_GET_SELECTEDFLASHCARDQUESTIONS";
        public static readonly string GetFlashCardQuestionCount = "SP_LMS_GET_FLASHCARD_QUESTION_COUNT";
        public static readonly string GetFlashCardQuestionComments = "SP_LMS_GET_FLASHCARDQUESTIONS_COMMENTS";
        public static readonly string UpdateFlashCardActive = "SP_LMS_UPDATE_FLASHCARD_ACTIVE_STATUS";
        public static readonly string UpdateFalshCardQuestion = "SP_LMS_UPDATE_FLASHCARDQUESTION";
        public static readonly string DeleteFlashCard = "SP_LMS_DELETE_FLASHCARD";
        public static readonly string UpdateFalshCard = "SP_LMS_UPDATE_FLASHCARD";
        public static readonly string GetFlashCardQuestions = "SP_LMS_GET_FLASHCARDQUESTIONS_LIST";






        public static readonly string UserSubCourseStatus = "Report_UserSubCourse";
        public static readonly string UserSubCourseSeatTime = "Report_UserSubCourse_SeatTime";
        public static readonly string UserSubCourseScore = "Report_UserSubCourse_Score";
        public static readonly string UserCourseStatus = "Report_UserCourseStatus";
        public static readonly string GetInactiveUsers = "Report_InactiveUsers";
        public static readonly string GetResourceNameByCourseId = "GetResourceByCourseId";

        // World DB Procedures

        public static readonly string InsertCountries = "SP_LMS_ADD_COUNTRIES";
        public static readonly string InsertStates = "SP_LMS_ADD_STATES";
        public static readonly string InsertCity = "SP_LMS_ADD_CITY";

        public static readonly string AssignCourseToUsers = "AssignCourseToUsers";
        public static readonly string GetLMSUsers = "GetLmsUsers";
        public static readonly string GetCourseDetailsById = "GetCourseDetailsById";
        public static readonly string GetContentsById = "GetContentsById";
        public static readonly string GetLMSCourseUsers = "GetLmsCourseUsers";
        public static readonly string SetCourseInvitationStatus = "SetInviteStatus";

        public static readonly string LearnerProgress = "LearnerProgress";
        public static readonly string LearnerActivity = "LearnerActivity";
        public static readonly string DepartmentProgress = "DepartmentProgress";
        public static readonly string CourseActivity = "CourseActivity";
        public static readonly string CourseAssessment = "CourseAssessment";
        public static readonly string CourseCompletionStatusReport = "CourseCompletionStatusReport";

        // Usage SP
        public static readonly string GetSupportRequests = "GetAllContactQuery";
        public static readonly string SetRequestStatus = "SetRequestStatus";
        public static readonly string GetUpcomingEvents = "GetAllEvents";
        public static readonly string SetEventStatus = "SetEventStatus";
        public static readonly string CreateEvent = "CreateEvent";
        public static readonly string GetEvent = "GetEventById";
        public static readonly string GetDashboard = "GetAllDashboard";
        public static readonly string GetCourseWiseReport = "GetCourseWiseReport";
        public static readonly string GetFilters = "GetFilters";
        public static readonly string GetUserWiseReport = "GetUserWiseReport";
        public static readonly string GetUserByMailId = "GetUserByMailId";
        public static readonly string DeleteEvent = "DeleteEvent";
        public static readonly string GetModuleReports = "GetModuleReports";
        public static readonly string GetQuickReport = "GetQuickReport";
        public static readonly string GetFlashCardQuestion = "SP_LMS_GET_FLASHCARDQUESTION_LIST";

    }
    public enum Status
    {
        Success = 0,
        Failed = 1
    }
    /*
    public class DataManager
    {
        protected DataAccessManager _dbManager = null;

        public DataManager()
        {
            _dbManager = DataAccessManager.GetInstance(System.Configuration.ConfigurationManager.AppSettings["AppDB"]);
        }
        #region User Functionality
        public DataSet CreateUser(User user)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(_dbManager.CreateParam("FirstName", user.FirstName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("LastName", user.LastName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("eMailID", user.Email, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Country", user.Country, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Password", user.Password, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("PhoneNumber", user.PhoneNumber, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Usertype", user.UserType, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SecurityQuestionID", user.SecurityQuestionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Answer", user.Answer, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserID", user.UserID, ParameterDirection.InputOutput));
            List<SqlParameter> UserParameters = new List<SqlParameter>();
            DataSet ds = _dbManager.Execute(StoreProcedure.CreateUser, parameters.ToArray());
            return ds;
        }

        public DataSet AuthenticateUser(String EMailID, String Password)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            int status = 1;
            parameters.Add(_dbManager.CreateParam("EMailID", EMailID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Password", Password, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("AuthenticationStatus", status, ParameterDirection.Output));

            DataSet ds = _dbManager.Execute(StoreProcedure.AuthenticateUser, parameters.ToArray());
            return ds;
        }

        public DataSet GetNotification(UInt64 NotificationID, UInt64 UserID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("NotificationID", NotificationID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserID", UserID, ParameterDirection.Input));
            DataSet ds = _dbManager.Execute(StoreProcedure.GetNotification, parameters.ToArray());
            return ds;
        }

        public bool IsMailExists(String EMailID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("eMailID", EMailID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.IsMailIDExists, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }


        public bool CreateDay(DayData dayData)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("DayName", dayData.DayName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Date", dayData.Date, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("DayID", dayData.DayID, ParameterDirection.InputOutput));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.CreateDay, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[3].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool DeleteDay(UInt64 DayID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("DayID", DayID, ParameterDirection.Input));

            return _dbManager.ExecuteNonQuery(StoreProcedure.DeleteDay, parameters.ToArray());
        }


        public bool DeleteSession(UInt64 SessionID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("SessionID", SessionID, ParameterDirection.Input));

            return _dbManager.ExecuteNonQuery(StoreProcedure.DeleteSession, parameters.ToArray());
        }

        public bool DeleteTopic(UInt64 TopicID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("TopicID", TopicID, ParameterDirection.Input));

            return _dbManager.ExecuteNonQuery(StoreProcedure.DeleteTopic, parameters.ToArray());
        }

        public bool CreateSession(SessionData sessionData)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("SessionName", sessionData.SessionName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("StartTime", sessionData.StartTime, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("EndTime", sessionData.EndTime, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("DayID", sessionData.DayID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("ModName", sessionData.ModeratorName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SessionID", sessionData.SessionID, ParameterDirection.InputOutput));
            parameters.Add(_dbManager.CreateParam("ModID", 0, ParameterDirection.Output));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.CreateSession, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[7].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool CreateTopic(TopicData topicData)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("TopicName", topicData.TopicName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("StartTime", topicData.StartTime, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("EndTime", topicData.EndTime, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SessionID", topicData.SessionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SpeakerName", topicData.SpeakerName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("TopicID", topicData.TopicID, ParameterDirection.InputOutput));
            parameters.Add(_dbManager.CreateParam("ModID", 0, ParameterDirection.Output));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.CreateTopic, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[7].SqlValue ?? "1").ToString()) == Status.Success);
        }


        public bool UploadSpeakerImage(UInt64 SpeakerID, Byte[] ImgBytes, String SpeakerDesc)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("SpeakerID", SpeakerID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SpeakerImage", ImgBytes, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SpeakerDescription", SpeakerDesc, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.UploadSpeakerImage, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public DataSet ForgotPassword(User user)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            user.UserID = 0;

            parameters.Add(_dbManager.CreateParam("eMailID", user.Email, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("ActivationCode", user.ActivationCode, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserID", user.UserID, ParameterDirection.Output));
          
            DataSet ds = _dbManager.Execute(StoreProcedure.ForgotPassword, parameters.ToArray());
            return ds;
        }

        public bool SubmitSecurityQuestion(User user)
        {
            
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("SecurityQuestionID", user.SecurityQuestionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Answer", user.Answer, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Email", user.Email, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SubmitSecurityQuestion, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[3].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool SubmitPoll(Poll poll)
        {

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("UserID", poll.UserID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("OptionID", poll.OptionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SubmitPoll, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[2].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool SetFeedbackOption(UInt64 FeedbackQuesID,String Option)
        {
            
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("FdbQuestionID",FeedbackQuesID , ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Option", Option, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetFeedbackOption, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[2].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool SetPassword(String Email, String Password)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("Password", Password, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Email", Email, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetPassword, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[2].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool DeleteFeedbackQuestion(UInt64 FdbQuestionID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("FdbQuestionID", FdbQuestionID, ParameterDirection.Input));

            return _dbManager.ExecuteNonQuery(StoreProcedure.DeleteFeedbackQuestion, parameters.ToArray());
           
        }

        

        public DataTable GetSecurityQuestionList()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetSecurityQuestionList, parameters.ToArray()).Tables[0];
        }

        public DataTable GetUserList()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetUserList, parameters.ToArray()).Tables[0];
        }

        public DataTable GetPollResult()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.PollResult, parameters.ToArray()).Tables[0];
        }


        public DataTable GetFeedbackQuestionID()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetFeedbackQuestionID, parameters.ToArray()).Tables[0];
        }

        public DataTable GetFeedbackQuestionList()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetFeedbackQuestion, parameters.ToArray()).Tables[0];
        }

        public DataTable GetSpeakerList()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetSpeakerList, parameters.ToArray()).Tables[0];
        }

        public bool CreateNotification(Notification Notifi)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("NotificationSub", Notifi.NotificationSub, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("NotificationMsg", Notifi.NotificationMsg, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("NotificationID", 0, ParameterDirection.Output));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.CreateNotification, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[3].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public DataTable GetTopicList()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetTopicList, parameters.ToArray()).Tables[0];
        }

        
        public DataTable GetTopicDetail(UInt64 TopicID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("TopicID", TopicID, ParameterDirection.Input));
            return _dbManager.Execute(StoreProcedure.GetTopicDetail, parameters.ToArray()).Tables[0];
        }

        public DataTable GetUserDetail(UInt64 UserID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("UserID", UserID, ParameterDirection.Input));
            return _dbManager.Execute(StoreProcedure.GetUserDetail, parameters.ToArray()).Tables[0];
        }

        public DataTable GetSpeakerDetail(UInt64 SpeakerID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("SpeakerID", SpeakerID, ParameterDirection.Input));
            return _dbManager.Execute(StoreProcedure.GetSpeakerPhoto, parameters.ToArray()).Tables[0];
        }

        public DataTable GetAllQuestion()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetAllQuestion, parameters.ToArray()).Tables[0];
        }

        public DataSet SubmitQuestion(Questions ques)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(_dbManager.CreateParam("UserID", ques.UserID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SessionID", ques.SessionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("SpeakerID", ques.SpeakerID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Question", ques.Question, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("QuestionID", ques.QuestionID, ParameterDirection.InputOutput));
            List<SqlParameter> UserParameters = new List<SqlParameter>();
            DataSet ds = _dbManager.Execute(StoreProcedure.SubmitQuestion, parameters.ToArray());
            return ds;
        }


        public DataSet SubmitFeedbackQuestion(CreateFeedback createFeedback)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(_dbManager.CreateParam("FdbQuestion", createFeedback.FdbQuestion, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("QuestionType", createFeedback.QuestionType, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("FdbQuestionID", createFeedback.FdbQuestionID, ParameterDirection.InputOutput));
            List<SqlParameter> UserParameters = new List<SqlParameter>();
            DataSet ds = _dbManager.Execute(StoreProcedure.SetFeedbackQuestion, parameters.ToArray());
            return ds;
        }


        public bool SetQuestionRead(UInt64 QuestionID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("QuestionID", QuestionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetQuestionRead, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool SetQuestionInActive(UInt64 QuestionID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("QuestionID", QuestionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetQuestionInActive, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[1].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool SetNotificationRead(UInt64 NotificationID, UInt64 UserID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("NotificationID", NotificationID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserID", UserID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetNotificationRead, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[2].SqlValue ?? "1").ToString()) == Status.Success);
        }


        public bool ClearPoll()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.ExecuteNonQuery(StoreProcedure.ClearPoll,parameters.ToArray());
        }

        public bool SetNotificationInActive(UInt64 NotificationID, UInt64 UserID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("NotificationID", NotificationID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserID", UserID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetNotificationInActive, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[2].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool SubmitFeedback(UInt64 UserID, UInt64 FdbQuestionID, String OptionID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("UserID", UserID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("FdbQuestionID", FdbQuestionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("OptionID", OptionID, ParameterDirection.Input));

            return  _dbManager.ExecuteNonQuery(StoreProcedure.SubmitFeedback, parameters.ToArray());
            
        }

        public DataTable GetConferenceList(UInt64 ConferenceID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("ConferenceID", ConferenceID, ParameterDirection.Input));

            return _dbManager.Execute(StoreProcedure.GetConferenceDetail, parameters.ToArray()).Tables[0];
        }

        public DataTable GetVenueList()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            return _dbManager.Execute(StoreProcedure.GetVenueList, parameters.ToArray()).Tables[0];
        }


        public DataTable GetFeedbackResult(UInt64 FdbQuestionID, Int32 UserType)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("FdbQuestionID", FdbQuestionID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserType", UserType, ParameterDirection.Input));
            return _dbManager.Execute(StoreProcedure.GetFeedbackResult, parameters.ToArray()).Tables[0];
        }

        public bool SetUSerType(UInt64 UserID, Int32 UserType)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("UserID", UserID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("UserType", UserType, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.SetUserType, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[2].SqlValue ?? "1").ToString()) == Status.Success);
        }

        public bool UploadPresentationDoc(UInt64 TopicID, Byte[] Doc, String PresentationName)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("TopicID", TopicID, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("PresentationDoc", Doc, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("PresentationName", PresentationName, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Status", 1, ParameterDirection.Output));

            _dbManager.Execute(StoreProcedure.UploadPresentationDoc, parameters.ToArray());
            return ((Status)Enum.Parse(typeof(Status), (parameters[3].SqlValue ?? "1").ToString()) == Status.Success);
        }

        #endregion

        public DataTable GetAgendaForDay(int DayID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("DayID", DayID, ParameterDirection.Input));

            return _dbManager.Execute(StoreProcedure.GetAgendaForDay, parameters.ToArray()).Tables[0];
        }
        public DataTable GetFacultyList(int FacultyType)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("FacultyType", FacultyType, ParameterDirection.Input));

            return _dbManager.Execute(StoreProcedure.GetFacultyList, parameters.ToArray()).Tables[0];
        }

        public bool SubmitMessage(ContactMsg Msg)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("Name", Msg.Name, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Email", Msg.Email, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("PhoneNumber", Msg.Phone, ParameterDirection.Input));
            parameters.Add(_dbManager.CreateParam("Message", Msg.Message, ParameterDirection.Input));

            _dbManager.Execute(StoreProcedure.SubmitMessage, parameters.ToArray());
            return true;
        }
        public int GetPollUserCount()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("Count", 0, ParameterDirection.Output));

            _dbManager.ExecuteNonQuery("SP_CONFERENCE_APP_GET_POLL_USER_COUNT", parameters.ToArray());
            return Int32.Parse((parameters[0].SqlValue ?? "0").ToString());
            
        }

        public DataTable GetSpeakerProfile(UInt64 topicID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("TopicID", topicID, ParameterDirection.Input));

            return _dbManager.Execute(StoreProcedure.GetSpeakerProfile, parameters.ToArray()).Tables[0];
        }

        public DataTable GetVideoFileName(String topicID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("TopicID", topicID, ParameterDirection.Input));

            return _dbManager.Execute(StoreProcedure.GetVideoFileName, parameters.ToArray()).Tables[0];
        }

        public DataTable GetAgendaForArchive(int DayID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(_dbManager.CreateParam("DayID", DayID, ParameterDirection.Input));

            return _dbManager.Execute(StoreProcedure.GetAgendaForArchive, parameters.ToArray()).Tables[0];
        }
    }*/
}