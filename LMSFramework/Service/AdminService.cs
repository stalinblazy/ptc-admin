﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Medtrix.WebServices;
using LearningManagementSystem.Components;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using Medtrix.DataAccessControl;
using LearningManagementSystem.Data;
using System.Data.SqlClient;

namespace LearningManagementSystem.WebServices
{
    internal enum Admin
    {
        SetEventStatus,
        UpdateEvent,
        GetDashboard,
        GetFilters,
        DeleteEvent,
        SetRequestStatus,
        GetQuickReport,
        SetDueDate
    }

    public class CourseDuedate
    {
        public Int32 CourseId = 0;
        public Int32 UserId = 0;
        public String DueDate = "";
        public Boolean DateNotRequired = false;
    }


    class AdminService : Service<WebScriptRequest<Object>>
    {
        protected override Object Execute(WebScriptRequest<Object> request, String data)
        {
            Admin command = (Admin)Enum.Parse(typeof(Admin), request.Command, true);
            switch (command)
            {
                case Admin.SetEventStatus:
                    return SetEventStatus(_json.Deserialize<WebScriptRequest<List<Events>>>(data).Data);
                case Admin.UpdateEvent:
                    return UpdateEvent(_json.Deserialize<WebScriptRequest<Events>>(data).Data);
                case Admin.GetDashboard:
                    return GetDashboard();
                case Admin.GetFilters:
                    return GetFilters();
                case Admin.DeleteEvent:
                    return DeleteEvents(_json.Deserialize<WebScriptRequest<List<Events>>>(data).Data);
                case Admin.SetRequestStatus:
                    return SetRequestStatus(_json.Deserialize<WebScriptRequest<List<SupportRequest>>>(data).Data);
                case Admin.GetQuickReport:
                    return GetQuickReport();
                case Admin.SetDueDate:
                    return SetDueDate(_json.Deserialize<WebScriptRequest<CourseDuedate>>(data).Data);
                    


            }
            return null;
        }

        public object SetDueDate(CourseDuedate cd)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(dataManager.CreateParam("@CourseId", cd.CourseId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserId", cd.UserId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@DueDate", !String.IsNullOrEmpty(cd.DueDate) ? cd.DueDate : null, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@DateNotRequired", cd.DateNotRequired, ParameterDirection.Input));

            return dataManager.ExecuteNonQuery("SetDueDate", parameters.ToArray());
            
        }

        private object SetRequestStatus(List<SupportRequest> data)
        {
            Boolean status = true;
            foreach (SupportRequest ev in data)
            {
                if (!SupportRequest.SetRequestStatus(ev))
                {
                    status = false;
                }
            }
            return status;
        }

        private object DeleteEvents(List<Events> events)
        {
            Boolean status = true;
            foreach (Events ev in events)
            {
                if (!Events.DeleteEvent(ev.ID,ev.AdminId))
                {
                    status = false;
                }
            }
            return status;
        }

        public object UpdateEvent(Events even)
        {
            return Events.UpdateEvent(even);
        }
        public Object GetDashboard()
        {
            Dictionary<String, Object> dashRes = new Dictionary<string, object>();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
           
            DataSet ds = dataManager.Execute(StoreProcedure.GetDashboard, parameters.ToArray());
            dashRes.Add("userreport", DataAccessManager.DataTableToJSON(ds.Tables[0]));
            dashRes.Add("mapreport", DataAccessManager.DataTableToJSON(ds.Tables[1]));
            dashRes.Add("modulereport", DataAccessManager.DataTableToJSON(ds.Tables[2]));
            dashRes.Add("notifications", DataAccessManager.DataTableToJSON(ds.Tables[3]));
            dashRes.Add("groups", DataAccessManager.DataTableToJSON(ds.Tables[4]));
            return dashRes;
        }

        public Object GetFilters()
        {
            Dictionary<String, Object> dashRes = new Dictionary<string, object>();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();

            DataSet ds = dataManager.Execute(StoreProcedure.GetFilters, parameters.ToArray());
            dashRes.Add("department", DataAccessManager.DataTableToJSON(ds.Tables[0]));
            dashRes.Add("course", DataAccessManager.DataTableToJSON(ds.Tables[1]));
            dashRes.Add("status", DataAccessManager.DataTableToJSON(ds.Tables[2]));
            dashRes.Add("groups", DataAccessManager.DataTableToJSON(ds.Tables[3]));
            dashRes.Add("countries", DataAccessManager.DataTableToJSON(ds.Tables[4]));
            dashRes.Add("region", DataAccessManager.DataTableToJSON(ds.Tables[5]));
            dashRes.Add("designation", DataAccessManager.DataTableToJSON(ds.Tables[6]));
            dashRes.Add("mreport", getModuleReports());
            
            return dashRes;
        }

        public static Object GetQuickReport()
        {
            Dictionary<String, Object> rep = new Dictionary<string, object>();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            DataSet ds = dataManager.Execute(StoreProcedure.GetQuickReport, parameters.ToArray());
            rep.Add("readreport", DataAccessManager.DataTableToJSON(ds.Tables[0]));
            rep.Add("modreport", DataAccessManager.DataTableToJSON(ds.Tables[1]));
            rep.Add("trackreport", DataAccessManager.DataTableToJSON(ds.Tables[2]));
            rep.Add("curriculumreport", DataAccessManager.DataTableToJSON(ds.Tables[2]));
            return rep;
        }

        public Object getModuleReports()
        {
            Dictionary<String, Object> dashRes = new Dictionary<string, object>();
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();

            DataSet ds = dataManager.Execute(StoreProcedure.GetModuleReports, parameters.ToArray());
            dashRes.Add("modgraph", DataAccessManager.DataTableToJSON(ds.Tables[0]));
            dashRes.Add("modquestions", DataAccessManager.DataTableToJSON(ds.Tables[1]));
            dashRes.Add("modqopts", DataAccessManager.DataTableToJSON(ds.Tables[2]));
            return dashRes;
        }

        public object SetEventStatus(List<Events> events)
        {
            Boolean status = true;
            foreach (Events ev in events)
            {
                if (!Events.SetEventStatus(ev))
                {
                    status = false;
                }
            }
            return status;
        }


    }
}
