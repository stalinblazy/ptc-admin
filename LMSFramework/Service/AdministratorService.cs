﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Medtrix.WebServices;
using LearningManagementSystem.Components;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace LearningManagementSystem.WebServices
{
    internal enum AdminCommand
    {
        ChangeUserSettings,
        ChangeCourseSettings,
        CreateContent,
        ChangePostingStatus,
        IsEmailExist,
        ChangeSupportStatus
    }


    class AdministratorService : Service<WebScriptRequest<Object>>
    {
        protected override Object Execute(WebScriptRequest<Object> request, String data)
        {
            AdminCommand command = (AdminCommand)Enum.Parse(typeof(AdminCommand), request.Command, true);
            switch (command)
            {
                case AdminCommand.ChangeUserSettings:
                    return ChangeUserSettings(_json.Deserialize<WebScriptRequest<List<User>>>(data).Data);
                case AdminCommand.ChangeCourseSettings:
                    return ChangeCourseSettings(_json.Deserialize<WebScriptRequest<List<Course>>>(data).Data);
                case AdminCommand.IsEmailExist:
                    return IsEmailExist(request.Data.ToString());

            }
            return null;
        }

        public object ChangeUserSettings(List<User> userSettings)
        {
            foreach (User user in userSettings)
            {
                User oUser = User.GetUser(user.UserID);
                oUser.Active = user.Active;
                oUser.Role = user.Role;
                oUser.UpdateUser();
                oUser.ChangeStatus();
            }
            return true;
        }


        public object ChangeCourseSettings(List<Course> courseSettings)
        {
            foreach (Course course in courseSettings)
            {
                Course newCourse = new Course();
                newCourse.CourseID = course.CourseID;
                newCourse.ChangeStatus(course.Active);
            }
            return true;
        }

        private object IsEmailExist(String Mail)
        {
            return User.IsMailExists(Mail);
        }
    }
}
