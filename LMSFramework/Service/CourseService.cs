﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Medtrix.WebServices;
using LearningManagementSystem.Components;

namespace LearningManagementSystem.Service
{
    internal enum CourseCommand
    {
        AuthenticateUser,
        GetCourseList,
        TrackActivity,
        TrackActivityList,
        SetCourseStatus
    }

    public class DownloadStatus
    {
        public UInt32 CourseID = 0;
        public String Status = "";
    }
    class Activity
    {
        public String UserID = string.Empty;
        public String CourseID = string.Empty;
        public String TimeStamp = string.Empty;
        public String SubCourseID = string.Empty;
        public String TrackID = string.Empty;
        public String TrackValue = string.Empty;
    }
    class CourseService : Service<WebScriptRequest<Object>>
    {
        protected override Object Execute(WebScriptRequest<Object> request, String data)
        {
            CourseCommand command = (CourseCommand)Enum.Parse(typeof(CourseCommand), request.Command, true);
            switch (command)
            {
                case CourseCommand.AuthenticateUser:
                    return Authenticate(_json.Deserialize<WebScriptRequest<User>>(data).Data);
                case CourseCommand.GetCourseList:
                    return GetUserCourses(_json.Deserialize<WebScriptRequest<User>>(data).Data);
                case CourseCommand.TrackActivity:
                    return TrackActivity(_json.Deserialize<WebScriptRequest<Activity>>(data).Data);
                case CourseCommand.TrackActivityList:
                    return TrackActivityList(_json.Deserialize<WebScriptRequest<List<Activity>>>(data).Data);
            }
            return null;
        }

        public object Authenticate(User user)
        {
            User authUser = User.Authenticate(user.Username, user.Password);
            authUser.Password = "";
            return authUser;           
        }

        public object GetUserCourses(User user)
        {
            return null;
        }
        public object TrackActivity(Activity activity)
        {
            return null;
        }
        public object TrackActivityList(List<Activity> ActivityList)
        {
            return null;
        }
    }
}
