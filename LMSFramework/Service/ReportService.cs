﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Medtrix.WebServices;
using Medtrix.DataAccessControl;
using LearningManagementSystem.Components;
using System.Data;

namespace LearningManagementSystem.WebServices
{
    internal enum ReportsCommand
    {
        NewRegistration,
        UserDistributionByCountry,
        UserDistributionBySalesRep,
        GetUserSubCourseStatus,
        GetUserSubCourseSeatTime,
        GetUserSubCourseScore,
        GetUserCourseStatus
    }


    class ReportService : Service<WebScriptRequest<Object>>
    {
        protected override Object Execute(WebScriptRequest<Object> request, String data)
        {
            ReportsCommand command = (ReportsCommand)Enum.Parse(typeof(ReportsCommand), request.Command, true);
            switch (command)
            {
                case ReportsCommand.NewRegistration:
                    return GetNewRegistrationReport(Convert.ToInt64(request.Data.ToString()));
                case ReportsCommand.UserDistributionByCountry:
                    return GetUserDistributionByCountry();
                case ReportsCommand.UserDistributionBySalesRep:
                    return GetUserDistributionBySalesRep();
                case ReportsCommand.GetUserSubCourseStatus:
                    return GetUserSubCourseStatus(_json.Deserialize<WebScriptRequest<Reports>>(data).Data);
                case ReportsCommand.GetUserSubCourseSeatTime:
                    return GetUserSubCourseSeatTime(_json.Deserialize<WebScriptRequest<Reports>>(data).Data);
                case ReportsCommand.GetUserSubCourseScore:
                    return GetUserSubCourseScore(_json.Deserialize<WebScriptRequest<Reports>>(data).Data);
                case ReportsCommand.GetUserCourseStatus:
                    return GetUserCourseStatus();
            }
            return null;
        }

        public object GetNewRegistrationReport(Int64 year)
        {
            DataSet ds = Reports.GetRegistrationReport(year);
            return DataAccessManager.DataTableToJSON(ds.Tables[0]);
        }

        public object GetUserDistributionByCountry()
        {
            return DataAccessManager.DataTableToJSON(Reports.GetUserDistributionByCountry());
        }

        private object GetUserDistributionBySalesRep()
        {
            return DataAccessManager.DataTableToJSON(Reports.GetUserDistributionBySalesRep());
        }

        private object GetUserSubCourseStatus(Reports rpt)
        {
            return DataAccessManager.DataTableToJSON(Reports.GetUserSubCourseStatus(rpt.SubCourseStatus, rpt.SubCourseType));
        }

        private object GetUserSubCourseSeatTime(Reports rpt)
        {
            return DataAccessManager.DataTableToJSON(Reports.GetUserSubCourseSeatTime(rpt.SubCourseType));
        }

        private object GetUserSubCourseScore(Reports rpt)
        {
            return DataAccessManager.DataTableToJSON(Reports.GetUserSubCourseScore(rpt.SubCourseType));
        }

        private object GetUserCourseStatus()
        {
            return DataAccessManager.DataTableToJSON(Reports.GetUserCourseStatus());
        }
    }
}
