﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Medtrix.MailService
{
    public class Mail
    {
        public Dictionary<string, string> to = new Dictionary<string, string>();
        public string subject = "How are you?";
        public List<string> from = new List<string>();
        public string html = "<b>Hello world e-mail!!!</b>";
        public Dictionary<string, string> bcc = new Dictionary<string, string>();
        public Dictionary<string, string> cc = new Dictionary<string, string>();
    }

    public class SendInBlue
    {
        class Response
        {
            public string code = string.Empty;
        }

        public static bool SendEMail(String To, String From, String Subject, String htmlBody, String document, bool isBccEnabled)
        {
            Boolean enableEmail = ConfigurationManager.AppSettings["enableEmail"].ToString() == "true" ? true : false;

            Medtrix.Trace.Logger.Log("Send email to:" + To + " Subject" + Subject);
            if(!enableEmail)
            {
                return true;
            }
            Mail m = new Mail();


            m.to[To] = To;
            m.from.Add(From);
            m.from.Add("support@ptcdegree.com");
            m.from.Add("PTC Admin");
            m.subject = Subject;
            m.html = htmlBody;

            if(isBccEnabled)
            {
                m.cc[ConfigurationManager.AppSettings["adminMail"]] = ConfigurationManager.AppSettings["adminMail"];
            }
            
            JavaScriptSerializer jserializer = new JavaScriptSerializer();

            string json = jserializer.Serialize(m);

            string base_url = "https://api.sendinblue.com/v2.0/";
            Stream stream = new MemoryStream();
            string url = base_url + "email";
            string content_type = "application/json";

            // Create request
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = content_type;
            request.Headers.Add("api-key", "M7BjyzQUEpFDg9rw");

            using (System.IO.Stream s = request.GetRequestStream())
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                    sw.Write(json);
            }

            try
            {

                HttpWebResponse response;
                response = request.GetResponse() as HttpWebResponse;
                // read the response stream and put it into a byte array
                stream = response.GetResponseStream() as Stream;
            }
            catch (System.Net.WebException ex)
            {
                //read the response stream If status code is other than 200 and put it into a byte array
                stream = ex.Response.GetResponseStream() as Stream;
            }

            byte[] buffer = new byte[32 * 1024];
            int nRead = 0;
            MemoryStream ms = new MemoryStream();
            do
            {
                nRead = stream.Read(buffer, 0, buffer.Length);
                ms.Write(buffer, 0, nRead);
            } while (nRead > 0);
            // convert read bytes into string
            ASCIIEncoding encoding = new ASCIIEncoding();
            String responseString = encoding.GetString(ms.ToArray());

            Medtrix.Trace.Logger.Log("SendInBlue Mail Status (Admin): " + responseString);

            Response res = jserializer.Deserialize<Response>(responseString);
            return res.code.ToLower() == "success";
        }

        public static bool SendEMail(String To, String From, String Subject, String htmlBody, String document, bool sendCopyAdmin, List<String> bcc)
        {
            Boolean enableEmail = ConfigurationManager.AppSettings["enableEmail"].ToString() == "true" ? true : false;

            Medtrix.Trace.Logger.Log("Send email to:" + To + " Subject" + Subject);
            if (!enableEmail)
            {
                return true;
            }
            Mail m = new Mail();


            m.to[To] = To;
            m.from.Add(From);
            m.from.Add("support@ptcdegree.com");
            m.from.Add("PTC Admin");
            m.subject = Subject;
            m.html = htmlBody;
           

            if (sendCopyAdmin)
            {
                m.cc[ConfigurationManager.AppSettings["adminMail"]] = ConfigurationManager.AppSettings["adminMail"];
            }
            if(bcc != null && bcc.Count > 0)
            {
                foreach (String email in bcc)
                {
                    m.bcc[email] = email;
                }
            }

            JavaScriptSerializer jserializer = new JavaScriptSerializer();

            string json = jserializer.Serialize(m);

            string base_url = "https://api.sendinblue.com/v2.0/";
            Stream stream = new MemoryStream();
            string url = base_url + "email";
            string content_type = "application/json";

            // Create request
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = content_type;
            request.Headers.Add("api-key", "M7BjyzQUEpFDg9rw");

            using (System.IO.Stream s = request.GetRequestStream())
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                    sw.Write(json);
            }

            try
            {

                HttpWebResponse response;
                response = request.GetResponse() as HttpWebResponse;
                // read the response stream and put it into a byte array
                stream = response.GetResponseStream() as Stream;
            }
            catch (System.Net.WebException ex)
            {
                //read the response stream If status code is other than 200 and put it into a byte array
                stream = ex.Response.GetResponseStream() as Stream;
            }

            byte[] buffer = new byte[32 * 1024];
            int nRead = 0;
            MemoryStream ms = new MemoryStream();
            do
            {
                nRead = stream.Read(buffer, 0, buffer.Length);
                ms.Write(buffer, 0, nRead);
            } while (nRead > 0);
            // convert read bytes into string
            ASCIIEncoding encoding = new ASCIIEncoding();
            String responseString = encoding.GetString(ms.ToArray());

            Medtrix.Trace.Logger.Log("SendInBlue Mail Status (Admin): " + responseString);

            Response res = jserializer.Deserialize<Response>(responseString);
            return res.code.ToLower() == "success";
        }

    }
}
