using System;
using System.Collections.Generic;
using System.Text;

using Medtrix.WebServices;
using LearningManagementSystem.WebServices;

namespace LearningManagementSystem.WebServices
{
    public enum ServiceType
    {
        Administrator,
        Admin,
        User,
        Reports,
        MobileApp,
        Tracker
    }
    public class ServiceFactory : ServiceController<ServiceType>
    {
        public static new IService GetService(ServiceType serviceType)
        {
            IService service = null;
            switch (serviceType)
            {
                case ServiceType.Administrator:
                    service = new AdministratorService();
                    break;
                case ServiceType.Admin:
                    service = new AdminService();
                    break;
                case ServiceType.User:
                    service = new UserService();
                    break;
                case ServiceType.Reports:
                    service = new ReportService();
                    break;
                case ServiceType.Tracker:
                    service = new TrackerService();
                    break;
            }
            return service;
        }

    }
}
