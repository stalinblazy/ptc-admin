using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Script.Serialization;
using Medtrix.Trace;

namespace Medtrix.WebServices
{
    internal class ResponseHeader
    {
        public Boolean isError = false;
        public String ErrorMessage = String.Empty;
        public Object Result = new object();
        public int ErrorCode = 0;

    }
    internal class WebScriptRequest<T> where T : new()
    {
        public String Command = String.Empty;
        public T Data = new T();
    }
    public interface IService
    {
        String DoService(String data, System.Web.UI.Page ExecutingPage);
    }
    public abstract class Service<T> : IService
    {
        protected JavaScriptSerializer _json = new JavaScriptSerializer();

        protected System.Web.UI.Page _ExecutingPage = null;
        public String DoService(String data, System.Web.UI.Page ExecutingPage)
        {
            _ExecutingPage = ExecutingPage;
            T request = default(T);
            ResponseHeader response = new ResponseHeader();
            try
            {
                Logger.Log(data);
                 _json.MaxJsonLength = 2147483644;
                // convert the request packet into generic structure
                request = _json.Deserialize<T>(data);

            }
            catch (Exception)
            {
                if (_json == null)
                    return "Failed to load json parser.";

                response.isError = true;
                response.ErrorMessage = "Invalid data format.";
                response.ErrorCode = 999;
            }
            try
            {
                if (request == null)
                    throw new Exception("Invalid request");
                response.Result = Execute(request, data);
            }
            catch (Exception ex)
            {
                response.isError = true;
                response.ErrorMessage = Convert.ToString((ex.Data["ErrorMessage"] ?? ex.Message.ToString()));
                response.ErrorCode = Convert.ToInt32((ex.Data["ErrorCode"] ?? (int)999).ToString());
                response.Result = ex.Data["result"] ?? new Object();
            }

            Logger.Log(_json.Serialize(response));
            return _json.Serialize(response);

        }

        protected abstract Object Execute(T request, String rawData);
    }

    public class CustomException : Exception
    {
        public UInt64 ErrorCode = 0;
        public String ErrorMessage = String.Empty;

        public CustomException(UInt64 ErrorCode, String ErrorMessage)
        {
            Exception ex = new Exception();
            ex.Data["ErrorCode"] = ErrorCode;
            ex.Data["ErrorMessage"] = ErrorMessage;
            throw ex;
        }
    }

    public abstract class ServiceController<T>
    {
        public static IService GetService(T serviceType) { return null; }

    }

}
