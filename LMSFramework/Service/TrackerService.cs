﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using Medtrix.WebServices;
using Medtrix.DataAccessControl;
using LearningManagementSystem.Components;
using LearningManagementSystem.Data;

namespace LearningManagementSystem.WebServices
{
    public class Tracker
    {
        public UInt64 UserID = 0;
        public UInt64 SubCourseID = 0;
        public DateTime TimeStamp = DateTime.UtcNow;
        
    }

    public class QuizTracker : Tracker
    {
        public UInt64 QuizID = 0;
        public float Percentage = 0;
        public float AnswerRating = 0.0f;
        public float Total = 0.0f;
    }

    public class CourseTracker : Tracker
    {
        public CourseStatus Status = CourseStatus.NotStarted;
    }

    public class SeatTimeTracker : Tracker
    {
        public Int64 SlideId = 0;
        public DateTime StartTime = DateTime.UtcNow;
        public DateTime EndTime = DateTime.UtcNow;
    }

    public class CourseAttempt : Tracker
    {
        public String AttemptID = String.Empty;
        public Int64 Score = 0;
        public Int64 SlideID = 0;
        public CourseStatus Status = CourseStatus.InProgress;
        public DateTime EndTime = DateTime.UtcNow;
    }

    public class AttemptAndScore
    {
        public String AttemptId = String.Empty;
        public Int64 Score = 0;
        public Int64 LastSlideID = 0;
    }

    internal enum TrackerCommand
    {
        Quiz,
        Status,
        Duration,
        SlideCount,
        Log,
        AddAttemptEntry,
        UpdateAttemptEntry,
        GetAttemptID
    }
    class TrackerService : Service<WebScriptRequest<Object>>
    {
        protected override Object Execute(WebScriptRequest<Object> request, String data)
        {
            TrackerCommand command = (TrackerCommand)Enum.Parse(typeof(TrackerCommand), request.Command, true);
            switch (command)
            {
                case TrackerCommand.Quiz:
                    return SetQuizInfo(_json.Deserialize<WebScriptRequest<QuizTracker>>(data).Data);
                case TrackerCommand.Status:
                    return SetCourseStatus(_json.Deserialize<WebScriptRequest<CourseTracker>>(data).Data);
                case TrackerCommand.Duration:
                    return AddSeatTime(_json.Deserialize<WebScriptRequest<SeatTimeTracker>>(data).Data, _ExecutingPage);
                case TrackerCommand.SlideCount:
                    return GetSlideCount(request.Data.ToString());
                case TrackerCommand.Log:
                    return Log(_json.Deserialize<WebScriptRequest<LogData>>(data).Data);
                case TrackerCommand.AddAttemptEntry:
                    return AddAttemptEntry(_json.Deserialize<WebScriptRequest<CourseAttempt>>(data).Data, _ExecutingPage);
                case TrackerCommand.GetAttemptID:
                    return GetAttemptID(_json.Deserialize<WebScriptRequest<CourseAttempt>>(data).Data);
                case TrackerCommand.UpdateAttemptEntry:
                    return UpdateAttemptEntry(_json.Deserialize<WebScriptRequest<CourseAttempt>>(data).Data, _ExecutingPage);
            }
            return null;
        }

        private object UpdateAttemptEntry(CourseAttempt attempt, System.Web.UI.Page page)
        {
            // Get User Agent
            String userAgent = FindMyDevice(page);

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@AttemptID", attempt.AttemptID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", attempt.UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", attempt.SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Timestamp", attempt.TimeStamp.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Score", attempt.Score, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", attempt.Status, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@EndTime", attempt.EndTime.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SlideID", attempt.SlideID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserAgent", userAgent, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.AddAttemptEntry, parameters.ToArray());
        }

        private object GetAttemptID(CourseAttempt attempt)
        {
            return DataAccessManager.DataTableToJSON(Course.GetAttemptID(Convert.ToInt64(attempt.UserID), Convert.ToInt64(attempt.SubCourseID), Convert.ToDateTime(attempt.TimeStamp)));
        }

        private object AddAttemptEntry(CourseAttempt attempt, System.Web.UI.Page page)
        {
            // Get User Agent
            String userAgent = FindMyDevice(page);

            Guid AttemptId = Guid.NewGuid();

            attempt.AttemptID = AttemptId.ToString();
            AttemptAndScore aaS = new AttemptAndScore();
            try
            {
                aaS.LastSlideID = (Int64)User.GetLastSlide(attempt.UserID, attempt.SubCourseID).Tables[0].Rows[0]["LastSlideID"];
            }
            catch (Exception e) {
                aaS.LastSlideID = 0;
                Medtrix.Trace.Logger.Log("Failed to get last id : " + e.StackTrace);
                Medtrix.Trace.Logger.Log("For user id : " + attempt.UserID + " and sub course id : " + attempt.SubCourseID);
            }

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@AttemptID", attempt.AttemptID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserID", attempt.UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", attempt.SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Timestamp", attempt.TimeStamp.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Score", 0, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@Status", CourseStatus.InProgress, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@EndTime", attempt.EndTime.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SlideID", aaS.LastSlideID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@UserAgent", userAgent, ParameterDirection.Input));
            dataManager.ExecuteNonQuery(StoreProcedure.AddAttemptEntry, parameters.ToArray());


            aaS.AttemptId = AttemptId.ToString();
            if(!Convert.IsDBNull(parameters[parameters.Count - 5].Value))
                aaS.Score = Convert.ToInt64(parameters[parameters.Count - 5].SqlValue.ToString());

            return aaS;
        }

        private object Log(LogData logData)
        {
            StatusOutput st = new StatusOutput();
            st.status = true;
            Medtrix.Trace.Logger.LogDetails(logData.log);

            return st;
        }

        private object GetSlideCount(string id)
        {
            return DataAccessManager.DataTableToJSON(Course.GetSlideCount(Convert.ToInt64(id)));
        }

        private object AddSeatTime(SeatTimeTracker seatTimeTracker, System.Web.UI.Page page)
        {
            // Get User Agent
            String userAgent = FindMyDevice(page);

            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", seatTimeTracker.UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Timestamp", seatTimeTracker.TimeStamp.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", seatTimeTracker.SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SlideId", seatTimeTracker.SlideId, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@StartTime", seatTimeTracker.StartTime.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@EndTime", seatTimeTracker.EndTime.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SeatTimeId", 0, ParameterDirection.InputOutput));
            parameters.Add(dataManager.CreateParam("@UserAgent", userAgent, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.AddSeatTime, parameters.ToArray());
        }

        private object SetCourseStatus(CourseTracker courseTracker)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", courseTracker.UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Timestamp", courseTracker.TimeStamp.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", courseTracker.SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Status", courseTracker.Status, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.SetCourseStatus, parameters.ToArray());
        }

        private object SetQuizInfo(QuizTracker quizTracker)
        {
            DataAccessManager dataManager = DataAccessManager.GetInstance();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(dataManager.CreateParam("@UserID", quizTracker.UserID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Timestamp", quizTracker.TimeStamp.ToUniversalTime(), ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@SubCourseID", quizTracker.SubCourseID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@QuizID", quizTracker.QuizID, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Percentage", quizTracker.Percentage, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@Rating", quizTracker.AnswerRating, ParameterDirection.Input));
            parameters.Add(dataManager.CreateParam("@CourseTotal", quizTracker.Total, ParameterDirection.Input));
            return dataManager.ExecuteNonQuery(StoreProcedure.AddQuizResult, parameters.ToArray());
        }

        private String FindMyDevice(System.Web.UI.Page page)
        {
            String deviceType = String.Empty;
            String userAgent = page.Request.UserAgent.ToString();

            // Internet Explorer
            Int32 msie = userAgent.IndexOf("MSIE");
            Int32 trident = userAgent.IndexOf("Trident/");

            // Google Chrome
            Int32 chrome = userAgent.IndexOf("Chrome/");

            // Mozilla Firefox
            Int32 firefox = userAgent.IndexOf("Firefox/");

            if (userAgent.ToLower().Contains("ipad")){
                deviceType = "iPad";
            }
            else if (msie > 0 || trident > 0){
                deviceType = "Internet Explorer";
            }
            else if (chrome > 0){
                deviceType = "Google Chrome";
            }
            else if (firefox > 0){
                deviceType = "Mozilla Firefox";
            }
            else{
                deviceType = "Web Browser";
            }
            return deviceType;
        }
    }
}
