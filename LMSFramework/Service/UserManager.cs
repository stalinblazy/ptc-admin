using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;

namespace LearningManagementSystem.UserManager
{
    public enum UserType
    {
        Invalid = 0,
        Admin = 1,
        SalesManager = 2,
        SalesRep = 3,
        User = 4,
        UserViewer = 6,
        SuperAdmin=5
    }

    public class UserSession
    {
        public UInt64 UserID = 0;
        public UserType UserType = UserType.Invalid;
        public static bool IsAuthorized(UserType defined, UserSession current)
        {
            if (defined == UserType.Invalid)
                return true;
            if (current == null)
                throw new UnauthorizedAccessException();
            if (defined == UserType.Admin || defined == UserType.UserViewer)
                return true;
            if (defined == current.UserType)
                return true;
            throw new UnauthorizedAccessException();
        }

        public static void IsAuthorizedsup(object defined, UserSession userSession)
        {
            throw new NotImplementedException();
        }

        public static bool IsAuthorizedsup(UserType defined, UserSession current)
        {
            if (defined == UserType.Invalid)
                return true;
            if (current == null)
                throw new UnauthorizedAccessException();
            if (defined == UserType.SuperAdmin || defined == UserType.UserViewer)
                return true;
            if (defined == current.UserType)
                return true;
            throw new UnauthorizedAccessException();
        }
    }


    public class UserManager
    {
        public static String GetDefaultPage(UserType type)
        {

            String page = "~/Default.aspx";
            switch (type)
            {
                case UserType.Admin:
                    page = "/Dashboard.aspx";
                    break;
                case UserType.UserViewer:
                    page = "/Dashboard.aspx";
                    break;
            }
            return page;
        }
        public static string GetSupDefaultPage(UserType type)
        {
            //String page = "http://localhost:49927/Default.aspx";
            String page = "~/Default.aspx";
            switch (type)
            {
                case UserType.SuperAdmin:
                    page = "SuperAdmin/Dashboard.aspx";
                    break;
                case UserType.UserViewer:
                    page = "SuperAdmin/Dashboard.aspx";
                    break;
            }
            return page;
        }

        public static Boolean ForgotPassword(String eMailID, String ActivationBaseURL)
        {
            DataTable dt = LearningManagementSystem.Components.User.ForgotPassword(eMailID);

            if (dt.Rows.Count > 0)
            {
                Int64 UserID = Convert.ToInt64(dt.Rows[0]["User_Id"].ToString());
                String key = dt.Rows[0]["Password"].ToString();
                String Name = dt.Rows[0]["First_Name"].ToString() + " " + dt.Rows[0]["Last_Name"].ToString();
                String Username = eMailID;
                if (UserID != 0)
                {
                    return SendPassword(eMailID, key, Name, Username);
                }
                else
                    return false;
            }
           
            return false;
        }

        private static Boolean SendPassword(String Mail, String Key, String Name, String UserName)
        {
            XmlDocument mailTemplate = new XmlDocument();
            mailTemplate.Load(System.Configuration.ConfigurationManager.AppSettings["ResetPwdTemplate"]);
            Replace(mailTemplate, "UserName", Name);
            Replace(mailTemplate, "passwd", Key);
            Replace(mailTemplate, "username", UserName);

            return Medtrix.EmailService.SendMail(Mail, System.Configuration.ConfigurationManager.AppSettings["ResetPwdSubject"], mailTemplate.OuterXml, null, String.Empty, "Info");
        }

        private static void Replace(XmlDocument doc, String id, String value)
        {
            XmlNodeList nl = doc.SelectNodes("//*[@id='" + id + "']");
            foreach (XmlNode node in nl)
            {
                node.InnerText = value;
            }
        }
    }
}
