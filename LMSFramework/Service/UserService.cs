﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using LearningManagementSystem.UserManager;
using Medtrix.WebServices;
using Medtrix.DataAccessControl;
using LearningManagementSystem.Components;
using request = LearningManagementSystem.Components.SupportRequest;
using user = LearningManagementSystem.Components.User;

namespace LearningManagementSystem.WebServices
{
    internal enum UserCommand
    {
        GetEmailID,
        AuthenticateUser,
        IsUsernameExist,
        GetStatesOfCountry,
        GetCityofState,
        GetCourseDetails,
        GetUpdatedContents,
        GetIssueList,
        SubmitTechSupport,
        ForgetPassword,
        Log,
        CheckMailAndCode,
        IsEmailExist,

        //World DB
        InsertCountry,
        InsertState,
        InsertCity
    }
    class UserService : Service<WebScriptRequest<Object>>
    {
        protected override Object Execute(WebScriptRequest<Object> request, String data)
        {
            UserCommand command = (UserCommand)Enum.Parse(typeof(UserCommand), request.Command, true);
            switch (command)
            {
                case UserCommand.GetEmailID:
                    return GetEmailID(request.Data.ToString());
                case UserCommand.AuthenticateUser:
                    return Authenticateuser(_json.Deserialize<WebScriptRequest<User>>(data).Data);
                case UserCommand.IsUsernameExist:
                    return IsUsernameExist(request.Data.ToString());
                case UserCommand.GetStatesOfCountry:
                    return GetStatesOfCountry(request.Data.ToString());
                case UserCommand.GetCityofState:
                    return GetCityOfState(request.Data.ToString());
                case UserCommand.GetCourseDetails:
                    return GetCourseDetails(_json.Deserialize<WebScriptRequest<User>>(data).Data);
             
                case UserCommand.ForgetPassword:
                    return ForgetPassword(_json.Deserialize<WebScriptRequest<EmailData>>(data).Data,_ExecutingPage);
                case UserCommand.Log:
                    return Log(_json.Deserialize<WebScriptRequest<LogData>>(data).Data);
                case UserCommand.CheckMailAndCode:
                    return CheckMailAndCode(_json.Deserialize<WebScriptRequest<MailAndCode>>(data).Data);
                case UserCommand.IsEmailExist:
                    return IsEmailExist(request.Data.ToString());
            }
            return null;
        }

        private object IsEmailExist(String Mail)
        {
            return User.IsMailExists(Mail);
        }

        private object CheckMailAndCode(MailAndCode code)
        {
            return DataAccessManager.DataTableToJSON(User.CheckMailAndCode(code.AccessCode, code.EmailId));
        }

        private object Log(LogData logdata)
        {
           StatusOutput st = new StatusOutput();
           st.status = true;
           Medtrix.Trace.Logger.LogDetails(logdata.log);
          return st;
        }

        private object ForgetPassword(EmailData emailData,System.Web.UI.Page page)
        {
            StatusOutput st = new StatusOutput();
            if (LearningManagementSystem.UserManager.UserManager.ForgotPassword(emailData.Email, page.Request.Url.AbsoluteUri.Substring(0, page.Request.Url.AbsoluteUri.LastIndexOf("/") + 1)))
                st.status = true;

            return st;
        }


        private object GetCityOfState(String stateId)
        {
            return DataAccessManager.DataTableToJSON(User.GetStatesCityList(Convert.ToInt64(stateId)));
        }

        private object GetStatesOfCountry(String countryId)
        {
            return DataAccessManager.DataTableToJSON(User.GetCountryStatesList(Convert.ToInt64(countryId)));
        }

       
       

        public static String GetTimeAgo(DateTime dt)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var ts = new TimeSpan(DateTime.Now.Ticks - dt.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 0)
            {
                return "not yet";
            }
            if (delta < 1 * MINUTE)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }
            if (delta < 2 * MINUTE)
            {
                return "a minute ago";
            }
            if (delta < 45 * MINUTE)
            {
                return ts.Minutes + " minutes ago";
            }
            if (delta < 90 * MINUTE)
            {
                return "an hour ago";
            }
            if (delta < 24 * HOUR)
            {
                return ts.Hours + " hours ago";
            }
            if (delta < 48 * HOUR)
            {
                return "yesterday";
            }
            if (delta < 30 * DAY)
            {
                return ts.Days + " days ago";
            }
            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }

       

        private object GetCourseDetails(User user)
        {
            List<CourseDetail> courseDetailList = new List<CourseDetail>();
            DataTable dt = LearningManagementSystem.Components.Course.GetAssignedCourse(user.UserID);

            foreach (DataRow dr in dt.Rows)
            {
                CourseDetail courseDetail = new CourseDetail();
                courseDetail.CourseID = Convert.ToUInt64(dr["Course_id"].ToString());
                courseDetail.CourseName = dr["Name"].ToString();
                courseDetail.Description = dr["Description"].ToString();
                courseDetail.Status = "Completed";
                
                List<SubCourseDetail> subCourseDetail = new List<SubCourseDetail>();
                DataTable SubCoursedt = LearningManagementSystem.Components.Course.GetUserSubCourse(Convert.ToUInt64(user.UserID), courseDetail.CourseID);
                foreach (DataRow Subdr in SubCoursedt.Rows)
                {
                    Medtrix.Trace.Logger.Log("Course ID: " + courseDetail.CourseID + "  User ID : " + user.UserID);
                    subCourseDetail.Add(new SubCourseDetail(Convert.ToUInt64(Subdr["Sub_Course_Id"].ToString()), Subdr["Module"].ToString(), Subdr["Category"].ToString(), Subdr["Status"].ToString(), Subdr["Percentage"].ToString(), "0", Subdr["Description"].ToString(), Subdr["Duration"].ToString()));
                }
                courseDetail.SubCourses = subCourseDetail;
                if (SubCoursedt.Rows.Count == 1)
                {
                    courseDetail.Status = courseDetail.SubCourses[0].Status;
                }
                else
                {
                    if (courseDetail.SubCourses[0].Status == "Completed" && courseDetail.SubCourses[1].Status == "Completed" && courseDetail.SubCourses[2].Status == "Completed")
                        courseDetail.Status = "Completed";
                    else if (courseDetail.SubCourses[0].Status == "Not Attempted" && courseDetail.SubCourses[1].Status == "Not Attempted" && courseDetail.SubCourses[2].Status == "Not Attempted")
                        courseDetail.Status = "Not Attempted";
                    else
                        courseDetail.Status = "In Progress";
                }
                courseDetailList.Add(courseDetail);
            }
            return courseDetailList;
        }

        private object IsUsernameExist(String username)
        {
            return User.IsUsernameExist(username);
        }

        private object Authenticateuser(User user)
        {
            DataSet status = LearningManagementSystem.Components.User.AuthenticateUser(user.Username, user.Password);
            DataTable dt = status.Tables[0];
            UserOutput us = new UserOutput();
            if (dt.Rows.Count > 0)
            {
                us.UserID = Convert.ToUInt64(dt.Rows[0]["User_id"].ToString());
                us.Email = dt.Rows[0]["Email_ID"].ToString();
                us.FirstName = dt.Rows[0]["First_Name"].ToString();
                us.LastName = dt.Rows[0]["Last_Name"].ToString();
            }
            return us;
        }

        private object GetEmailID(String accessToken)
        {
            return accessToken.ToLower().CompareTo("adcetris2014") == 0;//DataAccessManager.DataTableToJSON(User.GetEmailID(0,accessToken));
        }
    }
}
