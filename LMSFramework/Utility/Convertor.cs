﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;
using System.Data;

namespace LearningManagementSystem.Utility
{
    public class Convertor
    {
        public static MemoryStream DataTableToExcel(DataTable dt)
        {
            MemoryStream ms = new MemoryStream();
            using (ExcelPackage xlFile = new ExcelPackage())
            {
                ExcelWorksheet sheet = xlFile.Workbook.Worksheets.Add("Users");
                int row = 1, col = 1;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Cells[row, col++].Value = dc.ColumnName;
                }
                row++;
                foreach (DataRow dr in dt.Rows)
                {
                    col = 1;
                    foreach (object dc in dr.ItemArray)
                    {
                        if (dc.GetType() == typeof(DateTime))
                            sheet.Cells[row, col].Value = ((DateTime)dc).ToString("dd-MMM-yyyy HH:mm");
                        else
                            sheet.Cells[row, col].Value = dc;
                        col++;
                    }
                    row++;
                }
                xlFile.SaveAs(ms);
                ms.Position = 0;
            }
            return ms;
        }

        public static MemoryStream DataSetToExcel(DataSet ds)
        {
            MemoryStream ms = new MemoryStream();
            using (ExcelPackage xlFile = new ExcelPackage())
            {
                
                for (Int16 Index = 1; Index <= ds.Tables.Count; Index++)
                {
                    String sheetName = "Sheet " + Index.ToString();
                    ExcelWorksheet sheet = xlFile.Workbook.Worksheets.Add(sheetName);
                    DataTable dt = ds.Tables[Index - 1];
                    int row = 1, col = 1;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        sheet.Cells[row, col++].Value = dc.ColumnName;
                    }
                    row++;
                    foreach (DataRow dr in dt.Rows)
                    {
                        col = 1;
                        foreach (object dc in dr.ItemArray)
                        {
                            if (dc.GetType() == typeof(DateTime))
                                sheet.Cells[row, col].Value = ((DateTime)dc).ToString("dd-MMM-yyyy HH:mm");
                            else
                                sheet.Cells[row, col].Value = dc;
                            col++;
                        }
                        row++;
                    }
                }

                xlFile.SaveAs(ms);
                ms.Position = 0;
            }
            return ms;
        }
    }
}
