using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Net.Configuration;
using System.IO;

namespace Medtrix
{
    internal class EmailService
    {
        /// <summary>
        /// Send email to users specified
        /// </summary>
        /// <param name="from">From where to send email</param>
        /// <param name="recepients">Recepients email address ; seperated</param>
        /// <param name="subject">Subject of mail</param>
        /// <param name="body">Mail body</param>
        /// <returns>True is successfully sent, False otherwise</returns>
        internal static bool SendMail(String recepients,
                                    String subject, String body, Byte[] data , String FileName, String mailType)
        {
            bool status = false;
            try
            {
                //Don't proceed of details are not given
                if ( String.IsNullOrEmpty(recepients))
                    return status;

                NetworkCredential smtpCredentials = null;
                String userName = String.Empty;
                String paswword = String.Empty;

                if (mailType.Equals("Support"))
                {
                    userName = System.Configuration.ConfigurationManager.AppSettings["SmtpUserNameForSupport"];
                    paswword = System.Configuration.ConfigurationManager.AppSettings["SmtpPasswordForSupport"];
                }
                else if (mailType.Equals("Info"))
                {
                    userName = System.Configuration.ConfigurationManager.AppSettings["SmtpUserName"];
                    paswword = System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"];
                }
                String from = userName;
                if (!String.IsNullOrEmpty(userName) &&
                    !String.IsNullOrEmpty(paswword))
                {
                    smtpCredentials = new NetworkCredential(userName, paswword);
                }
                String hostAddress = System.Configuration.ConfigurationManager.AppSettings["SmtpServer"];
                int port = Convert.ToInt32( System.Configuration.ConfigurationManager.AppSettings["SmtpPort"]);
                if ((String.IsNullOrEmpty(hostAddress)) ||
                    (-1 == port))
                    return status;

                SmtpClient mailClient = new SmtpClient(hostAddress, port);
                //Check is network credentials are given
                if (null != smtpCredentials)
                    mailClient.Credentials = smtpCredentials;
                mailClient.EnableSsl = true;
                MailMessage msg = new MailMessage(from, recepients);
                msg.IsBodyHtml = true;
                msg.Body = body;
                msg.Subject = subject;
                if (data != null)
                {
                    MemoryStream ms = new MemoryStream(data);
                    msg.Attachments.Add(new Attachment(ms,FileName,"application/pdf"));
                }
                mailClient.Send(msg);
                status = true;
            }
            catch (Exception exp)
            {/*Need to throw our own exception here*/status = false;
            Medtrix.Trace.Logger.Log(exp.Message.ToString());
            }
            return status;
        }
    }
}
