USE [PTCV2]
GO
/****** Object:  Table [dbo].[Tbl_Assessment]    Script Date: 17-12-2020 19:46:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Assessment](
	[AssessmentID] [int] NULL,
	[QuestionId] [int] IDENTITY(1,1) NOT NULL,
	[Questions] [varchar](500) NULL,
	[TrackID] [int] NULL,
	[CirculumID] [nchar](10) NULL,
	[CreatedDate] [datetime] NULL,
	[NoOptions] [int] NULL,
	[CorrectAnswer] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Assessment_Options]    Script Date: 17-12-2020 19:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Assessment_Options](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NULL,
	[Options] [varchar](2000) NULL
) ON [PRIMARY]
GO
