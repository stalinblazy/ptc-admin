USE [PTCV2]
GO
/****** Object:  StoredProcedure [dbo].[SP_LMS_ADD_AssessmentQuestions]    Script Date: 17-12-2020 19:49:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_LMS_ADD_AssessmentQuestions] 
	@Assessment_Id INT,
	@Questions varchar(8000),
	@Answer varchar(8000),
	@NoOptions INT,
    @AdminId INT,
	@Question_Id	BIGINT OUTPUT
AS
BEGIN

	BEGIN
		INSERT INTO Tbl_Assessment(
						AssessmentId,
						Questions,
						CreatedDate,
						NoOptions,
						CorrectAnswer
						)
					VALUES (
						@Assessment_Id,		
	                    @Questions ,
	                    GETDATE(), 
	                    @NoOptions ,
                     	@Answer
					)
		SELECT @Question_Id = @@IDENTITY
	END
	
END